<!-- Get the master file with the Header and Footer -->
@extends('layouts.master4')

<!-- Main content of the page -->

@section('title', 'Recover Password')

@section('content')
<div class="container">
<div class="col-xs-12 col-md-8 col-md-offset-2">
    @if (count($errors) > 0)
    <div class="container">
            @foreach ($errors->all() as $error)
                <div class="alert alert-wami alert-dismissible default " role="alert">{{ $error }}</div>
            @endforeach
    </div>
@endif

  <div class="spacer-15"></div>
  <h1 class="text-center">Recover your password</h1>
  <div class="spacer-15"></div>
  @if ($data{'status'} == "false")
   {!! Form::open(['route' => ['reset.store']]) !!}
     <div style="display:none">
    {!! Form::text('id',$data{'id'}) !!}
     </div>
    <div class="form-group" id="fm-Password-1">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
      {!! Form::password('password',['class' => 'awesome form-control','placeholder' => 'New Password']) !!} 
      </div>
    </div>
    <div class="spacer-15"></div>

    <div class="form-group">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
      {!! Form::password('password_confirmation',['class' => 'awesome form-control','placeholder' => 'Confirm New Password']) !!}
      </div>
    </div>

    <div class="form-group">
    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
    {!! Form::button('Change Password',['type' => 'submit','class' => 'btn wami-btn padded pull-right']) !!}
    </div>
     </div>
    
     {!! Form::close() !!}

     @else
     <div class="container col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        @if($data{'type'} == "1")
                <div class="alert alert-wami alert-dismissible success text-center" role="alert">{!!$data{'message'}!!}</div>
        @elseif($data{'type'} == "2")
          <div class="alert alert-wami alert-dismissible default" role="alert">{!!$data{'message'}!!}</div>
        @endif
    </div>

     @endif

 </div>
 </div>


@endsection