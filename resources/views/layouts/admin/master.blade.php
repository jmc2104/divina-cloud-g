<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ URL::asset( 'bootstrap/css/bootstrap.min.css' ) }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ URL::asset('common/css/font-awesome.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ URL::asset( 'admin/css/AdminLTE.min.css' ) }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ URL::asset( 'admin/css/skins/skin-red-light.css' ) }}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{ URL::asset( 'plugins/datepicker/datepicker3.css' ) }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ URL::asset( 'plugins/daterangepicker/daterangepicker.css' ) }}">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ URL::asset( 'plugins/datatables/dataTables.bootstrap.css' ) }}">
  <!-- Custom style -->
  <link rel="stylesheet" href="{{ URL::asset( 'admin/css/custom.css' ) }}">
</head>

<body class="hold-transition skin-red-light fixed sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
    
    @include('layouts.admin.header')
    
    @include('layouts.admin.sidebar')
    
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      
      @yield('content')
      
    </div><!-- /.content-wrapper -->
    
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.a
      </div>
      <strong>Copyright &copy; 2017 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved.
    </footer>
    
  </div>
  <!-- ./wrapper -->
  <!-- jQuery 2.2.3 -->
  <script src="{{ URL::asset( 'plugins/jQuery/jquery-2.2.3.min.js' ) }}"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="{{ URL::asset( 'plugins/jQueryUI/jquery-ui.min.js' ) }}"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script> $.widget.bridge('uibutton', $.ui.button); </script>
  <!-- Bootstrap 3.3.6 -->
  <script src="{{ URL::asset( 'bootstrap/js/bootstrap.min.js' ) }}"></script>
  <!-- SlimScroll -->
  <script src="{{ URL::asset( 'plugins/slimScroll/jquery.slimscroll.min.js' ) }}"></script>
  <!-- Daterangepicker -->
  <script src="{{ URL::asset( 'plugins/daterangepicker/moment.min.js' ) }} "></script>
  <script src="{{ URL::asset( 'plugins/daterangepicker/daterangepicker.js' ) }}"></script>
  <!-- Datepicker -->
  <script src="{{ URL::asset( 'plugins/datepicker/bootstrap-datepicker.js' ) }}"></script>
  <!-- DataTables -->
  <script src="{{ URL::asset( 'plugins/datatables/jquery.dataTables.min.js' ) }}"></script>
  <script src="{{ URL::asset( 'plugins/datatables/dataTables.bootstrap.min.js' ) }}"></script>
  
  @yield('scripts')
  
  <!-- AdminLTE App -->
  <script src="{{ URL::asset( 'admin/js/app.js' ) }}"></script>
</body>
</html>
