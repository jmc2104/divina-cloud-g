<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
          </button>
        </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li><a href="users"><i class="fa fa-home"></i> <span>Home</span></a></li>
      <li class="header">User Management</li>
      <li id="nav_users"><a href="/a/users"><i class="fa fa-user"></i> <span>Users</span></a></li>
      <li id="nav_patients"><a href="/a/patients"><i class="fa fa-female text-pink"></i> <span>Patients</span></a></li>
      <li id="nav_doctors"><a href="/a/doctors"><i class="fa fa-stethoscope"></i> <span>Doctors</span></a></li>
      
      <li class="header">Implant Registrations</li>
      <li id="nav_surgeries"><a href="/a/surgeries"><i class="fa fa-heartbeat text-red"></i> <span>Surgeries</span></a></li>
      <li id="nav_registrations"><a href="/a/registrations"><i class="fa fa-pencil-square-o text-green"></i> <span>Registrations</span></a></li>
      <li id="nav_implants"><a href="/a/implants"><i class="fa fa-search text-muted"></i> <span>Implants</span></a></li>
      
      <li class="header">Extended Warranties</li>
      <li id="nav_warranties"><a href="/a/warranties"><i class="fa fa-file-text-o text-yellow"></i> <span>Warranties</span></a></li>
      <li id="nav_orders"><a href="/a/orders"><i class="fa fa-dollar text-green"></i> <span>Orders</span></a></li>
      <li id="nav_postpayments"><a href="/a/postpayments"><i class="fa fa-credit-card text-info"></i> <span>Payments</span></a></li>
      
      
      <li class="header">Monthly Reports</li>
      <li><a href="/a/reports"><i class="fa fa-bar-chart text-red"></i> <span>Reports</span></a></li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
