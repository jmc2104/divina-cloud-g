<html lang="en">
<head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <meta name="csrf-token" content="{{ csrf_token() }}">
 <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
 <title>Motivaimagine Web App - @yield('title')</title>

  <link rel="stylesheet" href = "{{ URL::asset('styles/bootstrap.css') }}" >

  <link rel="stylesheet" href = "{{ URL::asset('styles/custom.css') }}" >

 <link rel="stylesheet" href = "{{ URL::asset('styles/selectize.css') }}" >


<link rel="stylesheet" href = "{{ URL::asset('styles/fonts/fonts.css') }}" >
 
 <!-- Fonts -->
 <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
 <link rel="shortcut icon" href="{{ URL::asset('images/favicon.png') }}" type="image/x-icon" />
 <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
 <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
 <!--[if lt IE 9]>
 <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
 <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
 <![endif]-->
</head>
<body>


 <div class="container-fluid home">
  <div class="row background login">
    <div class="top-wrapper">
    <a href="https://register.motivaimagine.com/" class="pull-left logo-wrapper"><img class="img-responsive" src={{asset('images/logo-motivaimagine-white-small.png')}}></a>
    </div>
  </div>
  <div class="main-wrapper">
  @yield('content')
  </div>
  <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="row">
          <div class="footer-left col-xs-11 col-sm-6 no-pading">
            <ul class="nav navbar-nav navbar-left">
              <li><a target="_blank" href="http://establishmentlabs.com/">© Copyright 2017 | Establishment Labs | All rights reserved.</a></li>
            </ul><!-- /.nav -->
          </div><!-- /.footer-lef -->
          <div class="footer-right col-xs-12 col-sm-5">
            <ul class="nav navbar-nav navbar-right">
              <li><a target="_blank" href="https://motiva.health/terms-and-conditions/">Terms and Conditions</a></li>
              <li><a target="_blank" href="https://motiva.health/terms-of-service/">Terms of Service</a></li>
              <li><a target="_blank" href="https://motiva.health/privacy-policy/">Privacy Policy</a></li>
            </ul><!-- /.nav -->
          </div><!-- /.footer-right -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </nav>
</div>

 <!-- Scripts -->
 <footer class="row">
  <script src ="{{ URL::asset('scripts/main.js')}}" ></script>
  <script src ="{{ URL::asset('scripts/jquery-3.3.1.min.js')}}" ></script>
  <script src ="{{ URL::asset('scripts/bootstrap.min.js')}}" ></script>
  <script type="text/javascript">
          var getUrlParameter = function getUrlParameter(sParam) {
          var sPageURL = decodeURIComponent(window.location.search.substring(1)),
          sURLVariables = sPageURL.split('&'),sParameterName,i;
          for (i = 0; i < sURLVariables.length; i++) 
          {
            sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] === sParam) 
              {
                return sParameterName[1] === undefined ? true : sParameterName[1];
              }
            }
          }; 
          if (getUrlParameter('response_code')) $('#ResponseWarrantyModal').modal('show'); 

            function goBack() {
              window.history.back();
              window.history.back();
            }
          </script>

    </footer>
 

</body>
</html>