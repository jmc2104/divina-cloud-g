<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Motivaimagine Web App</title>
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
  <!-- Styles -->
  <link rel="stylesheet" href="{{ URL::asset('styles/bootstrap.min.css') }}" />
  <link rel="stylesheet" href="{{ URL::asset('styles/bootstrap-social.css') }}" />
  <link rel="stylesheet" href="{{ URL::asset('styles/font-awesome.css') }}" />
  <link rel="stylesheet" href="{{ URL::asset('styles/selectize.bootstrap3.css') }}" />
  <link rel="stylesheet" href="{{ URL::asset('styles/awesome-bootstrap-checkbox.css') }}" />
  <link rel="stylesheet" href="{{ URL::asset('styles/fancybox.css') }}" />
  <link rel="stylesheet" href="{{ URL::asset('styles/custom.css') }}" />
  <link rel="stylesheet" href="{{ URL::asset('fonts/custom/fonts.css') }}" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Page-Specific Styles -->
  @yield('styles')
</head>

@yield('content')

<footer>
  <!-- Base -->
  <script src="{{ URL::asset('scripts/jquery-3.1.0.min.js') }}"></script>
  <script src="{{ URL::asset('scripts/bootstrap.min.js') }}"></script>
  <script src="{{ URL::asset('scripts/selectize.js') }}"></script>
  <!-- Custom -->
  <script src="{{ URL::asset('scripts/custom.js') }}"></script>
  <!-- Auth -->
  <script src="{{ URL::asset('scripts/custom.js') }}"></script>
  <script src="{{ URL::asset('scripts/google-auth.js') }}"></script>
  <script src="{{ URL::asset('scripts/facebook-auth.js') }}"></script>
  <!-- Page-Specific Scripts -->
  @yield('scripts')
</footer>
</html>