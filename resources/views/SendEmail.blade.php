
<!-- Get the master file with the Header and Footer -->
@extends('layouts.master4')

<!-- Main content of the page -->
@section('content')
<div class="container">
  
 <div class="row">
 <div class="col-md-10 col-md-offset-1">
 <div class="panel-custom panel-primary">
@if (count($errors) > 0)
    <div class="container">
            @foreach ($errors->all() as $error)
                <div class="alert alert-warning" role="alert">{{ $error }}</div>
            @endforeach
    </div>
@endif
 <div class="panel-heading"></div>
 
 
 <div class="panel-body">
<div class = 'form-group  col-md-5 col-md-offset-3'>
   {!! Form::open(['route' => ['send.store']]) !!}
    <div class="form-group" >
      <label>To:</label>
      {!! Form::text('email',null,['placeholder' => 'example@gmail.com','class' => 'form-control']); !!}
    </div>
    <div class="form-group" >
      <label>Cc:</label>
      {!! Form::text('cc',null,['placeholder' => 'example@gmail.com','class' => 'form-control']); !!}
    </div>
   <div class="form-group">
    <label>Type Letter:</label>
     {!! Form::select('type', ['template-implant-register' => 'Implant Register', 'template_warranty_proof' => 'Warranty Proof'], 'E', ['class' => 'selectize-control form-control single']); !!}
  </div>
    <div class="form-group">
      <label>Language:</label>
     {!! Form::select('languages', ['Ar_SA' => 'Arabic', 'zh_CN' => 'Chinese', 'de_DE' => 'Derman ', 'en_US' => 'English', 'fr_FR' => 'French', 'it_IT' => 'Italian', 'ja_JP' => 'Japanese', 'ko_KR' => 'Korean', 'nn_NO' => 'Norwegian', 'pl_PL' => 'Polish', 'pt_BR' => 'Português ', 'es_ES' => 'Spanish', 'sv_SA' => 'Svenska ', 'vi_VN' => 'Vietnamese', 'tr_TR' => 'Turkish', 'ru_RU' => 'Russian'], 'en_US', ['class' => 'form-control']); !!}
  </div>


    <div class="form-group col-md-offset-3">
    {!! Form::button('Send Email',['type' => 'submit','class' => 'btn wami-btn padded']); !!}
    </div>
     {!! Form::close() !!}
  
 </div>
 </div>
 </div>
 </div>
 </div>
</div>


@endsection