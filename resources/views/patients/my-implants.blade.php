<!-- Get the master file with the Header and Footer -->
@extends('layouts.master')

<!-- Main content of the page -->
@section('content')

<div style="color:black">
	@forelse ($implants as $index => $item)
	<p>{{ $item->ID_SerialNumber }}</p>
	@empty
	{{ 'No Implants to Show' }}
	@endforelse
	
	@stop
</div>



<!-- Extra Scripts that need to be loaded only on this page -->
@section('scripts')
@stop