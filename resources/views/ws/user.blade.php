<!-- Get the master file with the Header and Footer -->
@extends('layouts.master')
<!-- Main content of the page -->
@section('content')
  
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Project name</a>
      </div>
      <div id="navbar" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
          <li class="active"><a href="#">Home</a></li>
          <li><a href="#about">About</a></li>
          <li><a href="#contact">Contact</a></li>
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </nav>
  
  
  <div class="container-fluid">
    
    
    <div class="row profile">
      <div class="col-md-3 text-center photo">
        <img class="img-circle" src="{{ $profile->photo }}" alt="">
        <p class="text-center">{{ $profile->country->name }}</p>
      </div>
      <div class="col-md-9 nameplate">
        <h2 class="page-header">{{ $profile->get_full_name() }} | <small>{{ $user->email }}</small></h2>
        <h5 class="text-left">
          Member since: {{ App\Helpers::date( $user->created_at, true ) }}
          |
          Last login: {{ App\Helpers::date( $user->last_login, true ) }}
          |
          <a href="#">Reset Password</a>
        </h5>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-12">
        
        @if ( $surgeries )
          @foreach ($surgeries as $surgery)
            
            
            <div class="col-md-4">
              <div class="panel panel-info">
                <!-- Default panel contents -->
                <div class="panel-heading">Imlant Registration</div>
                
                <table class="table">
                  <tr>
                    <th>Date of Surgery:</th>
                    <td>{{ App\Helpers::date( $surgery->date ) }}</td>
                  </tr>
                  <tr>
                    <th>Date of Registration:</th>
                    <td>{{ App\Helpers::date( $surgery->created_at, true ) }}</td>
                  </tr>
                  <tr>
                    <th>Doctor Name:</th>
                    <td>{{ $surgery->doctor->profile->get_full_name() }}</td>
                  </tr>
                </table>
                <hr>
                <!-- Table -->
                <table class="table">
                  <tr>
                    <th class="text-center">Serial N°</th>
                    <th class="text-center">Position</th>
                    <th class="text-center">Ext Warranty</th>
                  </tr>
                  @foreach ($surgery->registrations as $registration)
                    <tr>
                      <td class="text-center">{{ $registration->implant->serial }}</td>
                      <td class="text-center">{{ $registration->position }}</td>
                      <td class="text-center">{{ ( $registration->warranty->is_extended ) ? 'Yes' : 'No' }}</td>
                      
                    </tr>
                  @endforeach
                </table>
              </div>
            </div>
            
            
            
            
          @endforeach
        @endif
        
        
        
        
        
      </div>
    </div>
    
    
  </div>
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
@endsection
<!-- Extra Scripts that need to be loaded only on this page -->
@section('scripts')
@endsection
