<!-- Get the master file with the Header and Footer -->
@extends('layouts.master4')


@section('title', 'Warranty Purchase')
<!-- Main content of the page -->
    @section('content')
<div class="modal fade" id="ResponseWarrantyModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="gridSystemModalLabel">Payment Confirmation</h4>
          </div>
          <div class="modal-body  text-center">
            <br><br>
            <div class="program-logo-wrapper">
              @if($data{'warranty_type'} == 2)
                <img src={{asset('images/warranty/2Y-Program.png')}}>
              @else
                <img src={{asset('images/warranty/5Y-Program.png')}}>
              @endif
            </div>
            <br>
            @if ( $data{'status'} === '100' )
              <h3><b>Payment Completed</b></h3>
              <p>We have sent you an email with all the details of your order.</p>
              <h1>Thank you</h1>
              <a href="javascript:goBack();" class="button button-success">close</a>
            @else
              <h3><b>'Payment Failed'</b></h3>
              <p>There's an error with the payment"</p>
              <h3>Please Try Again</h3>
            @endif
            <p>Response Credomatic:</p><?php ECHO $data{'responsetext'}?>
            <br><br>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal --> 
@endsection
 