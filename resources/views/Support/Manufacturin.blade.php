<!-- Get the master file with the Header and Footer -->
@extends('Support.MasterPage')

<!-- Main content of the page -->

@section('title', 'Create Divina Device')

@section('content')
<body>
  <div class="container">
     
          {!! Form::open(['route' => ['support-main'],'name' => 'prueba']) !!}
     <div>
             <button class="rounded-circle btn floatingRigth" type='summit'}}"><img  width="35" height="35" src="{{ URL::asset('images/back3.png') }}"></button>
      </div>
 </form>
    <div class="row justify-content-center pt-3">
      <div class="col-12 text-center">
      </div><!-- /.col-12 -->
     
      {!! Form::open(['action' => 'SupportController@index']) !!}
          <button class="rounded-circle btn floating" ><img  width="35" height="35" src="{{ URL::asset('images/Logout2.png') }}"></button> 
        </form>
  </div><!-- /.container -->
      <hr>
      {!! Form::open(['route' => ['create-divina']]) !!}
      <div class="container">
            @foreach ($errors->all() as $error)
                <div class="alert alert-warning alert-dismissible default" role="alert">{{ $error }}</div>
            @endforeach
    </div>
    <div class="row justify-content-center pt-3">
      <div class="row justify-content-center py-3">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header text-center">
              <strong>Create Divina Device</strong>
            </div>
            <div class="card-body pb-0">
             <table class="table">
                  <tbody>
                    <tr>
                      <th>PO Number:</th><td>  {!! Form::text('PO',null,['class' => 'form-control','placeholder' => 'PO Number']) !!} </td>
                    </tr>
                    <tr>
                      <th>Serial Number:</th>
                      <td>
                        {!! Form::text('SN',null,['class' => 'form-control','placeholder' => 'Divina Serial Number']) !!}
                      </td>
                    </tr>
                    <tr>
                    <th>Hardware Version:</th>
                      <td>
                        {!! Form::text('H_Version',null,['class' => 'form-control','placeholder' => 'Hardware Version']) !!}
                      </td>
                  </tr>
                  <tr>
                    <th>Computer Serial Number:</th>
                      <td>
                        {!! Form::text('NUC_SN',null,['class' => 'form-control','placeholder' => 'Computer Serial Number']) !!}
                      </td>
                  </tr>
                  <tr>
                    <th>Linak Serial Number:</th>
                      <td>
                        {!! Form::text('Linak_SN',null,['class' => 'form-control','placeholder' => 'Linak Serial Number']) !!}
                      </td>
                  </tr>
                  <tr>
                    <th>Rail Serial Number:</th>
                      <td>
                        {!! Form::text('Rail_SN',null,['class' => 'form-control','placeholder' => 'Rail Serial Number']) !!}
                      </td>
                  </tr>
                   <tr>
                    <th>Rail Model:</th>
                      <td>
                        {!! Form::text('Rail_Model',null,['class' => 'form-control','placeholder' => 'Rail_Model']) !!}
                      </td>
                  </tr>
                  <tr>
                    <th>Windows Lincense:</th>
                      <td>
                        {!! Form::text('W_Lincese',null,['class' => 'form-control','placeholder' => 'Windows Lincense:']) !!}
                      </td>
                  </tr>
                  <tr>
                  <th>Shipping Destination:</th>
                      <td>
                        {!! Form::textarea('S_Destination',null,['class' => 'form-control','placeholder' => 'Shipping Destination']) !!}
                      </td>
                  </tr>
                  <tr>
                  <th>Shipping Date:</th>
                      <td>
                          {!! Form::date('Shipping_date', \Carbon\Carbon::now(),['class' => 'form-control','max' => Carbon\Carbon::now()->format('Y-m-d')])!!}
                      </td>
                  </tr>
                  <tr>
                  <th>Packing Date:</th>
                      <td>
                          {!! Form::date('Packing_date', \Carbon\Carbon::now(),['class' => 'form-control','max' => Carbon\Carbon::now()->format('Y-m-d')])!!}
                      </td>
                  </tr>
                  <tr>
                  <th>Manufacturing Date:</th>
                      <td>
                          {!! Form::date('manufacturing_date', \Carbon\Carbon::now(),['class' => 'form-control','max' => Carbon\Carbon::now()->format('Y-m-d')])!!}
                      </td>
                  </tr>
                </tbody>
            </table>
          </div>
          </div>
        </div>
      </div>
    </div><!-- /.row -->
    <div class="row justify-content-center pt-3">
    <button type="summit" class="btn btn-outline-success b-radius">Save</button>
    </div>
  </div><!-- /.container -->
   </form>

@endsection