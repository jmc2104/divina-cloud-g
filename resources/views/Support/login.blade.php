<!-- Get the master file with the Header and Footer -->
@extends('Support.MasterPage')

<!-- Main content of the page -->

@section('title', 'Login')

@section('content')
 @if (count($errors) > 0)
    <div class="container">
            @foreach ($errors->all() as $error)
                <div class="alert alert-warning alert-dismissible default " role="alert">{{ $error }}</div>
            @endforeach
    </div>
@endif
{!! Form::open(['route' => ['support.admin.store']]) !!}
 <div class="container">
    <div class="row justify-content-center align-items-center h-100">
  
      <div class="col-12 text-center">
        <form method="POST" action="/login">
          <div class="form-row justify-content-center mb-2">
            <div class="col-sm-8 col-md-6 col-lg-4">
              {!! Form::text('email',null,['class' => 'form-control big-input','placeholder' => 'Email']) !!}
              {!! Form::password('password',['class' => 'form-control big-input','placeholder' => 'Password']) !!}
            </div>
              
            </div>
            <div class="form-group">
              <div class=" justify-content-center mb-2">
                {!! Form::button('Login',['type' => 'submit','class' => 'btn wami-btn padded pull-right']) !!}
              </div>
            </div>
          </div>
        </form>
      </div><!-- /.col-12 -->
    </div><!-- /.row -->
  </div><!-- /.container -->

  </div>

@endsection