<!-- Get the master file with the Header and Footer -->
@extends('Support.MasterPage')

<!-- Main content of the page -->
@section('content')
<div class="container">
  {!! Form::open(['route' => ['support-main'],'name' => 'prueba']) !!}
     <div>
             <button class="rounded-circle btn floatingRigth" type='summit'}}"><img  width="35" height="35" src="{{ URL::asset('images/back3.png') }}"></button>
      </div>
 </form>
 <div class="col-12 text-center">
      </div><!-- /.col-12 -->
     
      {!! Form::open(['action' => 'SupportController@index']) !!}
          <button class="rounded-circle btn floating" ><img  width="35" height="35" src="{{ URL::asset('images/Logout2.png') }}"></button> 
        </form>
  </div><!-- /.container -->
  <hr>
  @if (!is_null($data['list']))
 
<div class="container">
<div class="col-xs-12 col-md-12 col-md-offset-1">
    @if (count($errors) > 0)
    <div class="container">
            @foreach ($errors->all() as $error)
                <div class="alert alert-warning" role="alert">{{ $error }}</div>
            @endforeach
    </div>
@endif
<div class="row justify-content-center pt-3">
   <div class="main-wrapper patients">
      <div class="spacer-15">&nbsp;</div>
      <h1 class="text-center">Divina Manufaturing List</h1>
      <div class="spacer-10">&nbsp;</div>
      <div  class="form-group row">
        <div class="col-sm-5  offset-1">
          <input type="text" id="search" class="form-control big-input" placeholder="Search Divina Serial Number" onchange="find_device()">
        </div>
        <div  class="col-form-label offset-4">
        <label class="text" >Total Records: {{$data['list']->count()}}</label>
        </div>
      </div>
   <div id="patients-table" class="col-md-12 col-md-offset-3">
     <div class="table-wrapper ">
          <div class="table-responsive table-scroll">
            <table class="table table-hover">
              <thead class="card-header">
                <tr>
                  <th style="cursor: pointer" onclick="sort('PO_Nbr')"> <span>PO</span> </th>
                   <th> <span>Divina SN</span> </th>
                   <th> <span>HW Version</span> </th>
                   <th> <span>Computer SN</span> </th>
                   <th> <span>Linak SN</span> </th>
                   <th> <span>Rail SN</span> </th>
                   <th> <span>Rail Model</span> </th>
                   <th> <span>Windows License</span> </th>
                   <th> <span>Shipping Destination</span> </th>
                   <th> <span>Packing Date</span> </th>
                   <th> <span>Shipping Date</span> </th>
                   <th> <span>Manufacturing Date</span> </th>
                   <th> <span>Created at</span> </th>
                </tr>
              </thead>
              <tbody class="list">
                    @foreach ($data['list'] as $device)
                    <tr style="background-color:white">
                      <td class='PO_Nbr'>{{$device->PO_Nbr}}</td>
                      <td class='Divina_SN'>{{$device->Divina_SN}} </td>
                      <td class='Divina_HW_Version'>{{$device->Divina_HW_Version}} </td>
                      <td class='Computer_SN'>{{$device->Computer_SN}} </td>
                      <td class='Linak_SN'>{{$device->Linak_SN}} </td>
                      <td class='Rail_SN'>{{$device->Rail_SN}} </td>
                      <td class='Rail_Model'>{{$device->Rail_Model}} </td>
                      <td class='Windows_License '>{{$device->Windows_License}}  </td>
                      <td class='Shipping_Destination'>{{$device->Shipping_Destination}} </td>
                      <td class='Packing_Date'>{{date('d-M-Y', strtotime($device->Packing_Date))}}</td>
                      <td class='Shipping_Date'>{{date('d-M-Y', strtotime($device->Shipping_Date))}}</td>
                      <td class='manufacturing_date'>{{date('d-M-Y', strtotime($device->manufacturing_date))}}</td>
                      <td class='created_at'>{{date('d-M-Y', strtotime($device->created_at))}} </td>
                      </tr>
                    @endforeach
                  
              </tbody>
            </table>
            
          </div><!-- /.table-responsive table-scroll -->
        </div><!-- /.table-wrapper  -->
        <nav aria-label="Page navigation example">
          <ul class="pagination pagination-sm">
          </ul>
        </div>
        </nav>
      </div><!-- /.row  -->        
      </div><!-- /.patients-table -->
 </div>
@ELSE

@endif

<script src="{{ URL::asset('scripts/list.min.js') }}"></script>

<script type="text/javascript">
  var options =
  {
    valueNames: ['PO_Nbr','Divina_SN','Divina_HW_Version','Computer_SN',
    'Linak_SN','Rail_SN','Rail_Model','Windows_License','Shipping_Destination',
    'Packing_Date','Shipping_Date','manufacturing_date','created_at'],
    pagination: { outerWindow : 3 },
    page: 5,
    totalpages: 15
  };

  var patient_list = new List('patients-table', options);
  var order = ['asc', 'desc'];

  function clear_filters()
  {
    /* Set default values */
    patient_list.search();
    patient_list.filter();
  }
  function filter2(){
     var filter = document.getElementById('inputfilter2').value;
      patient_list.filter(function(item)
      {
        if (item.values().result.trim() == filter)
          return item.values().result;
      });
    }

  function sort( column )
  {
    switch( column )
    {
      case 'PO_Nbr':
      order[0] = ( order[0] === 'desc' ) ? 'asc' : 'desc';
      patient_list.sort('PO_Nbr', { order: order[0] });
      break;
    }
  }
   function find_device()
  {
     var Find = document.getElementById("search").value;
     patient_list.search(Find,['Divina_SN']);
  }  
</script>

@endsection