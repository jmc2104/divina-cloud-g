<!-- Get the master file with the Header and Footer -->
@extends('Support.MasterPage')

<!-- Main content of the page -->
@section('content')
<div class="container">
  {!! Form::open(['route' => ['support-main'],'name' => 'prueba']) !!}
     <div>
             <button class="rounded-circle btn floatingRigth" type='summit'}}"><img  width="35" height="35" src="{{ URL::asset('images/back3.png') }}"></button>
      </div>
 </form>
    <div class="row justify-content-center  pt-3">

   

       {!! Form::open(['route' => ['result-warranty-report'],'name' => 'prueba']) !!}
      <div class="col-13 text-center">
          <div class="form-row justify-content-center mb-3">
            <div class="col-sm-8 col-md-6 col-lg-6">
              <h3>Start Date</h3>
               {!! Form::date('start_date',Carbon\Carbon::parse($data['start_date']),['class' => 'form-control big-input' ,'required']) !!}
            </div>
            <div class="col-sm-8 col-md-6 col-lg-6">
                <h3>End Date</h3>
               {!! Form::date('end_date',Carbon\Carbon::parse($data['end_date']),['class' => 'form-control big-input','required']) !!}
            </div>
            <br>
            <div class="col-sm-8 col-md-6 col-lg-3 " style="margin: 10px">
                <button type="summit" class="btn btn-outline-primary b-radius">Generate</button>
            </div>
          </div>
        </form>
       
      </div><!-- /.col-12 -->
       <form method="POST" action="https://portal-dot-motivaimagine-web.appspot.com/logout" onsubmit="toggle('loader', true)">
          <button class="rounded-circle btn floating" ><img  width="35" height="35" src="{{ URL::asset('images/Logout2.png') }}"></button>
           <div>
            <!-- <a class="rounded-circle btn floating  " href="{{Route('implant-search',array('data' => 1))}}"><img  width="35" height="35" src="{{ URL::asset('images/Logout2.png') }}"></a> -->
      </div>
        </form>
    </div><!-- /.row -->
  </div><!-- /.container -->
  <hr>
  @if (!is_null($data['list']))
 
<div class="container">
<div class="col-xs-12 col-md-12 col-md-offset-1">
    @if (count($errors) > 0)
    <div class="container">
            @foreach ($errors->all() as $error)
                <div class="alert alert-warning" role="alert">{{ $error }}</div>
            @endforeach
    </div>
@endif
<div class="row justify-content-center pt-3">
   <div class="main-wrapper patients">
      <div class="spacer-15">&nbsp;</div>
      <h1 class="text-center">Warranty Reports</h1>
      <div class="spacer-10">&nbsp;</div>
      
   <div id="patients-table" class="col-md-12 col-md-offset-2">
     <div class="table-wrapper ">
          <div class="table-responsive table-scroll">
            <table class="table table-hover">
              <thead class="card-header">
                <tr>
                  <th style="cursor: pointer" onclick="sort('transaction')"> <span>Transaction</span> </th>
                  <th> <span>Serial</span> </th>
                  <th> <span>Amount USD</span> </th>
                  <th style="cursor: pointer" onclick="sort('display_date')"> <span>Date of Purchase</span> </th>
                  <th> <span>Warranty Program</span> </th>
                  <th> <span>Month</span> </th>
                  <th> <span>Year</span> </th>
                </tr>
              </thead>
              <tbody class="list">
                    @foreach ($data['list'] as $warranty)
                      <tr style="background-color:white">
                        <td class="transaction">{{$warranty->Transaction}} </td>
                        <td class="serial">{{$warranty->Serial}} </td>
                        <td class="amount_usd">{{$warranty->Amount_USD}}</td>
                        <td class="display_date">{{date('d-M-Y', strtotime($warranty->Date_Purchase))}} </td>
                        <td class="warranty_program">{{$warranty->Warranty_Program}}Y</td>
                        <td class="month">{{$warranty->Month}}</td>
                        <td class="year">{{$warranty->Year}}</td>
                      </tr>
                    @endforeach
                  
              </tbody>
            </table>
            
          </div><!-- /.table-responsive table-scroll -->
        </div><!-- /.table-wrapper  -->
        
        <nav aria-label="Page navigation example">
        <div class="pagination pagination-sm"></div>
        </nav>
      </div><!-- /.row  -->        
      </div><!-- /.patients-table -->
 </div>
  {!! Form::open(['route' => ['export-warranty-report','start'=>$data['start_date'],'end'=>$data['end_date']]]) !!}
  <div class="text-center">
         <button type="summit" class="btn btn-outline-success b-radius">Export</button>
            </div>
  </div><!-- /.main-wrapper -->
</form>
@ELSE

@endif

<script src="{{ URL::asset('scripts/list.min.js') }}"></script>

<script type="text/javascript">
  var options =
  {
    valueNames: ['transaction','serial','amount_usd','display_date','warranty_program','month','year'],
    pagination: { outerWindow : 3 },
    page: 10,
    totalpages: 15
  };

  var patient_list = new List('patients-table', options);
  var order = ['asc', 'desc'];

  function clear_filters()
  {
    /* Set default values */
    patient_list.search();
    patient_list.filter();
  }
  function filter2(){
     var filter = document.getElementById('inputfilter2').value;
      patient_list.filter(function(item)
      {
        if (item.values().result.trim() == filter)
          return item.values().result;
      });
    }

  function sort( column )
  {
    switch( column )
    {
      case 'transaction':
      order[0] = ( order[0] === 'desc' ) ? 'asc' : 'desc';
      patient_list.sort('transaction', { order: order[0] });
      break;

      case 'display_date':
      order[4] = ( order[4] === 'desc' ) ? 'asc' : 'desc';
      patient_list.sort('display_date', { order: order[4] });
      break;
    }
  }


  
</script>

@endsection