<!DOCTYPE html>
<html>
     <head>
  <title>Support Portal</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/yeti/bootstrap.min.css">
  <link rel="stylesheet" href = "{{ URL::asset('styles/Support/portal.css') }}" >
   <link rel="stylesheet" href = "{{ URL::asset('styles/selectize.css') }}" >


  <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
  <div class="container">
   @yield('content')
  </div><!-- /.container -->
  <footer> 
    <!-- Extra Scripts that need to be loaded only on this page -->
    <script src ="{{ URL::asset('scripts/jquery-3.3.1.min.js')}}" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src ="{{ URL::asset('scripts/bootstrap.min.js')}}" ></script>
    <script src="https://motivaimagine.com/inquiries/public/js/app.js"></script>
    <script type="text/javascript">
          
          var getUrlParameter = function getUrlParameter(sParam) {
          var sPageURL = decodeURIComponent(window.location.search.substring(1)),
          sURLVariables = sPageURL.split('&'),sParameterName,i;
          for (i = 0; i < sURLVariables.length; i++) 
          {
            sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] === sParam) 
              {
                return sParameterName[1] === undefined ? true : sParameterName[1];
              }
            }
          }; 
          if (getUrlParameter('response_code')) $('#ResponseWarrantyModal').modal('show'); 

            function goBack() {
              window.history.back();
              window.history.back();
            }
          </script>

    </footer>
  </body>
</html>