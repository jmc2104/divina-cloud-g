<!-- Get the master file with the Header and Footer -->
@extends('Support.MasterPage')

<!-- Main content of the page -->

@section('title', 'main')

@section('content')
<body>
  <div class="container">
    <h1 class="text-center display-3 mt-4">
      Registration System Tools
    </h1>
    <form method="POST" action="https://portal-dot-motivaimagine-web.appspot.com/logout" onsubmit="toggle('loader', true)">
      <button type="submit" class="btn btn-outline-danger floating mx-2 my-3">Logout</button>
    </form>

    <hr class="my-5">

    <div class="card-deck card-menu">

      <div class="card bg-primary mb-3">
        <a class="card-link" href="{{Route('implant-search',array('data' => 1))}}">
          <div class="card-body text-light">
            <h4 class="card-title">Implant Search</h4>
            <p class="card-text">Registration, Warranty and implant info.</p>
          </div>
        </a>
      </div>

      <div class="card border-primary mb-3">
        <a class="card-link" href="https://masking.motivaimagine.com/" target="_blank">
          <div class="card-body text-primary">
            <h4 class="card-title">User Masking</h4>
            <p class="card-text">Impersonate any user.</p>
          </div>
        </a>
      </div>

      <div class="card border-primary mb-3">
        <a class="card-link" href="">
          <div class="card-body text-primary">
            <h4 class="card-title">Validation Codes</h4>
            <p class="card-text">Get lost validation codes</p>
          </div>
        </a>
      </div>
         <div class="card border-primary mb-3">
        <a class="card-link" href="{{Route('warranty-report')}}">
          <div class="card-body text-primary">
            <h4 class="card-title">Finances Report</h4>
            <p class="card-text">Get the warranties active bewteen two dates</p>
          </div>
        </a>
      </div>

    </div>

    
 
  </div><!-- /.row -->
</div><!-- /.container -->
@endsection