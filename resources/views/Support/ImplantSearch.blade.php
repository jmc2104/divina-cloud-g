<!-- Get the master file with the Header and Footer -->
@extends('Support.MasterPage')

<!-- Main content of the page -->

@section('title', 'Login')

@section('content')
<body>
  <div class="container">
      {!! Form::open(['route' => ['support-main'],'name' => 'prueba']) !!}
     <div>
             <button class="rounded-circle btn floatingRigth" type='summit'}}"><img  width="35" height="35" src="{{ URL::asset('images/back3.png') }}"></button>
      </div>
 </form>
    <div class="row justify-content-center pt-3">
      <div class="col-12 text-center">
       {!! Form::open(['route' => ['implant-search']]) !!}
          <div class="form-row justify-content-center mb-2">
            <div class="col-sm-8 col-md-6 col-lg-4">
               {!! Form::text('serial',null,['class' => 'form-control big-input','placeholder' => 'Search by Serial Number','required']) !!}
            </div>
          </div>
        </form>
       
      </div><!-- /.col-12 -->
        <form method="POST" action="https://portal-dot-motivaimagine-web.appspot.com/logout" onsubmit="toggle('loader', true)">
          <button class="rounded-circle btn floating" ><img  width="35" height="35" src="{{ URL::asset('images/Logout2.png') }}"></button>
           <div>
            <!-- <a class="rounded-circle btn floating  " href="{{Route('implant-search',array('data' => 1))}}"><img  width="35" height="35" src="{{ URL::asset('images/Logout2.png') }}"></a> -->
      </div>
        </form>
    </div><!-- /.row -->
  </div><!-- /.container -->
  <php?>
  <hr>

  <div class="container rel">
    <div id="loader" class="loader d-none">
      <img src="http://localhost/public/img/color-loader.gif">
    </div>
    @IF(!is_null($data))
      @IF  ($data['status'] == true) 
          <div class="row py-3">
        <div class="col-lg-4">
          <div class="card">
            <div class="card-header text-center">
              <strong>Implant</strong>
            </div>
            <div class="card-body pb-0">
              <table class="table">
                <tbody><tr>
                  <th>Serial Number:</th>
                  <td>{{ $data['implant']['serial'] }}</td>
                </tr>
                <tr>
                  <th>Reference</th>
                  <td>{{ $data['implant']['reference'] }}</td>
                </tr>
                <tr>
                  <th>Registered</th>
                  @IF (!is_null($data['surgery']))
                  <td>{{ date("d-M-Y",strtotime($data['surgery']['created_at'])) }}</td>
                  @ELSE
                  <td>N/A</td>
                  @ENDIF
                </tr>
              </tbody></table>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="card">
            <div class="card-header text-center">
              <strong>Warranty</strong>
            </div>
            <div class="card-body pb-0">
              <table class="table">
                <tbody><tr>
                  <th>Warranty Program</th>
                  <td>{{ $data['implant']['wprogram'] }}</td>
                </tr>
                <tr>
                  @IF (!is_null($data['warranty']))
                  <th>Start Date</th>
                  <td>{{ date("d-M-Y",strtotime($data['warranty']['start_date'])) }}</td>
                </tr>
                <tr>
                  <th>Expiration</th>
                  <td>{{ date("d-M-Y",strtotime($data['warranty']['end_date'])) }}</td>
                </tr>
                @ELSE
                <th>Start Date</th>
                  <td>N/A</td>
                </tr>
                <tr>
                  <th>Expiration</th>
                  <td>N/A</td>
                </tr>
                @ENDIF
              </tbody></table>
            </div>
          </div>
        </div>

        <div class="col-lg-4">
          <div class="card">
            <div class="card-header text-center">
              <strong>Payments</strong>
            </div>
            <div class="card-body pb-0">
              <table class="table">
                <tbody>
                  @IF (!is_null($data['order']))
                  <tr>
                  <th>Order #</th>
                  <td>{{ $data['order']['transaction_id'] }}</td>
                </tr>
                <tr>
                  <th>Date</th>
                  @IF ($data['warranty']['is_extended'] == 1)
                  <td>{{ date("d-M-Y",strtotime($data['order']['date'])) }}</td>
                  @ELSE
                  <td>N/A</td>
                  @ENDIF
                </tr>
                <tr>
                  <th>Amount</th>
                  <td>{{ $data['order']['amount'] }}</td>
                </tr>
                  @ELSE
                <tr>
                  <th>Order #</th>
                  <td>N/A</td>
                </tr>
                <tr>
                  <th>Date</th>
                  <td>N/A</td>
                </tr>
                <tr>
                  <th>Amount</th>
                  <td></td>
                </tr>
                  @ENDIF
              </tbody>
            </table>
            </div>
          </div>
        </div>
      </div><!-- /.row -->
      <hr>
      <div class="row justify-content-center py-3">
        <div class="col-lg-6">
          <div class="card">
            <div class="card-header text-center">
              <strong>Surgery</strong>
            </div>
            <div class="card-body pb-0">
             <table class="table">
                  <tbody>
                    @IF (!is_null($data['surgery']))
                    <tr>
                      
                      <th>Patient:</th><td>{{ $data['surgery']['name'] }} </td>

                    </tr>
                    <tr>
                      <th>Email</th>
                      <td><a href="https://masking.motivaimagine.com/?email={{ $data['surgery']['email'] }}" target="_blank">{{ $data['surgery']['email'] }}</a>
                       <a class="btn btn-outline-primary" href="{{ Route('account-update',array('serial'=> $data['implant']['serial'])) }}">Edit</a>
                      </td>
                  </tr>
                  <tr>
                    <th>Surgery Date</th>
                    <td>{{ date("d-M-Y",strtotime($data['surgery']['date'])) }}</td>
                  </tr>
                  <tr>
                    <th>Doctor:</th>
                    <td>{{ $data['surgery']['doctor_name'] }}</td>
                  </tr>
                  <tr>
                    <th>Country</th>
                    <td>{{ $data['surgery']['country_name'] }}</td>
                  </tr>
                  @ENDIF
                </tbody>
            </table>
          </div>
          </div>
        </div>
      </div>
    </div><!-- /.row -->
    @ELSE
      <h1 class="display-4 text-center">
        Implant not found
        </h1>
        <br>
        <p class="text-center">
          Check the Serial Number or contact support.
        </p>
    @ENDIF
    @ENDIF
  </div><!-- /.container -->

@endsection