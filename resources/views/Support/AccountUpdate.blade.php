<!-- Get the master file with the Header and Footer -->
@extends('Support.MasterPage')

<!-- Main content of the page -->

@section('title', 'Login')

@section('content')
<body>
  <div class="container">
      {!! Form::open(['route' => ['implant-search'],'serial' => $data['implant']['serial'] ]) !!}
     <div>
             <button class="rounded-circle btn floatingRigth" type='summit'}}"><img  width="35" height="35" src="{{ URL::asset('images/back3.png') }}"></button>
      </div>
 </form>
    <div class="row justify-content-center pt-3">
      <div class="col-12 text-center">
       {!! Form::open(['route' => ['implant-search']]) !!}
          <div class="form-row justify-content-center mb-2">
            <div class="col-sm-8 col-md-6 col-lg-4">
               {!! Form::text('serial',$data['implant']['serial'],['class' => 'form-control big-input','placeholder' => 'Search by Serial Number','required']) !!}
            </div>
          </div>
        </form>
      </div><!-- /.col-12 -->
      <form method="POST" action="https://portal-dot-motivaimagine-web.appspot.com/logout" onsubmit="toggle('loader', true)">
          <button class="rounded-circle btn floating" ><img  width="35" height="35" src="{{ URL::asset('images/Logout2.png') }}"></button>
           <div>
    </div><!-- /.row -->
  </div><!-- /.container -->
  <php?>
      <hr>
      {!! Form::open(['route' => ['setaccount-update']]) !!}
    <div class="container">
      <div class="container">
            @foreach ($errors->all() as $error)
                <div class="alert alert-warning alert-dismissible default" role="alert">{{ $error }}</div>
            @endforeach
    </div>
    <div class="row justify-content-center pt-3">
      <div class="row justify-content-center py-3">
        <div class="col-lg-12">
          <div style="display:none">
             {!! Form::text('id',$data['surgery']['id']) !!}
             {!! Form::text('serial',$data['implant']['serial']) !!}
          </div>
          <div class="card">
            <div class="card-header text-center">
              <strong>Surgery</strong>
            </div>
            <div class="card-body pb-0">
             <table class="table">
                  <tbody>
                    @IF (!is_null($data['surgery']))
                    <tr>
                      
                      <th>Patient:</th><td class="">  {!! Form::text('name',$data['surgery']['name'],['class' => 'form-control','disabled']) !!} </td>
                    </tr>
                    <tr>
                      <th>Email</th>
                      <td> {!! Form::text('email',$data['surgery']['email'],['class' => 'form-control']) !!}
                      </td>
                  </tr>
                  <tr>
                    <th>Surgery Date</th>
                    <td>
                      {!! Form::date('surgery_date', Carbon\Carbon::parse($data['surgery']['date'])->format('Y-m-d'),['class' => 'form-control','max' => Carbon\Carbon::now()->format('Y-m-d')]) !!}
                    </td>
                  </tr>
                  <tr>
                    <th>Doctor:</th>
                    <td>
                    {!! Form::text('doctor_name',$data['surgery']['doctor_name'],['class' => 'form-control']) !!}</td>
                  </tr>
                  <tr>
                    <th>Country</th>
                    <td>
                      {!! Form::select('country_id', ['1' => 'Vatican City','2' => 'Switzerland','3' => 'Andorra','4' => 'Estonia','5' => 'Iceland','6' => 'Armenia','7' => 'Albania','8' => 'Czech Republic','9' => 'Georgia','10' => 'Austria','11' => 'Ireland','12' => 'Gibraltar','13' => 'Greece','14' => 'Netherlands','15' => 'Portugal','16' => 'Norway','17' => 'Latvia','18' => 'Lithuania','19' => 'Luxembourg','20' => 'Spain','21' => 'Italy','22' => 'Romania','23' => 'Poland','24' => 'Belgium','25' => 'France','26' => 'Bulgaria','27' => 'Denmark','28' => 'Croatia','29' => 'Germany','30' => 'Hungary','31' => 'Bosnia/herzegovina','32' => 'Finland','33' => 'Belarus','34' => 'Faeroe Islands','35' => 'Monaco','36' => 'Cyprus','37' => 'Macedonia','38' => 'Slovakia','39' => 'Malta','40' => 'Slovenia','41' => 'San Marino','42' => 'Sweden','43' => 'United Kingdom','44' => 'Cook Islands','45' => 'Palau','46' => 'Tuvalu','47' => 'Nauru','48' => 'Kiribati','49' => 'Marshall Islands','50' => 'Niue','51' => 'Tonga','52' => 'New Zealand','53' => 'Australia','54' => 'Vanuatu','55' => 'Solomon Islands','56' => 'Samoa','57' => 'Fiji','58' => 'Micronesia','59' => 'Guinea-bissau','60' => 'Zambia','61' => 'Ivory Coast','62' => 'Western Sahara','63' => 'Equatorial Guinea','64' => 'Egypt','65' => 'Congo','66' => 'Central African Republic','67' => 'Angola','68' => 'Gabon','69' => 'Ethiopia','70' => 'Guinea','71' => 'Gambia','72' => 'Zimbabwe','73' => 'Cape Verde','74' => 'Ghana','75' => 'Rwanda','76' => 'Tanzania','77' => 'Cameroon','78' => 'Namibia','79' => 'Niger','80' => 'Nigeria','81' => 'Tunisia','82' => 'Liberia','83' => 'Lesotho','84' => 'Togo','85' => 'Chad','86' => 'Eritrea','87' => 'Libya','88' => 'Burkina Faso','89' => 'Djibouti','90' => 'Sierra Leone','91' => 'Burundi','92' => 'Benin','93' => 'South Africa','94' => 'Botswana','95' => 'Algeria','96' => 'Swaziland','97' => 'Madagascar','98' => 'Morocco','99' => 'Kenya','100' => 'Mali','100' => 'Mali','101' => 'Comoros','102' => 'Sao Tome And Principe','103' => 'Mauritius','104' => 'Malawi','105' => 'Somalia','106' => 'Senegal','107' => 'Mauritania','108' => 'Seychelles','109' => 'Uganda','110' => 'Sudan','111' => 'Mozambique','112' => 'Mongolia','113' => 'China','114' => 'Afghanistan','115' => 'Serbia','116' => 'Vietnam','117' => 'Canary Islands','118' => 'India','119' => 'Azerbaijan','120' => 'Indonesia','121' => 'Russia','122' => 'Laos','123' => 'Taiwan','124' => 'Turkey','125' => 'Sri Lanka','126' => 'Turkmenistan','127' => 'Tajikistan','128' => 'Papua New Guinea','129' => 'Thailand','130' => 'Nepal','131' => 'Pakistan','132' => 'Philippines','133' => 'Bangladesh','134' => 'Ukraine','135' => 'Brunei','136' => 'Japan','137' => 'Bhutan','138' => 'Hong Kong','139' => 'Kyrgyzstan','140' => 'Uzbekistan','141' => 'Burma (myanmar)','142' => 'Singapore','143' => 'Macau','144' => 'Cambodia','145' => 'Republic of Korea ','146' => 'Maldives','147' => 'Kazakhstan','148' => 'Malaysia','149' => 'Guatemala','150' => 'Antigua And Barbuda','151' => 'British Virgin Islands (uk)','152' => 'Anguilla (uk)','153' => 'Virgin Island','154' => 'Canada','155' => 'Grenada','156' => 'Aruba (netherlands)','157' => 'Costa Rica','158' => 'Cuba','159' => 'Puerto Rico (us)','160' => 'Nicaragua','161' => 'Trinidad And Tobago','162' => 'Guadeloupe (france)','163' => 'Panama','164' => 'Dominican Republic','165' => 'Dominica','166' => 'Barbados','167' => 'Haiti','168' => 'Jamaica','169' => 'Honduras','170' => 'Bahamas, The','171' => 'Belize','172' => 'Saint Kitts And Nevis','173' => 'El Salvador','174' => 'United States','175' => 'Martinique (france)','176' => 'Monsterrat (uk)','177' => 'Cayman Islands (uk)','178' => 'Mexico','179' => 'South Georgia and the South Sandwich Islands','180' => 'Paraguay','181' => 'Colombia','182' => 'Venezuela','183' => 'Chile','184' => 'Suriname','185' => 'Bolivia','186' => 'Ecuador','187' => 'French Guiana','188' => 'Argentina','189' => 'Guyana','190' => 'Brazil','191' => 'Peru','192' => 'Uruguay','193' => 'Falkland Islands','194' => 'Oman','195' => 'Lebanon','196' => 'Iraq','197' => 'Yemen','198' => 'Iran','199' => 'Bahrain','200' => 'Syria','201' => 'Qatar','202' => 'Jordan','203' => 'Kuwait','204' => 'Israel','205' => 'United Arab Emirates','206' => 'Saudi Arabia','207' => 'Liechtenstein','208' => "Democratic People's Republic of Korea",'209' => 'Montenegro'], $data['surgery']['country_id'], ['class' => 'form-control']); !!}

                    </td>
                  </tr>
                  @ENDIF
                </tbody>
            </table>
          </div>
          </div>
        </div>
      </div>

    </div><!-- /.row -->
    <div class="row justify-content-center pt-3">
    <button type="summit" class="btn btn-outline-success b-radius">Save</button>
    </div>
  </div><!-- /.container -->
   </form>

@endsection