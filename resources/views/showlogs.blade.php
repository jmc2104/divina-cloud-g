<!-- Get the master file with the Header and Footer -->
@extends('layouts.master4')

<!-- Main content of the page -->
@section('content')
<div class="container">
<div class="col-xs-12 col-md-10 col-md-offset-1">
    @if (count($errors) > 0)
    <div class="container">
            @foreach ($errors->all() as $error)
                <div class="alert alert-warning" role="alert">{{ $error }}</div>
            @endforeach
    </div>
@endif

   <div class="main-wrapper patients">
      <div class="spacer-15">&nbsp;</div>
      <h1 class="text-center">Divina Logs</h1>
      <div class="spacer-10">&nbsp;</div>
      
   <div id="patients-table" class="col-md-9 col-md-offset-2">

        <div class="table-filters">
          <h1>Find your Device</h1>
          <div class="row form-inline">
              <!---->
            <div class="form-group">
               <label class="control-label">Device Sn</label>
              <div id="type-helper" class="input-group mb-2 mr-sm-2 mb-sm-0" data-container="body" data-toggle="popover" data-html="true" data-placement="right" data-content="Select Device SN" data-trigger="focus">
                      <select class="form-control" id="dropDevice" name="inputType" required onchange="DeviceSelect()">
                        <?php
                        $Devices = $patient_list->unique('device_sn');

                        foreach ($Devices as $Device) {
                          echo "<option value=".$Device->device_sn.">".$Device->device_sn."<Patient</option>";
                        }
                        ?>
                      </select>
                    </div>
                  </div>
              <!---->
            <!-- /.filter-type -->
            <div class="form-group">
               <label class="control-label">Result</label>
              <div id="type-helper" class="input-group mb-2 mr-sm-2 mb-sm-0" data-container="body" data-toggle="popover" data-html="true" data-placement="right" data-content="Select Device SN" data-trigger="focus">
                      <select class="form-control" id="dropResult" name="inputType" required onchange="ResultSelect()">
    
                         <option value="success">success</option>;
                         <option value="error">error</option>;
                      </select>
                    </div>
                  </div><!-- /.filter-type -->

            <div class="filter-sort col-md-2">
              <a  onclick="clear_filters()" class="btn wami-btn padded pull-right">Clear</a>
            </div><!-- /.filter-type -->
          </div><!-- /.row -->
        </div><!-- /.table-filters -->

     <div class="table-wrapper">
          <div class="table-responsive table-scroll">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th style="cursor: pointer" onclick="sort('device_sn')"> <span>device_sn</span> </th>
                  <th style="cursor: pointer" onclick="sort('result')"> <span>Result</span> </th>
                  <th> <span>Description</span> </th>
                  <th style="cursor: pointer" onclick="sort('display_date')"> <span>Date</span> </th>
                </tr>
              </thead>
              <tbody class="list">
                @if ( $patient_list)
                  @foreach ($patient_list as $patient)
                    <tr>
                      <td class="device_sn">{{$patient->device_sn}} </td>
                      <td class="result">{{$patient->result}} </td>
                      <td class="description">{{$patient->description}} </td>
                      <td class="display_date">{{date('d-M-Y', strtotime($patient->date))}} </td>
                    </tr>
                  @endforeach
                 @endif
              </tbody>
            </table>
            
          </div><!-- /.table-responsive table-scroll -->
        </div><!-- /.table-wrapper  -->
 <div class="pagination pull-right"></div>

        
        
      </div><!-- /.patients-table -->
 
 </div>
  </div><!-- /.main-wrapper -->

<script type="text/javascript">
/*
  $('.datepicker').pickadate({
    max: true,
    today: false,
    clear: false,
    close: false,
    format: 'dd-mmm-yyyy',
    formatSubmit: 'yyyymmdd',
    hiddenName: true,
    onClose: function (context)
    {
      var filter = document.getElementsByName('inputSurgeryDate')[0].value;
      patient_list.filter(function(item)
      {
        return item.values().date == filter;
      });
    }
  });
  */
</script>
<script src="{{ URL::asset('scripts/list.min.js') }}"></script>

<script type="text/javascript">
  var options =
  {
    valueNames: [ 'device_sn', 'display_date','result' ],
    pagination: { outerWindow : 3 },
    page: 10
  };

  var patient_list = new List('patients-table', options);
  var order = ['asc', 'desc'];

  function clear_filters()
  {
    /* Set default values */
    patient_list.search();
    patient_list.filter();
  }
  function filter2(){
     var filter = document.getElementById('inputfilter2').value;
      patient_list.filter(function(item)
      {
        if (item.values().result.trim() == filter)
          return item.values().result;
      });
    }

  function sort( column )
  {
    switch( column )
    {
      case 'device_sn':
      order[0] = ( order[0] === 'desc' ) ? 'asc' : 'desc';
      patient_list.sort('device_sn', { order: order[0] });
      break;

      case 'display_date':
      order[1] = ( order[1] === 'desc' ) ? 'asc' : 'desc';
      patient_list.sort('display_date', { order: order[1] });
      break;

      case 'result':
      order[2] = ( order[2] === 'desc' ) ? 'asc' : 'desc';
      patient_list.sort('result', { order: order[2] });
      break;

    }
  }

  function DeviceSelect()
  {
     var Find = document.getElementById("dropDevice").value;
     patient_list.search(Find,['device_sn']);
  }
    function ResultSelect()
  {
    DeviceSelect();
     var filter = document.getElementById("dropResult").value;
     patient_list.filter(function(item)
      {
        if (item.values().result.trim() == filter)
          return item.values().result;
      });
  }
  
</script>
@endsection