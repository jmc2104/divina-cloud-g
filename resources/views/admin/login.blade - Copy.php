<!DOCTYPE html>
<html>
     <head>
  <title>Support Portal</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/yeti/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="https://motivaimagine.com/inquiries/public/css/portal.css">
</head>
<body>
  <div class="container">
   @yield('content')
  </div>
  <footer> 
    <!-- Extra Scripts that need to be loaded only on this page -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
    <script src="https://motivaimagine.com/inquiries/public/js/app.js"></script>
    </footer>
  </body>
</html>