<!-- Get the master file with the Header and Footer -->
@extends('layouts.admin.master')

<!-- Main content of the page -->
@section('content')
  
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Patients
      <small></small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
      <li><a href="#">Patients</a></li>
      <li class="active">All</li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content">
    
    <div class="row">
      <div class="col-md-3">
        
        <!-- Profile Image -->
        <div class="box box-primary">
          
          <div class="box-body box-profile">
            <img class="profile-user-img img-responsive img-circle" src="http://placehold.it/128x128" alt="User profile picture">
            
            <h3 class="profile-username text-center">{{ $patient->profile->get_full_name() }}</h3>
            
            <p class="text-muted text-center">{{ $patient->user->email }}</p>
            
            <ul class="list-group list-group-unbordered">
              <li class="list-group-item">
                <b>Country</b> <a class="pull-right">{{ $patient->country->name }}</a>
              </li>
              <li class="list-group-item">
                <b>Following</b> <a class="pull-right">543</a>
              </li>
              <li class="list-group-item">
                <b>Friends</b> <a class="pull-right">13,287</a>
              </li>
            </ul>
            
            <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
          </div>
          
        </div><!-- /.box -->
      </div>
    </div>
    
    
    
    
  </section>
  <!-- /.content -->
  
  
@endsection


<!-- Extra Scripts that need to be loaded only on this page -->
@section('scripts')
  
  <script type="text/javascript">
  document.getElementById('nav_patients').classList.add('active')
  </script>
  
  <script>
  $(function ()
  {
    $('#uiPatientsTable').DataTable();
  });
  </script>
  
@endsection
