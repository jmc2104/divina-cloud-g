<!-- Get the master file with the Header and Footer -->
@extends('layouts.admin.master')

<!-- Main content of the page -->
@section('content')
  
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Patients
      <small>Blank example to the fixed layout</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
      <li><a href="#">Patients</a></li>
      <li class="active">All</li>
    </ol>
  </section>
  
  <!-- Main content -->
  <section class="content">
    
    
    <div class="row">
      <div class="col-xs-12">
        
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Last 200 Patients Registered</h3>
          </div><!-- /.box-header -->
          <div class="box-body">
            <table id="uiPatientsTable" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Patient ID</th>
                  <th>Full Name</th>
                  <th>Email</th>
                  <th>Country</th>
                  <th>Created</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($dataPatients as $patient)
                  <tr>
                    <td> <a href="/a/patients/{{ $patient->id }}">{{ $patient->id }}</a> </td>
                    <td> {{ $patient->profile->get_full_name() }} </td>
                    <td> {{ $patient->user->email }} </td>
                    <td> {{ $patient->profile->country->name }} </td>
                    <td> {{ App\Helpers::date($patient->created_at) }} </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div>
    </div>
    
    
    
    
  </section>
  <!-- /.content -->
  
  
@endsection


<!-- Extra Scripts that need to be loaded only on this page -->
@section('scripts')
  
  <script type="text/javascript">
  document.getElementById('nav_patients').classList.add('active')
  </script>
  
  <script>
  $(function ()
  {
    $('#uiPatientsTable').DataTable();
  });
  </script>
  
@endsection
