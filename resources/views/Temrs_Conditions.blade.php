<!-- Get the master file with the Header and Footer -->
@extends('layouts.master4')

<!-- Main content of the page -->

@section('title', 'Recover Password')

@section('content')

     <div class="container col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        @if($data{'type'} == "1")
                <div class="alert alert-wami alert-dismissible success text-center" role="alert">{!!$data{'message'}!!}</div>
        @elseif($data{'type'} == "2")
          <div class="alert alert-wami alert-dismissible default" role="alert">{!!$data{'message'}!!}</div>
        @endif
    </div>



@endsection