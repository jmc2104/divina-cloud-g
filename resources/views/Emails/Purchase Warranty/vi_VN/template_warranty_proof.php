﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
   <title>Motiva Implants Matrix</title>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <style type="text/css">
      * {
         -webkit-text-size-adjust: none;
         -webkit-text-resize: 100%;
         text-resize: 100%;
      }
      .no-link a{
         text-decoration:none !important;
         color:#fff !important;
      }
      .no-link-2 a{
         text-decoration:none !important;
         color:#fff !important;
      }
      sup{vertical-align: super; font-size: x-small;}
      @media only screen and (max-width:750px) {
         table[class="wrapper"]{min-width:320px !important;}
         table[class="flexible"]{width:100% !important;}
         td[class="img-flex"] img{
            width:100% !important;
            height:auto !important;
         }
         td[class="no-float"]{float:none !important;}
         table[class="center"]{margin:0 auto !important;}
         td[class="header"]{padding:5px 10px 0 !important;}
         td[class="area-1"]{padding:10px 10px !important;}
         td[class="area-2"]{padding:15px 10px !important;}
         td[class="post"]{padding:0 0 20px;}
         td[class="post"] td[class="holder"]{
            padding:0 0 15px !important;
            height:auto !important;
         }
         td[class="aligncenter"],
         td[class="no-link"]{text-align:center !important;}
         td[class="col"],
         td[class="sidebar"]{padding:10px !important;}
         td[class="col"] td[class="holder"],
         td[class="sidebar"] td[class="text"]{padding:10px 0 0 !important;}
         td[class="col"] td[class="holder-2"]{height:auto !important;}
      }
   </style>
</head>
<body style="margin:0; padding:0;" bgcolor="#ffffff" link="#f5f5f5" >
   <table class="wrapper" width="100%" cellspacing="0" cellpadding="0" style="background-color: #ffffff; background-repeat:repeat;">
      <tr>
         <td height="10" style="font-size:0; line-height:0;" >&nbsp;</td>
      </tr>
      <tr>
         <td align="center" style="padding:0 5px 26px;">
            <table class="flexible" width="750" cellpadding="0" cellspacing="0">
               <tr>
                  <td class="area-1" style="padding:10px 0px 12px; background-image:url(http://motivaimplants.com/letters/images/gold-bg.png); background-size: 750px 63px; background-repeat: no-repeat; border-radius:2px" bgcolor="#ffffff">
                     <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                           <td class="header">
                              <table class="flexible" width="750" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                 <tr>
                                    <td mc:edit="block-1" class="aligncenter" align="center" style="padding:0 0 12px;">
                                       <a href="http://motivaimplants.com/"><img src="http://motivaimplants.com/letters/images/motiva.png" border="0" style="vertical-align:top;" width="135" height="45" alt="Motiva Implant Matrix" /></a>
                                    </td>
                                 </tr>
                              </table>
                           </td>
                        </tr>
                        <tr>
                           <td mc:edit="block-3" class="img-flex" style="padding:0 0 8px;">
                              <img src="http://motivaimplants.com/letters/images/img-extended.jpg" style="vertical-align:top; border-radius: 0px 0px 2px 2px" width="750" height="250" alt="Your Coverage has been Extended" />
                           </td>
                        </tr>
                        <tr>
                           <td style="padding:0 20px 30px;">
                              <table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                 <tr>
                                    <td class="post">
                                       <table width="100%" cellpadding="0" cellspacing="0">
                                          <tr>
                                             <td class="holder" height="70" valign="top">
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                   <tr>
                                                      <td mc:edit="block-4" style="font:12px/16px Helvetica, Arial, sans-serif; font-weight: 300; color:#9c9c9c; padding:0 0 10px; ">
                                                         Ngày: <?php echo ($today)?>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td mc:edit="block-4" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#222; font-weight: normal; padding:0 0 10px; ">
                                                       
                                                         Kính gửi Bà  <?php echo ($name)?>
                                                         
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
                                                         Các nhà sản xuất Mô cấy Motiva Establishment Labs xin cảm ơn bà đã lựa chọn Chương trình Bảo hành Gia hạn Motiva <?php echo ($year)?> năm, mà sẽ bao trả cho các chi phí y tế lên tới <?php echo ($currency)?> <?php echo ($coverage)?> mỗi mô cấy bị ảnh hưởng, trong trường hợp co cứng nang Baker Cấp III hoặc IV, vỡ hoặc luân chuyển, kể từ ngày phẫu thuật, theo các điều khoản và điều kiện có thể được xem xét trên trang web của chúng tôi. 
                                                         <br><br>

                                                         Hồ sơ của chúng tôi cho biết:
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td mc:edit="block-4" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666; padding:10px 0 10px 30px; ">
                                                         <?php if ($hasLeftImplant):?>
                                                            • Phẫu thuật của bạn cho <span style="color:#222; font-weight: normal;">Mô cấy bên trái</span> với <span style="color:#222; font-weight: normal;">Số Sê-ri: <?php echo ($snL)?></span>, được đăng ký thực hiện vào ngày <span style="color:#222; font-weight: normal;"><?php echo ($surgeryDateL)?></span>. Bảo hành của bạn hiện đã được gia hạn cho đến ngày <span style="color:#222; font-weight: normal;"><?php echo ($snLDateExp)?></span>.<br><br>
                                                         <?php endif ?>
                                                         <?php if ($hasRightImplant):?>
                                                           • Phẫu thuật của bạn cho <span style="color:#222; font-weight: normal;">Mô cấy bên phải</span> với <span style="color:#222; font-weight: normal;">Số Sê-ri: <?php echo ($snR)?></span>, được đăng ký thực hiện vào ngày <span style="color:#222; font-weight: normal;"><?php echo ($surgeryDateR)?></span>. Bảo hành của bạn hiện đã được gia hạn cho đến ngày <span style="color:#222; font-weight: normal;"><?php echo ($snRDateExp)?></span>.
                                                            <br><br>                                                                       
                                                         <?php endif ?>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
                                                         Tài khoản Motiva của bạn đã được cập nhật để thể hiện mong muốn tham gia vào Chương trình Bảo hành Mở rộng Motiva của bạn. Nếu bạn có bất kỳ câu hỏi hoặc mối quan tâm nào, vui lòng gửi thư cho chúng tôi theo địa chỉ email dưới đây. Chúng tôi đánh giá cao sự tin tưởng của bạn trong việc lựa chọn mô cấy Motiva.<br><br>
                                                         Trân trọng,<br><br>
                                                         Phòng Dịch vụ Khách hàng Establishment Labs<br>
                                                      </td>
                                                   </tr>
                                                </table>
                                             </td>
                                          </tr>
                                       </table>
                                    </td>
                                 </tr>
                              </table>
                           </td>
                        </tr>
                     </table>
                  </td>
               </tr>
               <tr>
                  <td class="area-2" style="padding:15px 20px 5px 21px; background-color:#652d89; border-radius:2px">
                     <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                           <td>
                              <table class="flexible" width="410" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                 <tr>
                                    <td mc:edit="block-33" class="aligncenter">
                                       <a href="http://motivaimplants.com/"><img src="http://motivaimplants.com/letters/images/motivaimagine.png" border="0" style="vertical-align:top;" width="220" height="37" alt="MotivaImagine" /></a>
                                    </td>
                                 </tr>
                              </table>
                              <table class="flexible" width="160" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                 <tr>
                                    <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
                                       <a href="https://www.facebook.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-facebook.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Facebook Icon" /></a>
                                    </td>
                                    <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
                                       <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://www.facebook.com/motivaimplants">Theo dõi chúng tôi trên Facebook</a>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
                                       <a href="https://twitter.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-twitter.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Twitter Icon" /></a>
                                    </td>
                                    <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
                                       <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://twitter.com/motivaimplants">Theo dõi chúng tôi trên Twitter</a>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
                                       <a href="https://instagram.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-instagram.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Instagram Icon" /></a>
                                    </td>
                                    <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
                                       <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://instagram.com/motivaimplants">Theo dõi chúng tôi trên Instagram</a>
                                    </td>
                                 </tr>
                              </table>
                           </td>
                        </tr>
                     </table>
                  </td>
               </tr>
            </table>
         </td>
      </tr>
      <tr>
         <td align="center" style="padding:2px 10px 7px;">
            <table class="flexible" width="600" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
               <tr>
                  <td class="no-float" align="center">
                     <table class="center" cellpadding="0" cellspacing="0">
                        <tr>
                           <td style="font:normal 9px/11px Helvetica, Arial, sans-serif; color:#888888;">
                              Nếu bà nhận được email này bị lỗi, nếu thông tin của bà không chính xác, hoặc nếu bà không đồng ý với Chính sách về Quyền riêng tư và các Điểm khoản Sử dụng có trên webside của chúng tôi: <a style="text-decoration:none; color:#111111;" href="http://motivaimplants.com/">www.motivaimplants.com</a>.
                           </td>
                        </tr>
                     </table>
                  </td>
               </tr>
            </table>
         </td>
      </tr>
   </table>
</body>
</html>