﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
   <title>Motiva Implants Matrix</title>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <style type="text/css">
      * {
         -webkit-text-size-adjust: none;
         -webkit-text-resize: 100%;
         text-resize: 100%;
      }
      .no-link a{
         text-decoration:none !important;
         color:#fff !important;
      }
      .no-link-2 a{
         text-decoration:none !important;
         color:#fff !important;
      }
      sup{vertical-align: super; font-size: x-small;}
      @media only screen and (max-width:750px) {
         table[class="wrapper"]{min-width:320px !important;}
         table[class="flexible"]{width:100% !important;}
         td[class="img-flex"] img{
            width:100% !important;
            height:auto !important;
         }
         td[class="no-float"]{float:none !important;}
         table[class="center"]{margin:0 auto !important;}
         td[class="header"]{padding:5px 10px 0 !important;}
         td[class="area-1"]{padding:10px 10px !important;}
         td[class="area-2"]{padding:15px 10px !important;}
         td[class="post"]{padding:0 0 20px;}
         td[class="post"] td[class="holder"]{
            padding:0 0 15px !important;
            height:auto !important;
         }
         td[class="aligncenter"],
         td[class="no-link"]{text-align:center !important;}
         td[class="col"],
         td[class="sidebar"]{padding:10px !important;}
         td[class="col"] td[class="holder"],
         td[class="sidebar"] td[class="text"]{padding:10px 0 0 !important;}
         td[class="col"] td[class="holder-2"]{height:auto !important;}
      }
   </style>
</head>
<body style="margin:0; padding:0;" bgcolor="#ffffff" link="#f5f5f5" >
   <table class="wrapper" width="100%" cellspacing="0" cellpadding="0" style="background-color: #ffffff; background-repeat:repeat;">
      <tr>
         <td height="10" style="font-size:0; line-height:0;" >&nbsp;</td>
      </tr>
      <tr>
         <td align="center" style="padding:0 5px 26px;">
            <table class="flexible" width="750" cellpadding="0" cellspacing="0">
               <tr>
                  <td class="area-1" style="padding:10px 0px 12px; background-image:url(http://motivaimplants.com/letters/images/gold-bg.png); background-size: 750px 63px; background-repeat: no-repeat; border-radius:2px" bgcolor="#ffffff">
                     <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                           <td class="header">
                              <table class="flexible" width="750" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                 <tr>
                                    <td mc:edit="block-1" class="aligncenter" align="center" style="padding:0 0 12px;">
                                       <a href="http://motivaimplants.com/"><img src="http://motivaimplants.com/letters/images/motiva.png" border="0" style="vertical-align:top;" width="135" height="45" alt="Motiva Implant Matrix" /></a>
                                    </td>
                                 </tr>
                              </table>
                           </td>
                        </tr>
                        <tr>
                           <td mc:edit="block-3" class="img-flex" style="padding:0 0 8px;">
                              <img src="http://motivaimplants.com/letters/images/img-extended.jpg" style="vertical-align:top; border-radius: 0px 0px 2px 2px" width="750" height="250" alt="Your Coverage has been Extended" />
                           </td>
                        </tr>
                        <tr>
                           <td style="padding:0 20px 30px;">
                              <table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                 <tr>
                                    <td class="post">
                                       <table width="100%" cellpadding="0" cellspacing="0">
                                          <tr>
                                             <td class="holder" height="70" valign="top">
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                   <tr>
                                                      <td mc:edit="block-4" style="font:12px/16px Helvetica, Arial, sans-serif; font-weight: 300; color:#9c9c9c; padding:0 0 10px; ">
                                                         Data: <?php echo ($today)?>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td mc:edit="block-4" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#222; font-weight: normal; padding:0 0 10px; ">
                                                       
                                                         Gentile. <?php echo ($name)?>
                                                         
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
                                                         Congratulazioni, ti sei registrata con successo al Programma di Garanzia Estesa Motiva!<br><br>
                                                         Establishment Labs produttore di Motiva Implants ti ringrazia per aver scelto il Programma di Garanzia Estesa Motiva <?php echo ($year)?>Y, che coprirà le spese mediche fino a <?php echo ($currency)?> <?php echo ($coverage)?> per protesi danneggiata, in caso di contrattura capsulare Baker Grado III o IV, rottura o rotazione, a partire dalla data dell’intervento, in base ai termini e condizioni consultabili nel nostro sito web.<br><br>

                                                         In base ai nostri registri:
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td mc:edit="block-4" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666; padding:10px 0 10px 30px; ">
                                                         <?php if ($hasLeftImplant):?>
                                                          
                                                            • L’intervento per la <span style="color:#222; font-weight: normal;">protesi sinistra</span> con <span style="color:#222; font-weight: normal;">Numero di serie: <?php echo ($snL)?></span>, è stato registrato per aver tenuto luogo il <span style="color:#222; font-weight: normal;"><?php echo ($surgeryDateL)?></span> La tua garanzia è stata <span style="color:#222; font-weight: normal;">estesa fino al <?php echo ($snLDateExp)?> </span><br><br>
                                                            
                                                         <?php endif ?>
                                                         <?php if ($hasRightImplant):?>
                                                          • L’intervento per la <span style="color:#222; font-weight: normal;">protesi destra</span> con <span style="color:#222; font-weight: normal;">Numero di serie: <?php echo ($snR)?></span>, è stato registrato per aver tenuto luogo il <span style="color:#222; font-weight: normal;"><?php echo ($surgeryDateR)?></span> La tua garanzia è stata <span style="color:#222; font-weight: normal;">estesa fino al <?php echo ($snRDateExp)?> </span><br><br>
                                                         <?php endif ?>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
                                                         Il tuo account Motiva è stato aggiornato per rispecchiare la tua partecipazione al Programma di Garanzia Estesa Motiva. Per qualsiasi domanda o dubbio, contattaci all’indirizzo e-mail indicato di seguito. Apprezziamo la tua fiducia nella scelta di Motiva Implants.<br><br>
                                                         Cordiali saluti,<br><br>
                                                         Servizio Clienti Establishment Labs<br>
                                                      </td>
                                                   </tr>
                                                </table>
                                             </td>
                                          </tr>
                                       </table>
                                    </td>
                                 </tr>
                              </table>
                           </td>
                        </tr>
                     </table>
                  </td>
               </tr>
               <tr>
                  <td class="area-2" style="padding:15px 20px 5px 21px; background-color:#652d89; border-radius:2px">
                     <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                           <td>
                              <table class="flexible" width="410" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                 <tr>
                                    <td mc:edit="block-33" class="aligncenter">
                                       <a href="http://motivaimplants.com/"><img src="http://motivaimplants.com/letters/images/motivaimagine.png" border="0" style="vertical-align:top;" width="220" height="37" alt="MotivaImagine" /></a>
                                    </td>
                                 </tr>
                              </table>
                              <table class="flexible" width="160" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                 <tr>
                                    <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
                                       <a href="https://www.facebook.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-facebook.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Facebook Icon" /></a>
                                    </td>
                                    <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
                                       <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://www.facebook.com/motivaimplants">Seguici su Facebook</a>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
                                       <a href="https://twitter.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-twitter.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Twitter Icon" /></a>
                                    </td>
                                    <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
                                       <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://twitter.com/motivaimplants">Seguici su Twitter</a>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
                                       <a href="https://instagram.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-instagram.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Instagram Icon" /></a>
                                    </td>
                                    <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
                                       <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://instagram.com/motivaimplants">Seguici su Instagram</a>
                                    </td>
                                 </tr>
                              </table>
                           </td>
                        </tr>
                     </table>
                  </td>
               </tr>
            </table>
         </td>
      </tr>
      <tr>
         <td align="center" style="padding:2px 10px 7px;">
            <table class="flexible" width="600" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
               <tr>
                  <td class="no-float" align="center">
                     <table class="center" cellpadding="0" cellspacing="0">
                        <tr>
                           <td style="font:normal 9px/11px Helvetica, Arial, sans-serif; color:#888888;">
                              Se hai ricevuto questa e-mail per errore, se le informazioni non sono corrette, o se non sei d’accordo con l’Informativa sulla Privacy e i Termini e Condizioni d’uso nel nostro sito web: <a style="text-decoration:none; color:#111111;" href="http://motivaimplants.com/">www.motivaimplants.com</a>.
                           </td>
                        </tr>
                     </table>
                  </td>
               </tr>
            </table>
         </td>
      </tr>
   </table>
</body>
</html>