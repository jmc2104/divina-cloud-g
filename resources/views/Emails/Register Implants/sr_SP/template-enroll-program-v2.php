<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Motiva Warranty Enroll Program</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style type="text/css">
		* {
			-webkit-text-size-adjust: none;
			-webkit-text-resize: 100%;
			text-resize: 100%;
		}
		.no-link a{
			text-decoration:none !important;
			color:#fff !important;
		}
		.no-link-2 a{
			text-decoration:none !important;
			color:#fff !important;
		}
		sup{vertical-align: super; line-height: 0; font-size: 0.8em;}
		@media only screen and (max-width:750px) {
			table[class="wrapper"]{min-width:320px !important;}
			table[class="flexible"]{width:100% !important;}
			td[class="img-flex"] img{
				width:100% !important;
				height:auto !important;
			}
			td[class="no-float"]{float:none !important;}
			table[class="center"]{margin:0 auto !important;}
			td[class="header"]{padding:5px 10px 0 !important;}
			td[class="area-1"]{padding:0 !important;}
			td[class="area-2"]{padding:15px 10px !important;}
			td[class="post"]{padding:0 0 20px;}
			td[class="post"] td[class="holder"]{
				padding:0 0 15px !important;
				height:auto !important;
			}
			td[class="aligncenter"],
			td[class="no-link"]{text-align:center !important;}
			td[class="col"],
			td[class="sidebar"]{padding:10px !important;}
			td[class="col"] td[class="holder"],
			td[class="sidebar"] td[class="text"]{padding:10px 0 0 !important;}
			td[class="col"] td[class="holder-2"]{height:auto !important;}
		}
		.tg  {border-collapse:collapse;border-spacing:0;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg .tg-d5wv{background-color:#a38546;color:#ffffff;text-align:center;vertical-align:top}
		.tg .tg-yw4l{vertical-align:top}
	</style>
</head>
<body style="margin:0; padding:0;" bgcolor="#eeeeee" link="#f5f5f5" >
	<table class="wrapper" width="100%" cellspacing="0" cellpadding="0">
		<tr><td height="10" style="font-size:0; line-height:0;" >&nbsp;</td></tr>
		<tr>
			<td align="center" style="padding:10px 5px 26px 5px;">
				<table class="flexible" width="750" cellpadding="0" cellspacing="0">
					<tr>
						<td class="area-1" style="border-radius: 6px 6px 3px 3px" bgcolor="#ffffff">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td mc:edit="block-3" class="img-flex" style="padding:0 0 8px;">
										<img src="http://motivaimplants.com/letters/cover.jpg" style="vertical-align:top; border-radius: 6px 6px 0px 0px" width="750" height="400" alt="Motiva Implants Logo" />
									</td>
								</tr>
								<tr>
									<td style="padding:20px 20px 30px 20px;">
										<table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td class="post">
													<table width="100%" cellpadding="0" cellspacing="0">
														<tr>
															<td class="holder" height="70" valign="top">
																<table width="100%" cellpadding="0" cellspacing="0">
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Program za upis jamstva za Motiva
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:18px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Razmišljaj unapred
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:15px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Unapredi se u “Motiva prošireni program garancije”
																		</td>
																	</tr>
																	<tr><!-- MAIN CONTENT -->
																		<td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
																			Poštovana gđice. <?=/*php*/$name/*php*/?>
																			<span style="float: right; color:#a1a1a1;"><?=/*php*/date("d-M-Y")/*php*/?></span>
																			<br>
																			<br>
																			“Establishment Labs” kao proizvođač “Motiva Implant Matrix”, želi Vam izraziti zahvalnost zato što ste registrovali Vaš “Motiva Implants”. Vaši “Motiva Implant Matrix” implantati za grudi potpadaju pod rokom trajanja proizvoda ograničenu garanciju. Ovaj program garancije Vam osigurava da proizvođač obezbedi zamenski proizvod u slučaju da se na bilo kojem “Motiva Implant Matrix” implantatu za grudi desi napuknuće. Pored navedenog, “Establishment Labs” u svojstvu proizvođača će obezbediti svoj Program politike zamene u slučaju kada se na implantatu desi poremećaj kapsularne kontrakcije Baker stepena III i IV. Svi dodatni detalji garancije opisani su u dokumentu “Uvek pouzdana garancija”, koji će Vam biti dostavljen od strane našega distributera, odnosno kojeg ćete moći videti na <a style="text-decoration:none; color:#a38546;" href="https://motivaimplants.com">www.motivaimplants.com</a>.
																			<br>
																			<br>
																			Isto tako bi smo željeli da Vam ponudimo priliku da unapredite vaš program garancije uključivanjem u naš “<?=/*php*/$years/*php*/?> Y Motiva prošireni program garancije” koji uključuje pokrivanje troškova medicinskih usluga u iznosu do <?=/*php*/isset($currency) ? $currency : '{{currency}}'/*php*/?> <?=/*php*/$coverage/*php*/?> po jednom implantatu na kojem se desila kapsularna kontrakcija, napuknuće ili okretanje počev od dana obavljanja hirurškog zahvata, a u skladu sa pravilima i uslovima koje možete pročitati na našoj internet stranici (<a style="text-decoration:none; color:#a38546;" href="http://motivaimplants.com/product-warranty/">www.motivaimplants.com/product-warranty/</a>).
																			<br>
																			<br>
																			Na odabranim tržištima nudimo naš program “Motiva uvijek pouzdana” u cilju omogućavanja finansijske podrške za ponovne hirurške zahvate u slučaju napuknuća implantata, okretanja implantata, kapsularne kontrakcije Baker stepena III i IV. Ovaj program pokriva period do <?=/*php*/$years/*php*/?> godina počev od dana operacije.
																			<br>
																			<br>
																			<p style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546;">
																				<b>U slučaju da ste zainteresovani u ovaj prošireni vid garancije i ako želite da proverite da li isti može biti primenjen na Vaše implantate to možete učiniti preko</b>
																				<a style="text-decoration:none; color:#888;" href="https://register.motivaimagine.com/">https://register.motivaimagine.com</a>.
																			</p>
																			<br>
																			<p align="center">
																				<a href="https://register.motivaimagine.com/" style="text-decoration:none; background: #a38546; color:#fff; font-weight: 300; padding:8px 14px; border-radius:30px"><b>Proširena garancija</b></a>
																			</p>
																			<br>
																			<br>
																			Molimo Vas da iskoristite priliku da unapredite svoj garantni program na “Motiva prošireni program garancije” sa opsegom bez premca kako bi stekli još veće poverenje u “Motiva Implants”.
																			<br>
																			<br/>
																			S uvažavanjem,
																			<br>
																			<br>
																			“Establishment Labs”, Odel odnosa sa korisnicima
																			<br>
																			<br>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>



							</table>
						</td>
					</tr>



					<tr>
						<td class="area-2" style="padding:15px 20px 5px 21px; background-color:#eeeeee;">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<table class="flexible" width="410" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-33" class="aligncenter">
													<a href="http://motivaimagine.com/"><img src="http://motivaimplants.com/letters/motivaimagine.png" border="0" style="vertical-align:top; padding: 0px 0px 12px 0px" width="210" height="37" alt="MotivaImagine" /></a>
												</td>
											</tr>
											<tr>
												<td mc:edit="block-35" class="aligncenter" style="font:9px/11px Helvetica, Arial, sans-serif; color:#fff; font-weight: 300;">
													<a style="text-decoration:none; color:#a1a1a1;" href="mailto:customerservice@motivaimplants.com">customerservice@motivaimplants.com</a> <br/>
													<a style="text-decoration:none; color:#a1a1a1; font-weight: 300;" href="http://motivaimplants.com/">www.motivaimplants.com</a> <br/>
												</td>
											</tr>																	
										</table>
										<table class="flexible" width="160" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-36" width="160" class="aligncenter" style="padding:10px 0 5px 0; text-align: right;">
													<a href="https://www.facebook.com/motivaimagine/"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-facebook-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Follow us on Facebook" /></a> <a href="https://twitter.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-twitter-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Pratite nas na Twitter-u" /></a> <a href="https://instagram.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-instagram-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Pratite nas na Instagramu" /></a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" style="padding:2px 10px 7px;">
				<table class="flexible" width="600" cellpadding="0" cellspacing="0" bgcolor="#eeeeee">
					<tr>
						<td class="no-float" align="center">
							<table class="center" cellpadding="0" cellspacing="0">
								<tr>

									<td style="font:normal 9px/11px Helvetica, Arial, sans-serif; color:#888888; text-align: center;">
										Ako ste ovu e-poštu primili pogrešno, ako vaše informacije nisu tačne, ili ako se ne slažete sa Politikom privatnosti i uslovima korišćenja sadržanim na našoj veb lokaciji: <a style="text-decoration:none; color:#555555;" href="http://motivaimplants.com/">www.motivaimplants.com</a>.
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>