<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Welcome to MotivaImagine</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style type="text/css">
		* {
			-webkit-text-size-adjust: none;
			-webkit-text-resize: 100%;
			text-resize: 100%;
		}
		.no-link a{
			text-decoration:none !important;
			color:#fff !important;
		}
		.no-link-2 a{
			text-decoration:none !important;
			color:#fff !important;
		}
		sup{vertical-align: super; line-height: 0; font-size: 0.8em;}
		@media only screen and (max-width:750px) {
			table[class="wrapper"]{min-width:320px !important;}
			table[class="flexible"]{width:100% !important;}
			td[class="img-flex"] img{
				width:100% !important;
				height:auto !important;
			}
			td[class="no-float"]{float:none !important;}
			table[class="center"]{margin:0 auto !important;}
			td[class="header"]{padding:5px 10px 0 !important;}
			td[class="area-1"]{padding:0 !important;}
			td[class="area-2"]{padding:15px 10px !important;}
			td[class="post"]{padding:0 0 20px;}
			td[class="post"] td[class="holder"]{
				padding:0 0 15px !important;
				height:auto !important;
			}
			td[class="aligncenter"],
			td[class="no-link"]{text-align:center !important;}
			td[class="col"],
			td[class="sidebar"]{padding:10px !important;}
			td[class="col"] td[class="holder"],
			td[class="sidebar"] td[class="text"]{padding:10px 0 0 !important;}
			td[class="col"] td[class="holder-2"]{height:auto !important;}
		}
		.tg  {border-collapse:collapse;border-spacing:0;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg .tg-d5wv{background-color:#a38546;color:#ffffff;text-align:center;vertical-align:top}
		.tg .tg-yw4l{vertical-align:top}
	</style>
</head>
<body style="margin:0; padding:0;" bgcolor="#eeeeee" link="#f5f5f5" >
	<table class="wrapper" width="100%" cellspacing="0" cellpadding="0">
		<tr><td height="10" style="font-size:0; line-height:0;" >&nbsp;</td></tr>
		<tr>
			<td align="center" style="padding:10px 5px 26px 5px;">
				<table class="flexible" width="750" cellpadding="0" cellspacing="0">
					<tr>
						<td class="area-1" style="border-radius: 6px 6px 3px 3px" bgcolor="#ffffff">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td mc:edit="block-3" class="img-flex" style="padding:0 0 8px;">
										<img src="http://motivaimplants.com/letters/cover.jpg" style="vertical-align:top; border-radius: 6px 6px 0px 0px" width="750" height="400" alt="Motiva Implants Logo" />
									</td>
								</tr>
								<tr>
									<td style="padding:20px 20px 30px 20px;">
										<table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td class="post">
													<table width="100%" cellpadding="0" cellspacing="0">
														<tr>
															<td class="holder" height="70" valign="top">
																<table width="100%" cellpadding="0" cellspacing="0">
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Dobro došli u MotivaImagine
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Vaš poverljiv partner
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Motiva Always Confident Warranty&reg;
																		</td>
																	</tr>
																	<tr><!-- MAIN CONTENT -->
																		<td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
																			Uvažena gđice. <?=/*php*/$name/*php*/?>
																			<span style="float: right; color:#a1a1a1;"><?=/*php*/date('d-M-Y')/*php*/?></span>
																			<br/>
																			<br/>
																			Čestitamo, uspešno ste registrovali Vaš Motiva implantat!
																			<br>
																			<br>
																			Molimo Vas da pročitate dole predočene informacije i proverite da li su identične onim na Vašem ličnom Motiva identifikacionom kartonu. 
																			<br/>
																			<br/>
																			<!-- Implant Information -->
																			<table width="100%" cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<?php if ( $has_left_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="left">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										Implantat sa leve strane: 
																									</td>
																								</tr>
																								<tr>                                                          
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">Serijski broj: </span><?=/*php*/$left_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Referenca: </span><?=/*php*/$left_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Obim: </span><?=/*php*/$left_vol/*php*/?> cc<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																						<?php if ( $has_right_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="right">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										Implantat sa desne strane: 
																									</td>
																								</tr>
																								<tr>
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">Serijski broj: </span><?=/*php*/$right_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Referenca: </span><?=/*php*/$right_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Obim: </span><?=/*php*/$right_vol/*php*/?> cc<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																					</td>
																				</tr>
																			</table>
																			<br>
																			<br>
																			Informacije koje ste dostavili će biti pohranjene u sistem praćenja “Establishment Labs-a”, što Vam treba uverenje, kao korisniku medicinskog proizvoda kojeg ste zaprimili, da možete biti locirani u slučaju potrebe da Vam trebamo dostaviti određeno obaveštenje.
																			<br>
																			<br>
																			“Establishment Labs” Vas uveravaju da će Vaši lični podaci biti zaštićeni te da će se njima upravljati u skladu sa našom Politikom zaštite privatnosti kao i shodno važećim zakonskim propisima uključujući i direktive 95/46/EC Evropskog parlamenta i Veća Evrope od 24. oktobra 1995. i u skladu sa Zakonom o zaštiti podataka (Velika Britanija).
																			<br>
																			<br>
																			<!-- Only if has implants -->
																			<?php if ( isset($is_q_inside) ? $is_q_inside : false ): ?>
																				Vaši implantati za dojke sadrže Q Inside Safety Technology ™, koja dozvoljava ljekarima da utvrdi ime proizvođača, serijski broj, broj serije, broj serije i druge specifične podatke o implantatu, izvan tijela koristeći vlasnički ručni čitač.
																				<br>
																				<br>
																			<?php endif ?>
																			Mi pridajemo najveći značaj zaštiti privatnosti pacijenta i poverljivosti. Baza podataka u kojoj se čuva proizvod i osobni podaci u vezi sa pacijentom prikupljeni tokom procesa registracije obrađuju se i čuvaju u bezbednim data centrima kojima se upravlja u skladu sa rigoroznim pravilima o zaštiti privatnosti definisanim kroz važeće zakone, prateći iste informacijske protokole koji su trenutno na snazi u bolnicama i hirurškim centrima. Osobne informacije koje su nam ustupljene će biti korištene u skladu sa dogovorom i uz odobrenje pacijenta. Mi nikome ne ustupamo informacije o Vašem emailu ili Vaše lične podatke bez Vaše saglasnosti.  
																			<br>
																			<br>
																			Vezano za “Uvećanje grudi sa Motiva Implant Matrix”: Informacija za pacijenta: molimo Vas kontaktirajte Vašega hirurga odmah ukoliko uočite bilo koji neočekivani simptom za koji mislite da je u vezi sa hirurškim zahvatom uvećanja grudi.
																			<br>
																			<br>
																			Imajte na umu da je Vaš “Motiva Implant Matrix<sup>®</sup>” implantat za grudi pokriven ograničenom garancijom koja obuhvata samo period roka trajanja ugrađenog proizvoda što je period garancije tokom kojeg “Establishment Lab” se obavezuje obezbediti zamenski implantat za bilo koji “Motiva Implant Matrix<sup>®</sup>” proizvod na kojem se desi napuknuće. Pored navedenog, u slučaju da implantat bude zahvaćen kapsularnom kontrakcijom stepena Baker III ili IV “Establishment Lab” obezbeđuje svoj Program politike zamjene. Svi detalji vezano za garanciju opisani su u dokumentu pod nazivom “Uvek pouzdana garancija”, koji Vam može dostaviti naš distributer ili kojeg možete pročitati na <a style="text-decoration:none; color:#a38546;" href="https://motivaimplants.com">www.motivaimplants.com</a>. U izabranom tržištu nudimo naš “<?=/*php*/$years/*php*/?>Y Motiva prošireni garantni program” kako bi smo Vam omogućili finansijsku potporu za ponovne hirurške zahvate u slučaju napuknuća implantata, okretanja implantata i kapsularne kontrakcije Baker stepena III i IV. Ovaj program pokriva maksimalno do <?=/*php*/$years/*php*/?> godine od dana operacije. 
																			<br>
																			<br>
																			<p style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546;">
																				<b>Ukoliko ste zainteresovani za ovaj prošireni program garancije i želite proveriti da li on može biti primenljiv na Vaš implantat to možete učiniti preko</b>
																				<a style="text-decoration:none; color:#888;" href="https://register.motivaimagine.com/">register.motivaimagine.com</a>
																			</p>
																			<br>
																			<p align="center">
																				<a href="https://register.motivaimagine.com/" style="text-decoration:none; background: #a38546; color:#fff; font-weight: 300; padding:8px 14px; border-radius:30px"><b>Prošireni program garancije</b></a>
																			</p>
																			<br>
																			<br>
																			U našem radu koristimo najnovija tehnološka dostignuća kako bi Vam mogli ponuditi najnaprednije implantate dostupne na tržištu imajući u vidu Vašu sigurnost kao primarni zadatak i što je još važnije garanciju za proizvod u skladu sa etičkim standardima. Sa našim “Uvek poverljivom garancijom<sup>®</sup>” i na biranim tržištima sa našim “<?=/*php*/isset($years) ? $years : '{{years}}'/*php*/?>Y prošireni program garancije”, formirali smo sistem podrške koji je bez premca… tu je za Vas kad i ako Vam bude potreban.
																			<br>
																			<br/>
																			<br/>
																			Osećajte se pouzdano… budite Motiva!
																			<br>
																			<br>
																			S uvažavanjem,<br>
																			Juan José Chacón-Quirós<br>
																			CEO<br>
																			Establishment Labs<br>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="area-2" style="padding:15px 20px 5px 21px; background-color:#eeeeee;">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<table class="flexible" width="410" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-33" class="aligncenter">
													<a href="http://motivaimagine.com/"><img src="http://motivaimplants.com/letters/motivaimagine.png" border="0" style="vertical-align:top; padding: 0px 0px 12px 0px" width="210" height="37" alt="MotivaImagine" /></a>
												</td>
											</tr>
											<tr>
												<td mc:edit="block-35" class="aligncenter" style="font:9px/11px Helvetica, Arial, sans-serif; color:#fff; font-weight: 300;">
													<a style="text-decoration:none; color:#a1a1a1;" href="mailto:customerservice@motivaimplants.com">customerservice@motivaimplants.com</a> <br/>
													<a style="text-decoration:none; color:#a1a1a1; font-weight: 300;" href="http://motivaimplants.com/">www.motivaimplants.com</a> <br/>
												</td>
											</tr>                                                                   
										</table>
										<table class="flexible" width="160" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-36" width="160" class="aligncenter" style="padding:10px 0 5px 0; text-align: right;">
													<a href="https://www.facebook.com/motivaimagine/"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-facebook-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Follow us on Facebook" /></a> <a href="https://twitter.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-twitter-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Follow us on Twitter" /></a> <a href="https://instagram.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-instagram-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Instagram Icon" /></a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" style="padding:2px 10px 7px;">
				<table class="flexible" width="600" cellpadding="0" cellspacing="0" bgcolor="#eeeeee">
					<tr>
						<td class="no-float" align="center">
							<table class="center" cellpadding="0" cellspacing="0">
								<tr>
									<td style="font:normal 9px/11px Helvetica, Arial, sans-serif; color:#888888; text-align: center;">
										Ako ste ovu e-poštu primili pogrešno, ako vaše informacije nisu tačne, ili ako se ne slažete sa Politikom privatnosti i uslovima korišćenja sadržanim na našoj veb lokaciji: <a style="text-decoration:none; color:#555555;" href="http://motivaimplants.com/">www.motivaimplants.com</a>.
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>