<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Welcome to MotivaImagine</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style type="text/css">
		* {
			-webkit-text-size-adjust: none;
			-webkit-text-resize: 100%;
			text-resize: 100%;
		}
		.no-link a{
			text-decoration:none !important;
			color:#fff !important;
		}
		.no-link-2 a{
			text-decoration:none !important;
			color:#fff !important;
		}
		sup{vertical-align: super; line-height: 0; font-size: 0.8em;}
		@media only screen and (max-width:750px) {
			table[class="wrapper"]{min-width:320px !important;}
			table[class="flexible"]{width:100% !important;}
			td[class="img-flex"] img{
				width:100% !important;
				height:auto !important;
			}
			td[class="no-float"]{float:none !important;}
			table[class="center"]{margin:0 auto !important;}
			td[class="header"]{padding:5px 10px 0 !important;}
			td[class="area-1"]{padding:0 !important;}
			td[class="area-2"]{padding:15px 10px !important;}
			td[class="post"]{padding:0 0 20px;}
			td[class="post"] td[class="holder"]{
				padding:0 0 15px !important;
				height:auto !important;
			}
			td[class="aligncenter"],
			td[class="no-link"]{text-align:center !important;}
			td[class="col"],
			td[class="sidebar"]{padding:10px !important;}
			td[class="col"] td[class="holder"],
			td[class="sidebar"] td[class="text"]{padding:10px 0 0 !important;}
			td[class="col"] td[class="holder-2"]{height:auto !important;}
		}
		.tg  {border-collapse:collapse;border-spacing:0;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg .tg-d5wv{background-color:#a38546;color:#ffffff;text-align:center;vertical-align:top}
		.tg .tg-yw4l{vertical-align:top}
	</style>
</head>
<body style="margin:0; padding:0;" bgcolor="#eeeeee" link="#f5f5f5" >
	<table class="wrapper" width="100%" cellspacing="0" cellpadding="0">
		<tr><td height="10" style="font-size:0; line-height:0;" >&nbsp;</td></tr>
		<tr>
			<td align="center" style="padding:10px 5px 26px 5px;">
				<table class="flexible" width="750" cellpadding="0" cellspacing="0">
					<tr>
						<td class="area-1" style="border-radius: 6px 6px 3px 3px" bgcolor="#ffffff">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td mc:edit="block-3" class="img-flex" style="padding:0 0 8px;">
										<img src="http://motivaimplants.com/letters/cover.jpg" style="vertical-align:top; border-radius: 6px 6px 0px 0px" width="750" height="400" alt="Motiva Implants Logo" />
									</td>
								</tr>
								<tr>
									<td style="padding:20px 20px 30px 20px;">
										<table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td class="post">
													<table width="100%" cellpadding="0" cellspacing="0">
														<tr>
															<td class="holder" height="70" valign="top">
																<table width="100%" cellpadding="0" cellspacing="0">
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Benvenuti in MotivaImagine
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Il tuo partner fidato
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Motiva Always Confident Warranty&reg;
																		</td>
																	</tr>
																	<tr><!-- MAIN CONTENT -->
																		<td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
																			Gentile. <?=/*php*/$name/*php*/?>
																			<span style="float: right; color:#a1a1a1;"><?=/*php*/date('d-M-Y')/*php*/?></span>
																			<br/>
																			<br/>
																			Congratulazioni, hai registrato con successo le tue protesi Motiva Implants!
																			<br>
																			<br>
																			Leggi con attenzione le informazioni riportate di seguito e assicurati che corrispondano alle informazioni della tua Motiva ID Card. 
																			<br/>
																			<br/>
																			<!-- Implant Information -->
																			<table width="100%" cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<?php if ( $has_left_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="left">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										Protesi lato sinistro                                 
																									</td>
																								</tr>
																								<tr>                                                          
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">Numero di serie:</span><?=/*php*/$left_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Riferimento:</span><?=/*php*/$left_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Volume:</span><?=/*php*/$left_vol/*php*/?> cc<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																						<?php if ( $has_right_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="right">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										Protesi lato destro
																									</td>
																								</tr>
																								<tr>
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">Numero di serie:</span><?=/*php*/$right_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Riferimento:</span><?=/*php*/$right_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Volume:</span><?=/*php*/$right_vol/*php*/?> cc<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																					</td>
																				</tr>
																			</table>
																			<br>
																			<br>
																			Le informazioni che hai fornito verranno inserite nel Sistema di Tracciabilità di Establishment Labs, che ti garantisce, come portatore del dispositivo medico che hai ricevuto, di essere rintracciato per eventuali comunicazioni.
																			<br>
																			<br>
																			 Establishment Labs garantisce che le tue informazioni personali verranno protette e gestite secondo la nostra Informativa sulla Privacy e le normative applicabili, inclusa la Direttiva 95/46/CE del Parlamento europeo e del Consiglio del 24 ottobre 1995, e il Data Protection Act (UK).
																			<br>
																			<br>
																			<!-- Only if has implants -->
																			<?php if ( isset($is_q_inside) ? $is_q_inside : false ): ?>
																				Il tuo protesi mammarie contengono Q Inside Safety Technology<sup>™</sup>, che permette ai medici di accertare il nome del produttore, il numero di serie, numero di lotto, numero di lotto e altri dati specifici di impianto, al di fuori del corpo utilizzando un lettore portatile di proprietà.
																				<br>
																				<br>
																			<?php endif ?>
																			Noi diamo la massima priorità alla privacy e alla riservatezza del paziente. I database dove vengono conservate le informazioni riguardanti i pazienti e i dispositivi medici, raccolte durante il processo di registrazione, si trovano in data center sicuri, che osservano le rigorose linee guida indicate delle normative vigenti, seguendo gli stessi protocolli per la sicurezza delle informazioni attualmente in vigore in ospedali e centri chirurgici. Tutti i dati personali forniti vengono rivelati solo secondo quanto convenuto negli accordi e con l’autorizzazione del paziente. Noi non condividiamo il tuo indirizzo e-mail o le tue informazioni personali senza il tuo consenso.
																			<br>
																			<br>
																			Come indicato nel documento Mastoplastica additiva con Motiva Implant Matrix®: Informazioni per il Paziente, contatta il tuo chirurgo immediatamente nel caso in cui dovessi sperimentare dei sintomi insoliti che potrebbero essere relazionati alla tua operazione di mastoplastica additiva.
																			<br>
																			<br>
																			Ricorda che le tue protesi mammarie Motiva Implant Matrix<sup>®</sup> sono coperte da una garanzia limitata alla durata di vita del dispositivo, la quale prevede che Establishment Labs provveda alla sostituzione di qualsiasi protesi mammaria Motiva Implant Matrix<sup>®</sup> in caso di rottura. Inoltre, nel caso in cui la protesi dovesse sviluppare una contrattura capsulare di Grado III o IV della Classificazione di Baker, Establishment Labs fornisce il Programma Polizza di Sostituzione. La descrizione dettagliata della garanzia è riportata nel documento Garanzia Always Confident Warranty<sup>®</sup>, che viene fornito dai nostri distributori o che può essere consultato su<a style="text-decoration:none; color:#a38546;" href="https://motivaimplants.com">www.motivaimplants.com</a>. 
																			 In mercati scelti offriamo il nostro Programma di Garanzia Estesa Motiva <?=/*php*/$years/*php*/?>Y per fornire assistenza finanziaria per interventi correttivi in caso di rottura della protesi, rotazione della protesi e contrattura capsulare Baker Grado III o IV. Questo programma offre copertura fino a <?=/*php*/$years/*php*/?> anni dalla data dell’intervento. 
																			<br>
																			<br>
																			<p style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546;">
																				<b>Se ti interessa questa garanzia estesa e vorresti sapere se questa sia applicabile alle tue protesi potrai verificarlo su</b>
																				<a style="text-decoration:none; color:#888;" href="https://register.motivaimagine.com/">register.motivaimagine.com</a>
																			</p>
																			<br>
																			<p align="center">
																				<a href="https://register.motivaimagine.com/" style="text-decoration:none; background: #a38546; color:#fff; font-weight: 300; padding:8px 14px; border-radius:30px"><b>Estendi Garanzia</b></a>
																			</p>
																			<br>
																			<br>
																			Noi lavoriamo con le ultime tecnologie per offrirti le protesi più innovative nel mercato, la tua sicurezza è una priorità, e soprattutto, ti offriamo la garanzia di un prodotto etico. Con la nostra Always Confident Warranty<sup>®</sup> e nei mercati scelti con il Programma di Garanzia Estesa Motiva <?=/*php*/isset($years) ? $years : '{{years}}'/*php*/?>Y abbiamo creato un sistema di supporto senza pari... qui per te quando e se ne avrai bisogno.
																			<br/>
																			<br/>
																			Puoi fidarti... Tu hai Motiva!
																			<br>
																			<br>
																			Cordiali saluti,<br>
																			Juan José Chacón-Quirós<br>
																			CEO<br>
																			Establishment Labs<br>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="area-2" style="padding:15px 20px 5px 21px; background-color:#eeeeee;">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<table class="flexible" width="410" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-33" class="aligncenter">
													<a href="http://motivaimagine.com/"><img src="http://motivaimplants.com/letters/motivaimagine.png" border="0" style="vertical-align:top; padding: 0px 0px 12px 0px" width="210" height="37" alt="MotivaImagine" /></a>
												</td>
											</tr>
											<tr>
												<td mc:edit="block-35" class="aligncenter" style="font:9px/11px Helvetica, Arial, sans-serif; color:#fff; font-weight: 300;">
													<a style="text-decoration:none; color:#a1a1a1;" href="mailto:customerservice@motivaimplants.com">customerservice@motivaimplants.com</a> <br/>
													<a style="text-decoration:none; color:#a1a1a1; font-weight: 300;" href="http://motivaimplants.com/">www.motivaimplants.com</a> <br/>
												</td>
											</tr>                                                                   
										</table>
										<table class="flexible" width="160" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-36" width="160" class="aligncenter" style="padding:10px 0 5px 0; text-align: right;">
													<a href="https://www.facebook.com/motivaimagine/"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-facebook-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Seguici su Facebook" /></a> <a href="https://twitter.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-twitter-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Seguici su Twitter" /></a> <a href="https://instagram.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-instagram-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Seguici su Instagram" /></a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" style="padding:2px 10px 7px;">
				<table class="flexible" width="600" cellpadding="0" cellspacing="0" bgcolor="#eeeeee">
					<tr>
						<td class="no-float" align="center">
							<table class="center" cellpadding="0" cellspacing="0">
								<tr>
									<td style="font:normal 9px/11px Helvetica, Arial, sans-serif; color:#888888; text-align: center;">
										Se hai ricevuto questa e-mail per errore, se le informazioni non sono corrette, o se non sei d’accordo con l’Informativa sulla Privacy e i Termini e Condizioni d’uso nel nostro sito web: <a style="text-decoration:none; color:#555555;" href="http://motivaimplants.com/">www.motivaimplants.com</a>.
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>