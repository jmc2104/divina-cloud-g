<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Welcome to MotivaImagine</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style type="text/css">
		* {
			-webkit-text-size-adjust: none;
			-webkit-text-resize: 100%;
			text-resize: 100%;
		}
		.no-link a{
			text-decoration:none !important;
			color:#fff !important;
		}
		.no-link-2 a{
			text-decoration:none !important;
			color:#fff !important;
		}
		sup{vertical-align: super; line-height: 0; font-size: 0.8em;}
		@media only screen and (max-width:750px) {
			table[class="wrapper"]{min-width:320px !important;}
			table[class="flexible"]{width:100% !important;}
			td[class="img-flex"] img{
				width:100% !important;
				height:auto !important;
			}
			td[class="no-float"]{float:none !important;}
			table[class="center"]{margin:0 auto !important;}
			td[class="header"]{padding:5px 10px 0 !important;}
			td[class="area-1"]{padding:0 !important;}
			td[class="area-2"]{padding:15px 10px !important;}
			td[class="post"]{padding:0 0 20px;}
			td[class="post"] td[class="holder"]{
				padding:0 0 15px !important;
				height:auto !important;
			}
			td[class="aligncenter"],
			td[class="no-link"]{text-align:center !important;}
			td[class="col"],
			td[class="sidebar"]{padding:10px !important;}
			td[class="col"] td[class="holder"],
			td[class="sidebar"] td[class="text"]{padding:10px 0 0 !important;}
			td[class="col"] td[class="holder-2"]{height:auto !important;}
		}
		.tg  {border-collapse:collapse;border-spacing:0;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg .tg-d5wv{background-color:#a38546;color:#ffffff;text-align:center;vertical-align:top}
		.tg .tg-yw4l{vertical-align:top}
	</style>
</head>
<body style="margin:0; padding:0;" bgcolor="#eeeeee" link="#f5f5f5" >
	<table class="wrapper" width="100%" cellspacing="0" cellpadding="0">
		<tr><td height="10" style="font-size:0; line-height:0;" >&nbsp;</td></tr>
		<tr>
			<td align="center" style="padding:10px 5px 26px 5px;">
				<table class="flexible" width="750" cellpadding="0" cellspacing="0">
					<tr>
						<td class="area-1" style="border-radius: 6px 6px 3px 3px" bgcolor="#ffffff">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td mc:edit="block-3" class="img-flex" style="padding:0 0 8px;">
										<img src="http://motivaimplants.com/letters/cover.jpg" style="vertical-align:top; border-radius: 6px 6px 0px 0px" width="750" height="400" alt="Motiva Implants Logo" />
									</td>
								</tr>
								<tr>
									<td style="padding:20px 20px 30px 20px;">
										<table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td class="post">
													<table width="100%" cellpadding="0" cellspacing="0">
														<tr>
															<td class="holder" height="70" valign="top">
																<table width="100%" cellpadding="0" cellspacing="0">
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Tervetuloa MotivaImagine
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Luotettu kumppanisi
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Motiva Always Confident Warranty&reg;
																		</td>
																	</tr>
																	<tr><!-- MAIN CONTENT -->
																		<td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
																			Hyvä Rva. <?=/*php*/$name/*php*/?>
																			<span style="float: right; color:#a1a1a1;"><?=/*php*/date('d-M-Y')/*php*/?></span>
																			<br/>
																			<br/>
																			Onnittelut, olet rekisteröinyt Motiva-implanttisi! 
																			<br>
																			<br>
																			 Ole hyvä ja lue jäljempänä olevat tiedot ja varmista, että ne vastaavat henkilökohtaisen Motiva-henkilökorttisi tietoja.
																			<br/>
																			<br/>
																			<!-- Implant Information -->
																			<table width="100%" cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<?php if ( $has_left_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="left">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										Vasemmanpuoleinen implantti 
																									</td>
																								</tr>
																								<tr>                                                          
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">Sarjanumero: </span><?=/*php*/$left_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Viite:</span><?=/*php*/$left_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Tilavuus: </span><?=/*php*/$left_vol/*php*/?> cc<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																						<?php if ( $has_right_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="right">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										Oikeanpuoleinen implantti
																									</td>
																								</tr>
																								<tr>
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">Sarjanumero:</span><?=/*php*/$right_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Viite:</span><?=/*php*/$right_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Tilavuus:</span><?=/*php*/$right_vol/*php*/?> cc<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																					</td>
																				</tr>
																			</table>
																			<br>
																			<br>
																			Antamasi tiedot syötetään Establishment Labs -jäljitettävyysjärjestelmään, jossa vakuutetaan sinulle, vastaanottamiesi lääketieteellisten laitteiden kantajana kehossasi, että sinut voi paikantaa ilmoitusasioissa. 
																			<br>
																			<br>
																			Establishment Labs vakuuttaa, että henkilötietosi suojataan ja käsitellään tietosuojakäytännön ja soveltuvien säännösten mukaan, mukaan lukien Euroopan parlamentin ja neuvoston 24.10.1995 annettu Euroopan parlamentin ja neuvoston direktiivi  95/46 / Euroopan Unioni ja tietosuojalaki (Yhdistynyt kuningaskunta). 
																			<br>
																			<br>
																			<!-- Only if has implants -->
																			<?php if ( isset($is_q_inside) ? $is_q_inside : false ): ?>
																				Rintaimplantit sisältävät Q Inside Safety Technology ™ -ratkaisua, jonka avulla lääkärit voivat tunnistaa valmistajan nimen, sarjanumeron, eränumeron, eränumeron ja muut implanttitiedot erillisen käsin lukijan avulla.
																				<br>
																				<br>
																			<?php endif ?>
																			Pidämme erittäin tärkeänä potilaan yksityisyyttä ja luottamuksellisuutta. Tietokannat, jotka tallentavat rekisteröintiprosessista kerätyt laite- ja potilastiedot, säilytetään suojatuissa tietokeskuksissa, joissa noudatetaan voimassaolevan lain mukaisia tiukkoja ohjeita tietosuojasta sekä samoja tietoturvaprotokollia, joita tällä hetkellä käytetään sairaaloissa ja kirurgisissa keskuksissa. Kaikki henkilökohtaiset tiedot toimitetaan vain sovitulla tavalla ja potilaan valtuutuksella. Emme jaa sähköpostiosoitettasi tai henkilökohtaisia tietoja muiden kanssa ilman suostumustasi. 
																			<br>
																			<br>
																			Noudatamme tuotettamme "Rintojen suurentaminen Motiva-Implanti Matrix<sup>®</sup>illa: Potilaalle annettavaa tietoa: Ota välittömästi yhteyttä kirurgiisi, suurentamiseen liittyvinä.
																			<br>
																			<br>
																			Muista, että Motiva Implant Matrix® rintaimplantteihin kuuluvat rajoitettu laitteen käyttöiän käsittävä takuu, jonka mukaan Establishment Labs toimittaa korvaavan implantin mitä tahansa repeämästä kärsivää Motiva Implant Matrix<sup>®</sup> rintaimplanttia varten. Lisäksi, jos implanttiin vaikuttaa Bakeri-astee III tai IV kapsulaarinen kontraktio, Establishment Labs tarjoaa vaihto-ohjelmaansa. Takuun täydelliset yksityiskohdat on kuvattu asiakirjassa Always Confident Warranty<sup>®</sup>, jonka jakelijamme voivat antaa tai jonka voi myös läpikäydä osoitteessa <a style="text-decoration:none; color:#a38546;" href="https://motivaimplants.com">www.motivaimplants.com</a>. 
																			<br>
																			Valikoiduilla markkinoilla tarjoamme <?=/*php*/$years/*php*/?>Y Motivan laajennettua takuuohjelmaa, Motiva Extended Warranty Program, joka tarjoaa rahoitustukea korjaaviin leikkauksiin implanttien repeämien, implanttien kiertoliikken ja kapsulaarisen kontraktion Baker-asteiden III tai IV osalta. Tämä ohjelma kattaa enintään kaksi vuotta leikkauspäivämäärästä.
																			<br>
																			<br>
																			<p style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546;">
																				<b>Jos olet kiinnostunut tästä laajennetusta takuusta, ja haluat tarkistaa, koskeeko se implanttejasi, voit tehdä sen kohdassa</b>
																				<a style="text-decoration:none; color:#888;" href="https://register.motivaimagine.com/">register.motivaimagine.com</a>
																			</p>
																			<br>
																			<p align="center">
																				<a href="https://register.motivaimagine.com/" style="text-decoration:none; background: #a38546; color:#fff; font-weight: 300; padding:8px 14px; border-radius:30px"><b>Laajenna Takuu</b></a>
																			</p>
																			<br>
																			<br>
																			Käytämme uusimpia tekniikoita, jotka tarjoavat sinulle markkinoiden innovatiivisimmat implantit, joissa turvallisuutesi on etusijalla ja, mikä tärkeintä, se on eettinen tuotetakuu. Aina luottamuksellisella takuulla® ja valituilla markkinoilla <?=/*php*/isset($years) ? $years : '{{years}}'/*php*/?>Y Motiva Extended Warranty Program -ohjelman kanssa olemme luoneet vertaansa vailla olevan tukijärjestelmän... sinua varten, ja jos tarvitset sitä. 
																			<br>
																			<br/>
																			<br/>
																			Ole luottavainen... Sinulla on Motiva!
																			<br>
																			<br>
																			Ystävällisin terveisin,<br>
																			Juan José Chacón-Quirós<br>
																			Toimitusjohtaja<br>
																			Establishment Labs<br>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="area-2" style="padding:15px 20px 5px 21px; background-color:#eeeeee;">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<table class="flexible" width="410" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-33" class="aligncenter">
													<a href="http://motivaimagine.com/"><img src="http://motivaimplants.com/letters/motivaimagine.png" border="0" style="vertical-align:top; padding: 0px 0px 12px 0px" width="210" height="37" alt="MotivaImagine" /></a>
												</td>
											</tr>
											<tr>
												<td mc:edit="block-35" class="aligncenter" style="font:9px/11px Helvetica, Arial, sans-serif; color:#fff; font-weight: 300;">
													<a style="text-decoration:none; color:#a1a1a1;" href="mailto:customerservice@motivaimplants.com">customerservice@motivaimplants.com</a> <br/>
													<a style="text-decoration:none; color:#a1a1a1; font-weight: 300;" href="http://motivaimplants.com/">www.motivaimplants.com</a> <br/>
												</td>
											</tr>                                                                   
										</table>
										<table class="flexible" width="160" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-36" width="160" class="aligncenter" style="padding:10px 0 5px 0; text-align: right;">
													<a href="https://www.facebook.com/motivaimagine/"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-facebook-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Seuraa meitä Facebookissa" /></a> <a href="https://twitter.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-twitter-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Seuraa meitä Twitterissä" /></a> <a href="https://instagram.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-instagram-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Seuraa meitä Instagramissa" /></a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" style="padding:2px 10px 7px;">
				<table class="flexible" width="600" cellpadding="0" cellspacing="0" bgcolor="#eeeeee">
					<tr>
						<td class="no-float" align="center">
							<table class="center" cellpadding="0" cellspacing="0">
								<tr>
									<td style="font:normal 9px/11px Helvetica, Arial, sans-serif; color:#888888; text-align: center;">
										Jos sait tämän sähköpostiviestin erehdyksessä, jos tietosi eivät ole oikein tai jos et ole samaa mieltä verkkosivustomme tietosuojakäytännöistä ja käyttöehdoista, käy sivulla: <a style="text-decoration:none; color:#555555;" href="http://motivaimplants.com/">www.motivaimplants.com</a>.
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>