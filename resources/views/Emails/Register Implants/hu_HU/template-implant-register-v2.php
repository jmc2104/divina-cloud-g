<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Welcome to MotivaImagine</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style type="text/css">
		* {
			-webkit-text-size-adjust: none;
			-webkit-text-resize: 100%;
			text-resize: 100%;
		}
		.no-link a{
			text-decoration:none !important;
			color:#fff !important;
		}
		.no-link-2 a{
			text-decoration:none !important;
			color:#fff !important;
		}
		sup{vertical-align: super; line-height: 0; font-size: 0.8em;}
		@media only screen and (max-width:750px) {
			table[class="wrapper"]{min-width:320px !important;}
			table[class="flexible"]{width:100% !important;}
			td[class="img-flex"] img{
				width:100% !important;
				height:auto !important;
			}
			td[class="no-float"]{float:none !important;}
			table[class="center"]{margin:0 auto !important;}
			td[class="header"]{padding:5px 10px 0 !important;}
			td[class="area-1"]{padding:0 !important;}
			td[class="area-2"]{padding:15px 10px !important;}
			td[class="post"]{padding:0 0 20px;}
			td[class="post"] td[class="holder"]{
				padding:0 0 15px !important;
				height:auto !important;
			}
			td[class="aligncenter"],
			td[class="no-link"]{text-align:center !important;}
			td[class="col"],
			td[class="sidebar"]{padding:10px !important;}
			td[class="col"] td[class="holder"],
			td[class="sidebar"] td[class="text"]{padding:10px 0 0 !important;}
			td[class="col"] td[class="holder-2"]{height:auto !important;}
		}
		.tg  {border-collapse:collapse;border-spacing:0;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg .tg-d5wv{background-color:#a38546;color:#ffffff;text-align:center;vertical-align:top}
		.tg .tg-yw4l{vertical-align:top}
	</style>
</head>
<body style="margin:0; padding:0;" bgcolor="#eeeeee" link="#f5f5f5" >
	<table class="wrapper" width="100%" cellspacing="0" cellpadding="0">
		<tr><td height="10" style="font-size:0; line-height:0;" >&nbsp;</td></tr>
		<tr>
			<td align="center" style="padding:10px 5px 26px 5px;">
				<table class="flexible" width="750" cellpadding="0" cellspacing="0">
					<tr>
						<td class="area-1" style="border-radius: 6px 6px 3px 3px" bgcolor="#ffffff">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td mc:edit="block-3" class="img-flex" style="padding:0 0 8px;">
										<img src="http://motivaimplants.com/letters/cover.jpg" style="vertical-align:top; border-radius: 6px 6px 0px 0px" width="750" height="400" alt="Motiva Implants Logo" />
									</td>
								</tr>
								<tr>
									<td style="padding:20px 20px 30px 20px;">
										<table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td class="post">
													<table width="100%" cellpadding="0" cellspacing="0">
														<tr>
															<td class="holder" height="70" valign="top">
																<table width="100%" cellpadding="0" cellspacing="0">
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Üdvözöljük a MotivaImagine
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Az Ön Megbízható Partnere
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Motiva Always Confident Warranty&reg; (Motiva Mindig Magabiztos Garancia)
																		</td>
																	</tr>
																	<tr><!-- MAIN CONTENT -->
																		<td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
																			Kedves. <?=/*php*/$name/*php*/?>
																			<span style="float: right; color:#a1a1a1;"><?=/*php*/date('d-M-Y')/*php*/?></span>
																			<br/>
																			<br/>
																			Gratulálunk, hogy sikeresen regisztrálta a Motiva implantátumait!
																			<br>
																			<br>
																			Kérjük, olvassa el az alábbi információkat, és ellenőrizze, hogy megegyeznek a személyes Motiva kártyáján feltüntetett információkkal.
																			<br/>
																			<br/>
																			<!-- Implant Information -->
																			<table width="100%" cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<?php if ( $has_left_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="left">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										Bal Oldali Implantátum
																									</td>
																								</tr>
																								<tr>                                                          
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">Sorszám:</span><?=/*php*/$left_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Reference:</span><?=/*php*/$left_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Űrtartalom:</span><?=/*php*/$left_vol/*php*/?> cc<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																						<?php if ( $has_right_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="right">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										Jobb Oldali Implantátum
																									</td>
																								</tr>
																								<tr>
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">Sorszám:</span><?=/*php*/$right_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Referencia:</span><?=/*php*/$right_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Űrtartalom:</span><?=/*php*/$right_vol/*php*/?> cc<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																					</td>
																				</tr>
																			</table>
																			<br>
																			<br>
																			Az Ön által megadott információkat bejegyezzük az Establishment Labs Nyomon Követhetőségi Rendszerébe, amely biztosítja Önnek, mint az átvett orvosi eszköz(ök) hordozójának, hogy megtaláljuk Önt egy szükséges értesítés esetén. 
																			<br>
																			<br>
																			Establishment Labs biztosítja a személyes adatok védelmét és kezelését az Adatvédelmi Irányelveink és további alkalmazandó szabályok, mint például az 95/46/EK európai parlamenti és a Tanács 1995. október 24-i irányelv és a Adatvédelmi törvény(UK) alapján.
																			<br>
																			<br>
																			<!-- Only if has implants -->
																			<?php if ( isset($is_q_inside) ? $is_q_inside : false ): ?>
																				Az emlőimplantátumok Q Inside Safety Technology™ -ot tartalmaznak, amely lehetővé teszi az orvosok számára, hogy egy saját kéziszámolót használva megállapítsák a gyártó nevét, sorozatszámát, tételszámát, tételszámát és egyéb implantátumadatokat. 
																				<br>
																				<br>
																			<?php endif ?>
																			Kiemelten fontos számunkra a beteg adatvédelme és titoktartása. Az eszközről és a betegről a regisztrációs folyamat alatt gyűjtött adatokat tartalmazó adatbázisokat olyan biztonságos adatközpontokban tároljuk, amelyek betartják az alkalmazandó jogszabályok szerinti szigorú adatvédelmi irányelveket, ugyanolyan biztonsági eljárásokat követve, mint amit jelenleg kórházak és sebészeti központok alkalmaznak. Minden szolgáltatott személyes adat csak a beteg engedélyével tehető közzé. Az Ön beleegyezése nélkül nem osztjuk meg e-mail címét és személyes információit. 
																			<br>
																			<br>
																			A  "Mellnagyobbítás Motiva Implant Matrix<sup>®</sup> mellimplantátummal" előírásai szerint: Információ a Beteg számára, kérjük, azonnal forduljon a sebészéhez, ha bármilyen olyan váratlan tüneteket tapasztal, ami kapcsolatos lehet a mellnagyobbítás műtéti eljárásával. 
																			<br>
																			<br>
																			Ne feledje, hogy a Motiva Implant Matrix® mellimplantátumait fedezi egy korlátozott, az eszköz élettartamára érvényes garancia, amely gondoskodik arról, hogy Establishment Labs pótlólagos implantátumot biztosít bármely elszakadt Motiva Implant Matrix® mellimplantátumhoz. Továbbá a III. vagy IV. fokozatú Baker kapszuláris kontraktúra által érintett implantátum esetén Establishment Labs szolgáltatja Termékcsere Garancia Programját. A garancia teljes részletei az Always Confident Warranty® (Mindig Magabiztos Garancia) dokumentumban találhatók, amelyet megkaphat forgalmazónktól, vagy áttekintheti a <a style="text-decoration:none; color:#a38546;" href="https://motivaimplants.com">www.motivaimplants.com</a> weboldalon. 
																			Egyes piacokon kínáljuk a <?=/*php*/$years/*php*/?>Y Motiva Meghosszabított Garancia Programot, annak érdekében hogy anyagi támogatást nyújtsunk a módosító műtétekhez implantátum szakadás, implantátum elfordulás és III. vagy IV. fokozatú Baker kapszuláris kontraktúra esetén. Ez a program fedezetet nyújt a műtét időpontjától számított <?=/*php*/$years/*php*/?> évig.
																			<br>
																			<br>
																			<p style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546;">
																				<b>Ha érdekli ez a kiterjesztett garancia, és szeretné ellenőrizni, hogy az ön implantátumaira vonatkozik-e, akkor ezt itt teheti meg</b>
																				<a style="text-decoration:none; color:#888;" href="https://register.motivaimagine.com/">register.motivaimagine.com</a>
																			</p>
																			<br>
																			<p align="center">
																				<a href="https://register.motivaimagine.com/" style="text-decoration:none; background: #a38546; color:#fff; font-weight: 300; padding:8px 14px; border-radius:30px"><b>Garancia Meghosszabbítása</b></a>
																			</p>
																			<br>
																			<br>
																			A legújabb technológiát alkalmazva ajánljuk Önnek a rendelkezésre álló leginnovatívabb implantátumokat az Ön biztonságának szemben tartásával és ami a legfontosabb, egy etikai termékgaranciával. Az Always Confident Warranty<sup>®</sup> (Mindig Magabiztos Garancia) és egyes piacokon a <?=/*php*/isset($years) ? $years : '{{years}}'/*php*/?>Y Motiva Meghosszabított Garancia Programjainkkal létrehoztunk egy egyedülálló támogatási rendszert... ez rendelkezésére áll, ha szüksége van rá. 
																			<br>
																			<br/>
																			<br/>
																			Legyen Magabiztos... Motiva-ja van!
																			<br>
																			<br>
																			Üdvözlettel,<br>
																			Juan José Chacón-Quirós<br>
																			VEZÉRIGAZGATÓ<br>
																			Establishment Labs<br>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="area-2" style="padding:15px 20px 5px 21px; background-color:#eeeeee;">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<table class="flexible" width="410" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-33" class="aligncenter">
													<a href="http://motivaimagine.com/"><img src="http://motivaimplants.com/letters/motivaimagine.png" border="0" style="vertical-align:top; padding: 0px 0px 12px 0px" width="210" height="37" alt="MotivaImagine" /></a>
												</td>
											</tr>
											<tr>
												<td mc:edit="block-35" class="aligncenter" style="font:9px/11px Helvetica, Arial, sans-serif; color:#fff; font-weight: 300;">
													<a style="text-decoration:none; color:#a1a1a1;" href="mailto:customerservice@motivaimplants.com">customerservice@motivaimplants.com</a> <br/>
													<a style="text-decoration:none; color:#a1a1a1; font-weight: 300;" href="http://motivaimplants.com/">www.motivaimplants.com</a> <br/>
												</td>
											</tr>                                                                   
										</table>
										<table class="flexible" width="160" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-36" width="160" class="aligncenter" style="padding:10px 0 5px 0; text-align: right;">
													<a href="https://www.facebook.com/motivaimagine/"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-facebook-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Kövessen minket a Facebook-on" /></a> <a href="https://twitter.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-twitter-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="	Kövessen minket a Twitter-en" /></a> <a href="https://instagram.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-instagram-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Kövessen minket az Instagram-on" /></a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" style="padding:2px 10px 7px;">
				<table class="flexible" width="600" cellpadding="0" cellspacing="0" bgcolor="#eeeeee">
					<tr>
						<td class="no-float" align="center">
							<table class="center" cellpadding="0" cellspacing="0">
								<tr>
									<td style="font:normal 9px/11px Helvetica, Arial, sans-serif; color:#888888; text-align: center;">
										Ha ezt az e-mail üzenetet tévesen kapta, vagy ha az ön adatai helytelenek, vagy ha nem ért egyet a weboldalunkon található Adatvédelmi Garanciával és Általános Szerződési Feltételekkel: <a style="text-decoration:none; color:#555555;" href="http://motivaimplants.com/">www.motivaimplants.com</a>.
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>