<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Welcome to MotivaImagine</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style type="text/css">
		* {
			-webkit-text-size-adjust: none;
			-webkit-text-resize: 100%;
			text-resize: 100%;
		}
		.no-link a{
			text-decoration:none !important;
			color:#fff !important;
		}
		.no-link-2 a{
			text-decoration:none !important;
			color:#fff !important;
		}
		sup{vertical-align: super; line-height: 0; font-size: 0.8em;}
		@media only screen and (max-width:750px) {
			table[class="wrapper"]{min-width:320px !important;}
			table[class="flexible"]{width:100% !important;}
			td[class="img-flex"] img{
				width:100% !important;
				height:auto !important;
			}
			td[class="no-float"]{float:none !important;}
			table[class="center"]{margin:0 auto !important;}
			td[class="header"]{padding:5px 10px 0 !important;}
			td[class="area-1"]{padding:0 !important;}
			td[class="area-2"]{padding:15px 10px !important;}
			td[class="post"]{padding:0 0 20px;}
			td[class="post"] td[class="holder"]{
				padding:0 0 15px !important;
				height:auto !important;
			}
			td[class="aligncenter"],
			td[class="no-link"]{text-align:center !important;}
			td[class="col"],
			td[class="sidebar"]{padding:10px !important;}
			td[class="col"] td[class="holder"],
			td[class="sidebar"] td[class="text"]{padding:10px 0 0 !important;}
			td[class="col"] td[class="holder-2"]{height:auto !important;}
		}
		.tg  {border-collapse:collapse;border-spacing:0;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg .tg-d5wv{background-color:#a38546;color:#ffffff;text-align:center;vertical-align:top}
		.tg .tg-yw4l{vertical-align:top}
	</style>
</head>
<body style="margin:0; padding:0;" bgcolor="#eeeeee" link="#f5f5f5" >
	<table class="wrapper" width="100%" cellspacing="0" cellpadding="0">
		<tr><td height="10" style="font-size:0; line-height:0;" >&nbsp;</td></tr>
		<tr>
			<td align="center" style="padding:10px 5px 26px 5px;">
				<table class="flexible" width="750" cellpadding="0" cellspacing="0">
					<tr>
						<td class="area-1" style="border-radius: 6px 6px 3px 3px" bgcolor="#ffffff">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td mc:edit="block-3" class="img-flex" style="padding:0 0 8px;">
										<img src="http://motivaimplants.com/letters/cover.jpg" style="vertical-align:top; border-radius: 6px 6px 0px 0px" width="750" height="400" alt="Motiva Implants Logo" />
									</td>
								</tr>
								<tr>
									<td style="padding:20px 20px 30px 20px;">
										<table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td class="post">
													<table width="100%" cellpadding="0" cellspacing="0">
														<tr>
															<td class="holder" height="70" valign="top">
																<table width="100%" cellpadding="0" cellspacing="0">
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Καλώς ήλθατε στο MotivaImagine
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Έμπιστος συνεργάτης σας
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Motiva Always Confident Warranty&reg;
																		</td>
																	</tr>
																	<tr><!-- MAIN CONTENT -->
																		<td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
																			Αγαπητή κυρία. <?=/*php*/$name/*php*/?>
																			<span style="float: right; color:#a1a1a1;"><?=/*php*/date('d-M-Y')/*php*/?></span>
																			<br/>
																			<br/>
																			Συγχαρητήρια, έχετε καταχωρήσει επιτυχώς τα εμφυτεύματα Motiva σας!
																			<br>
																			<br>
																			Σας παρακαλούμε να επιβεβαιώσετε αν οι πληροφορίες που αναγράφονται παρακάτω αντιστοιχούν στις πληροφορίες που αναγράφονται στην προσωπική κάρτα σας απο τη Motiva.
																			<br/>
																			<br/>
																			<!-- Implant Information -->
																			<table width="100%" cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<?php if ( $has_left_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="left">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										Αριστερό εμφύτευμα                          
																									</td>
																								</tr>
																								<tr>                                                          
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">Σειριακός Αριθμός:</span><?=/*php*/$left_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Κωδικός αναφοράς:</span><?=/*php*/$left_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Όγκος:</span><?=/*php*/$left_vol/*php*/?> cc<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																						<?php if ( $has_right_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="right">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										Δεξί εμφύτευμα
																									</td>
																								</tr>
																								<tr>
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">Σειριακός Αριθμός:</span><?=/*php*/$right_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Κωδικός αναφοράς:</span><?=/*php*/$right_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Όγκος:</span><?=/*php*/$right_vol/*php*/?> cc<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																					</td>
																				</tr>
																			</table>
																			<br>
																			<br>
																			Οι πληροφορίες που μας έχετε παραδώσει θα καταχωρηθούν στο σύστημα ανίχνευσης της Establishment Labs, το οποίο σας επιβεβαιώνει ως τον κάτοχο των ιατρικών εξαρτημάτων που έχετε λάβει και με αυτό τον τρόπο μπορούμε να σας εντοπίσουμε σε περίπτωση ειδοποίησης.
																			<br>
																			<br>
																			Η Establishment Labs σας επιβεβαιώνει οτι οι προσωπικές σας πληροφορίες είναι ασφαλείς και χρησιμοποιούνται σύμφωνα με την Πολιτική Aπορρήτου Προσωπικών Δεδομένων και των κανονισμών σε ισχύ, συμπεριλαμβανομένου του κανονισμού 95/46/EC απο το Ευρωπαϊκό Κοινοβούλιο και το Συμβούλιο της 24ης Οκτωβρίου 1995, καθώς επίσης και τo Θεσμό Δεδομένων (Ηνωμένο Βασίλειο).
																			<br>
																			<br>
																			<!-- Only if has implants -->
																			<?php if ( isset($is_q_inside) ? $is_q_inside : false ): ?>
																				Your breast implants contain Q Inside Safety Technology™, which allows physicians to ascertain the manufacturer name, serial number, lot number, batch number, and other implant specific data, from outside the body using a proprietary handheld reader. 
																				<br>
																				<br>
																			<?php endif ?>
																			Δίνουμε μέγιστη σημασία στο απόρρητο και την εμπιστευτικότητα των ασθενών μας. Οι βάσεις δεδομένων μας που περιέχουν τα στοιχεία του ασθενούς και των εξαρτημάτων, τα οποία συλλέγονται μέσω της διαδικασίας καταχώρησης, φυλάσσονται σε ασφαλή κέντρα δεδομένων. Ακολουθούμε τις ίδιες αυστηρές οδηγίες για την προστασία δεδομένων με νοσοκομεία και χειρουργικά κέντρα όπως ορίζονται απο την ισχύουσα νομοθεσία. Όλα τα προσωπικά δεδομένα που μας παρέχετε χρησιμοποιούνται σύμφωνα με τη συγκατάθεση του ασθενή και όπως έχει συμφωνηθεί. Δεν αποκαλύπτουμε το email σας ή τα προσωπικά σας δεδομένα χωρίς τη συγκατάθεσή σας.
																			<br>
																			<br>
																			Σύμφωνα με το άρθρο “Μεγέθυνση Μαστού με εμφυτεύματα Motiva®: Πληροφορίες για τον ασθενή”, σας παρακαλούμε να επικοινωνήσετε αμέσως με το χειρουργό σας αν παρατηρήσετε οποιοδήποτε σύμπτωμα που θεωρείτε οτι σχετίζεται με τη διαδικασία μεγέθυνσης μαστού στην οποία υποβληθήκατε.
																			<br>
																			<br>
																			Θα θέλαμε να σας υπενθυμίσουμε οτι τα εμφυτεύματα σας καλύπτονται για όλη τη διάρκεια της ύπαρξης του προϊόντος απο μία περιορισμένη εγγύηση που σας εξασφαλίζει την αντικατάσταση οποιουδήποτε εμφυτεύματος στήθους σε περίπτωση ρήξης. Εκτός από αυτό, σε περίπτωση που παρατηρήσετε συρρίκνωση της ινώδους κάψας σε βαθμό Baker III ή IV σε ένα από τα εμφυτεύματα σας, η Establishment Labs σας παρέχει ένα πρόγραμμα αντικατάστασης. Μπορείτε να βρείτε τη λεπτομερή εγγύηση στο αρχείο Εγγύηση “Πάντα Ασφαλής®” ,που παρέχεται απο τον αντιπρόσωπο μας ή μπορείτε να τη βρείτε στο site  <a style="text-decoration:none; color:#a38546;" href="https://motivaimplants.com">www.motivaimplants.com</a>.
																			Σε ορισμένες περιπτώσεις σας προτείνουμε το διετές πρόγραμμα επέκτασης εγγύησης της Motiva για να σας προσφέρουμε οικονομική υποστήριξη για επαναληπτικες εγχειρήσεις σε περιπτώσεις ρήξης του εμφυτεύματος, ξεφουσκώματος ή συρρίκνωσης της ινώδους κάψας σε βαθμο III ή IV.  Αυτό το πρόγραμμα σας καλύπτει μέχρι και για <?=/*php*/$years/*php*/?> χρόνια μετά την ημερομηνία της εγχείρησης.
																			<br>
																			<br>
																			<p style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546;">
																				<b>Αν ενδιαφέρεστε για την επέκταση της εγγύησης και θα θέλατε να επιβεβαιώσετε αν είναι διαθέσιμη για τα δικά σας εμφυτεύματα, μπορείτε να το κάνετε εδώ</b>
																				<a style="text-decoration:none; color:#888;" href="https://register.motivaimagine.com/">register.motivaimagine.com</a>
																			</p>
																			<br>
																			<p align="center">
																				<a href="https://register.motivaimagine.com/" style="text-decoration:none; background: #a38546; color:#fff; font-weight: 300; padding:8px 14px; border-radius:30px"><b>Extend Warranty</b></a>
																			</p>
																			<br>
																			<br>
																			Χρησιμοποιούμε την τελευταία τεχνολογία για να σας προσφέρουμε τα πιο καινοτόμα εμφυτεύματα της αγοράς έχοντας πάντα ως προτεραιότητα μας την ασφάλεια σας και μια ηθική εγγύηση προιόντος. Με την εγγύηση “Πάντα Ασφαλής® “ και σε ορισμένες περιπτώσεις με το <?=/*php*/isset($years) ? $years : '{{years}}'/*php*/?>χρόνια πρόγραμμα επέκτασης εγγύησης, έχουμε δημιουργήσει ένα απαράμιλλο σύστημα υποστήριξης... Είναι πάντα στη διάθεση σας όποτε και αν το χρειαστείτε.
																			<br>
																			<br/>
																			<br/>
																			Να νιώθετε ασφαλής... Έχετε Motiva!
																			<br>
																			<br>
																			Με εκτίμηση,<br>
																			Juan José Chacón-Quirós<br>
																			Με εκτίμηση,<br>
																			Establishment Labs<br>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="area-2" style="padding:15px 20px 5px 21px; background-color:#eeeeee;">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<table class="flexible" width="410" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-33" class="aligncenter">
													<a href="http://motivaimagine.com/"><img src="http://motivaimplants.com/letters/motivaimagine.png" border="0" style="vertical-align:top; padding: 0px 0px 12px 0px" width="210" height="37" alt="MotivaImagine" /></a>
												</td>
											</tr>
											<tr>
												<td mc:edit="block-35" class="aligncenter" style="font:9px/11px Helvetica, Arial, sans-serif; color:#fff; font-weight: 300;">
													<a style="text-decoration:none; color:#a1a1a1;" href="mailto:customerservice@motivaimplants.com">customerservice@motivaimplants.com</a> <br/>
													<a style="text-decoration:none; color:#a1a1a1; font-weight: 300;" href="http://motivaimplants.com/">www.motivaimplants.com</a> <br/>
												</td>
											</tr>                                                                   
										</table>
										<table class="flexible" width="160" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-36" width="160" class="aligncenter" style="padding:10px 0 5px 0; text-align: right;">
													<a href="https://www.facebook.com/motivaimagine/"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-facebook-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Ακολουθήστε μας στο Facebook" /></a> <a href="https://twitter.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-twitter-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Ακολουθήστε μας στο Twitter" /></a> <a href="https://instagram.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-instagram-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Ακολουθήστε μας στο Instagram" /></a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" style="padding:2px 10px 7px;">
				<table class="flexible" width="600" cellpadding="0" cellspacing="0" bgcolor="#eeeeee">
					<tr>
						<td class="no-float" align="center">
							<table class="center" cellpadding="0" cellspacing="0">
								<tr>
									<td style="font:normal 9px/11px Helvetica, Arial, sans-serif; color:#888888; text-align: center;">
										Αν λάβατε αυτό το email κατά λάθος, εάν οι πληροφορίες σας δεν είναι σωστές ή αν δεν συμφωνείτε με την Πολιτική Απορρήτου και τους Όρους  Χρήση που περιέχονται στον ιστότοπό μας: <a style="text-decoration:none; color:#555555;" href="http://motivaimplants.com/">www.motivaimplants.com</a>.
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>