﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <title>Motiva Implants Matrix</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <style type="text/css">
         * {
         -webkit-text-size-adjust: none;
         -webkit-text-resize: 100%;
         text-resize: 100%;
         }
         .no-link a{
         text-decoration:none !important;
         color:#fff !important;
         }
         .no-link-2 a{
         text-decoration:none !important;
         color:#fff !important;
         }
         sup{vertical-align: super; font-size: x-small;}
         @media only screen and (max-width:750px) {
         table[class="wrapper"]{min-width:320px !important;}
         table[class="flexible"]{width:100% !important;}
         td[class="img-flex"] img{
         width:100% !important;
         height:auto !important;
         }
         td[class="no-float"]{float:none !important;}
         table[class="center"]{margin:0 auto !important;}
         td[class="header"]{padding:5px 10px 0 !important;}
         td[class="area-1"]{padding:10px 10px !important;}
         td[class="area-2"]{padding:15px 10px !important;}
         td[class="post"]{padding:0 0 20px;}
         td[class="post"] td[class="holder"]{
         padding:0 0 15px !important;
         height:auto !important;
         }
         td[class="aligncenter"],
         td[class="no-link"]{text-align:center !important;}
         td[class="col"],
         td[class="sidebar"]{padding:10px !important;}
         td[class="col"] td[class="holder"],
         td[class="sidebar"] td[class="text"]{padding:10px 0 0 !important;}
         td[class="col"] td[class="holder-2"]{height:auto !important;}
         }
      </style>
   </head>
   <body style="margin:0; padding:0;" bgcolor="#ffffff" link="#f5f5f5" >
      <table class="wrapper" width="100%" cellspacing="0" cellpadding="0" style="background-color: #ffffff; background-repeat:repeat;">
         <tr>
            <td height="10" style="font-size:0; line-height:0;" >&nbsp;</td>
         </tr>
         <tr>
            <td align="center" style="padding:0 5px 26px;">
               <table class="flexible" width="750" cellpadding="0" cellspacing="0">
                  <tr>
                     <td class="area-1" style="padding:10px 0px 12px; background-image:url(http://motivaimplants.com/letters/images/gold-bg.png); background-size: 750px 63px; background-repeat: no-repeat; border-radius:2px" bgcolor="#ffffff">
                        <table width="100%" cellpadding="0" cellspacing="0">
                           <tr>
                              <td class="header">
                                 <table class="flexible" width="750" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td mc:edit="block-1" class="aligncenter" align="center" style="padding:0 0 12px;">
                                          <a href="http://motivaimplants.com/"><img src="http://motivaimplants.com/letters/images/motiva.png" border="0" style="vertical-align:top;" width="135" height="45" alt="Motiva Implant Matrix" /></a>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td mc:edit="block-3" class="img-flex" style="padding:0 0 8px;">
                                 <img src="http://motivaimplants.com/letters/images/img-regular-implant.jpg" style="vertical-align:top; border-radius: 0px 0px 2px 2px" width="750" height="250" alt="Your Trusted Partner: Motiva Always Confident Warranty" />
                              </td>
                           </tr>
                           <tr>
                              <td style="padding:0 20px 30px;">
                                 <table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td class="post">
                                          <table width="100%" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td class="holder" height="70" valign="top">
                                                   <table width="100%" cellpadding="0" cellspacing="0">
                                                      <tr>
                                                         <td mc:edit="block-4" style="font:12px/16px Helvetica, Arial, sans-serif; font-weight: 300; color:#9c9c9c; padding:0 0 10px; ">
                                                            Data: <?php echo ($today)?>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td mc:edit="block-4" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#222; font-weight: normal; padding:0 0 10px; ">
                                                            Estimada. <?php echo ($name)?>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
                                                            Parabéns, registrou com sucesso seus Implantes Motiva!
                                                            <br>
                                                            <br>
                                                            Leia, por favor, a informação fornecida abaixo e assegure-se que correspondem a sua informação pessoal da ID de seu Cartão Motiva.                             
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td style="padding:0 20px 42px;">
                               <?php if ($hasLeftImplant):?>
                                 <table class="flexible" width="345" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td class="post">
                                          <table width="100%" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td class="holder" height="100" valign="top">
                                                   <table width="100%" cellpadding="0" cellspacing="0">
                                                     <tr>
                                                         <td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#222222; padding:0 0 10px;">
                                                            Implante esquerdo                                                                                        
                                                         </td>
                                                      </tr>
                                                      <tr>                                                          
                                                         <td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666;">
                                                            <span style="color:#222; font-weight: normal;">Número de série:</span><?php echo ($snL)?><br>
                                                            <span style="color:#222; font-weight: normal;">Referência:</span> <?php echo ($referenceL)?><br>
                                                             <span style="color:#222; font-weight: normal;">Volume:</span> <?php echo ($volumeL)?> cc<br>
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                                <?php endif ?>
                                <?php if ($hasRightImplant):?>
                                 <table class="flexible" width="345" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td class="post">
                                          <table width="100%" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td class="holder" height="100" valign="top">
                                                   <table width="100%" cellpadding="0" cellspacing="0">
                                                      <tr>
                                                         <td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#222222; padding:0 0 10px;">
                                                            Implante direito
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666;">
                                                            <span style="color:#222; font-weight: normal;">Número de série:</span><?php echo ($snR)?><br>
                                                            <span style="color:#222; font-weight: normal;">Referência:</span> <?php echo ($referenceR)?><br>
                                                            <span style="color:#222; font-weight: normal;">Volume:</span> <?php echo ($volumeR)?> cc<br>
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                                <?php endif ?>
                              </td>
                           </tr>
                           <tr>
                              <td style="padding:0 20px 30px;">
                                 <table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td class="post">
                                          <table width="100%" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td class="holder" height="70" valign="top">
                                                   <table width="100%" cellpadding="0" cellspacing="0">
                                                      <tr>
                                                         <td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
                                                             As informações que nos forneceu serão inseridas no Sistema de Rastreabilidade dos Establishment Labs, o qual assegura-lhe, como portadora do(s) dispositivo(s) médico(s) que recebeu, que você pode ser localizada em caso de uma notificação.
                                                              <br>
                                                              <br>
                                                             A Establishment Labs assegura que suas informações pessoais serão protegidas e processadas de acordo com nossa Política de Privacidade e as regulamentações aplicáveis, incluindo a Diretiva 95/46/EC do Parlamento Europeu e do Conselho de 24 de outubro de 1995 e a Lei de Proteção de Dados (RU).
                                                              <br>
                                                              <br>
                                                             <?php if ($years == 5):?>
                                                               Os implantes mamários contêm Q Inside Safety Technology<sup>™</sup>, que permite aos médicos verificar o nome do fabricante, o número de série, o número do lote, o número do lote e outros dados específicos do implante, de fora do corpo usando um leitor portátil proprietário.
                                                             <br>
                                                             <br>
                                                            <?php endif ?>  
                                                             Implementamos a mais elevada prioridade na privacidade e confidencialidade. As bases de dados que armazenam os dados do dispositivo e os dados coletados do paciente do processo de registro são mantidos em centros de dados seguros observando as rígidas diretrizes de privacidade indicadas na legislação aplicável, seguindo os mesmos protocolos de segurança da informação aplicáveis atualmente nos hospitais e centros cirúrgicos. Todos os dados pessoais fornecidos são apenas divulgados conforme acordado com a permissão do paciente. Não compartilhamos seu endereço de e-mail ou informação pessoal sem sua permissão.
                                                             <br>
                                                             <br>
                                                            Em conformidade com a  ‘Breast Augmentation with Motiva Implant Matrix<sup>®</sup> Informação para o Paciente, por favor, contate imediatamente seu cirurgião, se experimentar alguns sintomas inesperados que considere serem relacionados com o procedimento cirúrgico de aumento do seu seio:

                                                            <br>
                                                            <br>
                                                            Lembre-se que seus implantes de seio Motiva Implant Matrix<sup>®</sup> estão cobertos por uma vida útil limitada do dispositivo, a garantia que prevê que a Establishment Labs fornecerá um implante de substituição para qualquer implante de seio Motiva Implant Matrix<sup>®</sup> que sofra uma ruptura. Além disso, no caso de um implante afetado por contratura capsular Baker Graus III ou IV, a Establishment Labs providencia seu Programa de Política de Substituição. Os detalhes completos da garantia são descritos no documento Always Confident Warranty<sup>®</sup>, conforme pode ser fornecida por nosso distribuidor ou que também pode ser revisada em <a href="https://www.motivaimplants.com">www.motivaimplants.com</a>.
                                                            <br>
                                                            <br>
                                                            Em mercados selecionados, oferecemos o Programa de Extensão de Garantia Motiva <?php echo ($years)?>Y Mpara assistência financeira para cirurgias de revisão em caso de ruptura do implante, rotação do implante e contratura capsular Baker Graus III ou IV. Esse programa fornece cobertura para até dois anos desde a data da cirurgia. <br>
                                                                    <b><p style="color:#c89c3c;">Se estiver interessada nessa extensão de garantia e gostaria de verificar se é aplicável a seus implantes, pode fazê-lo em</p></b><br>
                                                                    <p align="center"><a  href="https://register.motivaimagine.com/"></font><img src="http://motivaimplants.com/letters/images/Warranty-Mail-Button.jpg"><br>(Extensão de garantia)</a></p>.
                                                                    <br>
                                                                    <br>
                                                            Trabalhamos com as tecnologias mais recentes para oferecer-lhe os implantes mais inovadores no mercado com sua segurança como prioridade, e mais importante, uma garantia ética do produto. Com nossa Always Confident Warranty<sup>®</sup> e em mercados selecionados com nosso Programa de Extensão de Garantia Motiva <?php echo ($years)?>Y criamos um sistema de suporte sem paralelo. Está disponível para você, e se necessitar deles.
                                                            <br>
                                                            <br>
                                                            <br>
                                                            Esteja Confiante Tem Motiva!
                                                            <br><br>
                                                            Atenciosamente,<br>
                                                            Juan José Chacón-Quirós<br>
                                                            CEO<br>
                                                            Establishment Labs
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td class="area-2" style="padding:15px 20px 5px 21px; background-color:#652d89; border-radius:2px">
                        <table width="100%" cellpadding="0" cellspacing="0">
                           <tr>
                              <td>
                                 <table class="flexible" width="410" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td mc:edit="block-33" class="aligncenter">
                                          <a href="http://motivaimplants.com/"><img src="http://motivaimplants.com/letters/images/motivaimagine.png" border="0" style="vertical-align:top;" width="220" height="37" alt="MotivaImagine" /></a>
                                       </td>
                                    </tr>
                                  </table>
                                 <table class="flexible" width="160" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
                                          <a href="https://www.facebook.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-facebook.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Facebook Icon" /></a>
                                       </td>
                                       <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
                                          <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://www.facebook.com/motivaimplants">Siga-nos no Facebook</a>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
                                          <a href="https://twitter.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-twitter.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Twitter Icon" /></a>
                                       </td>
                                       <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
                                          <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://twitter.com/motivaimplants">Siga-nos no Twitter</a>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
                                          <a href="https://instagram.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-instagram.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Instagram Icon" /></a>
                                       </td>
                                       <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
                                          <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://instagram.com/motivaimplants">Siga-nos no Instagram</a>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td align="center" style="padding:2px 10px 7px;">
               <table class="flexible" width="600" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                  <tr>
                     <td class="no-float" align="center">
                        <table class="center" cellpadding="0" cellspacing="0">
                           <tr>
                              <td style="font:normal 9px/11px Helvetica, Arial, sans-serif; color:#888888;">
                                 Se recebeu esse e-mail por erro, se sua informação não está correta, ou se não concorda com nossa Política de Privacidade e com os Termos e Condições incluídos em nosso site da web: <a style="text-decoration:none; color:#111111;" href="http://motivaimplants.com/">www.motivaimplants.com</a>.
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
   </body>
</html>