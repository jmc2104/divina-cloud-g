<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Welcome to MotivaImagine</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style type="text/css">
		* {
			-webkit-text-size-adjust: none;
			-webkit-text-resize: 100%;
			text-resize: 100%;
		}
		.no-link a{
			text-decoration:none !important;
			color:#fff !important;
		}
		.no-link-2 a{
			text-decoration:none !important;
			color:#fff !important;
		}
		sup{vertical-align: super; line-height: 0; font-size: 0.8em;}
		@media only screen and (max-width:750px) {
			table[class="wrapper"]{min-width:320px !important;}
			table[class="flexible"]{width:100% !important;}
			td[class="img-flex"] img{
				width:100% !important;
				height:auto !important;
			}
			td[class="no-float"]{float:none !important;}
			table[class="center"]{margin:0 auto !important;}
			td[class="header"]{padding:5px 10px 0 !important;}
			td[class="area-1"]{padding:0 !important;}
			td[class="area-2"]{padding:15px 10px !important;}
			td[class="post"]{padding:0 0 20px;}
			td[class="post"] td[class="holder"]{
				padding:0 0 15px !important;
				height:auto !important;
			}
			td[class="aligncenter"],
			td[class="no-link"]{text-align:center !important;}
			td[class="col"],
			td[class="sidebar"]{padding:10px !important;}
			td[class="col"] td[class="holder"],
			td[class="sidebar"] td[class="text"]{padding:10px 0 0 !important;}
			td[class="col"] td[class="holder-2"]{height:auto !important;}
		}
		.tg  {border-collapse:collapse;border-spacing:0;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg .tg-d5wv{background-color:#a38546;color:#ffffff;text-align:center;vertical-align:top}
		.tg .tg-yw4l{vertical-align:top}
	</style>
</head>
<body style="margin:0; padding:0;" bgcolor="#eeeeee" link="#f5f5f5" >
	<table class="wrapper" width="100%" cellspacing="0" cellpadding="0">
		<tr><td height="10" style="font-size:0; line-height:0;" >&nbsp;</td></tr>
		<tr>
			<td align="center" style="padding:10px 5px 26px 5px;">
				<table class="flexible" width="750" cellpadding="0" cellspacing="0">
					<tr>
						<td class="area-1" style="border-radius: 6px 6px 3px 3px" bgcolor="#ffffff">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td mc:edit="block-3" class="img-flex" style="padding:0 0 8px;">
										<img src="http://motivaimplants.com/letters/cover.jpg" style="vertical-align:top; border-radius: 6px 6px 0px 0px" width="750" height="400" alt="Motiva Implants Logo" />
									</td>
								</tr>
								<tr>
									<td style="padding:20px 20px 30px 20px;">
										<table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td class="post">
													<table width="100%" cellpadding="0" cellspacing="0">
														<tr>
															<td class="holder" height="70" valign="top">
																<table width="100%" cellpadding="0" cellspacing="0">
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			به MotivaImagine خوش آمدید
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			شریک معتبر شما
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Motiva Always Confident Warranty&reg;
																		</td>
																	</tr>
																	<tr><!-- MAIN CONTENT -->
																		<td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
																			عزیز. <?=/*php*/$name/*php*/?> خانم
																			<span style="float: right; color:#a1a1a1;"><?=/*php*/date('d-M-Y')/*php*/?></span>
																			<br/>
																			<br/>
																			تبریک می گوییم، شما با موفقیت Motiva Implants خود را ثبت کرده اید!
																			<br>
																			<br>
																			لطفاً اطلاعات ارائه شده در ذیل را مطالعه نمایید و مطمئن شوید که با اطلاعات مندرج در کارت شناسایی Motiva شخصی شما مطابقت دارند.
																			<br/>
																			<br/>
																			<!-- Implant Information -->
																			<table width="100%" cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<?php if ( $has_left_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="left">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										ایمپلنت سمت چپ             
																									</td>
																								</tr>
																								<tr>                                                          
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">شماره سریال: </span><?=/*php*/$left_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">مرجع: </span><?=/*php*/$left_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">حجم: </span><?=/*php*/$left_vol/*php*/?> سی سی<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																						<?php if ( $has_right_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="right">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										ایمپلنت سمت راست
																									</td>
																								</tr>
																								<tr>
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">شماره سریال: </span><?=/*php*/$right_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">مرجع: </span><?=/*php*/$right_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">حجم: </span><?=/*php*/$right_vol/*php*/?> سی سی<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																					</td>
																				</tr>
																			</table>
																			<br>
																			<br>
																			اطلاعاتی که شما ارائه داده اید، در سیستم ردیابی Establishment Labs وارد خواهد شد، که به شما اطمینان می دهد، به عنوان دارنده دستگاه(های) پزشکی که دریافت کرده اید، که در صورت یک رویداد اطلاع رسانی موقعیت مکانی شما قابل ردیابی است.
																			<br>
																			<br>Establishment Labs اطمینان می دهد که اطلاعات شخصی شما مطابق با سیاست حریم خصوصی و مقررات قابل اعمال، اعم از قطعنامه 95/46/EC پارلمان اروپا و شورای 24 اکتبر 1995، و قانون حفاظت از داده (انگلستان) محافظت و رسیدگی خواهد شد.
																			<br>
																			<br>
																			<!-- Only if has implants -->
																			<?php if ( isset($is_q_inside) ? $is_q_inside : false ): ?>
																				ایمپلنت های سینه شامل Q Inside Safety Technology ™ است که به پزشکان اجازه می دهد نام و نام خانوادگی، شماره سریال، شماره سری، شماره سری و سایر اطلاعات مربوط به ایمپلنت را از خارج از بدن با استفاده از یک خواننده دستی اختصاصی شناسایی کند. 
																				<br>
																				<br>
																			<?php endif ?>
																			ما بالاترین اولویت را برای حریم خصوصی و محرمانه ماندن اطلاعات بیمار قائل هستیم. پایگاه های داده ای که داده های جمع آوری شده دستگاه و بیمار از روند ثبت نام را ذخیره می کنند، در مراکز داده ایمن نگهداری می شوند که مقررات سختگیرانه حریم خصوصی تعیین شده توسط قوانین قابل اجرا و نیز پروتکل های امنیت اطلاعاتی را که در حال حاضر در بیمارستان ها و مراکز جراحی اجرا می شوند، رعایت می کنند. تمام داده های شخصی ارائه شده فقط در صورت توافق و با اجازه بیمار افشا می شوند. ما نشانی ایمیل یا اطلاعات شخصی شما را بدون رضایت شما به اشتراک نمی گذاریم.
																			<br>
																			<br>
																			مطابق با "بزرگ کردن سینه با Motiva Implant Matrix®: اطلاعات برای بیمار، اگر هر گونه علائم غیر منتظره ای را تجربه می کنید که تصور می کنید به عمل جراحی سینه شما مربوط است، لطفاً بلافاصله با جراح خود تماس بگیرید.
																			<br>
																			<br>به خاطر داشته باشید که ایمپلنت های سینه Motiva Implant Matrix® شما تحت پوشش یک گارانتی محدود طول عمر دستگاه است که مشروط بر آن است که Establishment Labs یک ایمپلنت جایگزین برای هر یک از ایمپلنت های سینه Motiva Implant Matrix® که پاره می شوند، تامین خواهد کرد. به علاوه، در صورتیکه یک ایمپلنت دچار انقباض کپسولی درجه III یا IV  در مقیاس بیکر بشود، Establishment Labs برنامه سیاست جایگزینی خود را ارائه می دهد. جزئیات کامل درباره گارانتی در سند Always Confident Warranty® توضیح داده شده است، که ممکن است توسط توزیع کننده ما ارائه شود یا می توان آن را در <a style="text-decoration:none; color:#a38546;" href="https://motivaimplants.com">www.motivaimplants.com</a> مطالعه کنید. در بازارهای انتخاب شده ما Motiva Extended Warranty Program <?=/*php*/$years/*php*/?> ساله خود را ارائه می دهیم تا برای جراحی های ترمیمی در صورت پارگی ایمپلنت، چرخش ایمپلنت و انقباض کپسولی درجه III یا IV در مقایس بیکر کمک مالی ارائه دهیم. این برنامه از تاریخ جراحی حداکثر تا دو سال پوشش ارائه می دهد.

																			Remember that your Motiva Implant Matrix<sup>®</sup> breast implants are covered by a limited, lifetime of the device, warranty which provides that Establishment Labs will supply a replacement implant for any Motiva Implant Matrix<sup>®</sup> breast implant which suffers a rupture. Additionally, in case of an implant affected by Baker Grades III or IV capsular contracture, Establishment Labs provides its Replacement Policy Program. Complete details of the warranty are described in the document Always Confident Warranty<sup>®</sup>, as may be provided by our distributor or which can also be reviewed on <a style="text-decoration:none; color:#a38546;" href="https://motivaimplants.com">www.motivaimplants.com</a>. 
																			In select markets, we offer our <?=/*php*/$years/*php*/?>Y Motiva Extended Warranty Program to provide financial assistance for revision surgeries in case of implant rupture, implant rotation and capsular contracture Baker Grades III or IV. This program provides coverage for up to <?=/*php*/$years/*php*/?> years from the date of surgery.
																			<br>
																			<br>
																			<p style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546;">
																				<b>اگر به این گارانتی گسترده علاقه دارید و می خواهید مطمئن شوید که آیا این گارانتی برای ایمپلنت های شما قابل استفاده است یا خیر، می توانید این کار را در زیر انجام دهید</b>
																				<a style="text-decoration:none; color:#888;" href="https://register.motivaimagine.com/">register.motivaimagine.com</a>
																			</p>
																			<br>
																			<p align="center">
																				<a href="https://register.motivaimagine.com/" style="text-decoration:none; background: #a38546; color:#fff; font-weight: 300; padding:8px 14px; border-radius:30px"><b>گارانتی طولانی مدت</b></a>
																			</p>
																			<br>
																			<br>
																			ما با آخرین فناوری ها کار می کنیم تا با در نظر گرفتن ایمنی شما به عنوان اولویت اول و از همه مهمتر، یک گارانتی محصول اخلاقی، نوآورترین ایمپلنت های موجود در بازار را به شما ارائه دهیم. با Always Confident Warranty<sup>®</sup> ما و در بازارهای انتخاب شده با Motiva Extended Warranty Program <?=/*php*/isset($years) ? $years : '{{years}}'/*php*/?> ساله ما، یک سیستم پشتیبانی بی نظیر ایجاد کرده ایم... هر زمان و در صورت نیاز به آن در کنار شما است.
																			<br>
																			<br/>
																			<br/>
																			اعتماد به نفس داشته باشید... شما موتیوا را دارید!
																			<br>
																			<br>
																			با احترام،<br>
																			Juan José Chacón-Quirós<br>
																			مدیرعامل<br>
																			Establishment Labs<br>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="area-2" style="padding:15px 20px 5px 21px; background-color:#eeeeee;">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<table class="flexible" width="410" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-33" class="aligncenter">
													<a href="http://motivaimagine.com/"><img src="http://motivaimplants.com/letters/motivaimagine.png" border="0" style="vertical-align:top; padding: 0px 0px 12px 0px" width="210" height="37" alt="MotivaImagine" /></a>
												</td>
											</tr>
											<tr>
												<td mc:edit="block-35" class="aligncenter" style="font:9px/11px Helvetica, Arial, sans-serif; color:#fff; font-weight: 300;">
													<a style="text-decoration:none; color:#a1a1a1;" href="mailto:customerservice@motivaimplants.com">customerservice@motivaimplants.com</a> <br/>
													<a style="text-decoration:none; color:#a1a1a1; font-weight: 300;" href="http://motivaimplants.com/">www.motivaimplants.com</a> <br/>
												</td>
											</tr>                                                                   
										</table>
										<table class="flexible" width="160" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-36" width="160" class="aligncenter" style="padding:10px 0 5px 0; text-align: right;">
													<a href="https://www.facebook.com/motivaimagine/"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-facebook-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="به ما در Facebook بپیوندید" /></a> <a href="https://twitter.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-twitter-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="به ما در Twitter بپیوندید" /></a> <a href="https://instagram.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-instagram-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="به ما در Instagram بپیوندید" /></a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" style="padding:2px 10px 7px;">
				<table class="flexible" width="600" cellpadding="0" cellspacing="0" bgcolor="#eeeeee">
					<tr>
						<td class="no-float" align="center">
							<table class="center" cellpadding="0" cellspacing="0">
								<tr>
									<td style="font:normal 9px/11px Helvetica, Arial, sans-serif; color:#888888; text-align: center;">اگر شما به اشتباه این ایمیل را دریافت کرده اید، اگر اطلاعات شما درست نیست، یا اگر با سیاست حریم خصوصی و قوانین استفاده موجود در این وبسایت موافق نیستید: <a style="text-decoration:none; color:#555555;" href="http://motivaimplants.com/">www.motivaimplants.com</a>,.
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>