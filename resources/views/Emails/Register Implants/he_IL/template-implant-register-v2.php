<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Welcome to MotivaImagine</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style type="text/css">
		* {
			-webkit-text-size-adjust: none;
			-webkit-text-resize: 100%;
			text-resize: 100%;
		}
		.no-link a{
			text-decoration:none !important;
			color:#fff !important;
		}
		.no-link-2 a{
			text-decoration:none !important;
			color:#fff !important;
		}
		sup{vertical-align: super; line-height: 0; font-size: 0.8em;}
		@media only screen and (max-width:750px) {
			table[class="wrapper"]{min-width:320px !important;}
			table[class="flexible"]{width:100% !important;}
			td[class="img-flex"] img{
				width:100% !important;
				height:auto !important;
			}
			td[class="no-float"]{float:none !important;}
			table[class="center"]{margin:0 auto !important;}
			td[class="header"]{padding:5px 10px 0 !important;}
			td[class="area-1"]{padding:0 !important;}
			td[class="area-2"]{padding:15px 10px !important;}
			td[class="post"]{padding:0 0 20px;}
			td[class="post"] td[class="holder"]{
				padding:0 0 15px !important;
				height:auto !important;
			}
			td[class="aligncenter"],
			td[class="no-link"]{text-align:center !important;}
			td[class="col"],
			td[class="sidebar"]{padding:10px !important;}
			td[class="col"] td[class="holder"],
			td[class="sidebar"] td[class="text"]{padding:10px 0 0 !important;}
			td[class="col"] td[class="holder-2"]{height:auto !important;}
		}
		.tg  {border-collapse:collapse;border-spacing:0;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg .tg-d5wv{background-color:#a38546;color:#ffffff;text-align:center;vertical-align:top}
		.tg .tg-yw4l{vertical-align:top}
	</style>
</head>
<body style="margin:0; padding:0;" bgcolor="#eeeeee" link="#f5f5f5" >
	<table class="wrapper" width="100%" cellspacing="0" cellpadding="0">
		<tr><td height="10" style="font-size:0; line-height:0;" >&nbsp;</td></tr>
		<tr>
			<td align="center" style="padding:10px 5px 26px 5px;">
				<table class="flexible" width="750" cellpadding="0" cellspacing="0">
					<tr>
						<td class="area-1" style="border-radius: 6px 6px 3px 3px" bgcolor="#ffffff">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td mc:edit="block-3" class="img-flex" style="padding:0 0 8px;">
										<img src="http://motivaimplants.com/letters/cover.jpg" style="vertical-align:top; border-radius: 6px 6px 0px 0px" width="750" height="400" alt="Motiva Implants Logo" />
									</td>
								</tr>
								<tr>
									<td style="padding:20px 20px 30px 20px;">
										<table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td class="post">
													<table width="100%" cellpadding="0" cellspacing="0">
														<tr>
															<td class="holder" height="70" valign="top">
																<table width="100%" cellpadding="0" cellspacing="0">
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			ברוכים הבאים ל MotivaImagine
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			השותף שלך שניתן לבטוח בו
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			אחריות מוטיבה "תמיד בטוח/ה"
																		</td>
																	</tr>
																	<tr><!-- MAIN CONTENT -->
																		<td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
																			מר. <?=/*php*/$name/*php*/?> היקר
																			<span style="float: right; color:#a1a1a1;"><?=/*php*/date('d-M-Y')/*php*/?></span>
																			<br/>
																			<br/>
																			ברכותינו, רשמת בהצלחה את שתלי מוטיבה שלך!
																			<br>
																			<br>
																			אנא קרא את המידע למטה וודא שהוא מתאים למידע הנמצא על גבי כרטיס זיהוי המוטיבה האישי שלך
																			<br/>
																			<br/>
																			<!-- Implant Information -->
																			<table width="100%" cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<?php if ( $has_left_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="left">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										שתל צד שמאל               
																									</td>
																								</tr>
																								<tr>                                                          
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">מספר סידורי: </span><?=/*php*/$left_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">סימוכין: </span><?=/*php*/$left_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">נפח: </span><?=/*php*/$left_vol/*php*/?> cc<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																						<?php if ( $has_right_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="right">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										שתל צד ימני
																									</td>
																								</tr>
																								<tr>
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">מספר סידורי: </span><?=/*php*/$right_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">סימוכין: </span><?=/*php*/$right_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">נפח: </span><?=/*php*/$right_vol/*php*/?> cc<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																					</td>
																				</tr>
																			</table>
																			<br>
																			<br>
																			המידע אותו סיפקת יוכנס למערכת המעקב של Establishment Labs, המבטיחה לך, כנושא המתקן(ים) הרפואי(ים) אותם קיבלת, שתאותר במקרה של אירוע הודעה. 
																			<br>
																			<br>
																			Establishment Labs מבטיחה לך שהמידע הרפואי האישי שלך יהיה מוגן ומטופל בהתאם למדיניות הפרטיות שלנו והחוקים הישימים בנושא, כולל תקנה 95/46/EC של הפרלמנט האירופי והוועדה מיום 24 באוקטובר 1995 ומסמך הגנת הנתונים (הממלכה המאוחדת).
																			<br>
																			<br>
																			<!-- Only if has implants -->
																			<?php if ( isset($is_q_inside) ? $is_q_inside : false ): ?>
																				Your breast implants contain Q Inside Safety Technology™, which allows physicians to ascertain the manufacturer name, serial number, lot number, batch number, and other implant specific data, from outside the body using a proprietary handheld reader. 
																				<br>
																				<br>
																			<?php endif ?>
																			אנו שמים במקום הראשון את פרטיותו וחסיונו של המטופל/ת. מאגר הנתונים המאחסן את נתוני המתקן והמטופל/ת הנאספים מתהליך הרישום, נמצאים במרכזי נתונים מוגנים, אשר עוקבים אחרי הקווים המנחים המקפידים ביותר המעוגנים בחוק, על ידי מעקב אחרי הפרוטוקולים לבטיחות המידע הדומים לאלו אשר נמצאים בשימוש בבתי חולים ובמרכזי ניתוחים. כל הנתונים האישיים אשר סופקו ייחשפו רק כפי שהוסכם ועם אישור המטופל/ת. אנו לא משתפים את כתובת הדוא"ל שלך או המידע האישי ללא הסכמתך.
																			<br>
																			<br>
																			בהתאם ל-"Breast Augmentation with Motiva Implant Matrix<sup>®</sup>":  מידע למטופל/ת, אנא צור קשר עם המנתח שלך ברגע בו תרגיש בתסמינים אשר אתה חושב שהנם קשורים לתהליך ניתוח הגדלת החזה שלך.
																			<br>
																			<br>
																			זכור ששתלי השד Motiva Implant Matrix<sup>®</sup> שלך מכוסים על ידי אחריות מוגבלת, לכל תקופת החיים של המתקן, אשר קובעת ש-Establishment Labs תספק שתל חלופי עבור כל שתל שד Motiva Implant Matrix<sup>®</sup> אשר נמצא בו קרע. בנוסף, במקרה של שתל שד אשר נמצאה בו כוויצה קפסולרית, רמות בייקר 3 ו-4, Establishment Labs תקבע את תכנית מדיניות החלפת המוצר שלה. הפרטים המלאים של האחריות מתוארים במסמך האחריות "תמיד בטוח/ה", ככל שיינתן על ידי המפיץ שלנו וכמו כן ניתן לראותם גם <a style="text-decoration:none; color:#a38546;" href="https://motivaimplants.com">www.motivaimplants.com</a>.
																			<br> 
																			בשווקים נבחרים אנו מציעים את תכנית אחריות מוטיבה נרחבת ל <?=/*php*/$years/*php*/?>שָׁנָה על מנת לספק עזרה כלכלית במקרים של קרעי שתל, סיבובי שתל וכוויצה קפסולרית, רמות בייקר 3 או 4. תכנית זו מספקת כיסוי עד <?=/*php*/$years/*php*/?>שָׁנָה מיום הניתוח.
																			<br>
																			<br>
																			<p style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546;">
																				<b>אם אתה מעוניין באחריות נרחבת זו ותרצה לאשר שהיא מתאימה לשתלים שלך, אתה יכול לעשות זאת ב-</b>
																				<a style="text-decoration:none; color:#888;" href="https://register.motivaimagine.com/">register.motivaimagine.com</a>
																			</p>
																			<br>
																			<p align="center">
																				<a href="https://register.motivaimagine.com/" style="text-decoration:none; background: #a38546; color:#fff; font-weight: 300; padding:8px 14px; border-radius:30px"><b>אחריות נרחבת</b></a>
																			</p>
																			<br>
																			<br>
																			אנו עובדים עם הטכנולוגיות העדכניות ביותר על מנת לספק לך את השתלים החדישים ביותר בשוק כאשר הבטיחות שלך נמצאת בראש סדר העדיפויות שלנו, והכי חשוב, בעזרת אחריות מוצר אתית. עם אחריות "תמיד בטוח/ה" שלנו ועם תכנית אחריות נרחבת מוטיבה <?=/*php*/$years/*php*/?>שָׁנָה הניתנת לבחירה, יצרנו מערכת תמיכה ללא מתחרים...היא שם עבורך אם וכאשר תצטרך אותה.
																			<br>
																			<br/>
																			<br/>
																			היה בטוח... יש לך מוטיבה!
																			<br>
																			<br>
																			בכנות,<br>
																			Juan José Chacón-Quirós<br>
																			מנהל כללי<br>
																			Establishment Labs<br>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="area-2" style="padding:15px 20px 5px 21px; background-color:#eeeeee;">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<table class="flexible" width="410" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-33" class="aligncenter">
													<a href="http://motivaimagine.com/"><img src="http://motivaimplants.com/letters/motivaimagine.png" border="0" style="vertical-align:top; padding: 0px 0px 12px 0px" width="210" height="37" alt="MotivaImagine" /></a>
												</td>
											</tr>
											<tr>
												<td mc:edit="block-35" class="aligncenter" style="font:9px/11px Helvetica, Arial, sans-serif; color:#fff; font-weight: 300;">
													<a style="text-decoration:none; color:#a1a1a1;" href="mailto:customerservice@motivaimplants.com">customerservice@motivaimplants.com</a> <br/>
													<a style="text-decoration:none; color:#a1a1a1; font-weight: 300;" href="http://motivaimplants.com/">www.motivaimplants.com</a> <br/>
												</td>
											</tr>                                                                   
										</table>
										<table class="flexible" width="160" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-36" width="160" class="aligncenter" style="padding:10px 0 5px 0; text-align: right;">
													<a href="https://www.facebook.com/motivaimagine/"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-facebook-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="עקוב אחרינו בפייסבוק" /></a> <a href="https://twitter.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-twitter-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="עקוב אחרינו בטוויטר" /></a> <a href="https://instagram.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-instagram-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="עקוב אחרינו באינסטגרם" /></a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" style="padding:2px 10px 7px;">
				<table class="flexible" width="600" cellpadding="0" cellspacing="0" bgcolor="#eeeeee">
					<tr>
						<td class="no-float" align="center">
							<table class="center" cellpadding="0" cellspacing="0">
								<tr>
									<td style="font:normal 9px/11px Helvetica, Arial, sans-serif; color:#888888; text-align: center;">
										אם קיבלת את הדוא"ל הזה בטעות, אם המידע שלך איננו נכון או אם אינך מסכים עם מדיניות הפרטיות ותנאיי השימוש הנכללים באתר שלנו: <a style="text-decoration:none; color:#555555;" href="http://motivaimplants.com/">www.motivaimplants.com</a>.
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>