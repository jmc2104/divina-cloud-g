<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Welcome to MotivaImagine</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style type="text/css">
		* {
			-webkit-text-size-adjust: none;
			-webkit-text-resize: 100%;
			text-resize: 100%;
		}
		.no-link a{
			text-decoration:none !important;
			color:#fff !important;
		}
		.no-link-2 a{
			text-decoration:none !important;
			color:#fff !important;
		}
		sup{vertical-align: super; line-height: 0; font-size: 0.8em;}
		@media only screen and (max-width:750px) {
			table[class="wrapper"]{min-width:320px !important;}
			table[class="flexible"]{width:100% !important;}
			td[class="img-flex"] img{
				width:100% !important;
				height:auto !important;
			}
			td[class="no-float"]{float:none !important;}
			table[class="center"]{margin:0 auto !important;}
			td[class="header"]{padding:5px 10px 0 !important;}
			td[class="area-1"]{padding:0 !important;}
			td[class="area-2"]{padding:15px 10px !important;}
			td[class="post"]{padding:0 0 20px;}
			td[class="post"] td[class="holder"]{
				padding:0 0 15px !important;
				height:auto !important;
			}
			td[class="aligncenter"],
			td[class="no-link"]{text-align:center !important;}
			td[class="col"],
			td[class="sidebar"]{padding:10px !important;}
			td[class="col"] td[class="holder"],
			td[class="sidebar"] td[class="text"]{padding:10px 0 0 !important;}
			td[class="col"] td[class="holder-2"]{height:auto !important;}
		}
		.tg  {border-collapse:collapse;border-spacing:0;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg .tg-d5wv{background-color:#a38546;color:#ffffff;text-align:center;vertical-align:top}
		.tg .tg-yw4l{vertical-align:top}
	</style>
</head>
<body style="margin:0; padding:0;" bgcolor="#eeeeee" link="#f5f5f5" >
	<table class="wrapper" width="100%" cellspacing="0" cellpadding="0">
		<tr><td height="10" style="font-size:0; line-height:0;" >&nbsp;</td></tr>
		<tr>
			<td align="center" style="padding:10px 5px 26px 5px;">
				<table class="flexible" width="750" cellpadding="0" cellspacing="0">
					<tr>
						<td class="area-1" style="border-radius: 6px 6px 3px 3px" bgcolor="#ffffff">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td mc:edit="block-3" class="img-flex" style="padding:0 0 8px;">
										<img src="http://motivaimplants.com/letters/cover.jpg" style="vertical-align:top; border-radius: 6px 6px 0px 0px" width="750" height="400" alt="Motiva Implants Logo" />
									</td>
								</tr>
								<tr>
									<td style="padding:20px 20px 30px 20px;">
										<table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td class="post">
													<table width="100%" cellpadding="0" cellspacing="0">
														<tr>
															<td class="holder" height="70" valign="top">
																<table width="100%" cellpadding="0" cellspacing="0">
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Welcome to MotivaImagine
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			您的可信赖的合作伙伴 
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Mativa“始终有信心的担保”&reg;
																		</td>
																	</tr>
																	<tr><!-- MAIN CONTENT -->
																		<td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
																			亲爱的. <?=/*php*/$name/*php*/?> 女士:
																			<span style="float: right; color:#a1a1a1;"><?=/*php*/date('d-M-Y')/*php*/?></span>
																			<br/>
																			<br/>
																			恭喜您，您已经成功地注册了您的Motiva植入体！ 
																			<br>
																			<br>
																			请阅读下面提供的信息，并确保它与您的个人Motiva ID卡上的信息相符。
																			<br/>
																			<br/>
																			<!-- Implant Information -->
																			<table width="100%" cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<?php if ( $has_left_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="left">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										左侧植入体                                   
																									</td>
																								</tr>
																								<tr>                                                          
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">序列号:</span><?=/*php*/$left_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">参考:</span><?=/*php*/$left_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">体积:</span><?=/*php*/$left_vol/*php*/?> cc<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																						<?php if ( $has_right_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="right">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										右侧植入体 
																									</td>
																								</tr>
																								<tr>
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">序列号:</span><?=/*php*/$right_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">参考:</span><?=/*php*/$right_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">体积:</span><?=/*php*/$right_vol/*php*/?> cc<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																					</td>
																				</tr>
																			</table>
																			<br>
																			<br>
																			您已提供的信息将被输入Establishment Labs追踪系统，该系统可确保在发生了需要通知的事件时，可以找到作为您所收到的医疗器械的持有者的您本人。 
																			<br>
																			<br>
																			Establishment Labs确保您的个人信息将按照我们的隐私政策和适用法规（包括1995年10月24日欧洲议会和理事会的指令95/46/EC和英国数据保护法案）而得到保护。 
																			<br>
																			<br>
																			<!-- Only if has implants -->
																			<?php if ( isset($is_q_inside) ? $is_q_inside : false ): ?>
																				Your breast implants contain Q Inside Safety Technology™, which allows physicians to ascertain the manufacturer name, serial number, lot number, batch number, and other implant specific data, from outside the body using a proprietary handheld reader. 
																				<br>
																				<br>
																			<?php endif ?>
																			我们把病人隐私和保密放在最重要的位置。存储从注册过程中收集的关于设备和患者的数据的数据库被保存在安全的数据中心，这些数据中心遵循适用法规所规定的严格的隐私准则，与当前在医院和外科中心遵循相同的信息安全程序。提供的所有个人资料仅能按照约定和患者的授权进行披露。未经您的同意，我们不会分享您的电子邮件地址或个人信息。  
																			<br>
																			<br>
																			根据“用Motiva Implant Matrix进行隆胸<sup>®</sup>  ：患者信息”，如果您遇到任何您认为与您的隆胸手术有关的意外症状，请立即联系您的外科医生。 
																			<br>
																			<br>
																			记住：您的Motiva Implant Matrix <sup>®</sup> 乳房植入体具有有限的终身器械担保，如果任何一个Motiva Implant Matrix <sup>®</sup> 乳房植入体遭受破裂，Establishment Labs将提供替代植入体。另外，如果植入体出现了Baker III级或IV级包囊挛缩的现象，Establishment Labs提供其替换保单计划。可能已经由我们的分销商提供的“始终有信心的担保”（Always Confident Warranty）<sup>®</sup>文件中描述了该担保的完整细节  ，您也可以通过上网 <a style="text-decoration:none; color:#a38546;" href="https://motivaimplants.com">www.motivaimplants.com</a> 来审阅这份文件。在特定的市场中，我们提供<?=/*php*/$years/*php*/?>年期 Motiva延长担保计划，以便在出现植入体破裂、植入体旋转和Baker Grade III或IV包囊挛缩等情况下为修复手术提供经济援助。该计划提供<?=/*php*/$years/*php*/?>年的保险，从手术日期起生效。
																			<br>
																			<br>
																			<p style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546;">
																				<b>如果您对这种延长担保计划有兴趣，并想验证它是否适用于您的植入体，可以在</b>
																				<a style="text-decoration:none; color:#888;" href="https://register.motivaimagine.com/">register.motivaimagine.com</a>
																			</p>
																			<br>
																			<p align="center">
																				<a href="https://register.motivaimagine.com/" style="text-decoration:none; background: #a38546; color:#fff; font-weight: 300; padding:8px 14px; border-radius:30px"><b>“延长担保”中进行验证。</b></a>
																			</p>
																			<br>
																			<br>
																			我们用最新的技术为您提供市场上最具创新性的植入体，并将您的安全作为我们的首要事项，最重要的是为您提供符合道德标准的产品担保。有了我们的“始终有信心的担保” <?=/*php*/isset($years) ? $years : '{{years}}'/*php*/?> 和特定市场的2年期 Motiva延长担保计划，我们创造了一个无与伦比的支持系统......当您需要的时候，它就在您的身边。 
																			<br>
																			<br/>
																			<br/>
																			要自信... 您拥有了Motiva！ 
																			<br>
																			<br>
																			此致，<br>
																			Juan José Chacón-Quirós<br>
																			CEO<br>
																			Establishment Labs首席执行官<br>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="area-2" style="padding:15px 20px 5px 21px; background-color:#eeeeee;">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<table class="flexible" width="410" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-33" class="aligncenter">
													<a href="http://motivaimagine.com/"><img src="http://motivaimplants.com/letters/motivaimagine.png" border="0" style="vertical-align:top; padding: 0px 0px 12px 0px" width="210" height="37" alt="MotivaImagine" /></a>
												</td>
											</tr>
											<tr>
												<td mc:edit="block-35" class="aligncenter" style="font:9px/11px Helvetica, Arial, sans-serif; color:#fff; font-weight: 300;">
													<a style="text-decoration:none; color:#a1a1a1;" href="mailto:customerservice@motivaimplants.com">customerservice@motivaimplants.com</a> <br/>
													<a style="text-decoration:none; color:#a1a1a1; font-weight: 300;" href="http://motivaimplants.com/">www.motivaimplants.com</a> <br/>
												</td>
											</tr>                                                                   
										</table>
										<table class="flexible" width="160" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-36" width="160" class="aligncenter" style="padding:10px 0 5px 0; text-align: right;">
													<a href="https://www.facebook.com/motivaimagine/"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-facebook-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="在Facebook上关注我们" /></a> <a href="https://twitter.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-twitter-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="在Twitter上关注我们" /></a> <a href="https://instagram.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-instagram-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="在Instagram上关注我们" /></a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" style="padding:2px 10px 7px;">
				<table class="flexible" width="600" cellpadding="0" cellspacing="0" bgcolor="#eeeeee">
					<tr>
						<td class="no-float" align="center">
							<table class="center" cellpadding="0" cellspacing="0">
								<tr>
									<td style="font:normal 9px/11px Helvetica, Arial, sans-serif; color:#888888; text-align: center;">
										如果您错误地收到了此电子邮件，如果您的信息不正确，或者如果您不同意我们网站<a style="text-decoration:none; color:#555555;" href="http://motivaimplants.com/">www.motivaimplants.com</a>中的隐私政策和使用条款。
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>