<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Welcome to MotivaImagine</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style type="text/css">
		* {
			-webkit-text-size-adjust: none;
			-webkit-text-resize: 100%;
			text-resize: 100%;
		}
		.no-link a{
			text-decoration:none !important;
			color:#fff !important;
		}
		.no-link-2 a{
			text-decoration:none !important;
			color:#fff !important;
		}
		sup{vertical-align: super; line-height: 0; font-size: 0.8em;}
		@media only screen and (max-width:750px) {
			table[class="wrapper"]{min-width:320px !important;}
			table[class="flexible"]{width:100% !important;}
			td[class="img-flex"] img{
				width:100% !important;
				height:auto !important;
			}
			td[class="no-float"]{float:none !important;}
			table[class="center"]{margin:0 auto !important;}
			td[class="header"]{padding:5px 10px 0 !important;}
			td[class="area-1"]{padding:0 !important;}
			td[class="area-2"]{padding:15px 10px !important;}
			td[class="post"]{padding:0 0 20px;}
			td[class="post"] td[class="holder"]{
				padding:0 0 15px !important;
				height:auto !important;
			}
			td[class="aligncenter"],
			td[class="no-link"]{text-align:center !important;}
			td[class="col"],
			td[class="sidebar"]{padding:10px !important;}
			td[class="col"] td[class="holder"],
			td[class="sidebar"] td[class="text"]{padding:10px 0 0 !important;}
			td[class="col"] td[class="holder-2"]{height:auto !important;}
		}
		.tg  {border-collapse:collapse;border-spacing:0;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg .tg-d5wv{background-color:#a38546;color:#ffffff;text-align:center;vertical-align:top}
		.tg .tg-yw4l{vertical-align:top}
	</style>
</head>
<body style="margin:0; padding:0;" bgcolor="#eeeeee" link="#f5f5f5" >
	<table class="wrapper" width="100%" cellspacing="0" cellpadding="0">
		<tr><td height="10" style="font-size:0; line-height:0;" >&nbsp;</td></tr>
		<tr>
			<td align="center" style="padding:10px 5px 26px 5px;">
				<table class="flexible" width="750" cellpadding="0" cellspacing="0">
					<tr>
						<td class="area-1" style="border-radius: 6px 6px 3px 3px" bgcolor="#ffffff">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td mc:edit="block-3" class="img-flex" style="padding:0 0 8px;">
										<img src="http://motivaimplants.com/letters/cover.jpg" style="vertical-align:top; border-radius: 6px 6px 0px 0px" width="750" height="400" alt="Motiva Implants Logo" />
									</td>
								</tr>
								<tr>
									<td style="padding:20px 20px 30px 20px;">
										<table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td class="post">
													<table width="100%" cellpadding="0" cellspacing="0">
														<tr>
															<td class="holder" height="70" valign="top">
																<table width="100%" cellpadding="0" cellspacing="0">
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Bine ați venit la MotivaImagine
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Partenerul dumneavoastră de încredere
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Motiva Always Confident Warranty&reg;
																		</td>
																	</tr>
																	<tr><!-- MAIN CONTENT -->
																		<td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
																			Stimată Dnă. <?=/*php*/$name/*php*/?>
																			<span style="float: right; color:#a1a1a1;"><?=/*php*/date('d-M-Y')/*php*/?></span>
																			<br/>
																			<br/>
																			Felicitări, v-ați înregistrat cu succes Implanturile Motiva!
																			<br>
																			<br>
																			Vă rugăm să citiți informațiile prezentate mai jos și să vă asigurați că acestea corespund informațiilor de pe cardul personal de identitate Motiva.
																			<br/>
																			<br/>
																			<!-- Implant Information -->
																			<table width="100%" cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<?php if ( $has_left_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="left">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										Implantul de pe partea stângă
																									</td>
																								</tr>
																								<tr>                                                          
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">Număr de serie: </span><?=/*php*/$left_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Referință: </span><?=/*php*/$left_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Volum: </span><?=/*php*/$left_vol/*php*/?> cc<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																						<?php if ( $has_right_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="right">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										Implantul de pe partea dreaptă
																									</td>
																								</tr>
																								<tr>
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">Număr de serie: </span><?=/*php*/$right_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Referință: </span><?=/*php*/$right_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Volum: </span><?=/*php*/$right_vol/*php*/?> cc<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																					</td>
																				</tr>
																			</table>
																			<br>
																			<br>
																			Informațiile pe care ni le-ati oferit vor fi introduse în Sistemul de Trasabilitate al Establishment Labs, ceea ce vă va asigura ca și deținătoare a dispozitivului (dispozitivelor) medical (medicale) primit (primite) și va facilita localizarea dumneavoastră în cazul unei notificări.
																			<br>
																			<br>
																			Establishment Labs garantează că datele dumneavoastră personale vor fi protejate și tratate conform Politicii noastre de Confidențialitate și reglementărilor în vigoare, inclusiv a Directivei 95/46/CE adoptată de către Parlamentul European, a Consiliului din data de 24 octombrie 1995 și a Legii privind Protecția Datelor (Regatul Unit).
																			<br>
																			<br>
																			<!-- Only if has implants -->
																			<?php if ( isset($is_q_inside) ? $is_q_inside : false ): ?>
																				Implanturile mamare conțin Q Inside Safety Technology ™, care permite medicilor să identifice numele producătorului, numărul de serie, numărul lotului, numărul lotului și alte date specifice implantului, din afara corpului, folosind un cititor de dispozitive portabile. 
																				<br>
																				<br>
																			<?php endif ?>
																			Acordăm prioritate maximă vieții private și confidențialității pacientului. Bazele de date care stochează datele privind dispozitivele și pacienții, adunate în urma procesului de înregistrare, sunt păstrate în centre de date sigure, respectând orientările stricte privind viața privată, impuse de legislația în vigoare și urmând aceleași protocoale de protejare a informațiilor, ce sunt implementate în spitale și centrele chirurgicale. Toate datele personale oferite vor fi divulgate numai așa cum s-a stabilit și cu autorizarea din partea pacientului. Nu vom transmite adresa de email sau informațiile personale fără acordul dumneavoastră. 
																			<br>
																			<br>
																			Conform documentului „Mărirea Sânilor cu Implantul Matrix<sup>®</sup> Motiva: informații pentru pacient”, vă rugăm să contactați chirurgul dumneavoastră imediact ce experimentați orice simptome neobișnuite care considerați că au legătură cu procedura chirurgicală de mărire a sânilor.
																			<br>
																			<br>

																			Amintiți-vă că implanturile dumneavoastră mamare Matrix® Motiva sunt acoperite de o garanție limitată, pe toată durata de viață a dispozitivului, ceea ce înseamnă că Establishment Labs vor oferi un implant de schimb pentru orice implant mamar Matrix<sup>®</sup> Motiva care suferă o ruptură. În plus, în cazul unui implant afectat de contractură capsulară Baker de gradele III sau IV, Establishment Labs pune la dispoziție Programul aferent Politicii de Înlocuire. Detalii complete privind garanția sunt prezentate în documentul Always Confident Warranty®, care vă poate fi oferit de către distribuitor sau care poate fi consultat la adresa <a style="text-decoration:none; color:#a38546;" href="https://motivaimplants.com">www.motivaimplants.com</a>. Pentru anumite piețe, este disponibil Programul <?=/*php*/$years/*php*/?>Y pentru Garanție Extinsă Motiva cu scopul de a oferi asistență financiară pentru operațiile repetate, în cazul unei rupturi, rotații de implant, dar și a contracturii capsulare Baker de gradele III sau IV. Acest program oferă acoperire pentru maxim <?=/*php*/$years/*php*/?> ani de la data operației.
																			<br>
																			<br>
																			<p style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546;">
																				<b>Dacă sunteți interesat de această garanție extinsă și ați dori să verificați dacă aceasta se aplică implanturilor dumneavoastră, puteți face acest lucru la</b>
																				<a style="text-decoration:none; color:#888;" href="https://register.motivaimagine.com/">register.motivaimagine.com</a>
																			</p>
																			<br>
																			<p align="center">
																				<a href="https://register.motivaimagine.com/" style="text-decoration:none; background: #a38546; color:#fff; font-weight: 300; padding:8px 14px; border-radius:30px"><b>Garanție Extinsă</b></a>
																			</p>
																			<br>
																			<br>
																			Lucrăm cu ultimele tehnologii pentru a vă oferi cele mai inovatoare implanturi de pe piață, având ca prioritate siguranța dumneavoastră și, cel mai important, o garanție etică pentru produse. Cu garanția noastră Always Confident Warranty<sup>®</sup>  și, în cadrul anumitor piețe prin intermediul Programului <?=/*php*/isset($years) ? $years : '{{years}}'/*php*/?>Y Motiva pentru Garanție Extinsă, am creat un sistem de suport fără egal... este disponibil pentru dumneavoastră când și dacă aveți nevoie. 
																			<br>
																			<br/>
																			<br/>
																			Fiți încrezătoare... Vă puteți baza pe Motiva!
																			<br>
																			<br>
																			Cu stimă,<br>
																			Juan José Chacón-Quirós<br>
																			Director General Executiv<br>
																			Establishment Labs<br>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="area-2" style="padding:15px 20px 5px 21px; background-color:#eeeeee;">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<table class="flexible" width="410" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-33" class="aligncenter">
													<a href="http://motivaimagine.com/"><img src="http://motivaimplants.com/letters/motivaimagine.png" border="0" style="vertical-align:top; padding: 0px 0px 12px 0px" width="210" height="37" alt="MotivaImagine" /></a>
												</td>
											</tr>
											<tr>
												<td mc:edit="block-35" class="aligncenter" style="font:9px/11px Helvetica, Arial, sans-serif; color:#fff; font-weight: 300;">
													<a style="text-decoration:none; color:#a1a1a1;" href="mailto:customerservice@motivaimplants.com">customerservice@motivaimplants.com</a> <br/>
													<a style="text-decoration:none; color:#a1a1a1; font-weight: 300;" href="http://motivaimplants.com/">www.motivaimplants.com</a> <br/>
												</td>
											</tr>                                                                   
										</table>
										<table class="flexible" width="160" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-36" width="160" class="aligncenter" style="padding:10px 0 5px 0; text-align: right;">
													<a href="https://www.facebook.com/motivaimagine/"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-facebook-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Urmăriți-ne pe Facebook" /></a> <a href="https://twitter.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-twitter-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Urmăriți-ne pe Twitter" /></a> <a href="https://instagram.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-instagram-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Urmăriți-ne pe Instagram" /></a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" style="padding:2px 10px 7px;">
				<table class="flexible" width="600" cellpadding="0" cellspacing="0" bgcolor="#eeeeee">
					<tr>
						<td class="no-float" align="center">
							<table class="center" cellpadding="0" cellspacing="0">
								<tr>
									<td style="font:normal 9px/11px Helvetica, Arial, sans-serif; color:#888888; text-align: center;">
										IfDacă ați primit acest email din greșeală, dacă informațiile dumneavoastră nu sunt corecte sau dacă nu sunteți de acord cu Politica de Confidențialitate și Termenii de Utilizare de pe pagina noastră de internet: <a style="text-decoration:none; color:#555555;" href="http://motivaimplants.com/">www.motivaimplants.com</a>.
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>