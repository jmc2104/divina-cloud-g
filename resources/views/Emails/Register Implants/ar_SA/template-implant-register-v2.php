<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Welcome to MotivaImagine</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style type="text/css">
		* {
			-webkit-text-size-adjust: none;
			-webkit-text-resize: 100%;
			text-resize: 100%;
		}
		.no-link a{
			text-decoration:none !important;
			color:#fff !important;
		}
		.no-link-2 a{
			text-decoration:none !important;
			color:#fff !important;
		}
		sup{vertical-align: super; line-height: 0; font-size: 0.8em;}
		@media only screen and (max-width:750px) {
			table[class="wrapper"]{min-width:320px !important;}
			table[class="flexible"]{width:100% !important;}
			td[class="img-flex"] img{
				width:100% !important;
				height:auto !important;
			}
			td[class="no-float"]{float:none !important;}
			table[class="center"]{margin:0 auto !important;}
			td[class="header"]{padding:5px 10px 0 !important;}
			td[class="area-1"]{padding:0 !important;}
			td[class="area-2"]{padding:15px 10px !important;}
			td[class="post"]{padding:0 0 20px;}
			td[class="post"] td[class="holder"]{
				padding:0 0 15px !important;
				height:auto !important;
			}
			td[class="aligncenter"],
			td[class="no-link"]{text-align:center !important;}
			td[class="col"],
			td[class="sidebar"]{padding:10px !important;}
			td[class="col"] td[class="holder"],
			td[class="sidebar"] td[class="text"]{padding:10px 0 0 !important;}
			td[class="col"] td[class="holder-2"]{height:auto !important;}
		}
		.tg  {border-collapse:collapse;border-spacing:0;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg .tg-d5wv{background-color:#a38546;color:#ffffff;text-align:center;vertical-align:top}
		.tg .tg-yw4l{vertical-align:top}
	</style>
</head>
<body style="margin:0; padding:0;" bgcolor="#eeeeee" link="#f5f5f5" >
	<table class="wrapper" width="100%" cellspacing="0" cellpadding="0">
		<tr><td height="10" style="font-size:0; line-height:0;" >&nbsp;</td></tr>
		<tr>
			<td align="center" style="padding:10px 5px 26px 5px;">
				<table class="flexible" width="750" cellpadding="0" cellspacing="0">
					<tr>
						<td class="area-1" style="border-radius: 6px 6px 3px 3px" bgcolor="#ffffff">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td mc:edit="block-3" class="img-flex" style="padding:0 0 8px;">
										<img src="http://motivaimplants.com/letters/cover.jpg" style="vertical-align:top; border-radius: 6px 6px 0px 0px" width="750" height="400" alt="Motiva Implants Logo" />
									</td>
								</tr>
								<tr>
									<td style="padding:20px 20px 30px 20px;">
										<table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td class="post">
													<table width="100%" cellpadding="0" cellspacing="0">
														<tr>
															<td class="holder" height="70" valign="top">
																<table width="100%" cellpadding="0" cellspacing="0">
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			شريكك محل الثقة
																			Motiva Always Confident Warranty®
																		</td>
																	</tr>
																	<tr><!-- MAIN CONTENT -->
																		<td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
																			لعزيزة  <?=/*php*/$name/*php*/?>
																			<span style="float: right; color:#a1a1a1;"><?=/*php*/date('d-M-Y')/*php*/?></span>
																			<br/>
																			<br/>
																			 مبروك! تم بنجاح تسجيل حشوة صدر موتيفا الخاصة بك<br><br>
                                                            		     	<br>
																			<br>
																			الرجاء قراءة المعلومات ادناه والتأكد من تطابقها مع المعلومات على بطاقتك الائتمانية. 
																			<br/>
																			<br/>
																			<!-- Implant Information -->
																			<table width="100%" cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						 <?php if ($hasLeftImplant):?>
															                                 <table class="flexible" width="345" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
															                                    <tr>
															                                       <td class="post">
															                                          <table width="100%" cellpadding="0" cellspacing="0">
															                                             <tr>
															                                                <td class="holder" height="100" valign="top">
															                                                   <table width="100%" cellpadding="0" cellspacing="0">
															                                                     <tr>
															                                                         <td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#222222; padding:0 0 10px;">
															                                                            حشوة الجانب الايسر                                                                                         
															                                                         </td>
															                                                      </tr>
															                                                      <tr>                                                          
															                                                         <td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666;">
															                                                            <span style="color:#222; font-weight: normal;">رقم التسلسل: :</span><?php echo ($snL)?><br>
															                                                            <span style="color:#222; font-weight: normal;">الاشارة: </span> <?php echo ($referenceL)?><br>
															                                                            <span style="color:#222; font-weight: normal;">الحجم : </span> <?php echo ($volumeL)?> cc<br>
															                                                         </td>
															                                                      </tr>
															                                                   </table>
															                                                </td>
															                                             </tr>
															                                          </table>
															                                       </td>
															                                    </tr>
															                                 </table>
															                                <?php endif ?>
																						 <?php if ($hasRightImplant):?>
															                                 <table class="flexible" width="345" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
															                                    <tr>
															                                       <td class="post">
															                                          <table width="100%" cellpadding="0" cellspacing="0">
															                                             <tr>
															                                                <td class="holder" height="100" valign="top">
															                                                   <table width="100%" cellpadding="0" cellspacing="0">
															                                                      <tr>
															                                                         <td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#222222; padding:0 0 10px;">
															                                                            الجانب الأيمن الحشو
															                                                         </td>
															                                                      </tr>
															                                                      <tr>
															                                                         <td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666;">
															                                                            <span style="color:#222; font-weight: normal;">رقم التسلسل: :</span><?php echo ($snR)?><br>
															                                                            <span style="color:#222; font-weight: normal;">الاشارة: </span> <?php echo ($referenceR)?><br>
															                                                            <span style="color:#222; font-weight: normal;">الحجم :</span> <?php echo ($volumeR)?> cc<br>
															                                                         </td>
															                                                      </tr>
															                                                   </table>
															                                                </td>
															                                             </tr>
															                                          </table>
															                                       </td>
															                                    </tr>
															                                 </table>
															                                <?php endif ?>
																					</td>
																				</tr>
																			</table>
																			<br>
																			<br>
																			سوف يتم ادخال المعلومات في تتبع مختبر المؤسسة Establishment Labs Traceability وتؤكد لك المختبرات، كحامل لجهاز طبي او أجهزة طبية التي استلمتها ، ان بإمكانك معرفة مكان واستلام اشعارات للمناسبات.
 
																			<br>
																			<br>
																			تؤكد لك مختبرات المؤسسة ان معلوماتك الشخصية ستكون محمية ويتم التعامل بها طبقاً لسياسة خصوصية المعلومات وكل الاحكام السارية المفعول، من ضمنها الامر 95/46/EC  للمجلس الأوربي المؤرخ في 24 تشرين الأول 1995 ، وقانون حماية البيانات (المملكة المتحدة).

																			<br>
																			<br>
																			<!-- Only if has implants -->
																			<?php if ( isset($is_q_inside) ? $is_q_inside : false ): ?>
																				تحتوي غرسات الثدي على تقنية Q Inside Safety™ التي تسمح للأطباء بالتأكد من اسم الشركة المصنعة والرقم التسلسلي ورقم القسيمة ورقم الدفعة وغير ذلك من البيانات الخاصة بالزرعات ، من خارج الجسم باستخدام قارئ يدوي خاص.
																				<br>
																				<br>
																			<?php endif ?>
																			من اهم اولوياتنا هي خصوصية وسرية المريض. يتم حفظ بيانات المريض التي تجمع من عملية التسجيل في مراكز بيانات امينة والتي تتبع القواعد الارشادية للخصوصية التي يضعها التشريع الساري على خصوصية المعلومات، اتباعاً لتنظيمات امن المعلومات النافذة في المستشفيات والمراكز الجراحية. لا يتم اعلان المعلومات الا بإذن من المريض. نحن لا نشارك عنون بريدك الالكتروني او أي معلومة شخصية بدون اذنك.
																			<br>
																			<br>
																			وفقاً لمعلومات حشوة تكبير الثدي Motiva Implant Matrix®: موتيفا  يمكنك الاتصال الخاص مع جراحك حال شعورك بأعراض غير متوقعة تتعلق بعملية زيادة حجم الثدي التي اجريتيها.

																			<br>
																			<br>
																			تذكري ان حشوة زيادة حجم الثدي موتيفا Motiva Implant Matrix<sup>®</sup>  تحت تغطية ضمان محدود ومدى الحياة والذي يشترط على مختبرات المؤسسة استبدال الحشوة تحت علامتنا التجارية اذا حصل تمزق لها. بالإضافة الى ذلك اذا حدث انكماش للحشوة، لدى مختبرات المؤسسة برنامج سياسة الاستبدال . يوجد وصف كامل للضمان في الوثيقة المعنونة ضمان الثقة الدائمة Confident Warranty<sup>®</sup>, Always  والتي يمكن ان يجهزها .. واتي يمكن عرضها وقراءتها على الموقع الالكتروني. <a style="text-decoration:none; color:#a38546;" href="https://motivaimplants.com">www.motivaimplants.com</a>. في اختيار الأسواق، نعرض عليك  برنامج الضمان <?=/*php*/$years/*php*/?>سنوات لتوفير المساعدة المالية للجراحة التصحيحية في حالة تمزق الحشوة او أصابها الانكماش بدرجات Baker IIIاو IV  البرنامج يوفر غطاء لجانبي العملية.
																			<br>
																			<br>
																			<p style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546;">
																				<b><p style="color:#c89c3c;">
																				اذا اثار اهتمامك التسجيل في الضمان الممتد وتودين معرفة اذا كان البرنامج ينطبق عليك انظمي الى البرنامج
																			</p></b>
																				<a style="text-decoration:none; color:#888;" href="https://register.motivaimagine.com/">register.motivaimagine.com</a>
																			</p>
																			<br>
																			<p align="center">
																				<a href="https://register.motivaimagine.com/" style="text-decoration:none; background: #a38546; color:#fff; font-weight: 300; padding:8px 14px; border-radius:30px"><b>الضمان الممتد</b></a>
																			</p>
																			<br>
																			<br>
																			نحن نعمل مع احدث التقنيات والابتكارات لنعرض عليك احدث طراز من الحشوات في السوق مع أولوية، والاهم، ضمان منتوج أخلاقي. مع برنامح ضمان الثقة الدائمة في الأسواق مع برنامج الضمان الممتد لمدة سنتين  Motiva Extended Warranty Program ووضعنا نظام دعم معك دائما عند الحاجة
																			<br>
																			<br/>
																			<br/>
																			وني على ثقة ... لديك موتيفا
																			<br>
																			<br>
																			المخلص,<br>
																			Juan José Chacón-Quirós<br>
																			المدير التنفيذي<br>
																			ختبرات المؤسسة<br>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="area-2" style="padding:15px 20px 5px 21px; background-color:#eeeeee;">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<table class="flexible" width="410" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-33" class="aligncenter">
													<a href="http://motivaimagine.com/"><img src="http://motivaimplants.com/letters/motivaimagine.png" border="0" style="vertical-align:top; padding: 0px 0px 12px 0px" width="210" height="37" alt="MotivaImagine" /></a>
												</td>
											</tr>
											<tr>
												<td mc:edit="block-35" class="aligncenter" style="font:9px/11px Helvetica, Arial, sans-serif; color:#fff; font-weight: 300;">
													<a style="text-decoration:none; color:#a1a1a1;" href="mailto:customerservice@motivaimplants.com">customerservice@motivaimplants.com</a> <br/>
													<a style="text-decoration:none; color:#a1a1a1; font-weight: 300;" href="http://motivaimplants.com/">www.motivaimplants.com</a> <br/>
												</td>
											</tr>                                                                   
										</table>
										<table class="flexible" width="160" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-36" width="160" class="aligncenter" style="padding:10px 0 5px 0; text-align: right;">
													<a href="https://www.facebook.com/motivaimagine/"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-facebook-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="تابعنا على الفيسبوك" /></a> <a href="https://twitter.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-twitter-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="تابعنا على تويتر" /></a> <a href="https://instagram.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-instagram-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="تابعنا على الانستغرام" /></a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" style="padding:2px 10px 7px;">
				<table class="flexible" width="600" cellpadding="0" cellspacing="0" bgcolor="#eeeeee">
					<tr>
						<td class="no-float" align="center">
							<table class="center" cellpadding="0" cellspacing="0">
								<tr>
									<td style="font:normal 9px/11px Helvetica, Arial, sans-serif; color:#888888; text-align: center;">
										  وصلك  هذا الايميل عن طريق الخطأ، اذا  كانت معلواتك غير صحيحة او لم  توافق على سياسة الخصوصية وشروط الاستخدام في موقعنا الالكتروني: <a style="text-decoration:none; color:#111111;" href="http://motivaimplants.com/">www.motivaimplants.com</a>.
                           			</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>