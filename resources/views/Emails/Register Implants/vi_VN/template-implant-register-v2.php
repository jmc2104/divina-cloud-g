<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Welcome to MotivaImagine</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style type="text/css">
		* {
			-webkit-text-size-adjust: none;
			-webkit-text-resize: 100%;
			text-resize: 100%;
		}
		.no-link a{
			text-decoration:none !important;
			color:#fff !important;
		}
		.no-link-2 a{
			text-decoration:none !important;
			color:#fff !important;
		}
		sup{vertical-align: super; line-height: 0; font-size: 0.8em;}
		@media only screen and (max-width:750px) {
			table[class="wrapper"]{min-width:320px !important;}
			table[class="flexible"]{width:100% !important;}
			td[class="img-flex"] img{
				width:100% !important;
				height:auto !important;
			}
			td[class="no-float"]{float:none !important;}
			table[class="center"]{margin:0 auto !important;}
			td[class="header"]{padding:5px 10px 0 !important;}
			td[class="area-1"]{padding:0 !important;}
			td[class="area-2"]{padding:15px 10px !important;}
			td[class="post"]{padding:0 0 20px;}
			td[class="post"] td[class="holder"]{
				padding:0 0 15px !important;
				height:auto !important;
			}
			td[class="aligncenter"],
			td[class="no-link"]{text-align:center !important;}
			td[class="col"],
			td[class="sidebar"]{padding:10px !important;}
			td[class="col"] td[class="holder"],
			td[class="sidebar"] td[class="text"]{padding:10px 0 0 !important;}
			td[class="col"] td[class="holder-2"]{height:auto !important;}
		}
		.tg  {border-collapse:collapse;border-spacing:0;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg .tg-d5wv{background-color:#a38546;color:#ffffff;text-align:center;vertical-align:top}
		.tg .tg-yw4l{vertical-align:top}
	</style>
</head>
<body style="margin:0; padding:0;" bgcolor="#eeeeee" link="#f5f5f5" >
	<table class="wrapper" width="100%" cellspacing="0" cellpadding="0">
		<tr><td height="10" style="font-size:0; line-height:0;" >&nbsp;</td></tr>
		<tr>
			<td align="center" style="padding:10px 5px 26px 5px;">
				<table class="flexible" width="750" cellpadding="0" cellspacing="0">
					<tr>
						<td class="area-1" style="border-radius: 6px 6px 3px 3px" bgcolor="#ffffff">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td mc:edit="block-3" class="img-flex" style="padding:0 0 8px;">
										<img src="http://motivaimplants.com/letters/cover.jpg" style="vertical-align:top; border-radius: 6px 6px 0px 0px" width="750" height="400" alt="Motiva Implants Logo" />
									</td>
								</tr>
								<tr>
									<td style="padding:20px 20px 30px 20px;">
										<table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td class="post">
													<table width="100%" cellpadding="0" cellspacing="0">
														<tr>
															<td class="holder" height="70" valign="top">
																<table width="100%" cellpadding="0" cellspacing="0">
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Chào mừng bạn đến MotivaImagine
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Đối tác đáng tin cậy của bạn
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Motiva Always Confident Warranty&reg;
																		</td>
																	</tr>
																	<tr><!-- MAIN CONTENT -->
																		<td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
																			Kính gửi Bà. <?=/*php*/$name/*php*/?>
																			<span style="float: right; color:#a1a1a1;"><?=/*php*/date('d-M-Y')/*php*/?></span>
																			<br/>
																			<br/>
																			Xin chúc mừng, bà đã đăng ký thành công Mô cấy Motiva của mình!
																			<br>
																			<br>
																			Vui lòng đọc kỹ thông tin được cung cấp bên dưới và đảm bảo rằng nó khớp với thông tin trong Thẻ ID Motiva cá nhân của bà.
																			<br/>
																			<br/>
																			<!-- Implant Information -->
																			<table width="100%" cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<?php if ( $has_left_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="left">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										Mô cấy Bên Trái                                  
																									</td>
																								</tr>
																								<tr>                                                          
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">Số Sê-ri:</span><?=/*php*/$left_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Tham chiếu:</span><?=/*php*/$left_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Khối lượng:</span><?=/*php*/$left_vol/*php*/?> cc<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																						<?php if ( $has_right_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="right">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										Mô cấy Bên Phải
																									</td>
																								</tr>
																								<tr>
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">Số Sê-ri:</span><?=/*php*/$right_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Tham chiếu:</span><?=/*php*/$right_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Khối lượng:</span><?=/*php*/$right_vol/*php*/?> cc<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																					</td>
																				</tr>
																			</table>
																			<br>
																			<br>
																			Thông tin do bà cung cấp sẽ được nhập vào Hệ thống Truy xuất Nguồn gốc của Establishment Labs, mà đảm bảo rằng, với vai trò là người có (các) thiết bị y tế mà bà nhận được, bà có thể được xác định đúng địa chỉ trong trường hợp cần thông báo.  
																			<br>
																			<br>
																			Establishment Labs đảm bảo rằng thông tin cá nhân của bà sẽ được bảo vệ và xử lý tuân thủ theo Chính sách về Quyền riêng tư của chúng tôi và các quy định hiện hành, bao gồm Chỉ thị 95/46/EC của Nghị viện và Hội đồng Châu Âu ngày 24/10/1995, và Đạo luật Bảo vệ Dữ liệu (UK).
																			<br>
																			<br>
																			<!-- Only if has implants -->
																			<?php if ( isset($is_q_inside) ? $is_q_inside : false ): ?>
																				Cấy ghép vú của bạn bao hàm Q Inside Safety Technology<sup>™</sup>, cho phép các bác sĩ để xác định tên nhà sản xuất, số serial, số lô, số lô, số liệu cụ thể cấy ghép khác, từ bên ngoài cơ thể sử dụng một đầu đọc cầm tay độc quyền.
																				<br>
																				<br>
																			<?php endif ?>
																			Chúng tôi ưu tiên cao nhất đến quyền riêng tư và tính bảo mật của bệnh nhân. Các cơ sở dữ liệu lưu trữ các thiết bị và dữ liệu bệnh nhân được thu thập từ quá trình đăng ký, được lưu trữ trong các trung tâm dữ liệu bảo mật, tuân thủ theo những hướng dẫn về quyền riêng tư nghiêm ngặt theo quy định của các quy định hiện hành, tuân thủ theo các giao thức an ninh thông tin tương tự tại chỗ trong các bệnh viện và các trung tâm phẫu thuật. Tất cả các dữ liệu cá nhân được cung cấp chỉ được tiết lộ theo thỏa thuận và có sự cho phép của bệnh nhân. Chúng tôi không chia sẻ địa chỉ email hoặc thông tin cá nhân của bà mà không có sự đồng ý của bà.
																			<br>
																			<br>
																			Phù hợp với 'Chương trình Nâng Ngực với Motiva Implant Matrix<sup>®</sup>: Thông tin cho Bệnh nhân, vui lòng liên hệ với bác sĩ phẫu thuật của bà ngay lập tức nếu bà gặp phải bất kỳ triệu chứng không mong đợi nào mà bà nghĩ là có liên quan đến quy trình phẫu thuật nâng ngực của mình.
																			<br>
																			<br>
																			Hãy nhớ rằng mô cấy ngực Motiva Implant Matrix<sup>®</sup>® của bà được bảo hiểm theo tuổi thọ giới hạn của thiết bị, chương trình bảo hành trong đó quy định rằng Establishment Labs sẽ cung cấp một mô cấy thay thế cho bất kỳ mô cấy ngực Motiva Implant Matrix<sup>®</sup> nào bị vỡ. Ngoài ra, trong trường hợp một mô cấy bị ảnh hưởng bởi sự co cứng nang Baker Cấp III hoặc IV, Establishment Labs sẽ cung cấp Chương trình Chính sách Thay thế. Thông tin chi tiết đầy đủ về chương trình bảo hành được mô tả trong tài liệu Always Confident Warranty<sup>®</sup>®, như có thể được cung cấp bởi nhà phân phối của chúng tôi hoặc cũng có thể được xem xét rên <a style="text-decoration:none; color:#a38546;" href="https://motivaimplants.com">www.motivaimplants.com</a>.
																			<br>
																			Tại các thị trường được chọn, chúng tôi cung cấp Chương trình Bảo hành Gia hạn <?=/*php*/$years/*php*/?> năm nhằm cung cấp sự hỗ trợ tài chính cho các cuộc phẫu thuật hiệu chỉnh trong trường hợp vỡ mô cấy, luân chuyển mô cấy và co cứng nang Baker Cấp III hoặc IV. Chương trình này cung cấp phạm vi bảo hiểm lên đến <?=/*php*/$years/*php*/?> năm kể từ ngày phẫu thuật. 
																			<br>
																			<br>
																			<p style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546;">
																				<b>Nếu bà quan tâm đến chương trình bảo hành gia hạn này và muốn xác minh xem nó có áp dụng cho các mô cấy của bà không, bà có thể xem tại</b>
																				<a style="text-decoration:none; color:#888;" href="https://register.motivaimagine.com/">register.motivaimagine.com</a>
																			</p>
																			<br>
																			<p align="center">
																				<a href="https://register.motivaimagine.com/" style="text-decoration:none; background: #a38546; color:#fff; font-weight: 300; padding:8px 14px; border-radius:30px"><b>Cam kết Bảo hành Gia hạn</b></a>
																			</p>
																			<br>
																			<br>
																			Chúng tôi làm việc bằng các công nghệ mới nhất nhằm cung cấp cho bà những mô cấy đổi mới nhất trên thị trường cùng với việc ưu tiên đến sự an toàn của bà, và quan trọng nhất là một chương trình bảo hành sản phẩm có đạo đức. Với chương trình Always Confident Warranty<sup>®</sup> và trong các thị trường được chọn với Chương trình Bảo hành Gia hạn Motiva <?=/*php*/isset($years) ? $years : '{{years}}'/*php*/?> năm của chúng tôi, chúng tôi đã tạo ra một hệ thống hỗ trợ vượt trội... luôn sẵn sàng hỗ trợ bà khi và nếu bà cần.
																			<br>
																			<br/>
																			<br/>
																			Hãy Tự tin... Vì đã có Motiva!
																			<br>
																			<br>
																			Trân trọng,<br>
																			Juan José Chacón-Quirós<br>
																			CEO<br>
																			Establishment Labs<br>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="area-2" style="padding:15px 20px 5px 21px; background-color:#eeeeee;">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<table class="flexible" width="410" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-33" class="aligncenter">
													<a href="http://motivaimagine.com/"><img src="http://motivaimplants.com/letters/motivaimagine.png" border="0" style="vertical-align:top; padding: 0px 0px 12px 0px" width="210" height="37" alt="MotivaImagine" /></a>
												</td>
											</tr>
											<tr>
												<td mc:edit="block-35" class="aligncenter" style="font:9px/11px Helvetica, Arial, sans-serif; color:#fff; font-weight: 300;">
													<a style="text-decoration:none; color:#a1a1a1;" href="mailto:customerservice@motivaimplants.com">customerservice@motivaimplants.com</a> <br/>
													<a style="text-decoration:none; color:#a1a1a1; font-weight: 300;" href="http://motivaimplants.com/">www.motivaimplants.com</a> <br/>
												</td>
											</tr>                                                                   
										</table>
										<table class="flexible" width="160" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-36" width="160" class="aligncenter" style="padding:10px 0 5px 0; text-align: right;">
													<a href="https://www.facebook.com/motivaimagine/"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-facebook-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Theo dõi chúng tôi trên Facebook" /></a> <a href="https://twitter.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-twitter-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Theo dõi chúng tôi trên Twitter" /></a> <a href="https://instagram.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-instagram-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Theo dõi chúng tôi trên Instagram" /></a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" style="padding:2px 10px 7px;">
				<table class="flexible" width="600" cellpadding="0" cellspacing="0" bgcolor="#eeeeee">
					<tr>
						<td class="no-float" align="center">
							<table class="center" cellpadding="0" cellspacing="0">
								<tr>
									<td style="font:normal 9px/11px Helvetica, Arial, sans-serif; color:#888888; text-align: center;">
										Nếu bà nhận được email này bị lỗi, nếu thông tin của bà không chính xác, hoặc nếu bà không đồng ý với Chính sách về Quyền riêng tư và các Điểm khoản Sử dụng có trên webside của chúng tôi: <a style="text-decoration:none; color:#555555;" href="http://motivaimplants.com/">www.motivaimplants.com</a>.
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>