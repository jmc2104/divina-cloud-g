﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <title>Motiva Implants Matrix</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <style type="text/css">
         * {
         -webkit-text-size-adjust: none;
         -webkit-text-resize: 100%;
         text-resize: 100%;
         }
         .no-link a{
         text-decoration:none !important;
         color:#fff !important;
         }
         .no-link-2 a{
         text-decoration:none !important;
         color:#fff !important;
         }
         sup{vertical-align: super; font-size: x-small;}
         @media only screen and (max-width:750px) {
         table[class="wrapper"]{min-width:320px !important;}
         table[class="flexible"]{width:100% !important;}
         td[class="img-flex"] img{
         width:100% !important;
         height:auto !important;
         }
         td[class="no-float"]{float:none !important;}
         table[class="center"]{margin:0 auto !important;}
         td[class="header"]{padding:5px 10px 0 !important;}
         td[class="area-1"]{padding:10px 10px !important;}
         td[class="area-2"]{padding:15px 10px !important;}
         td[class="post"]{padding:0 0 20px;}
         td[class="post"] td[class="holder"]{
         padding:0 0 15px !important;
         height:auto !important;
         }
         td[class="aligncenter"],
         td[class="no-link"]{text-align:center !important;}
         td[class="col"],
         td[class="sidebar"]{padding:10px !important;}
         td[class="col"] td[class="holder"],
         td[class="sidebar"] td[class="text"]{padding:10px 0 0 !important;}
         td[class="col"] td[class="holder-2"]{height:auto !important;}
         }
      </style>
   </head>
   <body style="margin:0; padding:0;" bgcolor="#ffffff" link="#f5f5f5" >
      <table class="wrapper" width="100%" cellspacing="0" cellpadding="0" style="background-color: #ffffff; background-repeat:repeat;">
         <tr>
            <td height="10" style="font-size:0; line-height:0;" >&nbsp;</td>
         </tr>
         <tr>
            <td align="center" style="padding:0 5px 26px;">
               <table class="flexible" width="750" cellpadding="0" cellspacing="0">
                  <tr>
                     <td class="area-1" style="padding:10px 0px 12px; background-image:url(http://motivaimplants.com/letters/images/gold-bg.png); background-size: 750px 63px; background-repeat: no-repeat; border-radius:2px" bgcolor="#ffffff">
                        <table width="100%" cellpadding="0" cellspacing="0">
                           <tr>
                              <td class="header">
                                 <table class="flexible" width="750" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td mc:edit="block-1" class="aligncenter" align="center" style="padding:0 0 12px;">
                                          <a href="http://motivaimplants.com/"><img src="http://motivaimplants.com/letters/images/motiva.png" border="0" style="vertical-align:top;" width="135" height="45" alt="Motiva Implant Matrix" /></a>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td mc:edit="block-3" class="img-flex" style="padding:0 0 8px;">
                                 <img src="http://motivaimplants.com/letters/images/img-regular-implant.jpg" style="vertical-align:top; border-radius: 0px 0px 2px 2px" width="750" height="250" alt="Your Trusted Partner: Motiva Always Confident Warranty" />
                              </td>
                           </tr>
                           <tr>
                              <td style="padding:0 20px 30px;">
                                 <table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td class="post">
                                          <table width="100%" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td class="holder" height="70" valign="top">
                                                   <table width="100%" cellpadding="0" cellspacing="0">
                                                      <tr>
                                                         <td mc:edit="block-4" style="font:12px/16px Helvetica, Arial, sans-serif; font-weight: 300; color:#9c9c9c; padding:0 0 10px; ">
                                                            Tarih: <?php echo ($today)?>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td mc:edit="block-4" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#222; font-weight: normal; padding:0 0 10px; ">
                                                            Sayın. <?php echo ($name)?>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
                                                            Tebrikler, Motiva İmplantlarınızı  başarıyla kaydettiniz!
                                                            <br>
                                                            <br>
                                                            Lütfen aşağıda verilen bilgileri okuyun ve bu bilgilerin kişisel Motiva Kimlik Kartı'nızdaki bilgilerle eşleştiğinden emin olun.                                                                    
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td style="padding:0 20px 42px;">
                               <?php if ($hasLeftImplant):?>
                                 <table class="flexible" width="345" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td class="post">
                                          <table width="100%" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td class="holder" height="100" valign="top">
                                                   <table width="100%" cellpadding="0" cellspacing="0">
                                                     <tr>
                                                         <td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#222222; padding:0 0 10px;">
                                                            Sol Taraftaki İmplant           
                                                         </td>
                                                      </tr>
                                                      <tr>                                                          
                                                         <td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666;">
                                                            <span style="color:#222; font-weight: normal;">Seri Numarası:</span><?php echo ($snL)?><br>
                                                            <span style="color:#222; font-weight: normal;">Referans:</span> <?php echo ($referenceL)?><br>
                                                            <span style="color:#222; font-weight: normal;">Hacim:</span> <?php echo ($volumeL)?> cc<br>
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                                <?php endif ?>
                                <?php if ($hasRightImplant):?>
                                 <table class="flexible" width="345" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td class="post">
                                          <table width="100%" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td class="holder" height="100" valign="top">
                                                   <table width="100%" cellpadding="0" cellspacing="0">
                                                      <tr>
                                                         <td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#222222; padding:0 0 10px;">
                                                            Sağ Taraftaki İmplantc
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666;">
                                                            <span style="color:#222; font-weight: normal;">Sağ Taraftaki İmplant:</span><?php echo ($snR)?><br>
                                                            <span style="color:#222; font-weight: normal;">Referans:</span> <?php echo ($referenceR)?><br>
                                                            <span style="color:#222; font-weight: normal;">Hacim:</span> <?php echo ($volumeR)?> cc<br>
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                                <?php endif ?>
                              </td>
                           </tr>
                           <tr>
                              <td style="padding:0 20px 30px;">
                                 <table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td class="post">
                                          <table width="100%" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td class="holder" height="70" valign="top">
                                                   <table width="100%" cellpadding="0" cellspacing="0">
                                                      <tr>
                                                         <td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
                                                             Ver diğiniz bilgiler Establishment Labs İzleme Sistemi'ne eklenecektir, böylece aldığınız tıbbı cihaz(lar)ın sahibi olarak sizi bilgilendirmemiz gereken bir durum olduğunda sizinle kolayca iletişime geçebileceğiz.
                                                              <br>
                                                              <br>
                                                             Establishment Labs kişisel bilgilerinizin korunacağı ve Gizlilik Politikası ile Avrupa Parlamentosu ve Konsülü'nün 24 Ekim 1995 tarihli 95/46/AK Yönetmeliği ve Veri Güvenliği Yasası (Birleşik Krallık) dahil olmak üzere ilgili düzenlemelere uygun olarak muamele edileceği konusunda güvence vermektedir.
                                                              <br>
                                                              <br>
                                                            <?php if ($years == 5):?>
                                                             Göğüs implantları, doktorların üreticinin adını, seri numarasını, parti numarasını, parti numarasını ve diğer implanta özgü verileri tescilli el okuyucusundan bulmasını sağlayan Q Inside Safety Technology<sup>™</sup> içerir.
                                                              <br>
                                                              <br>
                                                            <?php endif ?>  
                                                             Hasta gizliliğine en yüksek önemi vermekteyiz. Kayıt süreci sırasında cihaz ve hastadan alınan bilgilerin saklandığı veri tabanları, ilgili yasanın zorunlu kıldığı katı gizlilik prensiplerine tabi olan ve hastaneler ile cerrahi müdahale merkezlerinde halihazırda uygulanan güvenlik protokollerini takip eden güvenli veri merkezlerinde tutulmaktadır. Verilen tüm kişisel bilgiler yalnızca anlaşma yapıldığı şekilde ve hastanın verdiği yetkiye göre paylaşılmaktadır. Sizin rızanız olmadan e-posta adresinizi veya kişisel bilgilerinizi paylaşmıyoruz.
                                                             <br>
                                                             <br>
                                                             Breast Augmentation with Motiva Implant Matrix<sup>®</sup> ile uyumlu olarak: Hasta İçin Gerekli Bilgi, lütfen cerrahi göğüs büyütme müdahaleniz ile ilgili olduğunu düşündüğünüz, beklemediğiniz herhangi bir semptomla karşılaşmanız durumunda derhal cerrahınızla iletişime geçin.
                                                            <br>
                                                            <br>
                                                             Motiva Implant Matrix<sup>®</sup> göğüs implantlarınız sınırlı, cihazın kullanım ömrü boyunca yırtılan herhangi bir Motiva Implant Matrix<sup>®</sup> göğüs implantı için Establishment Labs'in yedek bir ürün vermesini sağlayacak bir garanti kapsamı altında olduğunu aklınızda bulundurun. Buna ek olarak, Baker Grades III veya IV seviyesinde kapsül kontraktüründen etkilenen bir implant söz konusu olduğunda Establishment Labs sizi Değiştirme Politikası Programı'ndan faydalandıracaktır. Garantinin tüm ayrıntıları Always Confident Warranty<sup>®</sup>belgesinde belirtilmekte olup, distribütörümüz tarafından verilebilir ya da <a href="http://www.motivaimplants.com">www.motivaimplants.com</a> adresinde incelenebilir. <br>

                                                             Seçtiğimiz bazı pazarlarda implant yırtığı, implant rotasyonu ve Baker III veya IV seviyesinde kapsül kontraktürü olması durumunda revizyon cerrahisi için finansal destek sağlamak üzere <?php echo ($years)?>yıllık Motiva Genişletilmiş Garanti Programı'mızı sunuyoruz. Bu program cerrahi müdahale tarihinden itibaren <?php echo ($years)?> yıla kadar kapsama sunmaktadır. 
                                                             <br>

                                                                    <b><p style="color:#c89c3c;">Eğer bu genişletilmiş garanti ilginizi çektiyse ve implantlarınız için geçerli olup olmadığını öğrenmek istiyorsanız, şu adresten bunu kontrol edebilirsiniz: </p></b><br>
                                                                    <p align="center"><a  href="https://register.motivaimagine.com/"></font><img src="http://motivaimplants.com/letters/images/Warranty-Mail-Button.jpg"><br>(Garantiyi Genişlet)</a></p>.<br><br>

                                                            Size piyasada bulunan en yenilikçi implantları sunabilmek amacıyla, güvenliğinizi öncelik olarak ele alıp, en son teknolojileri kullanarak çalışıyor, ve en önemlisi ahlak boyutu olan bir ürün garantisi sunuyoruz. Always Confident Warranty<sup>®</sup> ve seçtiğimiz pazarlarda sunulan <?php echo ($years)?>yıllık Motiva Genişletilmiş Garanti Programı ile eşi benzeri olmayan bir destek sistemi oluşturduk... İstediğiniz her zaman, ihtiyacınız olduğunda yanınızda.
                                                            <br>
                                                            <br>
                                                            <br>
                                                            Kendinize Güven Duyun... Motiva'nız var!
                                                            <br>
                                                            <br>
                                                            Saygılarımla,
                                                            <br>
                                                            Juan José Chacón-Quirós
                                                            <br>
                                                            CEO
                                                            <br>
                                                            Establishment Labs
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td class="area-2" style="padding:15px 20px 5px 21px; background-color:#652d89; border-radius:2px">
                        <table width="100%" cellpadding="0" cellspacing="0">
                           <tr>
                              <td>
                                 <table class="flexible" width="410" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td mc:edit="block-33" class="aligncenter">
                                          <a href="http://motivaimplants.com/"><img src="http://motivaimplants.com/letters/images/motivaimagine.png" border="0" style="vertical-align:top;" width="220" height="37" alt="MotivaImagine" /></a>
                                       </td>
                                    </tr>
                                  </table>
                                 <table class="flexible" width="160" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
                                          <a href="https://www.facebook.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-facebook.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Facebook Icon" /></a>
                                       </td>
                                       <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
                                          <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://www.facebook.com/motivaimplants">Bizi Facebook'ta Takip Edin</a>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
                                          <a href="https://twitter.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-twitter.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Twitter Icon" /></a>
                                       </td>
                                       <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
                                          <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://twitter.com/motivaimplants">Bizi Twitter'da Takip Edin</a>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
                                          <a href="https://instagram.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-instagram.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Instagram Icon" /></a>
                                       </td>
                                       <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
                                          <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://instagram.com/motivaimplants">Bizi Instagram'da Takip Edin</a>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td align="center" style="padding:2px 10px 7px;">
               <table class="flexible" width="600" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                  <tr>
                     <td class="no-float" align="center">
                        <table class="center" cellpadding="0" cellspacing="0">
                           <tr>
                              <td style="font:normal 9px/11px Helvetica, Arial, sans-serif; color:#888888;">
                                 Bu e-postayı bir hata sonucu aldıysanız, eğer kişisel bilgileriniz doğru değilse veya Gizlilik Politikası ile web sayfamızda bulunan Kullanım Koşulları'nı onaylamıyorsanız: <a style="text-decoration:none; color:#111111;" href="http://motivaimplants.com/">www.motivaimplants.com</a>.
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
   </body>
</html>