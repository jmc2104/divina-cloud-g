﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <title>Motiva Implants Matrix</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <style type="text/css">
         * {
         -webkit-text-size-adjust: none;
         -webkit-text-resize: 100%;
         text-resize: 100%;
         }
         .no-link a{
         text-decoration:none !important;
         color:#fff !important;
         }
         .no-link-2 a{
         text-decoration:none !important;
         color:#fff !important;
         }
         sup{vertical-align: super; font-size: x-small;}
         @media only screen and (max-width:750px) {
         table[class="wrapper"]{min-width:320px !important;}
         table[class="flexible"]{width:100% !important;}
         td[class="img-flex"] img{
         width:100% !important;
         height:auto !important;
         }
         td[class="no-float"]{float:none !important;}
         table[class="center"]{margin:0 auto !important;}
         td[class="header"]{padding:5px 10px 0 !important;}
         td[class="area-1"]{padding:10px 10px !important;}
         td[class="area-2"]{padding:15px 10px !important;}
         td[class="post"]{padding:0 0 20px;}
         td[class="post"] td[class="holder"]{
         padding:0 0 15px !important;
         height:auto !important;
         }
         td[class="aligncenter"],
         td[class="no-link"]{text-align:center !important;}
         td[class="col"],
         td[class="sidebar"]{padding:10px !important;}
         td[class="col"] td[class="holder"],
         td[class="sidebar"] td[class="text"]{padding:10px 0 0 !important;}
         td[class="col"] td[class="holder-2"]{height:auto !important;}
         }
      </style>
   </head>
   <body style="margin:0; padding:0;" bgcolor="#ffffff" link="#f5f5f5" >
      <table class="wrapper" width="100%" cellspacing="0" cellpadding="0" style="background-color: #ffffff; background-repeat:repeat;">
         <tr>
            <td height="10" style="font-size:0; line-height:0;" >&nbsp;</td>
         </tr>
         <tr>
            <td align="center" style="padding:0 5px 26px;">
               <table class="flexible" width="750" cellpadding="0" cellspacing="0">
                  <tr>
                     <td class="area-1" style="padding:10px 0px 12px; background-image:url(http://motivaimplants.com/letters/images/gold-bg.png); background-size: 750px 63px; background-repeat: no-repeat; border-radius:2px" bgcolor="#ffffff">
                        <table width="100%" cellpadding="0" cellspacing="0">
                           <tr>
                              <td class="header">
                                 <table class="flexible" width="750" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td mc:edit="block-1" class="aligncenter" align="center" style="padding:0 0 12px;">
                                          <a href="http://motivaimplants.com/"><img src="http://motivaimplants.com/letters/images/motiva.png" border="0" style="vertical-align:top;" width="135" height="45" alt="Motiva Implant Matrix" /></a>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td mc:edit="block-3" class="img-flex" style="padding:0 0 8px;">
                                 <img src="http://motivaimplants.com/letters/images/img-regular-implant.jpg" style="vertical-align:top; border-radius: 0px 0px 2px 2px" width="750" height="250" alt="Your Trusted Partner: Motiva Always Confident Warranty" />
                              </td>
                           </tr>
                           <tr>
                              <td style="padding:0 20px 30px;">
                                 <table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td class="post">
                                          <table width="100%" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td class="holder" height="70" valign="top">
                                                   <table width="100%" cellpadding="0" cellspacing="0">
                                                      <tr>
                                                         <td mc:edit="block-4" style="font:12px/16px Helvetica, Arial, sans-serif; font-weight: 300; color:#9c9c9c; padding:0 0 10px; ">
                                                            Dato: <?php echo ($today)?>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td mc:edit="block-4" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#222; font-weight: normal; padding:0 0 10px; ">
                                                            Kjære. <?php echo ($name)?>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
                                                            Gratulerer, du har nå registrert dine Motiva implantater!<br><br>
                                                            Vær så snill å lese informasjonen under og sjekk at den matcher informasjonen på ditt personlige Motiva ID kort.                                                                    
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td style="padding:0 20px 42px;">
                               <?php if ($hasLeftImplant):?>
                                 <table class="flexible" width="345" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td class="post">
                                          <table width="100%" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td class="holder" height="100" valign="top">
                                                   <table width="100%" cellpadding="0" cellspacing="0">
                                                     <tr>
                                                         <td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#222222; padding:0 0 10px;">
                                                            Venstre implantat                                                                                        
                                                         </td>
                                                      </tr>
                                                      <tr>                                                          
                                                         <td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666;">
                                                            <span style="color:#222; font-weight: normal;">Serienummer:</span><?php echo ($snL)?><br>
                                                            <span style="color:#222; font-weight: normal;">Referanse:</span> <?php echo ($referenceL)?><br>
                                                            <span style="color:#222; font-weight: normal;">Volum:</span> <?php echo ($volumeL)?> cc<br>
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                                <?php endif ?>
                                <?php if ($hasRightImplant):?>
                                 <table class="flexible" width="345" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td class="post">
                                          <table width="100%" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td class="holder" height="100" valign="top">
                                                   <table width="100%" cellpadding="0" cellspacing="0">
                                                      <tr>
                                                         <td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#222222; padding:0 0 10px;">
                                                            Høyre implantat
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666;">
                                                            <span style="color:#222; font-weight: normal;">Serienummer:</span><?php echo ($snR)?><br>
                                                            <span style="color:#222; font-weight: normal;">Referanse:</span> <?php echo ($referenceR)?><br>
                                                            <span style="color:#222; font-weight: normal;">Volum:</span> <?php echo ($volumeR)?> cc<br>
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                                <?php endif ?>
                              </td>
                           </tr>
                           <tr>
                              <td style="padding:0 20px 30px;">
                                 <table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td class="post">
                                          <table width="100%" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td class="holder" height="70" valign="top">
                                                   <table width="100%" cellpadding="0" cellspacing="0">
                                                      <tr>
                                                         <td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
                                                             Informasjonen du har oppgitt vil bli lagt inn i Establishment Labs sitt sporbarhets system. Dette gjør at du som bærer av den/de medisinske komponentene du har fått kan bli funnet om vi trenger å kontakte deg om noe.
                                                              <br>
                                                              <br>
                                                             Establishment Labs garanterer at din personlige informasjon vil bli beskyttet og håndtert i henhold til våre Regler for personvern og gjeldende reguleringer, inkludert Direktiv 95/46/EC fra Europaparlamentet og Rådet av 24 oktober 1995, og Personregisterloven (UK).
                                                              <br>
                                                              <br>
                                                            <?php if ($years == 5):?>
                                                             Dine brystimplantater inneholde Q Inside Safety Technology<sup>™</sup>, som lar leger å fastslå produsentens navn, serienummer , varenummer , batchnummer , og andre implantat spesifikke data, fra utsiden av kroppen ved hjelp av en proprietær håndholdt leser. 
                                                             <br><br>
                                                            <?php endif ?>  
                                                             Konfidensialitet og personvern for pasienter har vår høyeste prioritet. Databasene som lagrer opplysninger om komponentene og pasienten fra registreringsprosessen ligger i sikre datasentre, som etterfølger de strenge personvernreglene gitt ved gjeldende lovgivning, og følger de samme sikkerhetsprotokoller som i dag gjelder sykehus og kirurgiske sentre. All personlig informasjon blir kun oppgitt som avtalt og med pasientens autorisasjon. Vi deler ikke din e-post adresse eller personlige informasjon uten ditt samtykke.
                                                             <br><br>
                                                            Følg rådene i ‘Breast Augmentation with Motiva Implant Matrix<sup>®</sup>: Informasjon til pasienten, og kontakt din kirurg øyeblikkelig dersom du skulle oppleve uventede symptomer som du mener har sammenheng med din operasjon for brystforstørrelse.
                                                            <br><br>
                                                            Husk at dine Motiva Implantat Matrix<sup>®</sup> bryst implantater er dekket av en livsvarig, begrenset garanti av komponentene, noe som innebærer at Establishment Labs vil levere et erstatnings implantat for ethvert Motiva Implantat Matrix<sup>®</sup> bryst implantat som får en ruptur. Og videre, dersom et implantat bli rammet av Baker Grad III eller IV kapsel sammentrekning, vil Establishment Labs aktivere sitt erstatningsprogram. Fullstendige detaljer om garantien er beskrevet i dokumentet Always Confident Warranty<sup>®</sup>(Alltid Trygg Garanti). Denne kan du få hos vår distributør, eller du kan lese den på  <a href="https://www.motivaimplants.com">www.motivaimplants.com</a>.
                                                            <br>
                                                            <br>
                                                            I enkelte markeder tilbyr vi vårt <?php echo ($years)?>års Motiva Extended Warranty Program (Motiva utvidede garantiprogram) som dekker finansiell assistanse til korrigerende kirurgi i tilfelle av implantat ruptur, rotasjon eller kapsel sammentrekning Baker Grad III eller IV. Denne ordningen tilbyr dekning i inntil <?php echo ($years)?> år etter operasjonsdagen. <br>
                                                                    <b><p style="color:#c89c3c;">Hvis du er interessert i denne utvidede dekningen og vil sjekke om den gjelder for dine implantater, kan du gjøre det på</p></b><br>
                                                                    <p align="center"><a  href="https://register.motivaimagine.com/"></font><img src="http://motivaimplants.com/letters/images/Warranty-Mail-Button.jpg"><br>(Utvid Garanti)</a></p>.<br><br>
                                                            Vi jobber med de nyeste teknologiene for å tilby deg markedets mest innovative implantater. Vi gjør det med din sikkerhet som prioritet, og viktigst, med en etisk produktgaranti. Med din Always Confident Warranty<sup>®</sup> og i enkelte markeder, vårt <?php echo ($years)?>års Motiva utvidete garantiprogram - har vi skapt et kundestøtte program helt uten like. Det er der for deg hvis du skulle trenge det.
                                                            <br><br><br>
                                                            Vær Trygg... Du har Motiva!
                                                            <br><br>
                                                            Vennlig hilsen,<br>
                                                            Juan José Chacón-Quirós<br>
                                                            CEO<br>
                                                            Establishment Labs
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td class="area-2" style="padding:15px 20px 5px 21px; background-color:#652d89; border-radius:2px">
                        <table width="100%" cellpadding="0" cellspacing="0">
                           <tr>
                              <td>
                                 <table class="flexible" width="410" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td mc:edit="block-33" class="aligncenter">
                                          <a href="http://motivaimplants.com/"><img src="http://motivaimplants.com/letters/images/motivaimagine.png" border="0" style="vertical-align:top;" width="220" height="37" alt="MotivaImagine" /></a>
                                       </td>
                                    </tr>
                                  </table>
                                 <table class="flexible" width="160" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
                                          <a href="https://www.facebook.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-facebook.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Facebook Icon" /></a>
                                       </td>
                                       <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
                                          <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://www.facebook.com/motivaimplants">Følg oss på Facebook</a>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
                                          <a href="https://twitter.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-twitter.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Twitter Icon" /></a>
                                       </td>
                                       <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
                                          <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://twitter.com/motivaimplants">Følg oss på Twitter</a>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
                                          <a href="https://instagram.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-instagram.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Instagram Icon" /></a>
                                       </td>
                                       <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
                                          <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://instagram.com/motivaimplants">Følg oss på Instagram</a>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td align="center" style="padding:2px 10px 7px;">
               <table class="flexible" width="600" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                  <tr>
                     <td class="no-float" align="center">
                        <table class="center" cellpadding="0" cellspacing="0">
                           <tr>
                              <td style="font:normal 9px/11px Helvetica, Arial, sans-serif; color:#888888;">
                                 Hvis du har mottatt denne e-posten ved en feiltakelse, hvis informasjonen her ikke er korrekt, eller hvis du ikke er enig i våre Retningslinjer for personvern eller Betingelser for bruk som du finner på <a style="text-decoration:none; color:#111111;" href="http://motivaimplants.com/">www.motivaimplants.com</a>, så ta kontakt med oss.
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
   </body>
</html>