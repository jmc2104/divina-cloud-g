﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <title>Motiva Implants Matrix</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <style type="text/css">
         * {
         -webkit-text-size-adjust: none;
         -webkit-text-resize: 100%;
         text-resize: 100%;
         }
         .no-link a{
         text-decoration:none !important;
         color:#fff !important;
         }
         .no-link-2 a{
         text-decoration:none !important;
         color:#fff !important;
         }
         sup{vertical-align: super; font-size: x-small;}
         @media only screen and (max-width:750px) {
         table[class="wrapper"]{min-width:320px !important;}
         table[class="flexible"]{width:100% !important;}
         td[class="img-flex"] img{
         width:100% !important;
         height:auto !important;
         }
         td[class="no-float"]{float:none !important;}
         table[class="center"]{margin:0 auto !important;}
         td[class="header"]{padding:5px 10px 0 !important;}
         td[class="area-1"]{padding:10px 10px !important;}
         td[class="area-2"]{padding:15px 10px !important;}
         td[class="post"]{padding:0 0 20px;}
         td[class="post"] td[class="holder"]{
         padding:0 0 15px !important;
         height:auto !important;
         }
         td[class="aligncenter"],
         td[class="no-link"]{text-align:center !important;}
         td[class="col"],
         td[class="sidebar"]{padding:10px !important;}
         td[class="col"] td[class="holder"],
         td[class="sidebar"] td[class="text"]{padding:10px 0 0 !important;}
         td[class="col"] td[class="holder-2"]{height:auto !important;}
         }
      </style>
   </head>
   <body style="margin:0; padding:0;" bgcolor="#ffffff" link="#f5f5f5" >
      <table class="wrapper" width="100%" cellspacing="0" cellpadding="0" style="background-color: #ffffff; background-repeat:repeat;">
         <tr>
            <td height="10" style="font-size:0; line-height:0;" >&nbsp;</td>
         </tr>
         <tr>
            <td align="center" style="padding:0 5px 26px;">
               <table class="flexible" width="750" cellpadding="0" cellspacing="0">
                  <tr>
                     <td class="area-1" style="padding:10px 0px 12px; background-image:url(http://motivaimplants.com/letters/images/gold-bg.png); background-size: 750px 63px; background-repeat: no-repeat; border-radius:2px" bgcolor="#ffffff">
                        <table width="100%" cellpadding="0" cellspacing="0">
                           <tr>
                              <td class="header">
                                 <table class="flexible" width="750" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td mc:edit="block-1" class="aligncenter" align="center" style="padding:0 0 12px;">
                                          <a href="http://motivaimplants.com/"><img src="http://motivaimplants.com/letters/images/motiva.png" border="0" style="vertical-align:top;" width="135" height="45" alt="Motiva Implant Matrix" /></a>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td mc:edit="block-3" class="img-flex" style="padding:0 0 8px;">
                                 <img src="http://motivaimplants.com/letters/images/img-regular-implant.jpg" style="vertical-align:top; border-radius: 0px 0px 2px 2px" width="750" height="250" alt="Your Trusted Partner: Motiva Always Confident Warranty" />
                              </td>
                           </tr>
                           <tr>
                              <td style="padding:0 20px 30px;">
                                 <table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td class="post">
                                          <table width="100%" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td class="holder" height="70" valign="top">
                                                   <table width="100%" cellpadding="0" cellspacing="0">
                                                      <tr>
                                                         <td mc:edit="block-4" style="font:12px/16px Helvetica, Arial, sans-serif; font-weight: 300; color:#9c9c9c; padding:0 0 10px; ">
                                                            Date: <?php echo ($today)?>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td mc:edit="block-4" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#222; font-weight: normal; padding:0 0 10px; ">
                                                            Sz.P. <?php echo ($name)?>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
                                                            Gratulujemy, dokonała Pani rejestracji implantów Motiva!<br><br>
                                                            Prosimy przeczytać poniższe informacje i upewnić się, że zgadzają się z informacjami podanymi na Pani karcie identyfikacyjnej Motiva.                  
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td style="padding:0 20px 42px;">
                               <?php if ($hasLeftImplant):?>
                                 <table class="flexible" width="345" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td class="post">
                                          <table width="100%" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td class="holder" height="100" valign="top">
                                                   <table width="100%" cellpadding="0" cellspacing="0">
                                                     <tr>
                                                         <td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#222222; padding:0 0 10px;">
                                                            Implantu lewego
                                                         </td>
                                                      </tr>
                                                      <tr>                                                          
                                                         <td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666;">
                                                            <span style="color:#222; font-weight: normal;">Numer seryjny:</span><?php echo ($snL)?><br>
                                                            <span style="color:#222; font-weight: normal;">Nr ref.:</span> <?php echo ($referenceL)?><br>
                                                            <span style="color:#222; font-weight: normal;">Objętość:</span> <?php echo ($volumeL)?> cc<br>
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                                <?php endif ?>
                                <?php if ($hasRightImplant):?>
                                 <table class="flexible" width="345" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td class="post">
                                          <table width="100%" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td class="holder" height="100" valign="top">
                                                   <table width="100%" cellpadding="0" cellspacing="0">
                                                      <tr>
                                                         <td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#222222; padding:0 0 10px;">
                                                            Implantu prawego
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666;">
                                                            <span style="color:#222; font-weight: normal;">Numer seryjny:</span><?php echo ($snR)?><br>
                                                            <span style="color:#222; font-weight: normal;">Nr ref:</span> <?php echo ($referenceR)?><br>
                                                            <span style="color:#222; font-weight: normal;">Objętość:</span> <?php echo ($volumeR)?> cc<br> 
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                                <?php endif ?>
                              </td>
                           </tr>
                           <tr>
                              <td style="padding:0 20px 30px;">
                                 <table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td class="post">
                                          <table width="100%" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td class="holder" height="70" valign="top">
                                                   <table width="100%" cellpadding="0" cellspacing="0">
                                                      <tr>
                                                         <td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
                                                             Podane przez Panią informacje zostaną wprowadzone do systemu monitorowania Establishment Labs. Dzięki temu możliwe będzie zlokalizowanie Pani jako posiadaczki urządzenia medycznego w przypadku konieczności przekazania powiadomienia.
                                                              <br>
                                                              <br>
                                                             Establishment Labs zobowiązuje się chronić i przetwarzać Pani dane osobowe zgodnie ze swoją polityką prywatności i obowiązującymi przepisami prawa, w tym Dyrektywą 95/46/WE Parlamentu Europejskiego i Rady z dnia 24 października 1995 r. i brytyjską Ustawą o ochronie danych osobowych.
                                                              <br>
                                                              <br>
                                                            <?php if ($years == 5):?>
                                                             Twoje implanty piersi zawierają Q Wewnątrz Safety Technology Q Inside Safety Technology<sup>™</sup>, która pozwala lekarzom, aby ustalić nazwę producenta, numer seryjny, numer partii, numer partii oraz innych danych szczegółowych implantu z zewnątrz ciała za pomocą firmowego czytnika ręcznego. 
                                                            <br><br>
                                                            <?php endif ?>  
                                                             Ochrona prywatności i poufności pacjentów jest dla nas największym priorytetem. Bazy danych, w których przechowujemy informacje o urządzeniach medycznych oraz dane osobowe pacjentek zgromadzone w procesie rejestracji, są prowadzone w bezpiecznych centrach danych zgodnie z rygorystycznymi wytycznymi dotyczącymi ochrony prywatności określonymi przez obowiązujące przepisy prawa i przy zastosowaniu takich samych protokołów bezpieczeństwa, jakie obecnie obowiązują w szpitalach i ośrodkach chirurgii. Wszystkie podane dane osobowe są ujawniane wyłącznie w sposób określony w zgodzie udzielonej przez pacjentkę. Bez Pani zgody nie ujawnimy Pani adresu e-mail ani innych danych osobowych.

                                                             <br><br>
                                                            Zgodnie z informacją dla pacjenta „Powiększenie piersi przy pomocy Motiva Implant Matrix<sup>®</sup>: należy niezwłocznie skontaktować się ze swoim chirurgiem, jeśli zauważy Pani u siebie nieoczekiwane objawy, które Pani zdaniem mogą być powiązane z zabiegiem chirurgicznym powiększenia piersi.
                                                            <br><br>
                                                            Należy pamiętać, że Pani implanty piersi Motiva Implant Matrix<sup>®</sup> są objęte ograniczoną wieczystą gwarancją, zgodnie z którą Establishment Labs dostarczy implant zamienny za każdy implant piersi Motiva Implant Matrix<sup>®</sup>,który ulegnie pęknięciu. Ponadto, jeżeli po wszczepieniu implantu wystąpił przykurcz torebki III lub IV stopnia według skali Bakera, Establishment Labs oferuje program wymiany implantu. Wszystkie szczegóły gwarancji zostały opisane w dokumencie zatytułowanym Always Confident Warranty<sup>®</sup>, można otrzymać go od naszego dystrybutora lub zapoznać się z nim na stronie <a href="https://www.motivaimplants.com">www.motivaimplants.com</a>.<br><br>
                                                           Na niektórych rynkach oferujemy również <?php echo ($years)?>letni program rozszerzonej gwarancji (<?php echo ($years)?>Y Motiva Extended Warranty Program) który zapewnia pomoc finansową przy operacjach rewizyjnych w przypadku pęknięcia implantu, rotacji implantu lub przykurczu torebki III lub IV stopnia według skali Bakera. Program ten zapewnia ochronę przez okres do dwóch lat od dnia operacji. <br>
                                                                    <b><p style="color:#c89c3c;">Jeśli jest Pani zainteresowana tą przedłużona gwarancją i chciałaby sprawdzić, czy ma ona zastosowanie do Pani implantów, można tego dokonać na stronie</p></b><br>
                                                                    <p align="center"><a  href="https://register.motivaimagine.com/"></font><img src="http://motivaimplants.com/letters/images/Warranty-Mail-Button.jpg"><br>(Rozszerz gwarancję)</a></p>.<br><br>
                                                            Wykorzystujemy najnowsze technologie, aby zaoferować Pani najbardziej innowacyjne implanty dostępne na rynku, mając na względzie przede wszystkim Pani bezpieczeństwo oraz gwarancję etyczną na produkt. Nasze programy gwarancyjne Always Confident Warranty<sup>®</sup> oraz dostępne na wybranych rynkach <?php echo ($years)?>Y Motiva Extended Warranty Program, to niespotykany na rynku system wsparcia... możesz z niego skorzystać kiedy i jeśli będziesz go potrzebować.
                                                            <br><br><br>
                                                            Bądź pewna siebie... Masz Motiva!
                                                            <br><br>
                                                            Z poważaniem,<br>
                                                            Juan José Chacón-Quirós<br>
                                                            Dyrektor Generalny<br>
                                                            Establishment Labs
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td class="area-2" style="padding:15px 20px 5px 21px; background-color:#652d89; border-radius:2px">
                        <table width="100%" cellpadding="0" cellspacing="0">
                           <tr>
                              <td>
                                 <table class="flexible" width="410" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td mc:edit="block-33" class="aligncenter">
                                          <a href="http://motivaimplants.com/"><img src="http://motivaimplants.com/letters/images/motivaimagine.png" border="0" style="vertical-align:top;" width="220" height="37" alt="MotivaImagine" /></a>
                                       </td>
                                    </tr>
                                  </table>
                                 <table class="flexible" width="160" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
                                          <a href="https://www.facebook.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-facebook.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Facebook Icon" /></a>
                                       </td>
                                       <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
                                          <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://www.facebook.com/motivaimplants">Śledź nas na Facebooku</a>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
                                          <a href="https://twitter.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-twitter.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Twitter Icon" /></a>
                                       </td>
                                       <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
                                          <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://twitter.com/motivaimplants">Śledź nas na Twitterze</a>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
                                          <a href="https://instagram.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-instagram.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Instagram Icon" /></a>
                                       </td>
                                       <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
                                          <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://instagram.com/motivaimplants">Śledź nas na Instagramie</a>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td align="center" style="padding:2px 10px 7px;">
               <table class="flexible" width="600" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                  <tr>
                     <td class="no-float" align="center">
                        <table class="center" cellpadding="0" cellspacing="0">
                           <tr>
                              <td style="font:normal 9px/11px Helvetica, Arial, sans-serif; color:#888888;">
                                 Jeśli otrzymałaś tę wiadomość przez pomyłkę, jeśli informacje podane  w tej wiadomości są niepoprawne lub jeśli nie akceptujesz polityki prywatności i regulaminu naszej witryny: <a style="text-decoration:none; color:#111111;" href="http://motivaimplants.com/">www.motivaimplants.com</a>.
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
   </body>
</html>