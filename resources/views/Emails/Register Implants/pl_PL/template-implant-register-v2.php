<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Welcome to MotivaImagine</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style type="text/css">
		* {
			-webkit-text-size-adjust: none;
			-webkit-text-resize: 100%;
			text-resize: 100%;
		}
		.no-link a{
			text-decoration:none !important;
			color:#fff !important;
		}
		.no-link-2 a{
			text-decoration:none !important;
			color:#fff !important;
		}
		sup{vertical-align: super; line-height: 0; font-size: 0.8em;}
		@media only screen and (max-width:750px) {
			table[class="wrapper"]{min-width:320px !important;}
			table[class="flexible"]{width:100% !important;}
			td[class="img-flex"] img{
				width:100% !important;
				height:auto !important;
			}
			td[class="no-float"]{float:none !important;}
			table[class="center"]{margin:0 auto !important;}
			td[class="header"]{padding:5px 10px 0 !important;}
			td[class="area-1"]{padding:0 !important;}
			td[class="area-2"]{padding:15px 10px !important;}
			td[class="post"]{padding:0 0 20px;}
			td[class="post"] td[class="holder"]{
				padding:0 0 15px !important;
				height:auto !important;
			}
			td[class="aligncenter"],
			td[class="no-link"]{text-align:center !important;}
			td[class="col"],
			td[class="sidebar"]{padding:10px !important;}
			td[class="col"] td[class="holder"],
			td[class="sidebar"] td[class="text"]{padding:10px 0 0 !important;}
			td[class="col"] td[class="holder-2"]{height:auto !important;}
		}
		.tg  {border-collapse:collapse;border-spacing:0;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg .tg-d5wv{background-color:#a38546;color:#ffffff;text-align:center;vertical-align:top}
		.tg .tg-yw4l{vertical-align:top}
	</style>
</head>
<body style="margin:0; padding:0;" bgcolor="#eeeeee" link="#f5f5f5" >
	<table class="wrapper" width="100%" cellspacing="0" cellpadding="0">
		<tr><td height="10" style="font-size:0; line-height:0;" >&nbsp;</td></tr>
		<tr>
			<td align="center" style="padding:10px 5px 26px 5px;">
				<table class="flexible" width="750" cellpadding="0" cellspacing="0">
					<tr>
						<td class="area-1" style="border-radius: 6px 6px 3px 3px" bgcolor="#ffffff">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td mc:edit="block-3" class="img-flex" style="padding:0 0 8px;">
										<img src="http://motivaimplants.com/letters/cover.jpg" style="vertical-align:top; border-radius: 6px 6px 0px 0px" width="750" height="400" alt="Motiva Implants Logo" />
									</td>
								</tr>
								<tr>
									<td style="padding:20px 20px 30px 20px;">
										<table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td class="post">
													<table width="100%" cellpadding="0" cellspacing="0">
														<tr>
															<td class="holder" height="70" valign="top">
																<table width="100%" cellpadding="0" cellspacing="0">
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Velkommen til MotivaImagine
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Din pålitede partner
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Motiva Always Confident Warranty&reg;
																		</td>
																	</tr>
																	<tr><!-- MAIN CONTENT -->
																		<td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
																			Sz.P. <?=/*php*/$name/*php*/?>
																			<span style="float: right; color:#a1a1a1;"><?=/*php*/date('d-M-Y')/*php*/?></span>
																			<br/>
																			<br/>
																			Gratulujemy, dokonała Pani rejestracji implantów Motiva!
																			<br>
																			<br>
																			Prosimy przeczytać poniższe informacje i upewnić się, że zgadzają się z informacjami podanymi na Pani karcie identyfikacyjnej Motiva. 
																			<br/>
																			<br/>
																			<!-- Implant Information -->
																			<table width="100%" cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<?php if ( $has_left_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="left">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										Implantu lewego                  
																									</td>
																								</tr>
																								<tr>                                                          
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">Numer seryjny:</span><?=/*php*/$left_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Nr ref.:</span><?=/*php*/$left_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Objętość:</span><?=/*php*/$left_vol/*php*/?> cc<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																						<?php if ( $has_right_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="right">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										Implantu prawego
																									</td>
																								</tr>
																								<tr>
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">Serial Number:</span><?=/*php*/$right_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Nr ref.:</span><?=/*php*/$right_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Objętość:</span><?=/*php*/$right_vol/*php*/?> cc<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																					</td>
																				</tr>
																			</table>
																			<br>
																			<br>
																			Podane przez Panią informacje zostaną wprowadzone do systemu monitorowania Establishment Labs. Dzięki temu możliwe będzie zlokalizowanie Pani jako posiadaczki urządzenia medycznego w przypadku konieczności przekazania powiadomienia.
																			<br>
																			<br>
																			Establishment Labs zobowiązuje się chronić i przetwarzać Pani dane osobowe zgodnie ze swoją polityką prywatności i obowiązującymi przepisami prawa, w tym Dyrektywą 95/46/WE Parlamentu Europejskiego i Rady z dnia 24 października 1995 r. i brytyjską Ustawą o ochronie danych osobowych.
																			<br>
																			<br>
																			<!-- Only if has implants -->
																			<?php if ( isset($is_q_inside) ? $is_q_inside : false ): ?>
																				Twoje implanty piersi zawierają Q Wewnątrz Safety Technology Q Inside Safety Technology<sup>™</sup>, która pozwala lekarzom, aby ustalić nazwę producenta, numer seryjny, numer partii, numer partii oraz innych danych szczegółowych implantu z zewnątrz ciała za pomocą firmowego czytnika ręcznego.  
																				<br>
																				<br>
																			<?php endif ?>
																			Ochrona prywatności i poufności pacjentów jest dla nas największym priorytetem. Bazy danych, w których przechowujemy informacje o urządzeniach medycznych oraz dane osobowe pacjentek zgromadzone w procesie rejestracji, są prowadzone w bezpiecznych centrach danych zgodnie z rygorystycznymi wytycznymi dotyczącymi ochrony prywatności określonymi przez obowiązujące przepisy prawa i przy zastosowaniu takich samych protokołów bezpieczeństwa, jakie obecnie obowiązują w szpitalach i ośrodkach chirurgii. Wszystkie podane dane osobowe są ujawniane wyłącznie w sposób określony w zgodzie udzielonej przez pacjentkę. Bez Pani zgody nie ujawnimy Pani adresu e-mail ani innych danych osobowych.
																			<br>
																			<br>
																			 Zgodnie z informacją dla pacjenta „Powiększenie piersi przy pomocy Motiva Implant Matrix<sup>®</sup>: należy niezwłocznie skontaktować się ze swoim chirurgiem, jeśli zauważy Pani u siebie nieoczekiwane objawy, które Pani zdaniem mogą być powiązane z zabiegiem chirurgicznym powiększenia piersi.
																			<br>
																			<br>
																			Należy pamiętać, że Pani implanty piersi Motiva Implant Matrix<sup>®</sup> są objęte ograniczoną wieczystą gwarancją, zgodnie z którą Establishment Labs dostarczy implant zamienny za każdy implant piersi Motiva Implant Matrix<sup>®</sup>,który ulegnie pęknięciu. Ponadto, jeżeli po wszczepieniu implantu wystąpił przykurcz torebki III lub IV stopnia według skali Bakera, Establishment Labs oferuje program wymiany implantu. Wszystkie szczegóły gwarancji zostały opisane w dokumencie zatytułowanym Always Confident Warranty<sup>®</sup>, można otrzymać go od naszego dystrybutora lub zapoznać się z nim na stronie  <a style="text-decoration:none; color:#a38546;" href="https://motivaimplants.com">www.motivaimplants.com</a>. 
																			<br>
																			<br>
																			Na niektórych rynkach oferujemy również <?=/*php*/$years/*php*/?>letni program rozszerzonej gwarancji (<?=/*php*/$years/*php*/?>Y Motiva Extended Warranty Program) który zapewnia pomoc finansową przy operacjach rewizyjnych w przypadku pęknięcia implantu, rotacji implantu lub przykurczu torebki III lub IV stopnia według skali Bakera. Program ten zapewnia ochronę przez okres do dwóch lat od dnia operacji.
																			<br>
																			<br>
																			<p style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546;">
																				<b>Jeśli jest Pani zainteresowana tą przedłużona gwarancją i chciałaby sprawdzić, czy ma ona zastosowanie do Pani implantów, można tego dokonać na stronie</b>
																				<a style="text-decoration:none; color:#888;" href="https://register.motivaimagine.com/">register.motivaimagine.com</a>
																			</p>
																			<br>
																			<p align="center">
																				<a href="https://register.motivaimagine.com/" style="text-decoration:none; background: #a38546; color:#fff; font-weight: 300; padding:8px 14px; border-radius:30px"><b>Rozszerz gwarancję</b></a>
																			</p>
																			<br>
																			<br>
																			 Wykorzystujemy najnowsze technologie, aby zaoferować Pani najbardziej innowacyjne implanty dostępne na rynku, mając na względzie przede wszystkim Pani bezpieczeństwo oraz gwarancję etyczną na produkt. Nasze programy gwarancyjne Always Confident Warranty<sup>®</sup> oraz dostępne na wybranych rynkach <?=/*php*/isset($years) ? $years : '{{years}}'/*php*/?>Y Motiva Extended Warranty Program, to niespotykany na rynku system wsparcia... możesz z niego skorzystać kiedy i jeśli będziesz go potrzebować.
																			<br>
																			<br/>
																			<br/>
																			Bądź pewna siebie... Masz Motiva!
																			<br>
																			<br>
																			Z poważaniem,<br>
																			Juan José Chacón-Quirós<br>
																			Dyrektor Generalny<br>
																			Establishment Labs<br>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="area-2" style="padding:15px 20px 5px 21px; background-color:#eeeeee;">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<table class="flexible" width="410" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-33" class="aligncenter">
													<a href="http://motivaimagine.com/"><img src="http://motivaimplants.com/letters/motivaimagine.png" border="0" style="vertical-align:top; padding: 0px 0px 12px 0px" width="210" height="37" alt="MotivaImagine" /></a>
												</td>
											</tr>
											<tr>
												<td mc:edit="block-35" class="aligncenter" style="font:9px/11px Helvetica, Arial, sans-serif; color:#fff; font-weight: 300;">
													<a style="text-decoration:none; color:#a1a1a1;" href="mailto:customerservice@motivaimplants.com">customerservice@motivaimplants.com</a> <br/>
													<a style="text-decoration:none; color:#a1a1a1; font-weight: 300;" href="http://motivaimplants.com/">www.motivaimplants.com</a> <br/>
												</td>
											</tr>                                                                   
										</table>
										<table class="flexible" width="160" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-36" width="160" class="aligncenter" style="padding:10px 0 5px 0; text-align: right;">
													<a href="https://www.facebook.com/motivaimagine/"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-facebook-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Śledź nas na Facebooku" /></a> <a href="https://twitter.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-twitter-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Śledź nas na Twitterze" /></a> <a href="https://instagram.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-instagram-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Śledź nas na Instagramie" /></a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" style="padding:2px 10px 7px;">
				<table class="flexible" width="600" cellpadding="0" cellspacing="0" bgcolor="#eeeeee">
					<tr>
						<td class="no-float" align="center">
							<table class="center" cellpadding="0" cellspacing="0">
								<tr>
									<td style="font:normal 9px/11px Helvetica, Arial, sans-serif; color:#888888; text-align: center;">
										 Jeśli otrzymałaś tę wiadomość przez pomyłkę, jeśli informacje podane  w tej wiadomości są niepoprawne lub jeśli nie akceptujesz polityki prywatności i regulaminu naszej witryny: <a style="text-decoration:none; color:#555555;" href="http://motivaimplants.com/">www.motivaimplants.com</a>.
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>