<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Welcome to MotivaImagine</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style type="text/css">
		* {
			-webkit-text-size-adjust: none;
			-webkit-text-resize: 100%;
			text-resize: 100%;
		}
		.no-link a{
			text-decoration:none !important;
			color:#fff !important;
		}
		.no-link-2 a{
			text-decoration:none !important;
			color:#fff !important;
		}
		sup{vertical-align: super; line-height: 0; font-size: 0.8em;}
		@media only screen and (max-width:750px) {
			table[class="wrapper"]{min-width:320px !important;}
			table[class="flexible"]{width:100% !important;}
			td[class="img-flex"] img{
				width:100% !important;
				height:auto !important;
			}
			td[class="no-float"]{float:none !important;}
			table[class="center"]{margin:0 auto !important;}
			td[class="header"]{padding:5px 10px 0 !important;}
			td[class="area-1"]{padding:0 !important;}
			td[class="area-2"]{padding:15px 10px !important;}
			td[class="post"]{padding:0 0 20px;}
			td[class="post"] td[class="holder"]{
				padding:0 0 15px !important;
				height:auto !important;
			}
			td[class="aligncenter"],
			td[class="no-link"]{text-align:center !important;}
			td[class="col"],
			td[class="sidebar"]{padding:10px !important;}
			td[class="col"] td[class="holder"],
			td[class="sidebar"] td[class="text"]{padding:10px 0 0 !important;}
			td[class="col"] td[class="holder-2"]{height:auto !important;}
		}
		.tg  {border-collapse:collapse;border-spacing:0;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg .tg-d5wv{background-color:#a38546;color:#ffffff;text-align:center;vertical-align:top}
		.tg .tg-yw4l{vertical-align:top}
	</style>
</head>
<body style="margin:0; padding:0;" bgcolor="#eeeeee" link="#f5f5f5" >
	<table class="wrapper" width="100%" cellspacing="0" cellpadding="0">
		<tr><td height="10" style="font-size:0; line-height:0;" >&nbsp;</td></tr>
		<tr>
			<td align="center" style="padding:10px 5px 26px 5px;">
				<table class="flexible" width="750" cellpadding="0" cellspacing="0">
					<tr>
						<td class="area-1" style="border-radius: 6px 6px 3px 3px" bgcolor="#ffffff">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td mc:edit="block-3" class="img-flex" style="padding:0 0 8px;">
										<img src="http://motivaimplants.com/letters/cover.jpg" style="vertical-align:top; border-radius: 6px 6px 0px 0px" width="750" height="400" alt="Motiva Implants Logo" />
									</td>
								</tr>
								<tr>
									<td style="padding:20px 20px 30px 20px;">
										<table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td class="post">
													<table width="100%" cellpadding="0" cellspacing="0">
														<tr>
															<td class="holder" height="70" valign="top">
																<table width="100%" cellpadding="0" cellspacing="0">
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Willkommen bei MotivaImagine
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Ihr vertrauenswürdiger Partner
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Motiva Always Confident Warranty&reg;
																		</td>
																	</tr>
																	<tr><!-- MAIN CONTENT -->
																		<td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
																			Sehr geehrte. <?=/*php*/$name/*php*/?>
																			<span style="float: right; color:#a1a1a1;"><?=/*php*/date('d-M-Y')/*php*/?></span>
																			<br/>
																			<br/>
																			 Herzlichen Glückwunsch, Sie haben Ihre Motiva-Implantate erfolgreich registriert!
																			<br>
																			<br>
																			Bitte lesen Sie die nachstehenden Angaben und stellen Sie bitte sicher, dass die Informationen mit denen auf Ihrer persönlichen Motiva ID-Karte übereinstimmen. 
																			<br/>
																			<br/>
																			<!-- Implant Information -->
																			<table width="100%" cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<?php if ( $has_left_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="left">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										Linkes Implantat                                 
																									</td>
																								</tr>
																								<tr>                                                          
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">Seriennummer:</span><?=/*php*/$left_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Referenz:</span><?=/*php*/$left_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Volumen:</span><?=/*php*/$left_vol/*php*/?> cc<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																						<?php if ( $has_right_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="right">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										Rechtes Implantat
																									</td>
																								</tr>
																								<tr>
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">Seriennummer:</span><?=/*php*/$right_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Referenz:</span><?=/*php*/$right_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Volumen:</span><?=/*php*/$right_vol/*php*/?> cc<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																					</td>
																				</tr>
																			</table>
																			<br>
																			<br>
																			 Die Informationen, die Sie eingegeben haben, werden im Rückverfolgungssystem der Establishment Labs gespeichert. Somit wird sichergestellt, dass Sie, als Trägerin der von Ihnen erhaltenen medizinischen Geräte, im Falle ein Benachrichtigungsereignis erreicht werden können.
																			<br>
																			<br>
																			 Establishment Labs versichert, dass Ihre persönlichen Daten gemäß unserer geltenden Vorschriften, einschließlich der Richtlinie 95/46/EG des Europäischen Parlaments und des Rates vom 24. Oktober 1995, und des Datenschutzgesetzes (UK), geschützt und behandelt werden.
																			<br>
																			<br>
																			<!-- Only if has implants -->
																			<?php if ( isset($is_q_inside) ? $is_q_inside : false ): ?>
																				Ihre Brustimplantate enthalten Q Inside Safety Technology ™,Die es dem Arzt erlaubt, den Herstellernamen, die Seriennummer, die Losnummer, die Chargennummer, und andere implantatspezifische Daten von außerhalb des Körpers Von außerhalb des Körpers verwenden einem proprietären Handheld-Lesegerät.
																				<br>
																				<br>
																			<?php endif ?>
																			Die Privatsphäre unserer Patienten, sowie die Vertraulichkeit, haben für uns den höchsten Stellenwert. Die Datenbanken, in denen die Geräte- und Patienten-Daten während des Registrierungsverfahrens eingegeben wurden, werden in sicheren Datenzentren gespeichert und gewartet. Diese Datenzentren unterliegen den gleichen strengen Datenschutzrichtlinien der geltenden Rechtsvorschriften, gemäß den gleichen Informations-Sicherheits-Protokollen, die derzeit in Krankenhäusern und OP-Zentren angewandt werden. Jegliche zur Verfügung  gestellten personenbezogene Daten, werden nur nach Absprache und mit der Genehmigung des Patienten freigegeben. Ihre E-Mail-Adresse oder personenbezogene Daten werden von uns ohne Ihre Zustimmung nicht freigegeben.
																			<br>
																			<br>
																			In Übereinstimmung mit der "Brustvergrößerung mit der Motiva-Implantat-Matrix®: Informationen für den Patienten, bitte kontaktieren Sie Ihren Arzt umgehend, sollten unerwartete Symptome auftreten, die ihres Erachtens mit Ihrer chirurgischen Brustvergrößerung zusammenhängen.
																			<br>
																			<br>
																			Denken Sie bitte daran, dass Ihre Motiva Implant Matrix<sup>®</sup> Brustimplantate einer auf die Lebensdauer des Geräts beschränkten Garantie unterliegen, insoweit die Establishment Labs ein Ersatz-Implantat für jedes Motiva-Implantat Matrix<sup>®</sup> Brust-Implantat liefert, das einen Riss aufweist. Darüber hinaus bietet Establishment Labs im Falle eines von der Baker-Klasse III oder IV Kapselkontraktur betroffenen Implantates, sein Austauschprogramm. Einzelheiten der Garantie werden im Dokument Always Confident Warranty<sup>®</sup>, beschrieben, wie durch  unsere Vertriebspartner zur Verfügung gestellt, oder auch auf <a style="text-decoration:none; color:#a38546;" href="https://motivaimplants.com">www.motivaimplants.com</a> eingesehen werden kann. 

																			In ausgewählten Märkten bieten wir unser <?=/*php*/$years/*php*/?>Y Motiva- Zusatzgarantie-Programm zur finanziellen Unterstützung für Überarbeitungs-Operationen bei Implantaten-Rissen, Implantat-Drehung und Kapselkontraktur der Baker-Klasse III oder IV. Dieses Programm bietet eine Garantieabsicherung von bis zu <?=/*php*/$years/*php*/?> Jahren, ab dem Zeitpunkt der Operation
																			<br>
																			<br>
																			<p style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546;">
																				<b>Falls Sie sich für diese erweiterte Garantie interessieren und überprüfen möchten, ob  sie auf Ihre Implantate angewendet werden kann, tun Sie dies bitte auf:</b>
																				<a style="text-decoration:none; color:#888;" href="https://register.motivaimagine.com/">register.motivaimagine.com</a>
																			</p>
																			<br>
																			<p align="center">
																				<a href="https://register.motivaimagine.com/" style="text-decoration:none; background: #a38546; color:#fff; font-weight: 300; padding:8px 14px; border-radius:30px"><b>Garantielaufzeit verlängern</b></a>
																			</p>
																			<br>
																			<br>
																			Wir arbeiten mit den neuesten Technologien, um Ihnen die innovativsten Implantate auf dem Markt zu bieten, mit Ihrer Sicherheit als höchster Priorität, und vor allem eine ethische Produktgarantie. Mit unserer "Always Confident Garantie<sup>®</sup>" und in ausgewählten Märkten mit unserem <?=/*php*/isset($years) ? $years : '{{years}}'/*php*/?>Y Motiva-Zusatzgarantie-Programm, haben wir ein unvergleichbares Support-System geschaffen... es steht Ihnen jederzeit zur Verfügung.
																			<br>
																			<br/>
																			<br/>
																			Seien Sie zuversichtlich... Sie haben Motiva!
																			<br>
																			<br>
																			Mit freundlichen Grüßen,<br>
																			Juan José Chacón-Quirós<br>
																			Geschäftsführer<br>
																			Establishment Labs<br>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="area-2" style="padding:15px 20px 5px 21px; background-color:#eeeeee;">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<table class="flexible" width="410" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-33" class="aligncenter">
													<a href="http://motivaimagine.com/"><img src="http://motivaimplants.com/letters/motivaimagine.png" border="0" style="vertical-align:top; padding: 0px 0px 12px 0px" width="210" height="37" alt="MotivaImagine" /></a>
												</td>
											</tr>
											<tr>
												<td mc:edit="block-35" class="aligncenter" style="font:9px/11px Helvetica, Arial, sans-serif; color:#fff; font-weight: 300;">
													<a style="text-decoration:none; color:#a1a1a1;" href="mailto:customerservice@motivaimplants.com">customerservice@motivaimplants.com</a> <br/>
													<a style="text-decoration:none; color:#a1a1a1; font-weight: 300;" href="http://motivaimplants.com/">www.motivaimplants.com</a> <br/>
												</td>
											</tr>                                                                   
										</table>
										<table class="flexible" width="160" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-36" width="160" class="aligncenter" style="padding:10px 0 5px 0; text-align: right;">
													<a href="https://www.facebook.com/motivaimagine/"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-facebook-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Folgen Sie uns auf Facebook" /></a> <a href="https://twitter.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-twitter-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Folgen Sie uns auf Twitter" /></a> <a href="https://instagram.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-instagram-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Folgen Sie uns auf Instagram" /></a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" style="padding:2px 10px 7px;">
				<table class="flexible" width="600" cellpadding="0" cellspacing="0" bgcolor="#eeeeee">
					<tr>
						<td class="no-float" align="center">
							<table class="center" cellpadding="0" cellspacing="0">
								<tr>
									<td style="font:normal 9px/11px Helvetica, Arial, sans-serif; color:#888888; text-align: center;">
										Sollten Sie diese e-Mail irrtümlicherweise erhalten haben, oder sollten Ihre Angaben inkorrekt sein, oder sollten Sie mit den auf unserer Website: <a style="text-decoration:none; color:#555555;" href="http://motivaimplants.com/">www.motivaimplants.com</a>.
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>