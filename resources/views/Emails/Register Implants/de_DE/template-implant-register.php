﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <title>Motiva Implants Matrix</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <style type="text/css">
         * {
         -webkit-text-size-adjust: none;
         -webkit-text-resize: 100%;
         text-resize: 100%;
         }
         .no-link a{
         text-decoration:none !important;
         color:#fff !important;
         }
         .no-link-2 a{
         text-decoration:none !important;
         color:#fff !important;
         }
         sup{vertical-align: super; font-size: x-small;}
         @media only screen and (max-width:750px) {
         table[class="wrapper"]{min-width:320px !important;}
         table[class="flexible"]{width:100% !important;}
         td[class="img-flex"] img{
         width:100% !important;
         height:auto !important;
         }
         td[class="no-float"]{float:none !important;}
         table[class="center"]{margin:0 auto !important;}
         td[class="header"]{padding:5px 10px 0 !important;}
         td[class="area-1"]{padding:10px 10px !important;}
         td[class="area-2"]{padding:15px 10px !important;}
         td[class="post"]{padding:0 0 20px;}
         td[class="post"] td[class="holder"]{
         padding:0 0 15px !important;
         height:auto !important;
         }
         td[class="aligncenter"],
         td[class="no-link"]{text-align:center !important;}
         td[class="col"],
         td[class="sidebar"]{padding:10px !important;}
         td[class="col"] td[class="holder"],
         td[class="sidebar"] td[class="text"]{padding:10px 0 0 !important;}
         td[class="col"] td[class="holder-2"]{height:auto !important;}
         }
      </style>
   </head>
   <body style="margin:0; padding:0;" bgcolor="#ffffff" link="#f5f5f5" >
      <table class="wrapper" width="100%" cellspacing="0" cellpadding="0" style="background-color: #ffffff; background-repeat:repeat;">
         <tr>
            <td height="10" style="font-size:0; line-height:0;" >&nbsp;</td>
         </tr>
         <tr>
            <td align="center" style="padding:0 5px 26px;">
               <table class="flexible" width="750" cellpadding="0" cellspacing="0">
                  <tr>
                     <td class="area-1" style="padding:10px 0px 12px; background-image:url(http://motivaimplants.com/letters/images/gold-bg.png); background-size: 750px 63px; background-repeat: no-repeat; border-radius:2px" bgcolor="#ffffff">
                        <table width="100%" cellpadding="0" cellspacing="0">
                           <tr>
                              <td class="header">
                                 <table class="flexible" width="750" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td mc:edit="block-1" class="aligncenter" align="center" style="padding:0 0 12px;">
                                          <a href="http://motivaimplants.com/"><img src="http://motivaimplants.com/letters/images/motiva.png" border="0" style="vertical-align:top;" width="135" height="45" alt="Motiva Implant Matrix" /></a>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td mc:edit="block-3" class="img-flex" style="padding:0 0 8px;">
                                 <img src="http://motivaimplants.com/letters/images/img-regular-implant.jpg" style="vertical-align:top; border-radius: 0px 0px 2px 2px" width="750" height="250" alt="Your Trusted Partner: Motiva Always Confident Warranty" />
                              </td>
                           </tr>
                           <tr>
                              <td style="padding:0 20px 30px;">
                                 <table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td class="post">
                                          <table width="100%" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td class="holder" height="70" valign="top">
                                                   <table width="100%" cellpadding="0" cellspacing="0">
                                                      <tr>
                                                         <td mc:edit="block-4" style="font:12px/16px Helvetica, Arial, sans-serif; font-weight: 300; color:#9c9c9c; padding:0 0 10px; ">
                                                            Datum: <?php echo ($today)?>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td mc:edit="block-4" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#222; font-weight: normal; padding:0 0 10px; ">
                                                            Sehr geehrte. <?php echo ($name)?>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
                                                            Herzlichen Glückwunsch, Sie haben Ihre Motiva-Implantate erfolgreich registriert!
                                                            <br>
                                                            <br>
                                                            Bitte lesen Sie die nachstehenden Angaben und stellen Sie bitte sicher, dass die Informationen mit denen auf Ihrer persönlichen Motiva ID-Karte übereinstimmen.                     
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td style="padding:0 20px 42px;">
                               <?php if ($hasLeftImplant):?>
                                 <table class="flexible" width="345" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td class="post">
                                          <table width="100%" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td class="holder" height="100" valign="top">
                                                   <table width="100%" cellpadding="0" cellspacing="0">
                                                     <tr>
                                                         <td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#222222; padding:0 0 10px;">
                                                            Linkes Implantat     
                                                         </td>
                                                      </tr>
                                                      <tr>                                                          
                                                         <td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666;">
                                                            <span style="color:#222; font-weight: normal;">Seriennummer:</span><?php echo ($snL)?><br>
                                                            <span style="color:#222; font-weight: normal;">Referenz:</span> <?php echo ($referenceL)?><br>
                                                            <span style="color:#222; font-weight: normal;">Volumen:</span> <?php echo ($volumeL)?> cc<br>
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                                <?php endif ?>
                                <?php if ($hasRightImplant):?>
                                 <table class="flexible" width="345" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td class="post">
                                          <table width="100%" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td class="holder" height="100" valign="top">
                                                   <table width="100%" cellpadding="0" cellspacing="0">
                                                      <tr>
                                                         <td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#222222; padding:0 0 10px;">
                                                            Rechtes Implantat
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666;">
                                                            <span style="color:#222; font-weight: normal;">Seriennummer:</span><?php echo ($snR)?><br>
                                                            <span style="color:#222; font-weight: normal;">Referenz:</span> <?php echo ($referenceR)?><br>
                                                            <span style="color:#222; font-weight: normal;">Volumen:</span> <?php echo ($volumeR)?> cc<br>
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                                <?php endif ?>
                              </td>
                           </tr>
                           <tr>
                              <td style="padding:0 20px 30px;">
                                 <table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td class="post">
                                          <table width="100%" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td class="holder" height="70" valign="top">
                                                   <table width="100%" cellpadding="0" cellspacing="0">
                                                      <tr>
                                                         <td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
                                                             Die Informationen, die Sie eingegeben haben, werden im Rückverfolgungssystem der Establishment Labs gespeichert. Somit wird sichergestellt, dass Sie, als Trägerin der von Ihnen erhaltenen medizinischen Geräte, im Falle ein Benachrichtigungsereignis erreicht werden können.
                                                              <br>
                                                              <br>
                                                             Establishment Labs versichert, dass Ihre persönlichen Daten gemäß unserer geltenden Vorschriften, einschließlich der Richtlinie 95/46/EG des Europäischen Parlaments und des Rates vom 24. Oktober 1995, und des Datenschutzgesetzes (UK), geschützt und behandelt werden.
                                                              <br>
                                                              <br>
                                                            <?php if ($years == 5):?>
                                                              Ihre Brustimplantate enthalten Q Inside Safety Technology ™,Die es dem Arzt erlaubt, den Herstellernamen, die Seriennummer, die Losnummer, die Chargennummer, und andere implantatspezifische Daten von außerhalb des Körpers Von außerhalb des Körpers verwenden einem proprietären Handheld-Lesegerät.
                                                             <br>
                                                             <br>
                                                            <?php endif ?> 
                                                             Die Privatsphäre unserer Patienten, sowie die Vertraulichkeit, haben für uns den höchsten Stellenwert. Die Datenbanken, in denen die Geräte- und Patienten-Daten während des Registrierungsverfahrens eingegeben wurden, werden in sicheren Datenzentren gespeichert und gewartet. Diese Datenzentren unterliegen den gleichen strengen Datenschutzrichtlinien der geltenden Rechtsvorschriften, gemäß den gleichen Informations-Sicherheits-Protokollen, die derzeit in Krankenhäusern und OP-Zentren angewandt werden. Jegliche zur Verfügung  gestellten personenbezogene Daten, werden nur nach Absprache und mit der Genehmigung des Patienten freigegeben. Ihre E-Mail-Adresse oder personenbezogene Daten werden von uns ohne Ihre Zustimmung nicht freigegeben.
                                                             <br>
                                                             <br>
                                                            In Übereinstimmung mit der "Brustvergrößerung mit der Motiva-Implantat-Matrix®: Informationen für den Patienten, bitte kontaktieren Sie Ihren Arzt umgehend, sollten unerwartete Symptome auftreten, die ihres Erachtens mit Ihrer chirurgischen Brustvergrößerung zusammenhängen.
                                                            <br>
                                                            <br>
                                                            Denken Sie bitte daran, dass Ihre Motiva Implant Matrix<sup>®</sup> Brustimplantate einer auf die Lebensdauer des Geräts beschränkten Garantie unterliegen, insoweit die Establishment Labs ein Ersatz-Implantat für jedes Motiva-Implantat Matrix<sup>®</sup> Brust-Implantat liefert, das einen Riss aufweist. Darüber hinaus bietet Establishment Labs im Falle eines von der Baker-Klasse III oder IV Kapselkontraktur betroffenen Implantates, sein Austauschprogramm. Einzelheiten der Garantie werden im Dokument Always Confident Warranty<sup>®</sup>, beschrieben, wie durch  unsere Vertriebspartner zur Verfügung gestellt, oder auch auf <a  href="www.motivaimplants.com"><br>www.motivaimplants.com</a> eingesehen werden kann. 
                                                            In ausgewählten Märkten bieten wir unser <?php echo ($years)?>Y Motiva- Zusatzgarantie-Programm zur finanziellen Unterstützung für Überarbeitungs-Operationen bei Implantaten-Rissen, Implantat-Drehung und Kapselkontraktur der Baker-Klasse III oder IV. Dieses Programm bietet eine Garantieabsicherung von bis zu <?php echo ($years)?> Jahren, ab dem Zeitpunkt der Operation
                                                            <br>
                                                                    <b><p style="color:#c89c3c;">Falls Sie sich für diese erweiterte Garantie interessieren und überprüfen möchten, ob  sie auf Ihre Implantate angewendet werden kann, tun Sie dies bitte auf: </p></b><br>
                                                                    <p align="center"><a  href="https://register.motivaimagine.com/"></font><img src="http://motivaimplants.com/letters/images/Warranty-Mail-Button.jpg"><br>(Garantielaufzeit verlängern)</a></p>.<br><br>
                                                            Wir arbeiten mit den neuesten Technologien, um Ihnen die innovativsten Implantate auf dem Markt zu bieten, mit Ihrer Sicherheit als höchster Priorität, und vor allem eine ethische Produktgarantie. Mit unserer "Always Confident Garantie<sup>®</sup>" und in ausgewählten Märkten mit unserem <?php echo ($years)?>Y Motiva-Zusatzgarantie-Programm, haben wir ein unvergleichbares Support-System geschaffen... es steht Ihnen jederzeit zur Verfügung.
                                                            <br>
                                                            <br>
                                                            <br>
                                                            Seien Sie zuversichtlich... Sie haben Motiva!
                                                            <br>
                                                            <br>
                                                            Mit freundlichen Grüßen,
                                                            <br>
                                                            Juan José Chacón-Quirós
                                                            <br>
                                                            Geschäftsführer
                                                            <br>
                                                            Establishment Labs
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td class="area-2" style="padding:15px 20px 5px 21px; background-color:#652d89; border-radius:2px">
                        <table width="100%" cellpadding="0" cellspacing="0">
                           <tr>
                              <td>
                                 <table class="flexible" width="410" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td mc:edit="block-33" class="aligncenter">
                                          <a href="http://motivaimplants.com/"><img src="http://motivaimplants.com/letters/images/motivaimagine.png" border="0" style="vertical-align:top;" width="220" height="37" alt="MotivaImagine" /></a>
                                       </td>
                                    </tr>
                                  </table>
                                 <table class="flexible" width="160" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
                                          <a href="https://www.facebook.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-facebook.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Facebook Icon" /></a>
                                       </td>
                                       <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
                                          <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://www.facebook.com/motivaimplants">Folgen Sie uns auf Facebook</a>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
                                          <a href="https://twitter.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-twitter.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Twitter Icon" /></a>
                                       </td>
                                       <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
                                          <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://twitter.com/motivaimplants">Folgen Sie uns auf Twitter</a>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
                                          <a href="https://instagram.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-instagram.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Instagram Icon" /></a>
                                       </td>
                                       <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
                                          <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://instagram.com/motivaimplants">Folgen Sie uns auf Instagram</a>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td align="center" style="padding:2px 10px 7px;">
               <table class="flexible" width="600" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                  <tr>
                     <td class="no-float" align="center">
                        <table class="center" cellpadding="0" cellspacing="0">
                           <tr>
                              <td style="font:normal 9px/11px Helvetica, Arial, sans-serif; color:#888888;">
                                 Sollten Sie diese e-Mail irrtümlicherweise erhalten haben, oder sollten Ihre Angaben inkorrekt sein, oder sollten Sie mit den auf unserer Website: <a style="text-decoration:none; color:#111111;" href="http://motivaimplants.com/">www.motivaimplants.com enthaltenen Datenschutz- und Nutzungsbedingungen nicht einverstanden sein.</a>.
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
   </body>
</html>