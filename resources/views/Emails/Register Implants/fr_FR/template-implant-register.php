﻿<!-- Motiva Warranty_FR -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <title>Motiva Implants Matrix</title>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <style type="text/css">
  * {
   -webkit-text-size-adjust: none;
   -webkit-text-resize: 100%;
   text-resize: 100%;
 }
 .no-link a{
   text-decoration:none !important;
   color:#fff !important;
 }
 .no-link-2 a{
   text-decoration:none !important;
   color:#fff !important;
 }
 sup{vertical-align: super; font-size: x-small;}
 @media only screen and (max-width:750px) {
   table[class="wrapper"]{min-width:320px !important;}
   table[class="flexible"]{width:100% !important;}
   td[class="img-flex"] img{
    width:100% !important;
    height:auto !important;
  }
  td[class="no-float"]{float:none !important;}
  table[class="center"]{margin:0 auto !important;}
  td[class="header"]{padding:5px 10px 0 !important;}
  td[class="area-1"]{padding:10px 10px !important;}
  td[class="area-2"]{padding:15px 10px !important;}
  td[class="post"]{padding:0 0 20px;}
  td[class="post"] td[class="holder"]{
    padding:0 0 15px !important;
    height:auto !important;
  }
  td[class="aligncenter"],
  td[class="no-link"]{text-align:center !important;}
  td[class="col"],
  td[class="sidebar"]{padding:10px !important;}
  td[class="col"] td[class="holder"],
  td[class="sidebar"] td[class="text"]{padding:10px 0 0 !important;}
  td[class="col"] td[class="holder-2"]{height:auto !important;}
}
</style>
</head>
<body style="margin:0; padding:0;" bgcolor="#ffffff" link="#f5f5f5" >
 <table class="wrapper" width="100%" cellspacing="0" cellpadding="0" style="background-color: #ffffff; background-repeat:repeat;">
  <tr>
   <td height="10" style="font-size:0; line-height:0;" >&nbsp;</td>
 </tr>
 <tr>
   <td align="center" style="padding:0 5px 26px;">
    <table class="flexible" width="750" cellpadding="0" cellspacing="0">
     <tr>
      <td class="area-1" style="padding:10px 0px 12px; background-image:url(http://motivaimplants.com/letters/images/gold-bg.png); background-size: 750px 63px; background-repeat: no-repeat; border-radius:2px" bgcolor="#ffffff">
       <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
         <td class="header">
          <table class="flexible" width="750" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
           <tr>
            <td mc:edit="block-1" class="aligncenter" align="center" style="padding:0 0 12px;">
             <a href="http://motivaimplants.com/"><img src="http://motivaimplants.com/letters/images/motiva.png" border="0" style="vertical-align:top;" width="135" height="45" alt="Motiva Implant Matrix" /></a>
           </td>
         </tr>
       </table>
     </td>
   </tr>
   <tr>
     <td mc:edit="block-3" class="img-flex" style="padding:0 0 8px;">
      <img src="http://motivaimplants.com/letters/images/img-regular-implant.jpg" style="vertical-align:top; border-radius: 0px 0px 2px 2px" width="750" height="250" alt="Your Trusted Partner: Motiva Always Confident Warranty" />
    </td>
  </tr>
  <tr>
   <td style="padding:0 20px 30px;">
    <table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
     <tr>
      <td class="post">
       <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
         <td class="holder" height="70" valign="top">
          <table width="100%" cellpadding="0" cellspacing="0">
           <tr>
            <td mc:edit="block-4" style="font:12px/16px Helvetica, Arial, sans-serif; font-weight: 300; color:#9c9c9c; padding:0 0 10px; ">
             Date: <?php echo ($today)?>
           </td>
         </tr>
         <tr>
          <td mc:edit="block-4" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#222; font-weight: normal; padding:0 0 10px; ">
           Chère. <?php echo ($name)?>
         </td>
       </tr>
       <tr>
        <td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
         Félicitations ! Vous avez enregistré vos Implants Motiva avec succès!
         <br>
         <br>
         Veuillez lire les informations ci-dessous afin de savoir si elles correspondent bien aux informations personnelles de votre Carte d'Identité Motiva.                                                                    
       </td>
     </tr>
   </table>
 </td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
 <td style="padding:0 20px 42px;">
  <?php if ($hasLeftImplant):?>
    <table class="flexible" width="345" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
     <tr>
      <td class="post">
       <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
         <td class="holder" height="100" valign="top">
          <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
             <td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#222222; padding:0 0 10px;">
              Implant côté gauche
            </td>
          </tr>
          <tr>                                                          
           <td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666;">
            <span style="color:#222; font-weight: normal;">Numéro de série:</span><?php echo ($snL)?><br>
            <span style="color:#222; font-weight: normal;">Référence:</span> <?php echo ($referenceL)?><br>
            <span style="color:#222; font-weight: normal;">Volume:</span> <?php echo ($volumeL)?> cc<br>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</td>
</tr>
</table>
<?php endif ?>
<?php if ($hasRightImplant):?>
 <table class="flexible" width="345" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
  <tr>
   <td class="post">
    <table width="100%" cellpadding="0" cellspacing="0">
     <tr>
      <td class="holder" height="100" valign="top">
       <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
         <td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#222222; padding:0 0 10px;">
          Implant côté droit
        </td>
      </tr>
      <tr>
       <td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666;">
        <span style="color:#222; font-weight: normal;">Numéro de série:</span><?php echo ($snR)?><br>
        <span style="color:#222; font-weight: normal;">Référence:</span> <?php echo ($referenceR)?><br>
        <span style="color:#222; font-weight: normal;">Volume:</span> <?php echo ($volumeR)?> cc<br>
      </td>
    </tr>
  </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
<?php endif ?>
</td>
</tr>
<tr>
 <td style="padding:0 20px 30px;">
  <table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
   <tr>
    <td class="post">
     <table width="100%" cellpadding="0" cellspacing="0">
      <tr>
       <td class="holder" height="70" valign="top">
        <table width="100%" cellpadding="0" cellspacing="0">
         <tr>
          <td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
           Les informations que vous avez fournies seront entrées dans le Système de Traçabilité d'Establishment Labs, ce qui vous permettra, en tant que porteur du ou des dispositifs médicaux reçus, d'être localisé et de recevoir des notifications.
           <br>
           <br>
           Establishment Labs vous assure de protéger vos données personnelles et qu'elles seront traitées conformément à notre Politique de Confidentialité et réglementation en vigueur incluant la Directive 95/46/EC du Parlement européen et du Conseil datant du 24 octobre 1995 et la Loi sur la protection des données (Data Protection Act, RU).
           <br>
           <br>
           Vos implants mammaires contiennent Q Inside Safety Technology<sup>™</sup>, qui permet aux médecins de vérifier le nom du fabricant, le numéro de série, le numéro de lot, le numéro de lot et d'autres données spécifiques à l'implant, à l'extérieur du corps à l'aide d'un lecteur de poche propriétaire.
           <br>
           <br>
           La vie privée et la confidentialité du patient sont prioritaires pour nous. Les bases de données qui collectent les informations du patient et de ses implants grâce au processus d'enregistrement, sont maintenues dans des centres de données sécurisés qui respectent nos directives de confidentialité très strictes conformément à la législation applicable et suivant les mêmes protocoles de sécurité des informations en vigueur dans les hôpitaux et les centres chirurgicaux. Toutes les informations personnelles seront uniquement divulguées comme convenu avec l'autorisation du patient. Nous ne communiquerons pas votre adresse e-mail ou vos informations personnelles sans votre consentement.
           <br>
           <br>
           Selon l'augmentation mammaire effectuée avec Motiva Implant Matrix<sup>®</sup> : information pour le patient, si vous ressentez des symptômes inattendus et que vous pensez qu'ils sont en relation avec la chirurgie de votre augmentation mammaire, veuillez contacter immédiatement votre chirurgien.
           <br>
           <br>
           N'oubliez pas que vos implants mammaires Motiva Implant Matrix<sup>®</sup> sont couverts par une garantie limitée, tout au long de la durée de vie de votre dispositif, qui vous assure un remplacement d'implant par Establishment Labs pour n'importe quel implant mammaire Motiva Implant Matrix<sup>®</sup> qui aurait pu se rompre. De plus, dans le cas où votre implant serait affecté par une contracture capsulaire de grade III ou IV selon l'échelle de Baker, Establishment Labs vous propose son Programme de Politique de Substitution. Complétez les détails de la garantie décrit dans le document Always Confident Warranty<sup>®</sup> qui peut être fourni par notre distributeur, ces informations seront également disponibles sur www.motivaimplants.com. Nous proposons notre Programme d'Extension de Garantie Motiva <?php echo ($years)?>Y dans un contexte particulier afin d'apporter une aide financière lors de ré-opérations, en cas de rupture ou rotation d'implant ou de contracture capsulaire de grade III ou IV selon l'échelle de Baker. Ce programme vous permet d'obtenir une couverture pendant <?php echo ($years)?> ans à partir de la date de votre intervention chirurgicale.
           <br>
           <b><p style="color:#c89c3c;">Si vous êtes intéressé par cette extension de garantie et que vous souhaitez savoir si elle s'applique à vos implants, rendez-vous sur </p></b><br>
           <p align="center"><a  href="https://register.motivaimagine.com/"></font><img src="http://motivaimplants.com/letters/images/Warranty-Mail-Button.jpg"><br>(Extension de garantie)</a></p>.
           <br>
           <br>
           Nous travaillons avec les toutes dernières technologies pour vous offrir les implants les plus innovants du marché en faisant de votre sécurité notre priorité mais surtout en vous proposant une garantie éthique du produit. Avec notre garantie Always Confident Warranty<sup>®</sup> et, selon les produits, notre Programme d'Extension de Garantie Motiva <?php echo ($years)?>Y, nous avons créé un système de soutien sans précédent... il est fait pour vous, quand vous le voulez et si vous le souhaitez.
           <br><br><br>
           Soyez confiant... Vous êtes avec Motiva !
           <br><br>
           Cordialement,<br>
           Juan José Chacón-Quirós<br>
           PDG<br>
           Establishment Labs
         </td>
       </tr>
     </table>
   </td>
 </tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
  <td class="area-2" style="padding:15px 20px 5px 21px; background-color:#652d89; border-radius:2px">
   <table width="100%" cellpadding="0" cellspacing="0">
    <tr>
     <td>
      <table class="flexible" width="410" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
       <tr>
        <td mc:edit="block-33" class="aligncenter">
         <a href="http://motivaimplants.com/"><img src="http://motivaimplants.com/letters/images/motivaimagine.png" border="0" style="vertical-align:top;" width="220" height="37" alt="MotivaImagine" /></a>
       </td>
     </tr>
   </table>
   <table class="flexible" width="160" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
     <tr>
      <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
       <a href="https://www.facebook.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-facebook.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Facebook Icon" /></a>
     </td>
     <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
       <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://www.facebook.com/motivaimplants">Suivez-nous sur Facebook</a>
     </td>
   </tr>
   <tr>
    <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
     <a href="https://twitter.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-twitter.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Twitter Icon" /></a>
   </td>
   <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
     <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://twitter.com/motivaimplants">Suivez-nous sur Twitter</a>
   </td>
 </tr>
 <tr>
  <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
   <a href="https://instagram.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-instagram.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Instagram Icon" /></a>
 </td>
 <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
   <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://instagram.com/motivaimplants">Suivez-nous sur Instagram</a>
 </td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
 <td align="center" style="padding:2px 10px 7px;">
  <table class="flexible" width="600" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
   <tr>
    <td class="no-float" align="center">
     <table class="center" cellpadding="0" cellspacing="0">
      <tr>
       <td style="font:normal 9px/11px Helvetica, Arial, sans-serif; color:#888888;">
        Si vous recevez cet e-mail par erreur, si vos informations ne sont pas correctes ou si vous n'êtes pas en accord avec la Politique de Confidentialité et les Conditions Générales d'Utilisation contenues sur notre site Internet:<a style="text-decoration:none; color:#111111;" href="http://motivaimplants.com/">www.motivaimplants.com</a>.
      </td>
    </tr>
  </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</body>
</html>