<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Welcome to MotivaImagine</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<style type="text/css">
		* {
			-webkit-text-size-adjust: none;
			-webkit-text-resize: 100%;
			text-resize: 100%;
		}
		.no-link a{
			text-decoration:none !important;
			color:#fff !important;
		}
		.no-link-2 a{
			text-decoration:none !important;
			color:#fff !important;
		}
		sup{vertical-align: super; line-height: 0; font-size: 0.8em;}
		@media only screen and (max-width:750px) {
			table[class="wrapper"]{min-width:320px !important;}
			table[class="flexible"]{width:100% !important;}
			td[class="img-flex"] img{
				width:100% !important;
				height:auto !important;
			}
			td[class="no-float"]{float:none !important;}
			table[class="center"]{margin:0 auto !important;}
			td[class="header"]{padding:5px 10px 0 !important;}
			td[class="area-1"]{padding:0 !important;}
			td[class="area-2"]{padding:15px 10px !important;}
			td[class="post"]{padding:0 0 20px;}
			td[class="post"] td[class="holder"]{
				padding:0 0 15px !important;
				height:auto !important;
			}
			td[class="aligncenter"],
			td[class="no-link"]{text-align:center !important;}
			td[class="col"],
			td[class="sidebar"]{padding:10px !important;}
			td[class="col"] td[class="holder"],
			td[class="sidebar"] td[class="text"]{padding:10px 0 0 !important;}
			td[class="col"] td[class="holder-2"]{height:auto !important;}
		}
		.tg  {border-collapse:collapse;border-spacing:0;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
		.tg .tg-d5wv{background-color:#a38546;color:#ffffff;text-align:center;vertical-align:top}
		.tg .tg-yw4l{vertical-align:top}
	</style>
</head>
<body style="margin:0; padding:0;" bgcolor="#eeeeee" link="#f5f5f5" >
	<table class="wrapper" width="100%" cellspacing="0" cellpadding="0">
		<tr><td height="10" style="font-size:0; line-height:0;" >&nbsp;</td></tr>
		<tr>
			<td align="center" style="padding:10px 5px 26px 5px;">
				<table class="flexible" width="750" cellpadding="0" cellspacing="0">
					<tr>
						<td class="area-1" style="border-radius: 6px 6px 3px 3px" bgcolor="#ffffff">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td mc:edit="block-3" class="img-flex" style="padding:0 0 8px;">
										<img src="http://motivaimplants.com/letters/cover.jpg" style="vertical-align:top; border-radius: 6px 6px 0px 0px" width="750" height="400" alt="Motiva Implants Logo" />
									</td>
								</tr>
								<tr>
									<td style="padding:20px 20px 30px 20px;">
										<table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td class="post">
													<table width="100%" cellpadding="0" cellspacing="0">
														<tr>
															<td class="holder" height="70" valign="top">
																<table width="100%" cellpadding="0" cellspacing="0">
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Sveiki atvykę į "MotivaImagine"
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Jūsų patikimas partneris
																		</td>
																	</tr>
																	<tr><!-- MAIN TITLE -->
																		<td mc:edit="block-5" style="font:22px/26px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546; padding:0px 0px 22px 0px; text-align: center;">
																			Motiva Always Confident Warranty&reg;
																		</td>
																	</tr>
																	<tr><!-- MAIN CONTENT -->
																		<td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
																			Gerb. p. <?=/*php*/$name/*php*/?>
																			<span style="float: right; color:#a1a1a1;"><?=/*php*/date('d-M-Y')/*php*/?></span>
																			<br/>
																			<br/>
																			Sveikiname, Jūs sėkmingai užsiregistravote „Motiva Implants“ puslapyje!
																			<br>
																			<br>
																			Perskaitykite toliau pateikiamą informaciją ir patikrinkite, ar ji atitinka Jūsų „Motiva“ ID kortelėje nurodytą informaciją.
																			<br/>
																			<br/>
																			<!-- Implant Information -->
																			<table width="100%" cellpadding="0" cellspacing="0">
																				<tr>
																					<td>
																						<?php if ( $has_left_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="left">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										Implantas kairėje pusėje
																									</td>
																								</tr>
																								<tr>                                                          
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">Serijos numeris: </span><?=/*php*/$left_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Nuoroda: </span><?=/*php*/$left_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Tūris: </span><?=/*php*/$left_vol/*php*/?> cc<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																						<?php if ( $has_right_implant ): ?>
																							<table width="45%" cellpadding="0" cellspacing="0" align="right">
																								<tr>
																									<td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#a38546; padding: 10px; text-align: center;">
																										Implantas dešinėje pusėje
																									</td>
																								</tr>
																								<tr>
																									<td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666; padding: 10px; text-align: center;">
																										<span style="color:#222; font-weight: normal;">Serijos numeris: </span><?=/*php*/$right_sn/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Nuoroda: </span><?=/*php*/$right_ref/*php*/?><br>
																										<span style="color:#222; font-weight: normal;">Tūris: </span><?=/*php*/$right_vol/*php*/?> cc<br>
																									</td>
																								</tr>
																							</table>
																						<?php endif ?>
																					</td>
																				</tr>
																			</table>
																			<br>
																			<br>
																			Jūsų pateikta informacija bus suvesta į „Establishment Labs“ atsekamumo sistemą, kuri užtikrina, jog jei mums reikėtų perduoti tam tikrą informaciją, bus galima surasti būtent Jus, kurie nešiojate šiuos medicininius produktus.
																			<br>
																			<br>
																			„Establishment Labs“ užtikrina, kad Jūsų asmeninė informacija būtų saugoma ir tvarkoma pagal mūsų Privatumo politiką ir taikomus įstatymus, įskaitant 1995 m. Europos Parlamento ir Tarybos direktyvą 95/46/EC bei (JK) Duomenų apsaugos įstatymą.
																			<br>
																			<br>
																			<!-- Only if has implants -->
																			<?php if ( isset($is_q_inside) ? $is_q_inside : false ): ?>
																				Jūsų krūtų implantai yra "Q Inside Safety Technology ™", leidžianti gydytojams patikrinti gamintojo pavadinimą, serijos numerį, serijos numerį, serijos numerį ir kitus implantų specifinius duomenis iš kūno išorės, naudojant patentuotą delninį skaitytuvą.
																				<br>
																				<br>
																			<?php endif ?>
																			Pacientų privatumas ir konfidencialumas yra mūsų prioritetas. Duomenų bazės, kuriose saugomi duomenys apie produktus ir pacientus, surinkti registracijos metu, yra laikomos saugiuose duomenų centruose, kurie griežtai laikosi privatumo rekomendacijų, nustatytų taikomuose įstatymuose bei vadovaujasi tais pačiais informacijos apsaugos protokolais, kurie galioja ligoninėse ir chirurgijos centruose. Visa pateikta asmeninė informacija gali būti atskleista tik pagal susitarimą ir leidus pacientui. Be jūsų sutikimo mes niekam neatskleisime Jūsų elektroninio pašto adreso ar asmeninės informacijos. 
																			<br>
																			<br>
																			Dėl krūtų padidinimo su „Motiva Implant Matrix<sup>®</sup>“: Informacija pacientui, nedelsiant kreipkitės į chirurgą, jei pasireikštų nenumatyti simptomai, kurie, Jūsų nuomone, galėtų būti susiję su krūtų didinimo operacija.
																			<br>
																			<br>

																			Atminkite, kad „Motiva Implant Matrix®“ krūtų implantams galioja ribotas draudimas implanto naudojimo laikotarpiui, pagal kurį „Establishment Labs“ pakeis įplyšusį „Motiva Implant Matrix<sup>®</sup>“ krūtų implantą nauju. Be to, jei įvyktų III ir IV laipsnio (Baker skalėje) kapsulės kontraktūra, „Establishment Labs“ taiko implanto pakeitimo programą. Išsami informacija apie garantiją pateikiama „Always Confident Warranty<sup>®</sup>“ garantijos dokumente, kurią įteikia platintojas arba ją galima peržiūrėti interneto svetainėje adresu <a style="text-decoration:none; color:#a38546;" href="https://motivaimplants.com">www.motivaimplants.com</a>. 
																			<br>
																			Pasirinktose rinkose mes siūlome <?=/*php*/$years/*php*/?> metų „Motiva“ garantijos pratęsimo programą, pagal kurią finansuojame kartotinės operacijos išlaidas, jei implantas suplyšta, apsisuka arba įvyksta III ir IV laipsnio (Baker skalėje) kapsulės kontraktūra. Pagal šią programą padengiamos išlaidos iki <?=/*php*/$years/*php*/?> metų nuo operacijos dienos.
																			<br>
																			<br>
																			<p style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#a38546;">
																				<b>Jei susidomėjote šiuo pratęstos garantijos draudimu ir norėtumėte pasitikrinti, ar jis taikoma Jūsų turimiems implantams, tai galite padaryti adresu</b>
																				<a style="text-decoration:none; color:#888;" href="https://register.motivaimagine.com/">register.motivaimagine.com</a>
																			</p>
																			<br>
																			<p align="center">
																				<a href="https://register.motivaimagine.com/" style="text-decoration:none; background: #a38546; color:#fff; font-weight: 300; padding:8px 14px; border-radius:30px"><b>Pratęsta garantija</b></a>
																			</p>
																			<br>
																			<br>
																			Mes naudojame naujausias technologijas, kad galėtume pasiūlyti Jums rinkoje naujausius implantus užtikrindami Jūsų saugumą ir, svarbiausia, etišką produktų garantiją. Siūlydami „Always Confident Warranty<sup>®</sup>“ garantiją bei pasirinktose rinkose pristatomą <?=/*php*/isset($years) ? $years : '{{years}}'/*php*/?> metų „Motiva“ pratęstos garantijos programą, mes sukūrėme analogo neturinčią pagalbos sistemą.... Ji skirta Jums, jei ir kai jos reikia.
																			<br>
																			<br/>
																			<br/>
																			Pasitikėkite... Jūs turite „Motiva“!
																			<br>
																			<br>
																			Pagarbiai,<br>
																			Juan José Chacón-Quirós<br>
																			Generalinis direktorius<br>
																			Establishment Labs<br>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="area-2" style="padding:15px 20px 5px 21px; background-color:#eeeeee;">
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td>
										<table class="flexible" width="410" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-33" class="aligncenter">
													<a href="http://motivaimagine.com/"><img src="http://motivaimplants.com/letters/motivaimagine.png" border="0" style="vertical-align:top; padding: 0px 0px 12px 0px" width="210" height="37" alt="MotivaImagine" /></a>
												</td>
											</tr>
											<tr>
												<td mc:edit="block-35" class="aligncenter" style="font:9px/11px Helvetica, Arial, sans-serif; color:#fff; font-weight: 300;">
													<a style="text-decoration:none; color:#a1a1a1;" href="mailto:customerservice@motivaimplants.com">customerservice@motivaimplants.com</a> <br/>
													<a style="text-decoration:none; color:#a1a1a1; font-weight: 300;" href="http://motivaimplants.com/">www.motivaimplants.com</a> <br/>
												</td>
											</tr>                                                                   
										</table>
										<table class="flexible" width="160" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
											<tr>
												<td mc:edit="block-36" width="160" class="aligncenter" style="padding:10px 0 5px 0; text-align: right;">
													<a href="https://www.facebook.com/motivaimagine/"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-facebook-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Sekite mus „Facebook“ socialiniame tinkle" /></a> <a href="https://twitter.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-twitter-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Sekite mus „Twitter“ socialiniame tinkle" /></a> <a href="https://instagram.com/motivaimplants"><img src="http://motivaimplants.com/newsletter/2017.05.24/images/social-instagram-grey.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Sekite mus „Instagram“ socialiniame tinkle" /></a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center" style="padding:2px 10px 7px;">
				<table class="flexible" width="600" cellpadding="0" cellspacing="0" bgcolor="#eeeeee">
					<tr>
						<td class="no-float" align="center">
							<table class="center" cellpadding="0" cellspacing="0">
								<tr>
									<td style="font:normal 9px/11px Helvetica, Arial, sans-serif; color:#888888; text-align: center;">
										Jei Jūs šį elektroninį laišką gavote per klaidą, Jūsų informacija yra neteisinga arba Jūs nesutinkate su mūsų interneto svetainėje pateikiama Privatumo politika ir naudojimo sąlygomis: <a style="text-decoration:none; color:#555555;" href="http://motivaimplants.com/">www.motivaimplants.com</a>.
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>