﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <title>Motiva Implants Matrix</title>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <style type="text/css">
         * {
         -webkit-text-size-adjust: none;
         -webkit-text-resize: 100%;
         text-resize: 100%;
         }
         .no-link a{
         text-decoration:none !important;
         color:#fff !important;
         }
         .no-link-2 a{
         text-decoration:none !important;
         color:#fff !important;
         }
         sup{vertical-align: super; font-size: x-small;}
         @media only screen and (max-width:750px) {
         table[class="wrapper"]{min-width:320px !important;}
         table[class="flexible"]{width:100% !important;}
         td[class="img-flex"] img{
         width:100% !important;
         height:auto !important;
         }
         td[class="no-float"]{float:none !important;}
         table[class="center"]{margin:0 auto !important;}
         td[class="header"]{padding:5px 10px 0 !important;}
         td[class="area-1"]{padding:10px 10px !important;}
         td[class="area-2"]{padding:15px 10px !important;}
         td[class="post"]{padding:0 0 20px;}
         td[class="post"] td[class="holder"]{
         padding:0 0 15px !important;
         height:auto !important;
         }
         td[class="aligncenter"],
         td[class="no-link"]{text-align:center !important;}
         td[class="col"],
         td[class="sidebar"]{padding:10px !important;}
         td[class="col"] td[class="holder"],
         td[class="sidebar"] td[class="text"]{padding:10px 0 0 !important;}
         td[class="col"] td[class="holder-2"]{height:auto !important;}
         }
      </style>
   </head>
   <body style="margin:0; padding:0;" bgcolor="#ffffff" link="#f5f5f5" >
      <table class="wrapper" width="100%" cellspacing="0" cellpadding="0" style="background-color: #ffffff; background-repeat:repeat;">
         <tr>
            <td height="10" style="font-size:0; line-height:0;" >&nbsp;</td>
         </tr>
         <tr>
            <td align="center" style="padding:0 5px 26px;">
               <table class="flexible" width="750" cellpadding="0" cellspacing="0">
                  <tr>
                     <td class="area-1" style="padding:10px 0px 12px; background-image:url(http://motivaimplants.com/letters/images/gold-bg.png); background-size: 750px 63px; background-repeat: no-repeat; border-radius:2px" bgcolor="#ffffff">
                        <table width="100%" cellpadding="0" cellspacing="0">
                           <tr>
                              <td class="header">
                                 <table class="flexible" width="750" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td mc:edit="block-1" class="aligncenter" align="center" style="padding:0 0 12px;">
                                          <a href="http://motivaimplants.com/"><img src="http://motivaimplants.com/letters/images/motiva.png" border="0" style="vertical-align:top;" width="135" height="45" alt="Motiva Implant Matrix" /></a>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td mc:edit="block-3" class="img-flex" style="padding:0 0 8px;">
                                 <img src="http://motivaimplants.com/letters/images/img-regular-implant.jpg" style="vertical-align:top; border-radius: 0px 0px 2px 2px" width="750" height="250" alt="Your Trusted Partner: Motiva Always Confident Warranty" />
                              </td>
                           </tr>
                           <tr>
                              <td style="padding:0 20px 30px;">
                                 <table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td class="post">
                                          <table width="100%" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td class="holder" height="70" valign="top">
                                                   <table width="100%" cellpadding="0" cellspacing="0">
                                                      <tr>
                                                         <td mc:edit="block-4" style="font:12px/16px Helvetica, Arial, sans-serif; font-weight: 300; color:#9c9c9c; padding:0 0 10px; ">
                                                            日付: <?php echo ($today)?>
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td mc:edit="block-4" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#222; font-weight: normal; padding:0 0 10px; ">
                                                            親愛なる <?php echo ($name)?> さん、
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
                                                            モティーヴァインプラントのご登録、おめでとうございます。
                                                            <br>
                                                            <br>
                                                            以下の情報を読んで、あなたのMotiva IDカードの情報と一致することを確認してください。 
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                           <tr>
                              <td style="padding:0 20px 42px;">
                               <?php if ($hasLeftImplant):?>
                                 <table class="flexible" width="345" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td class="post">
                                          <table width="100%" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td class="holder" height="100" valign="top">
                                                   <table width="100%" cellpadding="0" cellspacing="0">
                                                     <tr>
                                                         <td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#222222; padding:0 0 10px;">
                                                            左側インプラント           
                                                         </td>
                                                      </tr>
                                                      <tr>                                                          
                                                         <td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666;">
                                                            <span style="color:#222; font-weight: normal;">シリアル番号:</span><?php echo ($snL)?><br>
                                                            <span style="color:#222; font-weight: normal;">リファレンス:</span> <?php echo ($referenceL)?><br>
                                                            <span style="color:#222; font-weight: normal;">ボリューム:</span> <?php echo ($volumeL)?> cc<br>
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                                <?php endif ?>
                                <?php if ($hasRightImplant):?>
                                 <table class="flexible" width="345" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td class="post">
                                          <table width="100%" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td class="holder" height="100" valign="top">
                                                   <table width="100%" cellpadding="0" cellspacing="0">
                                                      <tr>
                                                         <td mc:edit="block-4" style="font:14px/16px Helvetica, Arial, sans-serif;  color:#222222; padding:0 0 10px;">
                                                            右サイドインプラント
                                                         </td>
                                                      </tr>
                                                      <tr>
                                                         <td mc:edit="block-5" style="font:300 13px/16px Helvetica, Arial, sans-serif; color:#666;">
                                                            <span style="color:#222; font-weight: normal;">シリアル番号:</span><?php echo ($snR)?><br>
                                                            <span style="color:#222; font-weight: normal;">リファレンス:</span> <?php echo ($referenceR)?><br>
                                                            <span style="color:#222; font-weight: normal;">ボリューム:</span> <?php echo ($volumeR)?> cc<br>
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                                <?php endif ?>
                              </td>
                           </tr>
                           <tr>
                              <td style="padding:0 20px 30px;">
                                 <table class="flexible" width="710" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td class="post">
                                          <table width="100%" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td class="holder" height="70" valign="top">
                                                   <table width="100%" cellpadding="0" cellspacing="0">
                                                      <tr>
                                                         <td mc:edit="block-5" style="font:14px/18px Helvetica, Arial, sans-serif; font-weight: 300; color:#666;">
                                                             あなたの提供した情報は、あなたが確立された医療機器の受託者として、通知イベントの時に連絡されることを保証するEstablishment Labs Traceability Systemに入力されます。
                                                              <br>
                                                              <br>
                                                             Establishment Labsは、あなたの個人情報が私たちのプライバシーポリシーと1995年10月24日の欧州議会および理事会の指令95/46 / ECと1995年10月24日の理事会のデータ保護法（英国）に従って保護されることを保証します。
                                                              <br>
                                                              <br>
                                                             私たちは、患者のプライバシーと守秘義務を最も重視しています。登録プロセスから収集されたデバイスと患者のデータを保存するデータベースは安全なデータセンターで管理され、病院や外科センターで現在実施されているのと同じ情報セキュリティプロトコルに従っています。提供されるすべての個人情報は、合意された通り、患者の承認を得て開示されます。お客様の同意なしに、お客様の電子メールアドレスおよび個人情報を共有しません。
                                                             <br>
                                                             <br>
                                                             「Motiva ImplantMatrix<sup>®</sup>による乳房増強：患者のための情報」に従って、あなたの乳房増強外科や手術に関すると思われる予期しない症状が発生した場合は、すぐに外科医に連絡してください。
                                                            <br>
                                                            <br>
                                                            あなたのMotiva ImplantMatrix<sup>®</sup>乳房インプラントは、生存期間が限られていることを覚えておいてください。そして、Establishment Labsは、破損したMotiva ImplantMatrix<sup>®</sup>乳房インプラントの代替インプラントを提供します。さらに、Baker Grades III・IV被膜拘縮の影響を受けたインプラントの場合に、Establishment Labsは交換ポリシープログラムを提供しています。保証の詳細については、私たちの販売代理店から提供されているか、もしくはwww.motivaimplants.comでレビューすることができるAlways Confident　Warranty<sup>®</sup>の文書に記載されています。選択された市場では、インプラントの破裂、インプラントの回転、被膜拘縮Baker Grades III・IVの場合に、改訂手術のための資金援助を提供する<?php echo ($years)?>Y Motiva延長保証プログラムを提供しています。このプログラムは、手術の日から最大<?php echo ($years)?>年間のカバレッジを提供します。
                                                                    <b><p style="color:#c89c3c;">この延長保証に興味があり、インプラントに適用されているかどうか確認したい場合は： </p></b><br>
                                                                    <p align="center"><a  href="https://register.motivaimagine.com/"></font><img src="http://motivaimplants.com/letters/images/Warranty-Mail-Button.jpg"><br(保証を延長する)></a></p>.
                                                                    <br>
                                                                    <br>
                                                            私たちは、あなたの安全を重視し、技術を駆使して市場で最も革新的なインプラントを提供しています。そして、倫理的な製品保証を提供しています。Always Confident Warranty<sup>®</sup>（常に自信を持って保証）と<?php echo ($years)?>Y Motiva Extended Warranty Programの選択された市場で、最も素晴らしいサポートシステムを作成しました...必要な時にあなたのためにあります。
                                                            <br>
                                                            <br>
                                                            <br>
                                                            自身して…モティーヴァがあるから！
                                                            <br><br>
                                                            敬具、<br>
                                                            Juan José Chacón-Quirós<br>
                                                            ワン・ホセー・チヤッコーンキロース<br>
                                                            Establishment Labs
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td class="area-2" style="padding:15px 20px 5px 21px; background-color:#652d89; border-radius:2px">
                        <table width="100%" cellpadding="0" cellspacing="0">
                           <tr>
                              <td>
                                 <table class="flexible" width="410" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td mc:edit="block-33" class="aligncenter">
                                          <a href="http://motivaimplants.com/"><img src="http://motivaimplants.com/letters/images/motivaimagine.png" border="0" style="vertical-align:top;" width="220" height="37" alt="MotivaImagine" /></a>
                                       </td>
                                    </tr>
                                  </table>
                                 <table class="flexible" width="160" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                    <tr>
                                       <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
                                          <a href="https://www.facebook.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-facebook.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Facebook Icon" /></a>
                                       </td>
                                       <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
                                          <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://www.facebook.com/motivaimplants">Facebookでフォロー</a>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
                                          <a href="https://twitter.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-twitter.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Twitter Icon" /></a>
                                       </td>
                                       <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
                                          <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://twitter.com/motivaimplants">Twitterでフォロー</a>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td mc:edit="block-36" width="33" style="padding:0 0 5px;">
                                          <a href="https://instagram.com/motivaimplants"><img src="http://motivaimplants.com/letters/images/social-instagram.png" border="0" style="vertical-align:top;" width="24" height="24" alt="Instagram Icon" /></a>
                                       </td>
                                       <td mc:edit="block-37" style="font:11px/16px Arial, Helvetica, sans-serif; color:#9b6101; padding:0 0 5px;">
                                          <a style="text-decoration:none; color:#a47cbc; font-weight: 300;" href="https://instagram.com/motivaimplants">Instagramでフォロー</a>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td align="center" style="padding:2px 10px 7px;">
               <table class="flexible" width="600" cellpadding="0" cellspacing="0" bgcolor="#ffffff">
                  <tr>
                     <td class="no-float" align="center">
                        <table class="center" cellpadding="0" cellspacing="0">
                           <tr>
                              <td style="font:normal 9px/11px Helvetica, Arial, sans-serif; color:#888888;">
                                 間違いでこのメールを受け取った場合、情報が正しくない場合、または私たちのウェブサイトにあるプライバシーポリシーおよび利用規約に同意しない場合は<a style="text-decoration:none; color:#111111;" href="http://motivaimplants.com/">www.motivaimplants.com</a>をご覧ください。
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
   </body>
</html>