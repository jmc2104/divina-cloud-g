<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <title>Reset your password</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <style type="text/css">
    #ko_onecolumnBlock_5 .textintenseStyle a, #ko_onecolumnBlock_5 .textintenseStyle a:link, #ko_onecolumnBlock_5 .textintenseStyle a:visited, #ko_onecolumnBlock_5 .textintenseStyle a:hover {color: #fff;color: ;text-decoration: none;text-decoration: none;font-weight: bold;}
    #ko_onecolumnBlock_5 .textlightStyle a:visited, #ko_onecolumnBlock_5 .textlightStyle a:hover {color: #3f3d33;color: ;text-decoration: none;text-decoration: ;font-weight: bold;}
  </style>
  <style type="text/css">
    /* CLIENT-SPECIFIC STYLES */
    #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" message */
    .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height: 100%;} /* Force Hotmail to display normal line spacing */
    body, table, td, a{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;} /* Prevent WebKit and Windows mobile changing default text sizes */
    table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;} /* Remove spacing between tables in Outlook 2007 and up */
    img{-ms-interpolation-mode:bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */
    /* RESET STYLES */
    body{margin:0; padding:0;}
    img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
    table{border-collapse:collapse !important;}
    body{height:100% !important; margin:0; padding:0; width:100% !important;}
    /* iOS BLUE LINKS */
    .appleBody a{color:#68440a; text-decoration: none;}
    .appleFooter a{color:#999999; text-decoration: none;}
    /* MOBILE STYLES */
    @media screen and (max-width: 525px)
    {
      /* ALLOWS FOR FLUID TABLES */
      table[class="wrapper"]{
        width:100% !important;
        min-width:0px !important;
      }

      /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
      td[class="mobile-hide"]{
        display:none;}

        img[class="mobile-hide"]{
          display: none !important;
        }

        img[class="img-max"]{
          width:100% !important;
          max-width: 100% !important;
          height:auto !important;
        }

        /* FULL-WIDTH TABLES */
        table[class="responsive-table"]{
          width:100%!important;
        }

        /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */
        td[class="padding"]{
          padding: 10px 5% 15px 5% !important;
        }

        td[class="padding-copy"]{
          padding: 10px 5% 10px 5% !important;
          text-align: center;
        }

        td[class="padding-meta"]{
          padding: 30px 5% 0px 5% !important;
          text-align: center;
        }

        td[class="no-pad"]{
          padding: 0 0 0px 0 !important;
        }

        td[class="no-padding"]{
          padding: 0 !important;
        }

        td[class="section-padding"]{
          padding: 10px 15px 10px 15px !important;
        }

        td[class="section-padding-bottom-image"]{
          padding: 10px 15px 0 15px !important;
        }

        /* ADJUST BUTTONS ON MOBILE */
        td[class="mobile-wrapper"]{
          padding: 10px 5% 15px 5% !important;
        }

        table[class="mobile-button-container"]{
          margin:0 auto;
          width:100% !important;
        }

        a[class="mobile-button"]{
          width:80% !important;
          padding: 15px !important;
          border: 0 !important;
          font-size: 16px !important;
        }

      }
    </style>
  </head>
  <body style="margin: 0; padding: 0;" bgcolor="#ffffff" align="center">
    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="ko_headerBlock_3">
      <tbody>
        <tr>
          <td bgcolor="#ffffff" align="center" style="padding: 20px 15px 20px 15px;">
            <table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
              <!-- LOGO/Title TEXT -->
              <tbody>
                <tr>
                  <td>
                    <!-- CONTENT -->
                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                      <tbody>
                        <tr>
                          <td valign="top" style="padding: 0;" class="mobile-wrapper">
                            <!-- IMAGE -->
                            <table border="0" cellpadding="0" cellspacing="0" width="30%" class="responsive-table" align="CENTER">
                              <tbody>
                                <tr>
                                  <td bgcolor="#ffffff">
                                    <br>
                                    <a target="_new" href="">
                                      <img alt="" image-droppable="" width="250" class="img-max" border="0" style="display: block; font-family: Helvetica, Arial, sans-serif; color: #3F3D33; font-size: 16px;" src="https://register.motivaimagine.com/wp-content/themes/motivaimagine/images/logos/logo-motivaimagine-golden-small.png">
                                    </a>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>  
        </tr>
      </tbody>
    </table>

    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="ko_onecolumnBlock_5">
      <tbody>
        <tr class="row-a">
          <td bgcolor="#ffffff" align="center" class="section-padding" style="padding-top: 30px; padding-left: 15px; padding-bottom: 30px; padding-right: 15px;">
            <table border="0" cellpadding="0" cellspacing="0" width="500" class="responsive-table">
              <tbody>
                <tr>
                  <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tbody>
                        <tr>
                          <td>
                            <!-- COPY -->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tbody>
                                <tr>
                                  <td align="left" class="padding-copy" style="font-size: 15px; font-family: Helvetica, Arial, sans-serif; color: #3F3D33; padding-top: 0px;">
                                    <br>
                                    Dear {{$name}}<br>
                                    <br>
                                  </td>
                                </tr>
                                <tr>
                                  <td align="left" class="padding-copy textlightStyle" style="padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #3F3D33;">
                                    <p style="margin:0px;">
                                      A request to reset the password for your account has been made at MotivaImagine.&nbsp;You may now log in by clicking this link or copying and pasting it to your browser:                                    </p>
                                    <p align="center"> <a href={{$link}}>
                                      {{$link}}                                 </a> </p>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <!-- BULLETPROOF BUTTON -->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="mobile-button-container">
                              <tbody>
                                <tr>
                                  <td align="center" style="padding: 35px 0 0 0;" class="padding-copy">
                                    <table border="0" cellspacing="0" cellpadding="0" class="responsive-table">
                                      <tbody>
                                        <tr>
                                          <td align="center">
                                            <a target="_new" class="mobile-button" style="display: inline-block; font-size: 18px; font-family: Helvetica, Arial, sans-serif; font-weight: normal; color: #ffffff; text-decoration: none; background: url('https://register.motivaimagine.com/wp-content/themes/motivaimagine/images/textures/gold_web.jpg'); padding-top: 15px; padding-bottom: 15px; padding-left: 25px; padding-right: 25px; border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-bottom: 3px solid rgb(151, 120, 72);" href={{$link}}>
                                              Set New Password →</a>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <!-- COPY -->
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                  <tr>
                                    <td align="center" class="padding-copy textlightStyle" style="padding: 50px 0 0 0; font-size: 14px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #3F3D33;">
                                      <p style="margin:0px;">
                                        This link can only be used once and it will expire after one day if it's not used.                                       </p>
                                    </td>
                                  </tr>
                               </table>
                             </td>
                           </tr>
                         </table>
                       </td>
                     </tr>
                   </tbody>
                 </table>
               </td>
             </tr>
           </tbody>
         </table>
       </td>
     </tr>
   </tbody>
 </table>
</td>
</tr>
</tbody>
</table>

<img src="https://u4745828.ct.sendgrid.net/wf/open?upn=bFd6vOFySTSYngoWC8CHXb-2B9-2BIpBBZ3hlBb-2Fo26oSsoVYCLEzzblQQju4-2FKGHnAOl9-2BteLElHKQfSi0GMyBrAdCigz5bq8ZtSwLdLf4ex7owHOoLY1pz3VLnc7lHrVrwWPsg0hTHAJcuDYm1TKJ8RsSe6kigzpdtM2wAUxj4hdeQ0FFk3X4uBL58rvGBL8hJBVkonDul2Jol6KyzLh3csbkvrNWsso1wloZ4fMTV-2FyqP6LVtYmVExIXoAS9n8i3R" alt="" width="1" height="1" border="0" style="height:1px !important;width:1px !important;border-width:0 !important;margin-top:0 !important;margin-bottom:0 !important;margin-right:0 !important;margin-left:0 !important;padding-top:0 !important;padding-bottom:0 !important;padding-right:0 !important;padding-left:0 !important;"/>
</body>
</html>
