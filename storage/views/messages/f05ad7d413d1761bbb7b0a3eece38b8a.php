<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Motivaimagine Web App</title>
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
  <!-- Styles -->
  <link rel="stylesheet" href="<?php echo e(URL::asset('styles/bootstrap.min.css')); ?>" />
  <link rel="stylesheet" href="<?php echo e(URL::asset('styles/custom.css')); ?>" />

  <!-- Body -->
  <div id="homepage" class="container-fluid">
    <div class="row background-login">
      <div class="col-xs-12 main-wrapper">
        <div class="row fixed-center">

        <?php
          LaravelGettext::setLocale( 'es_ES');
          echo  _('About');
          ?>

          

          <div id="login" class="col-xs-12 col-md-4 col-lg-4">
            <!-- Tab Hidden Links -->
            <a id="home-nav" href="#home-tab" aria-controls="home-tab" role="tab" data-toggle="tab"></a>
            <a id="signup-nav" href="#signup-tab" aria-controls="signup-tab" role="tab" data-toggle="tab"></a>
            <a id="login-nav" href="#login-tab" aria-controls="login-tab" role="tab" data-toggle="tab"></a>
            <a id="password-nav" href="#password-tab" aria-controls="password-tab" role="tab" data-toggle="tab"></a>
            <!-- Tabs -->
            <div class="tab-content">

              <!-- HOME TAB -->
              <div id="home-tab" role="tabpanel" class="tab-pane fade in active">
                <h1 class="text-center"> Registration  </h1>
                <div class="btn-group-vertical btn-container" role="group" aria-label="...">
                  <a id="facebookOauth-S" class="btn btn-social btn-facebook">
                    <span class="fa fa-facebook"></span>  Sign up with Facebook  
                  </a>
                  <a id="googleOauth-S" class="btn btn-social btn-google">
                    <span class="fa fa-google"></span>  Sign up with Google  
                  </a>
                  <a href="#signup-tab" aria-controls="signup-tab" role="tab" data-toggle="tab" id="SignupBtn-1" class="btn wami-btn center-block">
                    Email Address  
                  </a>
                </div>
                <div class="center-block text-center">
                  <span class="help-block"> Already have an account?  </span>
                  <a href="#login-tab" aria-controls="login-form" role="tab" data-toggle="tab" id="LoginBtn-1" class="btn wami-btn">
                    Login here  
                  </a>
                </div>
              </div><!-- /.home-tab -->

              <!-- SIGNUP TAB -->
              <div id="signup-tab" role="tabpanel" class="tab-pane fade">
                <h1 class="text-center"> Registration  </h1>
                <form id="signupForm" class="form-horizontal" method="POST" action="<?=admin_url('admin-post.php');?>" onSubmit="setLoadingState(1)">
                  <fieldset>
                    <input type="hidden" id="inputAction" name="action" value="register_user">
                    <input type="hidden" id="inputTypeSignup" name="inputType" value="E">
                    <input type="hidden" id="inputToken" name="inputToken" value="0">
                    <input type="hidden" id="inputPicture" name="inputPicture" value="0">

                    <div class="form-group">
                      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
                        <input type="text" class="form-control" id="inputFirstName" name="inputFirstName" placeholder=" Name  " required>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
                        <input type="text" class="form-control" id="inputLastName" name="inputLastName" placeholder=" Last Name  " required>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
                        <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder=" Email Address  " required>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
                        <input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder=" Password  " required>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
                        <input type="password" class="form-control" id="inputPassword-2" name="inputPassword-2" placeholder=" Confirm Password  " required>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
                        <select class="form-control" id="inputCountry" name="inputCountry" required >
                          <?php foreach ($dataview->country_list as $country): ?>
                            <option value="<?=$country['Country_id']?>" <?=$s = ($country['Country_Code'] == $dataview->user_country) ? 'selected' : ''?>>
                              <?=$country['Country_Name']?>
                            </option>  
                          <?php endforeach ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
                        <div class="radio radio-wami">
                          <input type="radio" name="inputTerms" id="inputTerms" value="1" checked required>
                          <label id="TermsLink" data-toggle="modal" data-target="#TermsModal">
                            I have read and agree with the Terms and Conditions and Privacy Policy for this website and registration procedure.  
                            <a type="button"><span class="glyphicon glyphicon-new-window"></span></a>
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
                        <span class="help-block"> Already have an account?  </span>
                        <a href="#login-tab" aria-controls="login-form" role="tab" data-toggle="tab" id="LoginBtn-1" class="btn wami-btn">
                          Login here  
                        </a>
                        <button id="SignupBtn-2" class="btn wami-btn pull-right" type="submit">
                          Register  
                        </button>
                      </div>
                    </div>
                  </fieldset>
                </form><!-- /#signupForm -->
              </div><!-- /.signup-tab -->

              <!-- LOGIN TAB -->
              <div id="login-tab" role="tabpanel" class="tab-pane fade">
                <h1 class="text-center"> Login  </h1><br>
                <form id="loginForm" class="form-horizontal" method="POST" action="<?=admin_url('admin-post.php');?>" onSubmit="setLoadingState(1)">
                  <div class="btn-group-vertical btn-container" role="group" aria-label="...">
                    <a id="facebookOauth-L" class="btn btn-social btn-facebook">
                      <span class="fa fa-facebook"></span>  Sign in with Facebook  
                    </a>
                    <a id="googleOauth-L" class="btn btn-social btn-google">
                      <span class="fa fa-google"></span>  Sign in with Google  
                    </a>
                  </div>
                  <fieldset>
                    <input type="hidden" name="action" value="login_user">
                    <input type="hidden" id="inputTypeLogin" name="inputType" value="E">
                    <input type="hidden" id="inputToken" name="inputToken" value="0">
                    <input type="hidden" id="inputPicture" name="inputPicture" value="0">

                    <div class="form-group" id="fm-Login-Email">
                      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
                        <input type="email" class="form-control" id="inputEmail" name="inputEmail" placeholder=" Email Address  " required>
                      </div>
                    </div>
                    <div class="form-group" id="fm-Login-Password">  
                      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
                        <input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder=" Password  " required>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
                        <a class="text-right" href="#password-tab" aria-controls="home-form" role="tab" data-toggle="tab">
                          <span class="help-block"> Forgot password? Get help  </span>
                        </a>
                        <button type="submit" class="btn wami-btn padded pull-right"> Login  </button>
                      </div>
                    </div>
                    <p class="text-center">
                      <a href="#home-tab" aria-controls="home-form" role="tab" data-toggle="tab" class="wami-link">
                        Cancel  
                      </a>
                    </p>
                  </fieldset>
                </form>
              </div><!-- /.login-tab -->

              <!-- PASSWORD TAB -->
              <div id="password-tab" role="tabpanel" class="tab-pane fade">
                <h1 class="text-center"> Recover your password  </h1><br>
                <form id="loginForm" class="form-horizontal" method="POST" action="<?=admin_url('admin-post.php');?>" onSubmit="setLoadingState(1)">
                  <fieldset>
                    <input type="hidden" name="action" value="reset_password">
                    <div class="form-group" id="fm-Password-Email">
                      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
                        <input type="email" class="form-control" id="inputEmail-P" name="inputEmail" placeholder=" Email Address  " autofocus required>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
                        <button type="submit" class="btn wami-btn padded pull-right"> Recover your password  </button>
                      </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-10 col-md-offset-1">
                        <br><a href="#home-tab" aria-controls="home-form" role="tab" data-toggle="tab" class="wami-link pull-right"> Cancel  </a>
                      </div>
                    </div><!-- /.form-group -->
                  </fieldset>
                </form>
              </div><!-- /.password-tab -->
            </div><!-- /.tab-content -->

            <?php if (isset( $_GET['err'] ) OR isset( $_GET['ref'] ) ): ?>
              <?php /* Define the text to display in case of an error */
              $alert_text = 'default';
              $class = 'default';
# Select the corresponding message
              switch ($_GET["err"]) {
# Authentication
                case '101': $alert_text = 'Username and Password do not match'; break;
                case '102': $alert_text = 'This email address is not registered'; break;
                case '103': $alert_text = 'This email address is already registered'; break;
                case '105': $alert_text = 'This link is not valid anymore. Request a new one'; break;
                case '106': $alert_text = 'This link is not valid'; break;
                case '107': $alert_text = 'This link has expired. Please request a new one'; break;
                case '108': $alert_text = 'Please wait 15 min before requesting a new link'; break;
              }
              switch ($_GET["ref"]) {
# Authentication
                case 'auth': $alert_text = "You've been away for too long. Please login again"; break;
                case 'newpass': $alert_text = 'Your password has been changed. You can now login'; $class='success'; break;
                case 'reqpass': $alert_text = 'We´ve sent you an email with instructions'; $class='success'; break;
                case 'login': $alert_text = 'default'; break;
              }?>
              <?php if ($alert_text !== 'default'): ?>
                <div class="col-sm-12">
                  <div class="alert alert-wami alert-dismissible <?=$class?>" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <?=_e($alert_text, 'avia_framework');?>
                  </div>
                </div> 
              <?php endif ?>
            <?php endif ?>
          </div><!-- /#login -->

          <div id="features" class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-7 col-md-offset-1 col-lg-7">
            <h1> Choose with confidence  </h1>  
            <ul class="list-unstyled">
              <li> Register Your Implants  </li>
              <li> Buy an extended warranty  </li>
              <li> MotivaImagine Center Locator  </li>
            </ul>
          </div><!-- /.features -->

          <div class="col-xs-12">
            <button type="button" class="slidebar-toggle js-toggle-bottom-slidebar visible-xs">
              <span class="glyphicon glyphicon-menu-up" aria-hidden="true"></span>
            </button><!-- /.button -->
          </div><!-- /.col-xs-12 -->

        </div><!-- /.fixed-center -->
      </div><!-- /.main-wrapper -->
    </div><!-- /.row -->  
</div><!-- /.container -->