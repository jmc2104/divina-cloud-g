/* ---------------------------------- HELPERS ---------------------------------- */
function toggle( element, visibility )
{
  if ( visibility )
  document.getElementById( element ).classList.remove( 'hidden' );
  else
  document.getElementById( element ).classList.add( 'hidden' );
}

function redirect( url ) { window.location.href=url }

function log( object ) { console.log( object ); }

/* JQuery UI */
function modal(modal) { $(modal).modal('show'); }

function onRadioLabelClick( radio , callback, args)
{
  document.getElementById( radio ).checked = true;
  window[callback]( args );
}

function valueOf( id )
{
  var element = document.getElementById( id );
  return element.value ? element.value : element.innerHTML;
}

function get( id )
{
  return document.getElementById( id );
}

/* ______________________________ END: HELPERS ______________________________ */
