function enableRadio(element) {
  var active_sn = false;
  if (element == 'radio_sn') {
    active_sn = true;
    toggleType('SN');
  } else {
    toggleType('ESN');
  }
  document.getElementById('radio_sn').checked = active_sn;
  document.getElementById('radio_esn').checked = !active_sn;
}

function enableRadioSide(element) {
  var active_L = false;
  if (element == 'L') {
    active_L = true;
    toggleSide('L');
  } else {
    toggleSide('R');
  }
  document.getElementById('inputSide-1').checked = active_L;
  document.getElementById('inputSide-2').checked = !active_L;
}

function enableRadioAmount(element) {
  var active_1 = false;
  if (element == 1) {
    active_1= true;
    toggleAmount(1);
  } else {
    toggleAmount(2);
  }
  document.getElementById('inputAmmount-1').checked = active_1;
  document.getElementById('inputAmmount-2').checked = !active_1;
}


 // Set default values
 localStorage.clear();
 localStorage.setItem('amount', 2);
 localStorage.setItem('side', 'L');
 localStorage.setItem('type', 'SN');

  // Custom JQuery Functions
  jQuery.fn.visible = function() {
    return this.css('visibility', 'visible');
  };

  jQuery.fn.invisible = function() {
    return this.css('visibility', 'hidden');
  };

// Bootstrap Slider Defaults
$('#Btn-Prev').invisible();
$('#Btn-Sbt').invisible();

 // Indicators
 $('#CurrStep').html(1);
 $('#TotalStep').html(5);

 /*--------------------------------------------------------------------------------- Form */
 $('#implantsForm').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) { 
    e.preventDefault();
    return false;
  }
});

 /*--------------------------------------------------------------------------------- Date */
 function updateDate(argument) {
  var fullDate = $('#inputDate').val().split('-');
  switch(argument) {
    case 'D':
    fullDate[2] = $('#inputDays').val();
    $('#inputDate').val(fullDate.join('-'));
    break;
    case 'M':
    fullDate[1] = $('#inputMonth').val();
    month = fullDate.join('-');
    $('#inputDate').val(month);
    break;
    case 'Y':
    fullDate[0] = $('#inputYear').val();
    $('#inputDate').val(fullDate.join('-'));
    break;
  }
  console.log('New Date: ' + $('#inputDate').val());
}
/*--------------------------------------------------------------------------------- Amount */
function toggleAmount(argument) {
  if (argument == 1) {
    document.getElementById('inputSide-1').disabled = 0;
    document.getElementById('inputSide-2').disabled = 0;
    $('#right_vc').removeAttr('required');
    $('#right_sn').removeAttr('required');
    $('#right_esn').removeAttr('required');
    $('#TotalStep').html(4);
  } else {
   document.getElementById('inputSide-1').disabled = 1;
   document.getElementById('inputSide-2').disabled = 1;
   $('#TotalStep').html(5);
 }
 localStorage.setItem('amount', argument);
 console.log('amount: ' + argument);
}
/*--------------------------------------------------------------------------------- Side */
function toggleSide(argument) {
  localStorage.setItem('side', argument);
  if (argument == 'R') {
    /* remvove left requirements */
    $('#left_vc').removeAttr('required');
    $('#left_sn').removeAttr('required');
    $('#left_esn').removeAttr('required');
    /* set right requirements */
    $('#right_vc').attr('required');
    $('#right_sn').attr('required');
    $('#right_esn').attr('required');
  } else {
    /* remvove right requirements */
    $('#right_vc').removeAttr('required');
    $('#right_sn').removeAttr('required');
    $('#right_esn').removeAttr('required');
    /* set Left requirements */
    $('#left_vc').attr('required');
    $('#left_sn').attr('required');
    $('#left_esn').attr('required');
  }
  localStorage.setItem('side', argument);
  console.log('side: ' + argument);
}
/*--------------------------------------------------------------------------------- Type */
function toggleType(argument) {
  if (argument == 'ESN') {
    $('#input-left-esn').show();
    $('#input-left-sn').hide();
    $('#input-right-esn').show();
    $('#input-right-sn').hide();
  } else {
    $('#input-left-esn').hide();
    $('#input-left-sn').show();
    $('#input-right-esn').hide();
    $('#input-right-sn').show();
  }
  console.log('type is: ' + argument);
}
/*--------------------------------------------------------------------------------- Sider */
$('#carousel-register').on('slid.bs.carousel', function () {
  // Get the index of the slider
  var id = $('.item.active').attr('id').split('-');
  var step = +id[1];
  var index = +step-1;

  console.log('Slided to: ' + index + ' (Step:' + step + ')' );
  onStepChange(index);
});

function onStepChange(index) {
  /* Get the settings */  
  var amount =  localStorage.getItem('amount');
  var side = localStorage.getItem('side');

  switch (index) {
    case 0:
    $('#CurrStep').html(1);
    setBackBtn(false);
    setNextBtn(1);
    break;

    case 1: /*---------------------------------------- Step 2 */
    setBackBtn(0);
    setNextBtn(2);
    $('#CurrStep').html(2);

    if ($('#surgeonSelect').val() == '') {
      $('#Btn-Next').invisible();
    } else {
      $('#Btn-Next').visible();
    }
    break;
    
    case 2: /*---------------------------------------- Step 3 */
    $('#CurrStep').html(3);
    setBackBtn(1); // Set the back button
    setSubmitBtn(0); // Hide the submit button
    setNextBtn(3); // display the button
    setNextStep(3); // Set Next to step 4 or 5
    break;
    
    case 3: /*---------------------------------------- Step 4 */
    $('#CurrStep').html(4);
    if (amount == 2) { /* w/ 2 implants */
      setNextBtn(4);
      setSubmitBtn(0);
    } else { /* w/ 1 impant */
      setNextBtn(0);
      setSubmitBtn(1);
    }
    setBackBtn(2);
    break;

    case 4: /*---------------------------------------- Step 5 */
    if (amount == 2) {
      /* w/ 2 impants */
      setBackBtn(3); // The left implant
      $('#CurrStep').html(5);
    } else { /* w/ 1 impant */
      setBackBtn(2);
      $('#CurrStep').html(4);
    }
    setNextBtn(0);
    setSubmitBtn(1);
    break;
  }
}

function setNextStep(argument) {
  /* Get the settings */  
  var amount =  localStorage.getItem('amount');
  var side = localStorage.getItem('side');

  if (amount == 2) {
    /* Most of cases */
    $('#Btn-Next').attr('data-slide-to', argument);
    console.log('1 Next is: ' + argument);
  } else { /* w/ 1 impant */
    if (side == "R") {
      var temporal = +argument+1;
      $('#Btn-Next').attr('data-slide-to', temporal);
      console.log('2 Next is: ' + temporal);
    } else { // Side is L
      $('#Btn-Next').attr('data-slide-to', argument);
      console.log('3 Next is: ' + argument);
    }
  }
}

function setBackBtn(argument) {
  if (argument === false) {
    $('#Btn-Prev').invisible();
  } else {
    $('#Btn-Prev').attr('data-slide-to', argument);
    $('#Btn-Prev').visible();
  }
}

function setNextBtn(argument) {
  if (argument == 0) {
    $('#Btn-Next').invisible();
  } else {
    $('#Btn-Next').attr('data-slide-to', argument);
    $('#Btn-Next').visible();
  }
}

function setSubmitBtn(argument) {
  if (argument == 0) {
    $('#Btn-Sbt').invisible();
  } else {
    $('#Btn-Sbt').visible();
  }
}