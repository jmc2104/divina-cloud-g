var googleUser = {};
var startApp = function() {
  gapi.load('auth2', function(){
  // Retrieve the singleton for the GoogleAuth library and set up the client.
  auth2 = gapi.auth2.init({
    client_id: '338261597301-g11sfn4iau5qn7p86fpkt5j911v4lnlp.apps.googleusercontent.com',
    cookiepolicy: 'single_host_origin',
  });
  attachSignup(document.getElementById('googleOauth-S'));
  attachSignin(document.getElementById('googleOauth-L'));
});
};
function attachSignup(element) {
  auth2.attachClickHandler(element, {},
    function(googleUser) {
      onSignUp(googleUser);
    }, function(error) {
      alert(JSON.stringify(error, undefined, 2));
    });
}
function attachSignin(element) {
  auth2.attachClickHandler(element, {},
    function(googleUser) {
      onSignIn(googleUser);
    }, function(error) {
      alert(JSON.stringify(error, undefined, 2));
    });
}
function onSignUp(googleUser) {
 // Useful data for your client-side scripts:
 var profile = googleUser.getBasicProfile();
 /* console.log(JSON.stringify(profile)); */

  // The ID token you need to pass to your backend:
  var id_token =  googleUser.getAuthResponse().id_token;
  
  document.getElementById('inputFirstName').value = profile.getGivenName();
  document.getElementById('inputLastName').value =  profile.getFamilyName();
  document.getElementById('inputEmail').value = profile.getEmail();
  document.getElementById('inputPicture').value = profile.getImageUrl();

  document.getElementById('inputPassword').value = 0;
  document.getElementById('inputPassword-2').value = 0;
  document.getElementById('inputPassword').className = 'hidden';
  document.getElementById('inputPassword-2').className = 'hidden';
  
  document.getElementById('inputToken').value = id_token.substring(0, 10);
  document.getElementById('inputTypeSignup').value = 'G';
  // Show the form
  $('#signup-nav').tab('show');
};

function onSignIn(googleUser) {
 // Useful data for your client-side scripts:
 var profile = googleUser.getBasicProfile();

  //console.log(JSON.stringify(profile));
  // The ID token yourou need to pass to your backend:
  var id_token =  googleUser.getAuthResponse().id_token;
  id_token = id_token.substring(0, 10);

  $('#login-tab #inputEmail').val( profile.getEmail() );
  $('#login-tab #inputPicture').val( profile.getImageUrl() );
  $('#login-tab #inputPassword').val( id_token );
  $('#login-tab #inputToken').val( id_token );      
  $('#login-tab #inputTypeLogin').val( 'G' );

  // Show the form
  setLoadingState(1);
  $('#loginForm').submit();
  //$('#login-nav').tab('show');
};
function signOut() {
  var auth2 = gapi.auth2.getAuthInstance();
  auth2.signOut().then(function () {
    var elements = document.getElementsByTagName("input");
    for (var ii=0; ii < elements.length; ii++) {
      if (elements[ii].type == "text" || elements[ii].type == "password") {
        elements[ii].value = "";
      }
    }
  });
}
startApp();