/* 
* Motivaimagine Center Locator Functions
* Author: Gerson Rodriguez <nosreg216@gmail.com>
*/

var map; /* Google Map Object */
var markers = [];

var myLocation; /* Current Location of the client */

var searchUrl = SITE_URL + '/wp-content/services/mapping/wami_map_xml.php';
var markerIcon = SITE_URL + '/wp-content/services/mapping/assets/marker-32.png';
var placeholder = SITE_URL + '/wp-content/services/mapping/assets/placeholder.jpg';

var ribbon = SITE_URL + '/wp-content/services/mapping/assets/certified-ribbon.svg';
var iconMedical = SITE_URL + '/wp-content/services/mapping/assets/medical-small.svg';
var iconScan = SITE_URL + '/wp-content/services/mapping/assets/scan-small.svg';
var iconInfo = SITE_URL + '/wp-content/services/mapping/assets/info-small.svg';

/* ---------------------------------- Geo Location ---------------------------------- */
function geoloc_init() {
	if (navigator.geolocation) { /* Si el navegador tiene geolocalizacion */
		navigator.geolocation.getCurrentPosition(onLocationSuccess, onLocationError);
	}else{
		alert('Oops! Tu navegador no soporta geolocalización. Bájate Chrome, que es gratis!');
	}
}
function onLocationSuccess(position) {
	/* Save current cords */  
	myLocation = new google.maps.LatLng( position.coords.latitude, position.coords.longitude );
	/* Render the map */
	renderMap(myLocation);
}

function onLocationError(err) {

	/* Error texts */
	var error_msg = [];
	error_msg[0] = "Oops! Algo ha salido mal";
	error_msg[1] = "Oops! No has aceptado compartir tu posición";
	error_msg[2] = "Oops! No se puede obtener la posición actual";
	error_msg[3] = "Oops! Hemos superado el tiempo de espera";
	console.log('Eror: ' + error_msg[err.code])

	/* Belgium - Germany - Austria */
	myLocation = new google.maps.LatLng( 49, 10 );
	/* Render the map */
	renderMap(myLocation);
}

/* ---------------------------------- Map rendering ---------------------------------- */
function renderMap(myLocation) {  

	map = new google.maps.Map(document.getElementById("map"), {
		center: myLocation,
		zoom: 7,
		minZoom: 3,
		mapTypeId: 'roadmap',
		mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU}
	});

	var mySearchUrl = searchUrl;
	mySearchUrl += '?latitude=' + myLocation.lat();
	mySearchUrl += '&longitude=' + myLocation.lng();

	setMarkersOnMap(mySearchUrl);
}

function setMarkersOnMap( mySearchUrl ) {

	/* Spam the console */
	console.log( mySearchUrl );
	
	downloadUrl(mySearchUrl, function(data) {
		//var xml = data.responseXML;
		var XML = data.responseXML.documentElement;
		var markerNodes = XML.getElementsByTagName("marker");

		/* Clear all old markers */		
		document.getElementById('media-list').innerHTML = '';
		for (var i = 0; i < markers.length; i++) {
			markers[i].setMap(null);
		} 
		markers = [];

		/* Iterate result and create markers */		
		for (var i = 0; i < markerNodes.length; i++) {
			/* coords */
			var lat = parseFloat(markerNodes[i].getAttribute("latitude"));
			var lng = parseFloat(markerNodes[i].getAttribute("longitude"));
			var latlng = new google.maps.LatLng( lat, lng	);
			/* info */			
			var name = markerNodes[i].getAttribute("name");
			var address = markerNodes[i].getAttribute("address");
			var city = markerNodes[i].getAttribute("city");
			var distance = markerNodes[i].getAttribute("distance");
			var category = markerNodes[i].getAttribute("category");
			var picture = markerNodes[i].getAttribute("picture");
			var website = markerNodes[i].getAttribute("website");


			if (website == '') {
				website = 'http://motivaimagine.com?pid=' + name;
			}

			/* Create marker and media item */
			createMarker(latlng, name, address, category, website);
			createMedia(i, name, address, city, picture, category, website);
		} /* End of iterantions */

		/* Center the map on the closest marker */
		//map.setCenter(markers[0].position);
		map.setZoom(6);

		/* Load the list filtering plugin for the media list*/		
		filterSearch_init();
	});
}

function createMedia (index, name, address, city, picture, category, website)
{
	var text; /* Category text to display */
	var icon; /* Category icon to display */

	switch (category) {
		case '1': text = 'Motiva Implants Center'; break;
		case '2': text = 'Motivaimagine Certified Center'; break;
		case '0': text = 'Motiva Distributor'; break;
	} 

	if (picture == 'default') {
		picture = placeholder;
	}

	var html = "<li class='media' onclick='GoToMarker(" + index + ")'>";

	html += "<div class='media-left'>";
	html += "<img class='media-object' src='"+ picture +"'>";
	html += "<label class='media-label'>"+ city + "</label>";
	html += "</div>"; /* media-object */

	html += "<div class='media-body'>";

	html += "<div class='media-inner'>";

	html += '<a target="new" href="' + website + '">';
	html += "<h4 class='media-heading name-text'>"+ name + "</h4>";
	html += '</a>'; /* Link */

	html += "<label>"+ text +"</label>";
	html += "<div class='address'>";
	html += "<p class='address-text'>"+ address +"</p>";
	html += "</div>"; /* address */
	html += "</div>";/* Inner */
	html += "<div class='media-icons'>";

	/*
	* Distributors - Information: 0
	* MotivaCenter - Consultation: 1
	* MotivaImagine - 3D Scan : 2
	*/
	html += "<img src='"+ iconInfo +"'>";

	if (category > '0') {
		html += "<img src='"+ iconMedical +"'>";
	}

	if (category > '1') {
		html += "<img src='"+ iconScan +"'>";
	}

	html += "</div>"; /* Icons */	

	html += "<div class='media-overlay'>";

	html += "<div class='ribbon'>";
	if (category == '2') {
		html += "<img src='"+ ribbon +"'>";	
	}
	html += "</div>";/* Ribbon */

	html += "</div>";/* Overlay */

	html += "</div>"; /* media-body */
	html += "</li>"; /* media */

	document.getElementById('media-list').innerHTML += html;
}

function createMarker(latlng, name, address, category, website) {

	var html = '<div class="marker">';
	html += '<a target="new" href="' + website + '">';
	html += '<div class="title">' + name + '</div>';	
	html += '<p class="more">Learn More</p> ';
	html += '<div class="icons">';

	/*
	* Distributors - Information: 0
	* MotivaCenter - Consultation: 1
	* MotivaImagine - 3D Scan : 2
	*/
	html += "<img src='"+ iconInfo +"'>";

	if (category > '0') {
		html += "<img src='"+ iconMedical +"'>";
	}

	if (category > '1') {
		html += "<img src='"+ iconScan +"'>";
	}

	html += '</div>'; /* .icons */

	html += '</a>'; /* Link */

	html += '</div>'; /* .marker */
	
	var infowindow = new google.maps.InfoWindow({
		content: html
	});

	var marker = new google.maps.Marker({
		map: map,
		title: name,
		position: latlng,
		icon: markerIcon,
		animation: google.maps.Animation.DROP
	});

	markers.push(marker);

	google.maps.event.addListener(marker, 'click', function() {
		/* Close All windows */
		google.maps.event.trigger(map, 'click');
		/* Open the marker window */		
		infowindow.open(map, marker);
		/* Center the map on current marker */		
		map.setCenter(marker.position);
	});

	google.maps.event.addListener(map, 'click', function() {
		infowindow.close();
	});
}

function GoToMarker(index) {
	google.maps.event.trigger(markers[index], 'click');
}

/* ---------------------------------- AJAX AND XML ---------------------------------- */
function downloadUrl(url, callback) {
	var request = window.ActiveXObject ?
	new ActiveXObject('Microsoft.XMLHTTP') :
	new XMLHttpRequest;
	request.onreadystatechange = function() {
		if (request.readyState == 4) {
			request.onreadystatechange = doNothing;
			callback(request, request.status);
		}
	};
	request.open('GET', url, true);
	request.overrideMimeType('text/xml; charset=iso-8859-1');
	request.send(null);
}
function doNothing() {}
/* ---------------------------------- User Functions ---------------------------------- */

function updateMarkers( maplocation )
{

	if (maplocation == false) {
		maplocation = myLocation;
	}

	/* Level of filter to apply */
	var filterLevel; 

	/* Get the filter elements */
	var filter_1 = document.getElementById('filter-1');
	var filter_2 = document.getElementById('filter-2');
	var filter_3 = document.getElementById('filter-3');

	/* Check what filtes are active */
	if (filter_1.checked === true) {
		filterLevel = 'mtv';
	}
	if (filter_2.checked === true) {
		filterLevel = 'mic';
	}
	if (filter_3.checked === true) {
		filterLevel = 'all';
	}

	/* Attach filters to the URL */
	
	var mySearchUrl = searchUrl;

	mySearchUrl += '?filter=' + filterLevel;
	mySearchUrl += '&latitude=' + maplocation.lat();
	mySearchUrl += '&longitude=' + maplocation.lng();
	

	setMarkersOnMap( mySearchUrl );

	/* Display the menu tab */
	$('#results-nav').tab('show');
}

function setFilterLevel(element, filter) {
}

function toogle_map_radio( element ) {
	var status = document.getElementById(element).checked;
	document.getElementById(element).checked = !status;
}

function clearInput() {
	document.getElementById("inputAddress").value = '';
}

/* ---------------------------------- GEOCODER ---------------------------------- */
function searchLocations() {
	var address = document.getElementById("inputAddress").value;

	if (address.length > 1) {
		var geocoder = new google.maps.Geocoder();
		geocoder.geocode({address: address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				var myCenter = results[0].geometry.location;
				updateMarkers( myCenter );
				map.setCenter(myCenter);
			} else {
				alert(address + ' Not Found');
			}
		});
	} else {
		updateMarkers( false );
	}
}

function filterSearch_init() {
	var options = { valueNames: [ 'name-text', 'address-text' ] };
	var patientList = new List('media-filter', options);
}
