	var SECRT_KEY = "ts5DXD28GXg4Tn4dmQhM3463qqS2dCJb";

	// Valiation flags
	var name = false;
	var card = false;
	var cvv = false;
	var date = false;
	var type = false;

	// Custom JQuery Functions
	jQuery.fn.visible = function() {
		return this.css('visibility', 'visible');
	};

	jQuery.fn.invisible = function() {
		return this.css('visibility', 'hidden');
	};

	$('#BuyWarrantyModal').on('shown.bs.modal', function (e) {
		setCredomaticDefaults();
	})

		// Initialize defualt behaviours
		//$('#SubmitPayment').attr("disabled", true);
		
		function onMetohdChange(argument) {
			// Clear all active buttons
			$('.payment-metod img').removeClass('active');

			switch(argument) {

				case 'mastercard':
				$('.payment-metod #mastercard').addClass('active');
				$("#paypal-fields").hide();
				$("#form-fields").show();
				$("#ccselect").val('mastercard');
				setCredomaticDefaults();				
				break;

				case 'visa':
				$('.payment-metod #visa').addClass('active');
				$("#paypal-fields").hide();
				$("#form-fields").show();
				$("#ccselect").val('visa');
				setCredomaticDefaults();
				break;

				case 'amex':
				$('.payment-metod #amex').addClass('active');
				$("#paypal-fields").hide();
				$("#form-fields").show();
				$("#ccselect").val('amex');
				setCredomaticDefaults();
				break;

				case 'paypal':
				$('.payment-metod #paypal').addClass('active');
				$('#SubmitPayment').attr("disabled", false);
				$("#paypal-fields").show();
				$("#form-fields").hide();
				break;
			}
		}


		function setCredomaticDefaults() {
			// Generate the hash
			var time = Math.floor(Date.now() / 1000);
			var order = 2000;
			var amount = request_real_amount();
			var str = order + '|' + amount + '|' + time + '|' + SECRT_KEY;
			var hash = md5(str);
			
			// Set the values in the Form
			$('#amount').val(amount); // CAREFUL
			$('#time').val(time);
			$('#hash').val(hash);
			$('#orderid').val(order);

			//Logs
			//console.log('usd amount:' + amount);

		}

		function request_real_amount() {
			var price = document.getElementById('wa-price').innerHTML;
			var currency = document.getElementById('wa-currency').innerHTML;
			var retval;

			switch(currency){
				case 'EUR':
				retval = price * 1.1;
				break;
				case 'GBP':
				retval = price * 1.3;
				break;
				case 'USD':
				retval = price * 1.0;
				break;
				case 'AUD':
				retval = price * 0.77;
				break;
			}
			return retval;
		}

		function onExpDateChange() {
			month = $('#inputExpMonth').val();
			year = $('#inputExpYear').val();
			date = month + '/' + year;
			$('#ccexp').val(date);
			console.log('New Date: ' + date);
			valdiateForm();
		}

		function valdiateForm() {
			//name 
		}


		function detectCardType(number) {
			var re = {
				visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
				mastercard: /^5[1-5][0-9]{14}$/,
				amex: /^3[47][0-9]{13}$/,
				discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/
			}

			for(var key in re) {
				if(re[key].test(number)) {
					return key
				}
			}
		}