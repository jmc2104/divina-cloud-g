  // On Application Ready
  $(document).ready(function() {
    /* Add an event to all links that should display the loader animation */
    var elements = document.getElementsByClassName('loads');
    for(var i = 0, len = elements.length; i < len; i++) {
      elements[i].onclick = function() {
        setLoadingState(1);
      }
    }

    /* Initializes all the tooltips and popovers on each page*/
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
      $('[data-toggle="popover"]').popover()
    });
    /* Sets a timer to dismiss alerts */
    //var handler = setTimeout(dismissAlert, 7000);
    //function dismissAlert() { $('.alert').alert('close'); }
  });

  // Displays the loader on every page
  function setLoadingState(argument) {
  	if (argument == 1) {
  		document.getElementById('loader').setAttribute('class', '');
  	} else {
  		document.getElementById('loader').setAttribute('class', 'hidden');
  	}
  }

	// Handles what happens when the user changes the language
	function onChangeLang(argument) {
    setLoadingState(1);
    var url = location.protocol + '//' + location.host + location.pathname;
    location.href = url + '?lang=' +argument;
  }

  var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;
    for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');

      if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined ? true : sParameterName[1];
      }
    }
  };