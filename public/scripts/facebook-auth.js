var fbAsyncInit = function() {
	//SDK loaded, initialize it
	FB.init({
		appId      : '310780482613649',
		xfbml      : true,
		version    : 'v2.8'
	});
	//check user session and refresh it
	FB.getLoginStatus(function(response) {
		if (response.status === 'connected') {
			//user is authorized
			var token = response.authResponse.accessToken;
			//alert(token);
		}
	});
};

//load the JavaScript SDK
(function(d, s, id){
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

/*--------------------------- Add events buttons -------------------------*/

//add event listener to register button
document.getElementById('facebookOauth-S').addEventListener('click', function() {
	//do the login
	FB.login(function(response) {
		if (response.authResponse) {
			//user just authorized your app
			var id_token = response.authResponse.accessToken;
			onFbSignUp(id_token);
			//getUserData(id_token);
		}
	},
	{scope: 'email,public_profile', return_scopes: true});
}, false);



//add event listener to login button
document.getElementById('facebookOauth-L').addEventListener('click', function() {
	//do the login
	console.log("facebookOauth-L");
	FB.login(function(response) {
		if (response.authResponse) {
			var id_token = response.authResponse.accessToken;
			onFbSignIn(id_token);
			//getUserData(id_token);
		}
	},
	{scope: 'email,public_profile', return_scopes: true});
}, false);

/*--------------------------- END Add events buttons -------------------------*/

//Llenar los datos del formulario de registro
function onFbSignUp(token) {
	document.getElementById('inputToken').value = token;
	FB.api('/me', {locale: 'tr_TR', fields: 'id,first_name,last_name,email,picture'}, function(response) {

		document.getElementById('inputFirstName').value = response.first_name;
		document.getElementById('inputLastName').value =  response.last_name;
		document.getElementById('inputEmail').value = response.email;
	  	//document.getElementById('inputEmail-2').value = 0;
	  	document.getElementById('inputPassword').value = 0;
	  	document.getElementById('inputPassword-2').value = 0;
	  	//document.getElementById('inputEmail-2').className = 'hidden';
	  	document.getElementById('inputPassword').className = 'hidden';
	  	document.getElementById('inputPassword-2').className = 'hidden';
	  	document.getElementById('inputTypeSignup').value = 'F';

	  });

	if (!token) {
		FB.getLoginStatus(function(response) {

			if (response.status === 'connected') {
			//user is authorized
			var token = response.authResponse.accessToken;
			document.getElementById('inputToken').value = token;
		}
	});
	}
	$('#signup-nav').tab('show');
}

//Llenar los datos del formulario de login
function onFbSignIn(token) {
	FB.api('/me', {locale: 'tr_TR', fields: 'id,email,picture'}, function(response) {
		$('#login-tab #inputEmail').val( response.email );
		$('#login-tab #inputPassword').val( token );
		$('#login-tab #inputToken').val( token );      
		$('#login-tab #inputTypeLogin').val( 'F' );
	});

	if (!token) {
		FB.getLoginStatus(function(response) {
			if (response.status === 'connected') {
			//user is authorized
			var token = response.authResponse.accessToken;
			$('#login-tab #inputPassword').val( token );
			$('#login-tab #inputToken').val( token );   
		}
	});
	}
	setLoadingState(1);
	$('#login-tab').tab('show');
	var handler = setTimeout(submitFB, 3000);
	function submitFB() { $('#loginForm').submit(); }
	 // Show the form
	}