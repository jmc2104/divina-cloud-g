<?php

namespace DivinaApp\Models\Passport;

use Illuminate\Database\Eloquent\Model;

class AccessControl extends Model
{
    //
    protected $connection = 'cloudsql';
    protected $table = "access_control";
    
    public function User()
    {
    	$this->belongsto("DivinaApp\Models\Passport\User");
    }
}
