<?php

namespace DivinaApp\Models\Passport;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $connection = 'cloudsql';
    protected $table = 'countries';

    /**
     * Full Name of the user.
     *
     * @var string
     **/
     /* ---------------------------------- Relationships ---------------------------------- */

    /**
     * Get the user that owns the profile.
     * Eloquent will find a user with the 'Id' value of this table as PK;
     */
    public function profile()
    {
 		return $this->hasOne('DivinaApp\Models\Profile', 'Country_id');
    }
   
    public function surgeries()
    {
        return $this->hasmany('DivinaApp\Models\Surgery','Country_id');
    }
    
    public function distributors()
    {
        return $this->hasmany('DivinaApp\Models\distributors','Country_id');
    }

    public function registration()
    {
        return $this->hasOne('DivinaApp\Models\Registration', 'Country_id');
    }
    
    public function Postpayments()
    {
        return $this->hasMany('DivinaApp\Models\Postpayment');
    }

    public function location()
    {
        return $this->belongsTo('DivinaApp\Models\Location');
    }
}
