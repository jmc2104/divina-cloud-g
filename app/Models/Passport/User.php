<?php

namespace DivinaApp\Models\Passport;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;
    //
    protected $connection = 'cloudsql';
    protected $table = "users";

    /** Get the identifier that will be stored in the subject claim of the JWT.
     * @return mixed
     */

    public function getJWTIdentifier()
    {
    	return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    //---------------------------------Relationship---------------------
    public function password()
    {
      return $this->hasOne("DivinaApp\Models\Passport\Password", "user_id");
    }
    public function profile()
    {
    	return $this->hasOne("DivinaApp\Models\Passport\Profile","user_id");
    }

    
    public function AccessControl()
    {
      return $this->hasmany("DivinaApp\Models\Passport\AccessControl","user_id");
    }
    //---------------------------------Divina3.0---------------------------
    public function divina_user()
    {
      return $this->hasOne("DivinaApp\Models\Divina\DivinaUser","user_id");
    }
    public function patient_files()
    {
      return $this->hasmany("DivinaApp\Models\Divina\PatientFile","patient_id");
    }
}
