<?php

namespace DivinaApp\Models\Passport;

use Illuminate\Database\Eloquent\Model;

class Password extends Model
{
    //
    protected $connection = 'cloudsql';
    protected $table = 'passwords';

    public function user()
    {
    	return $this->belongsTo('DivinaApp\Models\User');
    }
}
