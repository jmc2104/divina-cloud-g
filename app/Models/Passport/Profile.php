<?php

namespace DivinaApp\Models\Passport;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $connection = 'cloudsql';
    protected $table = 'profiles';

    /**
     * Full Name of the user.
     *
     * @var string
     **/
    public $full_name;

    /* ---------------------------------- Relationships ---------------------------------- */

    /**
     * Get the user that owns the profile.
     * Eloquent will find a user with the 'User_Id' value of this table as PK;
     */
    public function user()
    {
        return $this->belongsTo('DivinaApp\Models\Passport\User', 'user');
    }
    
    public function country()
    {
    	return $this->belongsTo('DivinaApp\Models\Passport\Country');
    }
}
