<?php

namespace DivinaApp\Models\Divina;

use Illuminate\Database\Eloquent\Model;

class FileDetail extends Model
{
    //
    protected $connection = 'cloudsql2';
    protected $table = "file_details";

    public function PatientFile()
    {
    	return $this->belongsTo("DivinaApp\Models\Passport\PatientFile");
    }

    public function CaseFile()
    {
        return $this->belongsTo("DivinaApp\Models\Divina\CaseFile");
    }



}
