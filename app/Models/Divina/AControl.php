<?php

namespace DivinaApp\Models\Divina;

use Illuminate\Database\Eloquent\Model;

class AControl extends Model
{
    protected $connection = 'cloudsql2';
    protected $table = "access_control";

    public function divina_user()
    {
    	return $this->belongsto("DivinaApp\Models\Divina\DivinaUser");
    }

    public function patient_file()
    {
    	return $this->belongsto("DivinaApp\Models\Divina\PatientFile");
    }
}
