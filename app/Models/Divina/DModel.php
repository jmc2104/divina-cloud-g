<?php

namespace DivinaApp\Models\Divina;

use Illuminate\Database\Eloquent\Model;

class DModel extends Model
{
    //
    protected $connection = 'cloudsql2';
    protected $table = "models";

    public function Case_File_Model()
    {
    	return $this->hasOne("DivinaApp\Models\Divina\CaseFileModel","model_id");
    }
}
