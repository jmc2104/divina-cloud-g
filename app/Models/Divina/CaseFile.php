<?php

namespace DivinaApp\Models\Divina;

use Illuminate\Database\Eloquent\Model;

class CaseFile extends Model
{
    //
    protected $connection = 'cloudsql2';
    protected $table = "case_files";

    public function case_file_models()
    {
    	return $this->hasmany("DivinaApp\Models\Divina\CaseFileModel");
    }

    public function patient_file()
    {
    	return $this->belongsTo("DivinaApp\Models\Divina\PatientFile");
    }

    public function file_detail()
    {
        return $this->hasOne("DivinaApp\Models\Divina\FileDetail");
    }
}
