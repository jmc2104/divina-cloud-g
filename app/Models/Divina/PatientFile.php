<?php

namespace DivinaApp\Models\Divina;

use Illuminate\Database\Eloquent\Model;

class PatientFile extends Model
{
    //
    protected $connection = 'cloudsql2';
    protected $table = "patient_files";

    public function file_details()
    {
        return $this->hasmany("DivinaApp\Models\Divina\FileDetail");
    }

    public function access_controls()
    {
    	return $this->hasmany("DivinaApp\Models\Divina\DAccesscontrol");
    }

    public function cases_files()
    {
        return $this->hasmany("DivinaApp\Models\Divina\CaseFile");
    }

    public function user()
    {
    	return $this->belongsTo("DivinaApp\Models\Passport\User");
    }
    
}
