<?php

namespace DivinaApp\Models\Divina;

use Illuminate\Database\Eloquent\Model;

class CrisalixAccess extends Model
{
    //
    protected $connection = 'cloudsql2';
    protected $table = "crisalix_access";

    public function divina_user()
    {
    	return $this->belongsto("DivinaApp\Models\Divina\DivinaUser");
    }
}
