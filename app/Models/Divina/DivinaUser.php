<?php

namespace DivinaApp\Models\Divina;

use Illuminate\Database\Eloquent\Model;

class DivinaUser extends Model
{
    //
    protected $connection = 'cloudsql2';
    protected $table = "div_users";


    public function user()
    {
      return $this->belongsto("DivinaApp\Models\Passport\User");
    }
    
    public function crisalix_access()
    {
      return $this->hasOne("DivinaApp\Models\Divina\CrisalixAccess","div_user_id");
    }

    public function access_controls()
    {
      return $this->hasmany("DivinaApp\Models\Divina\DAccessControl","div_user_id");
    }
}
