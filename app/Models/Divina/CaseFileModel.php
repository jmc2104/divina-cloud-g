<?php

namespace DivinaApp\Models\Divina;

use Illuminate\Database\Eloquent\Model;

class CaseFileModel extends Model
{
    //
    protected $connection = 'cloudsql2';
    protected $table = "case_file_models";
    //
    public function CaseFile()
    {
    	return $this->belongsto("DivinaApp\Models\Divina\CaseFile");
    }

    public function DModel()
    {
    	return $this->belongsto("DivinaApp\Models\Divina\DModel",'model_id');
    }
}
