<?php

namespace DivinaApp\Http\Controllers;

use Storage;
use JWTAuth;
use JWTException;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DivinaApp\Http\Requests;
use DivinaApp\Http\Response;
use DivinaApp\Http\Controllers\Controller;
use DivinaApp\Models\User;
use DivinaApp\Models\UserM;




class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

 

  
    /**
     * API Register
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $credentials = $request->only('name', 'email', 'password');
        
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users'
        ];
        $validator = Validator::make($credentials, $rules);
        if($validator->fails()) {
            return response()->json(['success'=> false, 'error'=> $validator->messages()]);
        }
        $name = $request->name;
        $email = $request->email;
        $password = $request->password;
        
        $user = User::create(['name' => $name, 'email' => $email, 'password' => Hash::make($password)]);
        $verification_code = str_random(30); //Generate verification code
        DB::table('user_verifications')->insert(['user_id'=>$user->id,'token'=>$verification_code]);
        $subject = "Please verify your email address.";
        Mail::send('email.verify', ['name' => $name, 'verification_code' => $verification_code],
            function($mail) use ($email, $name, $subject){
                $mail->from(getenv('FROM_EMAIL_ADDRESS'), "From User/Company Name Goes Here");
                $mail->to($email, $name);
                $mail->subject($subject);
            });
        return response()->json(['success'=> true, 'message'=> 'Thanks for signing up! Please check your email to complete your registration.']);
    }
    
    public function verifyUser($verification_code)
    {
        $check = DB::table('user_verifications')->where('token',$verification_code)->first();
        if(!is_null($check)){
            $user = User::find($check->user_id);
            if($user->is_verified == 1){
                return response()->json([
                    'success'=> true,
                    'message'=> 'Account already verified..'
                ]);
            }
            $user->update(['is_verified' => 1]);
            DB::table('user_verifications')->where('token',$verification_code)->delete();
            return response()->json([
                'success'=> true,
                'message'=> 'You have successfully verified your email address.'
            ]);
        }
        return response()->json(['success'=> false, 'error'=> "Verification code is invalid."]);
    }

    public function authenticate(Request $request)
    {
        /*$credentials = $request::only('email','password');
        try 
        {
            if ($token = JWTAuth::attempt($credentials)) 
            {
                return  $this->response->error(['error'=>'User credentials are not correct'],401);
            }        
         } 
         catch (JWTException $ex) 
         {
            return $this->response->error(['error'=> 'Something went wrong']);
         } 
         return $this->response->array(compact('token'))->setStatusCode(200);*/
         $user = User::first();
         $token = JWTAuth::fromUser($user);
         return $token;
    }
public function entro()
{
    return 'entro';
}

   function login( Request $request )
    {
        /* Get the Request */        
        $uEmail = $request->input('email');
        $uPass = $request->input('pass');
        $uLogin = $request->input('login'); 
        $uApp_token = $request->input('app_token'); 
        if ( is_null( $uEmail ))
            return array('status'=> "false", 'code'=> "301");
        /* Get the User */  
        $user = User::where("email", $uEmail)->first();
         /* Check if the user exits */       
        if( is_null( $user ) )
            return array('status'=> "false", 'code'=> "102");
        if ($user->terms_conditions == 0) 
            return array('status'=> "false", 'code'=> "113");
        if (is_null($uApp_token)) 
        {
            if (is_null($uLogin))
                return array('status'=> "false", 'code'=> "301");
            if ( $uLogin == 'E')
            {
                /* Check if the parameters are set */        
                 if (is_null( $uPass))
                    return array('status'=> "false", 'code'=> "301");
                /* Get the password */
                $dbPass = $user->password->hash;
                /* Validate the Login */
                $isValid = password_verify( $uPass,$dbPass );   
            }
            else
            {
                /* Get the token Facebook o Google*/
                $Itoken = $request->input('token');
                if (is_null( $Itoken))
                    return array('status'=> "false", 'code'=> "301");
                $dbPass = $user->password->token;
                $isValid = password_verify($Itoken,$dbPass);   
                /* ... OAuh Validation will happen here ... */         
            }
            if ( $isValid )
            {
                $user->app_token = md5(uniqid($uEmail, true));
                return $this->Profile($user);
            }
            else
                return array('status'=> "false", 'code'=> "101");
        }
        else
        {
            $dbPass = $user->app_token;
            if ($dbPass == $uApp_token)
                return $this->Profile($user);
            else
                return array('status'=> "false", 'code'=> "112");
        }
    } /* End: login */

    function Profile($user)
    {
        $Result = collect();
        $user->last_login = date("Y-m-d H:i:s");
        $user->save();
        $profile = $user->profile;
        $profile->app_token = $user->app_token;
        $profile->type = $user->type;
        $profile->email = $user->email;
        if ($user->type == 1)
            $profile->patient_id = $user->Patient->id;
        elseif($user->type == 2)
            $profile->doctor_id = $user->Doctor->id;
        unset($profile['birthdate'],$profile['photo'],$profile['sex'],$profile['updated_at'],$profile['created_at'],$profile['id']);
        $token = JWTAuth::fromUser($user);
        $profile->token = $token;
        $Result->prepend($profile,'Profile');
        $Result->prepend(100,'code');  
        $Result->prepend(true,'status');
        return $Result;
    }
}
