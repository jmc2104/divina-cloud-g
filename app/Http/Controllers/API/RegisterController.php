<?php

namespace DivinaApp\Http\Controllers\API;

use DB;
use Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\UploadedFile;

use DivinaApp\Http\Requests;
use DivinaApp\Http\Response;
use DivinaApp\Http\Controllers\Controller;

use DivinaApp\Models\Passport\User;
use DivinaApp\Models\Passport\Password;
use DivinaApp\Models\Passport\Profile;
use DivinaApp\Models\Passport\AccessControl;

use DivinaApp\Models\Divina\DivinaUser;
use DivinaApp\Models\Divina\PatientFile;
use DivinaApp\Models\Divina\FileDetail;
use DivinaApp\Models\Divina\AControl;
use DivinaApp\Models\Divina\DModel;
use DivinaApp\Models\Divina\CaseFile;
use DivinaApp\Models\Divina\CaseFileModel;

class RegisterController extends Controller
{
    public function RegisterPatients(Request $request)
    {
        $setting = collect();
        $setting->prepend($request->input('div_user'),"div_id");
        $setting->prepend($request->input('crixalis_id'),"crixalispatient_id");
        $setting->prepend($request->input('email'),'email');
        $setting->prepend($request->input('first_name'),'first_name'); 
        $setting->prepend($request->input('last_name'),'last_name');
        $setting->prepend($request->input('method','E'),'method');
        $setting->prepend($request->input('platform','D'),'platform');
        $setting->prepend($request->input('country_id'),'country_id');
        $setting->prepend($request->input('type','0'),'type');
        $setting->prepend($request->input('email'),'email');
        $setting->prepend($request->input('pass','Motiva2019'),'pass');
        $setting->prepend($request->input('height',0),'height');
        $setting->prepend($request->input('weight',0),'weight');
        $setting->prepend($request->input('pinch',0),'pinch');
        $setting->prepend($request->input('skin_elasticity',0),'skin_elasticity');
        $setting->prepend($request->input('cup_size',0),'cup_size');
        $this->GenerateInsertLog($setting);
        try 
        {
            $db_passport = DB::connection('cloudsql');
            $db_divina = DB::connection('cloudsql2');
            $db_passport->beginTransaction();
            $db_divina->beginTransaction();
            $patient_user_id = $this->CreateUserPassport($setting);
            $patient_file_id = $this->CreateDivinaPatient($patient_user_id,$setting->get('crixalispatient_id'));
            $not_exist = $this->CreateDivinaAccess($setting->get('div_id'),$patient_file_id);
            if ($not_exist)
            {
                $setting->prepend($this->CreateCaseFile($patient_file_id,0),'case_id'); 
                $this->CreateFileDetails($patient_file_id,$setting);
                $db_passport->commit();
                $db_divina->commit();
                return response()->json([
                    'status' => true,
                    'code' =>100,
                    'data' => ['patient_id'=> $patient_user_id,'case_id'=>$setting->get("case_id")->id],
                ]);
            }
            else
            return response()->json([
            'status' => true,
            'code' =>100,
            'data' => ['patient_id'=> $patient_user_id,'patient_file_id' => $patient_file_id], 
        ]);
        }
        catch (Exception $e) 
        {
             $db_passport->rollback();
             $db_divina->rollback();
               return response()->json([
                'status' => 'false',
                'code' =>300, 
        ]); 
        }
    }

    public function AttachModelCase(Request $request)
    {
        $patient_id = $request->input('patient_id');
        $cases_id = $request->input('cases_id','0');
        $type = $request->input('type','0');
        $description = $request->input('description',"");
        $file = $request->file('scan_file');
        try
        {
            $db_divina = DB::connection('cloudsql2');
            $db_divina->beginTransaction();
            if (!is_null($file))
            {

                $path = $this->GeneratePath("$patient_id/$cases_id/",$file);
                $this->inputStorage($path,$file);
                $status = 1;
            }
            else
            {
                $path = "$patient_id/$cases_id/".$request->input('file_name');
                $status = 0;
            }
            $model_id = $this->CreateModel($path,$type,$status);
            $this->CreateCaseFileModels($cases_id,$model_id,$type,$description);
            $db_divina->commit();
            return response()->json([
                'status' => true,'code'=>100,
            ]); 
        }
        catch (Exception $e) 
        {
            $db_divina->rollback();
            return response()->json([
                'status' => false,'code'=>300,]);
        }
    }

    public function CreateUserPassport($setting)
    {
        $user  = user::where('email',$setting->get('email'))->first();
        if(is_null($user))
        {
            $user = new User();
            $Profile = new Profile();
            $Password = new password();
            $user->email =  $setting->get('email');
            $user->platform = $setting->get('platform');
            $user->method = $setting->get('method');
            if ($setting->get('type') == "2") 
            {
            //----Create Doctor--------------------------//
                $doctor = new Doctor();
                $doctor->user_id = $user->id;
                $doctor->save();
            }
            $user->save();
             //----Create Profile--------------------------//
            $Profile->user_id = $user->id;
            $Profile->first_name =  $setting->get('first_name');
            $Profile->last_name =  $setting->get('last_name');
            $Profile->country_id =  $setting->get('country_id');
            $Profile->save();
            //-----Create Password by token or pass--------//
            $Password->user_id = $user->id;
            if($setting->get('method') == "E")
                $Password->hash = password_hash($setting->get('pass'),  PASSWORD_BCRYPT);
            else
                $Password->token =  password_hash($setting->get('token'),  PASSWORD_BCRYPT);
            $Password->save();
        }
        //-----Create AccessControlDivina--------//
        $this->CreateAccesControlPassport($user->id);
        return $user->id;
    }

    public function CreateAccesControlPassport($user_id)
    {
        $Access_control = AccessControl::where('user_id',$user_id)
        ->Where('service_id','2')->get();
        if ($Access_control->isEmpty()) 
        {
            $Access_control = new AccessControl();
            $Access_control->user_id = $user_id;
            $Access_control->service_id = 2;
            $Access_control->save();
        }
    }

    public function CreateDivinaPatient($user_id,$crixalispatient_id)
    {
        $P_file = PatientFile::where('user_id',$user_id)->first();
        if (is_null($P_file))
        {
            $P_file = new PatientFile();
            $P_file->user_id = $user_id;
            $P_file->crixalis_id = $crixalispatient_id;
            $P_file->save();
        }
        return $P_file->id;
    }

    public function CreateDivinaAccess($div_user,$patien_file_id)
    {
        if (is_null(AControl::where('div_user_id',$div_user)->where('patient_file_id',$patien_file_id)->first()))
        {
            $Access_control = new AControl();
            $Access_control->div_user_id = $div_user;
            $Access_control->patient_file_id  = $patien_file_id;
            $Access_control->access_level = 1;
            $Access_control->type = 1;
            $Access_control->save();
            return true;
        }
        else
            return false;
    }

    public function CreateFileDetails($patient_file_id,$setting)
    {
        $File_detail = new FileDetail();
        $File_detail->patient_file_id = $patient_file_id;
        $File_detail->case_file_id = $setting->get("case_id")->id;
        $File_detail->height = $setting->get("height");
        $File_detail->weight = $setting->get("weight");
        $File_detail->pinch = $setting->get("pinch");
        $File_detail->skin_elasticity = $setting->get("skin_elasticity");
        $File_detail->cup_size = $setting->get("cup_size");
        $File_detail->save();
        return $File_detail->id;
    }
    
    public function UpdateCreateFileDetails($patient_file_id,$case_id)
    {
        $File_detail = FileDetail::where('patient_file_id',$patient_file_id)->orderBy("created_at",'asc')->first();
        $File_detail->case_file_id = $case_id;
        $File_detail->save();
    }

    public function CreateModel($path,$type,$status)
    {
        $Models = new DModel();
        $Models->url = $path;
        $Models->type = $type;
        $Models->is_online = $status;
        $Models->save();
        return $Models->id;
    }

    public function CreateCaseFile($patien_file_id,$case_id)
    {
        if ($case_id == 0) 
        {
            $Case = new CaseFile();
            $Case->patient_file_id = $patien_file_id;
            $Case->save();
            //$this->UpdateCreateFileDetails($PatientFile->id,$Case->id);
        }
        else
        {
            $Case = CaseFile::find($case_id);
        }
        return $Case;
    }

    public function CreateCaseFileModels($case_file_id,$model_id,$type,$description)
    {
        $CFM = new CaseFileModel();
        $CFM->case_file_id = $case_file_id;
        $CFM->model_id = $model_id;
        if ($type != 0) 
        {
          $base = CaseFileModel::where("case_file_id",$case_file_id)
            ->where("base_model",0)->first();
            $CFM->base_model = $base->model_id;
            $CFM->implant_description = $description;
        }
        $CFM->save();
    }

    public function inputStorage($path,$file)
    {
        //$key = md5(uniqid(rand(), true));
        //$path = $path.$key;
        //$encryptedFile = Crypt::encrypt(file_get_contents($file));
        Storage::put($path, file_get_contents($file));
    }

    public function UpdateModel(Request $request)
    {
        $file = $request->file('scan_file');
        $Model = DModel::find($request->input('model_id'));
        if(!is_null($Model))
        {
            $this->inputStorage($Model->url,$file);
            $Model->is_online = 1;
            $Model->save();
            return response()->json([
            'status' => true,'code'=>100,]);
        }
        else
        {
             return response()->json([
            'status' => false,'code'=>300,]);
        }
    }
    public function GeneratePath($base,$file)
    {
        $filename = $file->getClientOriginalName();
        $name = pathinfo($filename, PATHINFO_FILENAME);
        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        $base = $base. $name .".".$extension;
        return $base;
    }

    private function GenerateInsertLog($setting)
    {
        $setting->prepend(date("Y-m-d | h:i:sa"),'time');
        $path = "log/InsertLog.txt";
        $exists = Storage::disk()->exists('path');
        if ($exists == true)
        {
            Storage::put($path,$setting->toJson());
        }
        else 
        {
            Storage::append($path, $setting->toJson());

        }
    }
}
