<?php
namespace DivinaApp\Http\Controllers\API;

use DB;
use Storage;
use UrlSigner;
use Carbon\Carbon;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\URL;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\Request;

use DivinaApp\Http\Requests;
use DivinaApp\Http\Response;
use DivinaApp\Http\Controllers\Controller;

use DivinaApp\Http\Controllers\API\RegisterController;
use DivinaApp\Models\Passport\User;
use DivinaApp\Models\Passport\Profile;

use DivinaApp\Models\Divina\DivinaUser;
use DivinaApp\Models\Divina\PatientFile;
use DivinaApp\Models\Divina\FileDetail;
use DivinaApp\Models\Divina\AControl;
use DivinaApp\Models\Divina\DModel;
use DivinaApp\Models\Divina\CaseFile;
use DivinaApp\Models\Divina\CaseFilesModel;



class PatientController extends Controller
{
    public function PatientList($div_user)
    {
        $result = collect();
        $CaseFiles = collect();
        $patientlist = collect();
        foreach (AControl::where('div_user_id',$div_user)->get() as $access_control)
        {
            $profile = $access_control->patient_file->user->profile;
            unset($profile["id"],$profile["sex"],$profile["created_at"],$profile["updated_at"]);
            $profile->crixaliz_id = $access_control->patient_file->crixalis_id;
            $patientlist->prepend($profile);
        }
        $result->prepend($patientlist,"patientlist");
        $result->prepend(100,'code');
        $result->prepend(true,'status');
        return $result;
    }

    public function PatientProfile($patient_id)
    {
        $data = collect();
        $patient_file = PatientFile::Where('user_id',$patient_id)->first();
        if (!is_null($patient_file)) 
        {
            return $this->profile($patient_file->user);
            $data->prepend($patient_file->User->Profile->Country->name,'country');
            $data->prepend($patient_file->User->Profile->birthdate,'birthdate');
            $data->prepend($patient_file->User->email,'email');
            $data->prepend($patient_file->User->Profile->last_name,'last_name');
            $data->prepend($patient_file->User->Profile->first_name,'first_name');
            $data->prepend(100,'code');
            $data->prepend(true,'status');
        }
        else
        {
             $data->prepend(300,'code');
             $data->prepend(false,'status');
        }
        return $data;
    }

    public function PatientCases($div_user,$patient_id)
    {
        $data = collect();
        $casefiles = collect();
        $case_model = collect();
        $patient_file = PatientFile::Where('user_id',$patient_id)->first();
        AControl::where('div_user_id',$div_user)->where('patient_file_id',$patient_file->id)->first();
        $data->prepend(collect(),'cases');
        /*$data->prepend($patient_file->User->Profile->Country->name,'country');
        $data->prepend($patient_file->User->Profile->birthdate,'birthdate');
        $data->prepend($patient_file->User->email,'email');
        $data->prepend($patient_file->User->Profile->last_name,'last_name');
        $data->prepend($patient_file->User->Profile->first_name,'first_name');*/
        $data->prepend(100,'code');
        $data->prepend(true,'status');
        foreach ($patient_file->cases_files as $case) 
        {
        if ($case->soft_delete == 1)
            continue;
            else
            {
                if (is_null($case->file_detail))
                {
                    $case->height = NULL;
                    $case->weight = NULL;
                    $case->pinch = NULL;
                    $case->skin_elasticity = NULL;
                    $case->cup_size = NULL; 
                }
                else
                {
                    $case->height = $case->file_detail->height;
                    $case->weight = $case->file_detail->weight;
                    $case->pinch = $case->file_detail->pinch;
                    $case->skin_elasticity = $case->file_detail->skin_elasticity;
                    $case->cup_size = $case->file_detail->cup_size;
                }
                unset($case["created_at"],
                    $case["updated_at"],
                    $case["patient_file_id"],
                    $case["soft_delete"],$case["file_detail"]
                );
                $casefiles->push($case);
                foreach ($case->case_file_models as $file_model ) 
                {
                         if ($file_model->soft_delete == "1")
                            continue;
                        else
                        {
                            $file_model->url = $file_model->DModel->url;
                            $file_model->typemodel = $file_model->DModel->type;
                            $file_model->is_online = $file_model->DModel->is_online;
                            unset($file_model['updated_at'],
                                $file_model['created_at'],
                                $file_model["soft_delete"],
                                $file_model["DModel"]);
                             $case_model->push($file_model);
                        }
                }
                $case->case_files_models = $case_model;
                $case_model = collect();
                unset($case["case_file_models"]);
            }
        }
        $data->put("cases", $casefiles);
        return $data; 
        unset($patient_file["created_at"],$patient_file["updated_at"]);
    }

    public function PatientUpdate(Request $request)
    {
        try
        {
            DB::beginTransaction();
            $IUser_id = $request->input('user_id');
            $IFirst =  $request->input('first_name');
            $ILast =  $request->input('last_name');
            $Ibirthdate =  date("Y-m-d",strtotime($request->input('birthdate')));
            $user = User::findOrFail($IUser_id);
            if ($user->isEmpty == false) 
            {
                $user->Profile->first_name= $IFirst;
                $user->Profile->last_name = $ILast;
                $user->Profile->birthdate = $Ibirthdate;
                $user->Profile->save();
                DB::commit();
                $status = array('status'=> true, 'code'=> 100);
                return $status;
            }
            else
            {
                $status = array('status'=> false, 'code'=> 102);
                return $status;
            }
        } 
        catch (Exception $e) 
        {
            DB::rollback();
            $status = array('status'=> false, 'code'=> 301);
            return $status;
        }
    }

    public function PatientDelete(Request $request)
    {
        $IUser_id = $request->input('user_id');
        $Idiv_user = $request->input('div_user');
        $PatientFile = PatientFile::where("user_id",$IUser_id)->first();
        $Access = AControl::where('div_user_id',$Idiv_user)->where('patient_file_id',$PatientFile->id)->first();
        if (!is_null($Access))
            $Access->delete();
        return response()->json([
            'status' => true,
            'code' =>100,]);
    }

    public function FileDetailUpdate(Request $request)
    {
        $setting = collect();
        $setting->prepend($request->input('case_id'),'case_id');
        $setting->prepend($request->input('height ',0),'height');
        $setting->prepend($request->input('weight',0),'weight');
        $setting->prepend($request->input('pinch',0),'pinch');
        $setting->prepend($request->input('skin_elasticity',0),'skin_elasticity');
        $setting->prepend($request->input('cup_size',0),'cup_size');
        $file_detail = FileDetail::where('case_file_id',$setting->get('case_id'))->first();
        if (!is_null($file_detail))
        {
            $file_detail->height = $setting->get("height");
            $file_detail->weight  = $setting->get("weight");
            $file_detail->pinch = $setting->get("pinch");
            $file_detail->skin_elasticity = $setting->get("skin_elasticity");
            $file_detail->cup_size = $setting->get("cup_size");
            $file_detail->save();
        }
        else
        {
            return response()->json([
            'status' => false,'code'=>300,]);
        }
        return response()->json([
            'status' => true,'code'=>100,]);
    }

    public function PatientCaseDelete(Request $request)
    {
        $CaseFile =  CaseFile::find($request->input("case_id"));
        if(!is_null($CaseFile))
        {
            $CaseFile->soft_delete = 1;
            $CaseFile->save();
              return response()->json([
            'status' => true,'code'=>100,]);
        }
        else
        {
            return response()->json([
            'status' => false,'code'=>102,]);
        }
    }

    public function outStorage($model_id)
    {
        $Model = DModel::findOrFail($model_id);
        if ($Model->isEmpty == false)
        {
            $path = $Model->url;
            $storagePath  = Storage::disk()->getDriver()->getAdapter()->getPathPrefix();
            /*$path = $storagePath."146826/34/592995f703c5010927f6baa88bc1bd85";
            return response()->download($path);*/
            $semento = explode('/', $path);
            $tem_path = $semento[0]."/".$semento[1]."/".$semento[2];
            //Storage::put($tem_path, $decryptedContents);
            return response()->download($storagePath.$tem_path);
        }
        else
        {
            return response()->json([
            'status' => false,'code'=>102,]);
        }
    }

    public function Profile($user)
    {
        $Result = collect();
        $profile = $user->profile;
        $profile->email = $user->email;
        unset($profile['birthdate'],$profile['photo'],$profile['sex'],$profile['updated_at'],$profile['created_at'],$profile['id']);
        $Result->prepend($profile,'Profile');
        $Result->prepend(100,'code');  
        $Result->prepend(true,'status');
        return $Result;
    }

    public function FileDetailCreate(Request $request)
    {
        try
        {
            $db_divina = DB::connection('cloudsql2');
            $db_divina->beginTransaction();
            $rc = new RegisterController();
            $setting = collect(); 
            $div_user = $request->input('div_user');
            $patient_id = $request->input('patient_id');
            $patient_file = PatientFile::where('user_id',$patient_id)->first();
            $acontrol = AControl::Where("div_user_id",$div_user)->Where('patient_file_id',$patient_file->id)->first();
            if (!is_null($acontrol)) 
            {
                $setting->prepend($request->input('height',0),'height');
                $setting->prepend($request->input('weight',0),'weight');
                $setting->prepend($request->input('pinch',0),'pinch');
                $setting->prepend($request->input('skin_elasticity',0),'skin_elasticity');
                $setting->prepend($request->input('cup_size',0),'cup_size');
                $setting->prepend($rc->CreateCaseFile($patient_file->id,0),'case_id'); 
                $rc->CreateFileDetails($patient_file->id,$setting);
                $db_divina->commit();
                return response()->json([
                    'status' => true,'code'=>100,
                    'data' => ['case_id'=>$setting->get("case_id")->id],
                ]);
            }
            else
                  return response()->json([
            'status' => false,'code'=>102,]);
        }
        catch (Exception $e)
        {
            $db_divina->rollback();
            return response()->json([
                'status' => false,'code'=>300,]);
        }
    }

    public function GenerateLink(Request $request)
    {
        $model_id = $request->input('model_id');
        $base= url("protected-route/".$model_id);
        $url = UrlSigner::sign($base, Carbon::now()->addMinutes(5) );
        return response()->json([
                    'status' => true,'code'=>100,
                    'url' => $url,]);
    }
}