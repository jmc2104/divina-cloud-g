<?php

namespace DivinaApp\Http\Controllers\API;

use Auth;
use Carbon\Carbon;
use DB;
use JWTAuth;
use Validator;

use Illuminate\Http\Request;

use DivinaApp\Http\Requests;
use DivinaApp\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function login(Request $request)
    {

        $credentials = $request->only('email', 'password');
        $rules = [
            'email' => 'required|email',
            'password' => 'required',
        ];

        $validator = Validator::make($credentials, $rules);
        if($validator->fails()) {
            return response()->json([
                'status' => false, 
                'message' => $validator->messages()
            ]);
        }
        try {
            // Attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'status' => false, 
                    'message' => 'We can`t find an account with this credentials.'], 401);

            }
        } catch (JWTException $e) {
            // Something went wrong with JWT Auth.
            return response()->json([
                'status' => false, 
                'message' => 'Failed to login, please try again.'
            ], 500);
        }
        // All good so return the token
        $result = $this->Profile(Auth::User());
        return response()->json([
            'status' => true, 
            'data' => $result,  
            'token' => $token,
             
        ]);
    }

    /**
     * Logout
     * Invalidate the token. User have to relogin to get a new token.
     * @param Request $request 'header'
     */
    public function logout() 
    {
        // Get JWT Token from the request header key "Authorization"
        $token = JWTAuth::getToken();
        // Invalidate the token
        try {
            JWTAuth::invalidate($token);
            return response()->json([
                'status' => true, 
                'code'=> 100,
            ]);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
             return response()->json([
                'status' => false, 
                'code'=> 200,
            ]);
        }
    }
    
    public function refresh()
    {
        $token = JWTAuth::getToken();
        if(!$token)
            return response()->json([
                'status' => false, 
                'code'=> 200,
            ]);
        try
        {
            $token = JWTAuth::refresh($token);
        }
        catch(TokenInvalidException $e)
        {
             return response()->json([
                'status' => false, 
                'code'=> 200,
            ]);
        }
        return response()->json([
                'status' => true, 
                'code'=> 100,
                'token'=> $token,
            ]);
    }

    function Profile($user)
    {
        $Result = collect();
        $user->last_login = date("Y-m-d H:i:s");
        $user->save();
        $profile = $user->profile;
        $profile->type = $user->type_id;
        if ($user->type_id == 1)
            $profile->patient_id = $user->Patient->id;
        elseif($user->type_id == 2)
            $profile->doctor_id = $user->Doctor->id;
        $profile->divina_user_id = $user->divina_user->id;
        $profile->clientId = $user->divina_user->crisalix_access->clientId;
        $profile->clientSecret = $user->divina_user->crisalix_access->clientSecret;
        unset($profile['user_id'],$profile['email'],$profile['birthdate'],$profile['photo'],$profile['sex'],$profile['updated_at'],$profile['created_at'],$profile['id']);
        $Result->prepend($profile,'Profile');
        return $Result;
    }

}
