<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Helpers;
use App\Models\User;
use App\Models\Profile;
use App\Models\Patient;
use App\Models\Surgery;
use App\Models\Doctor;
use App\Models\Registration;
use App\Models\Warranty;

use App\Http\Controllers\Controller;

class AdminController extends Controller
{
  
  
  function home()
  {
    return view('admin.home');
  }
  
  
  function patients()
  {
    $patients = Patient::take(200)->orderBy('id', 'desc')->get();
    
    return view('admin.patients', [ 'dataPatients' => $patients ]);
  }
  
  function patient_detail( $pid )
  {
    $patient = Patient::findOrFail( $pid );
    return view('admin.patient_detail', [ 'patient' => $patient ]);
    
  }
  
}
