<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Helpers;
use App\Models\User;
use App\Models\Profile;
use App\Models\Patient;
use App\Models\Surgery;
use App\Models\Doctor;
use App\Models\Registration;
use App\Models\Warranty;

use App\Http\Controllers\Controller;

class SearchController extends Controller
{
  /**
  * Search an user by email
  *
  * Displays a view with patient profile, registered implants, and warranties if apply.
  *
  * @param string email email address of the user to search
  */
  public function email( $email = null )
  {
    /* Check if the parameters are set */
    if ( ! $email  ) return ( new Response('Bad Request', 400) );
    
    /* Clean the string  */
    $email = Helpers::clean( $email );
    
    /* Find the user or die */
    $user = User::where('email', $email )->first();
    
    /* Die if the user is not found */
    if ( ! $user ) return ( new Response('User not found', 404) );
    
    /* Collection of Data to display */
    $dataview = array();
    
    /* Load the user profile */
    $dataview['user'] = $user;
    $dataview['profile'] = $user->profile;
    
    /* If the user is a patient, get their implants */
    if ( $user->type == 1 )
    $dataview['surgeries'] = $user->patient->surgeries;
    
    return view('ws.user')->with( $dataview );
    
  }
  
  
  /**
  * undocumented function summary
  *
  * Undocumented function long description
  *
  * @param type var Description
  * @return return type
  */
  public function serial()
  {
    $s = Surgery::first();
    
    
    //echo Patient::find( 20916 );
    
    echo $s->doctor->profile->get_full_name();
    
  }
  
  
  
  /**
  * Search an user by id
  *
  * Returns everything related to the found user
  *
  * @param int pid patient_id
  * @return array
  */
  private function get_surgeries( $pid )
  {
    
  }
  
}
