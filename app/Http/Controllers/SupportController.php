<?php

namespace DivinaApp\Http\Controllers;
use Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

use DivinaApp\Http\Requests;
use DivinaApp\Http\Controllers\Controller;
use DivinaApp\Models\User;
use DivinaApp\Models\Profile;
use DivinaApp\Models\Patient;
use DivinaApp\Models\Password;
use DivinaApp\Models\Surgery;
use DivinaApp\Models\Registration;
use DivinaApp\Models\Implant;
use DivinaApp\Models\Warranty;
use DivinaApp\Models\Order;
use DivinaApp\Models\Country;
use DivinaApp\Models\FinanceReport;
use Carbon\Carbon;




class SupportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

        public function rules()
    {
        return [
          'password' => 'required|min:4',
          'email' => 'required|email',
        ];
    }

      public function messages()
    {
        return [
            'password.required' => 'Please provide a Password',
            'email.email' => 'email formatted not validation'
        ];
    }

    public function index()
    {
        //
        return view('Support.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //return view('Support.login');
        return view('Support.ImplantSearch');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $uEmail = $request->input('email');
        $uPass = $request->input('password');
        $validator = Validator::make($request->all(),$this->rules(),$this->messages());
         if ($validator->fails()) {
            return view('Support.login')
                        ->withErrors($validator);
        }   
        $user = User::where("email", $uEmail)->first();
        if ($user != null) 
        {
            if ($user->type == 3) 
            {
                $dbPass = $user->password->hash;
                $isValid = password_verify($uPass,$dbPass);
                if ($isValid)
                {
                    return view('Support.main');
                }
                else
                    return view('Support.login')->withErrors('Username and Password do not match');
            }
            else
            {  
                return view('Support.login')->withErrors('The account does not have permissions to enter');;
            }
        }
        else
        {
             return view('Support.login')->withErrors('The Email or Password does not correspond');;
        }
    }

    public function main(request $request)
    {
        return view('Support.main');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function ImplantSearch(Request $request)
    {
        $data =  $this->infoImplant($request->input('serial'));
        return view('Support.ImplantSearch')->with('data',$data);
    
    }

    public function infoImplant($serial)
    {
        $data = collect();
        $data->prepend(null,'order');
        $data->prepend(null,'warranty');
        $data->prepend(null,'surgery');
        $data->prepend(null,'implant');
        $data->prepend(false,'status');
        $Implant = Implant::where("serial",$serial)->first();
        if (!is_null($Implant))
        {
             $Implant->reference = $Implant->Catalog->reference;
             $Implant->wprogram = $Implant->program->name;
            
             if ($Implant->is_registered) 
             {
                $data->put('surgery',$this->infosurgery($Implant));
                $this->infoWarranty($Implant,$data);
             }
             unset($Implant['id'],
                $Implant['catalog_id'],
                $Implant['program_id'],
                $Implant['distributor_id'],
                $Implant['lot'],
                $Implant['electronic_serial'],
                $Implant['apply_warranty'],
                $Implant['sold_warranty'],
                $Implant['is_q_inside'],
                $Implant['is_tracked'],
                $Implant['is_registered'],
                $Implant['allow_ew'],
                $Implant['created_at'],
                $Implant['updated_at'],
                $Implant['Catalog'],
                $Implant['program'],
                $Implant['registration']);
             $data->put('implant',$Implant);
             $data->put('status',true);
             return $data;
        }
        else
        {
            return $data;
        }
    }

     public function infoWarranty($Implant,$data)
     {
        if (!is_null($warranty = $Implant->registration->warranty))
        {
            $order = $Implant->registration->warranty->Order;
            unset($warranty['id'],
                $warranty['order_id'],
                $warranty['registration_id'],
                $warranty['created_at'],
                $warranty['updated_at'],
                $warranty['Order']);
            $data->put('warranty',$warranty);
                unset($order['id'],
                $order['giftcard_id'],
                $order['country_id'],
                $order['created_at'],
                $order['updated_at']);
            $data->put('order',$order);
            return $data;
        }
        else
        {
            return $data;
        }
        
     }

     public function infosurgery($Implant)
     {
        $surgery = $Implant->registration->surgery;
        $surgery->email = $surgery->Patient->User->email;
         $a = profile::where("user_id",$surgery->Patient->user_id)->first(["first_name","last_name","user_id"]);
        $surgery->name = $a->first_name . " " . $a->last_name;
        $surgery->country_name = $surgery->Country->name;
        unset($surgery['patient_id'],
        $surgery['doctor_id'],
        $surgery['registrant_id'],
        $surgery['placement_id'],
        $surgery['incision_site_id'],
        $surgery['indication_id'],
        $surgery['is_active'],
        $surgery['updated_at'],
        $surgery['Patient'],
        $surgery['Country']);
        return $surgery;
     }

     public function GetAccountUpdate(Request $request )
     {
        $data =  $this->infoImplant($request->input('serial'));
         return view('Support.AccountUpdate')->with('data',$data);
     }


     public function SetAccountUpdate(Request $request )
     {
        DB::beginTransaction();
        $validation = true;
        $surgery = Surgery::find($request->input('id'));
        //echo $surgery;
        $surgery->Patient->User->email;
        if ($surgery->Patient->User->email != $request->input('email'))
        {
            if(is_null(User::where('email', $request->input('email'))->first()))
            {
                $surgery->Patient->User->email = $request->input('email');
                $surgery->Patient->User->save();
            }
            else
            {
                $validation = false;
                return back()->withErrors('email is already registered');
            }
        }
        $a = date("Y-m-d",strtotime($surgery->date));
        $b = date("Y-m-d",strtotime($request->input('surgery_date')));
        if ($a != $b) 
        {
            $surgery->date = $b;
            $surgery->save();
            $this->WarrantyUpdate($surgery);

        }
        if($surgery->doctor_name != $request->input('doctor_name'))
        {
            $surgery->doctor_name = $request->input('doctor_name');
            $surgery->save();
        }
        if($surgery->country_id != $request->input('country_id'))
        {
            $surgery->country_id = $request->input('country_id');
            $surgery->save();
        }
        if ($validation) 
        {
           DB::commit();
           $data = ['serial','10090012-03'];
           $request = Request();
           $request->request->add(['serial','10090012-03']);
            return view('Support.ImplantSearch')->with('data',null);
        }
        else
        {
            echo "roolback";
            DB::roolback();
        }
        
     }

    public function WarrantyUpdate($surgery)
    {
        foreach ($surgery->Registrations as $registration) 
        {
           if(!is_null($Warranty = $registration->Warranty))
           {
            $Warranty->start_date = $surgery->date;
            if ($registration->Warranty->is_extended == 1) 
            {
                $Warranty->end_date = date('Y-m-d', strtotime( "+ $registration->implant->program->period year", strtotime($surgery->date) ) );
            }
            else
                $Warranty->end_date = date('Y-m-d', strtotime( "+ 1 year", strtotime($surgery->date) ) );
            $Warranty->save();
            }
        }
    }

    public function GetFinanceReport(Request $request)
    {
        $data['start_date'] =  date('Y-m-d', strtotime($request->input('start_date')));
        $data['end_date'] = date('Y-m-d', strtotime($request->input('end_date')));
        $data['list'] = FinanceReport::whereBetween('Date_Purchase',[$data['start_date'],$data['end_date']])->get();
        if ( $data['list']->isEmpty())
            $data['list'] = null;
        return view('Support.Reports')->with('data',$data);
    }    

    public function ReportExport(Request $request)
    {
        ini_set('max_execution_time', 180);
        $start = date('Y-m-d', strtotime($request->input('start')));
        $end = date('Y-m-d', strtotime($request->input('end')));
         Excel::create('Laravel Excel', function($excel) use ($start,$end) {
 
            $excel->sheet('warranties', function($sheet) use ($start,$end) {
 
                $Reports = FinanceReport::whereBetween('Date_Purchase',[$start,$end])->get();
 
                $sheet->fromArray($Reports);
 
            });
        })->export('xls');
 
    }
}
