<?php

namespace DivinaApp\Http\Controllers;

use Illuminate\Http\Request;

use DivinaApp\Http\Requests;
use DivinaApp\Http\Controllers\Controller;
use DivinaApp\Models\Country;
use DivinaApp\Models\Version;
use DivinaApp\Models\Transaction_log;

class AppController extends Controller
{
    /**
     * Web Service Documentation
     *
     * @return void
     **/
    function help()
    {
        print '<pre>MotivaImagine Web API Version 1.0.0</pre>';
    } /* End: help */

    function countries()
    {
        $Result = collect();
    	$list = Country::ALL('id','name');
        $Result->prepend($list,'Countries'); 
        $Result->prepend('100','code'); 
        $Result->prepend('true','status');
    	return $Result;
    }

  function version($type)
    {
        $Result = collect();
        try 
        { 
            $Version = Version::Where('device_type',$type)->first(["latest_version","url","maintenance_mode"]);
            if (!is_null($Version)) 
            { 
                $Result->prepend($Version,'Version');
                $Result->prepend('true','status');
                $Result->prepend(100,'code');
              
            }
            else
            {
                $Result->prepend('false','status');
                $Result->prepend(301,'code');
            }
        } 
        catch (Exception $e) 
        {
            $Result->prepend('false','status');
            $Result->prepend(301,'code');
        } 
        return $Result;
    }

    function transaction_log($error,$platform,$user_id,$process)
    {
        $transaction_log = new Transaction_log;
        $transaction_log->user_id = $user_id;
        $transaction_log->platform = $platform;
        $transaction_log->process = $process;
        if ($process == "R")
             $transaction_log->code = $error['code'];
         else
         {
            $transaction_log->code = $error['status'];
            $transaction_log->comment =$error['responsetext'];
         } 

        $transaction_log->save();
    }

}
