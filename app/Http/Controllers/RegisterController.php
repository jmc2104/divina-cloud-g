<?php

namespace DivinaApp\Http\Controllers;

use Storage;
use Illuminate\Http\Request;

use DivinaApp\Http\Requests;
use DivinaApp\Http\Controllers\Controller;

class RegisterController extends Controller
{
      public function RegisterPatients(Request $request)
    {
        $setting = collect();
        $setting->prepend($request->input('div_user'),"div_id");
        $setting->prepend($request->input('crixalis_id'),"crixalispatient_id");
        $setting->prepend($request->input('email'),'email');
        $setting->prepend($request->input('first_name'),'first_name');
        $setting->prepend($request->input('last_name'),'last_name');
        $setting->prepend($request->input('method','E'),'method');
        $setting->prepend($request->input('platform','D'),'platform');
        $setting->prepend($request->input('country_id'),'country_id');
        $setting->prepend($request->input('type','0'),'type');
        $setting->prepend($request->input('email'),'email');
        $setting->prepend($request->input('pass','Motiva2019'),'pass');
        $setting->prepend($request->input('heigth'));
        $setting->prepend($request->input('weigth'));
        $setting->prepend($request->input('skin_elasticity'));
        $setting->prepend($request->input('cup_size'));
        try 
        {
            DB::rollback();
            $patient_user_id = $this->CreateUserPassport($setting);
            $patient_file_id = $this->CreateDivinaPatient($patient_user_id,$setting->get('crixalispatient_id'));
            $this->CreateDivinaAccess($setting->get('div_id'),$patient_file_id);
            DB::commit();
            //$this->CreateCaseFile($patient_user_id,0);
        }
        catch (Exception $e) 
        {
             DB::rollback();
        }
        return response()->json([
            'status' => 'success', 
            'data' => ['patient_id'=> $patient_user_id], 
        ]); 
    }

    public function RegisterCases(Request $request)
    {
        $cases_id = $request->input('cases_id','0');
        $type = $request->input('type','0');
        $patient_id = $request->input('patient_id');
        $file = $request->file('scan');
        try
        {
            DB::beginTransaction();
            $case = $this->CreateCaseFile($patient_id,$cases_id);
            $path = $this->inputStorage("$patient_id/$case->id",$file);
            $model_id = $this->CreateModel($path,$type);
            $this->CreateCaseFileModels($case->id,$model_id,$type);
            DB::commit();
              return response()->json([
            'status' => true,"code"=>100, 
        ]); 
        }
        catch (Exception $e) 
        {
            DB::rollback();
            return response()->json([
            'status' => false,"code"=>300, 
        ]); 
        }

    }

    public function CreateUserPassport($setting)
    {
        $user  = user::where('email',$setting->get('email'))->get();
        echo var_dump($user->isEmpty());
        die;
        if($user->isEmpty())
        {
            $user = new User();
            $Profile = new Profile();
            $Password = new password();
            $user->email =  $setting->get('email');
            $user->platform = $setting->get('platform');
            $user->method = $setting->get('method');
            if ($setting->get('type') == "2") 
            {
            //----Create Doctor--------------------------//
                $doctor = new Doctor();
                $doctor->user_id = $user->id;
                $doctor->save();
            }
            $user->save();
             //----Create Profile--------------------------//
            $Profile->user_id = $user->id;
            $Profile->First_Name =  $setting->get('first_name');
            $Profile->Last_Name =  $setting->get('last_name');
            $Profile->country_id =  $setting->get('country_id');
            $Profile->save();
            //-----Create Password by token or pass--------//
            $Password->user_id = $user->id;
            if($setting->get('method') == "E")
                $Password->hash = password_hash($setting->get('pass'),  PASSWORD_BCRYPT);
            else
                $Password->token =  password_hash($setting->get('token'),  PASSWORD_BCRYPT);
            $Password->save();
        }
        //-----Create AccessControlDivina--------//
        $this->CreateAccesControlPassport($user->id);
        return $user->id;
    }

    public function CreateAccesControlPassport($user_id)
    {
        $Access_control = AccessControl::where('user_id',$user_id)
        ->Where('service_id','2')->get();
        if ($Access_control->isEmpty()) 
        {
            $Access_control = new AccessControl();
            $Access_control->user_id = $user_id;
            $Access_control->service_id = 2;
            $Access_control->save();
        }
    }

    public function CreateDivinaPatient($user_id,$crixalispatient_id)
    {
        $P_file = PatientFile::where('user_id',$user_id)->get();
        if ($P_file->isEmpty())
        {
            $P_file = new PatientFile();
            $P_file->patient_id = $user_id;
            $P_file->crixalis_id = $crixalispatient_id;
            $P_file->save();
        }
        return $P_file->id;
    }

    public function CreateDivinaAccess($divi_user_id,$patien_file_id)
    {
        $Access_control = new DAccessControl;
        $Access_control->div_user_id = $divi_user_id;
        $Access_control->patient_file_id  = $patien_file_id;
        $Access_control->access_level = 1;
        $Access_control->type = 1;
        $Access_control->save();
    }

    public function CreateModel($path,$type)
    {
        $Models = new Models();
        $Models->url = $path;
        $Models->type = $type;
        $Models->save();
        return $Models->id;
    }

    public function CreateCaseFile($patient_id,$case_id)
    {
        if ($case_id == 0) 
        {
            $Case = new CaseFiles();
            $a =  PatientFile::where('patient_id',$patient_id)->first();
            $Case->patient_file_id =  $a->id;
            $Case->save();
        }
        else
        {
            $Case = CaseFiles::find($case_id);
        }
        return $Case;
    }

    public function CreateCaseFileModels($case_file_id,$model_id,$type,$description)
    {
        $CFM = new CaseFilesModels();
        $CFM->case_file_id = $case_file_id;
        $CFM->model_id = $model_id;
        if ($type != 0) 
        {
          $base = CaseFilesModels::where("case_file_id",$case_file_id)
            ->where("base_model",0)->first();
            $CFM->base_model = $base->model_id;
            $CFM->implant_description = $description;
        }
        $CFM->save();
    }


    public function inputStorage(Request $request)
    {
        //$encryptedFile = Crypt::encrypt(file_get_contents($file));
        $file = $request->file('file');
        $path = Storage::put("01/03", $file);
        return $path;
    }

    public function outStorage(Request $request )
    {
        $encryptedContents = Storage::get('archivoencriptado');
        $decryptedContents = Crypt::decrypt($encryptedContents);
        $a = Storage::put("archivoencriptado.pdf", $decryptedContents);
        return response()->download($a)->deleteFileAfterSend(true);
        return Storage::download('archivoencriptado.pdf')->deleteFileAfterSend(true);
    }
}
