<?php

namespace DivinaApp\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Closure;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
     protected $routes = [
            'login',
            'testfile',
            'api/patient/create',
            'api/patient/case/attach',
            'api/patient/file/create',
            'api/patient/update',
            'api/patient/delete',
            'api/patient/model',
            'api/patient/model/sync',
            'api/case/update',
            'api/case/delete',
    ];

     /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     *
     * @throws \Illuminate\Session\TokenMismatchException
     */
     
    public function handle($request, Closure $next)
    {
        //add this condition 
        foreach($this->routes as $route)
        {
            if ($request->is($route)) 
            {
                return $next($request);
            }
        }
    return parent::handle($request, $next);
  }





     /* public function handle($request, Closure $next)
    {
        echo $request->path();
        if(in_array($request->path(), $this->routes)){
            return $next($request);
        }else{
            return parent::handle($request, $next);
        }
    }*/

}

