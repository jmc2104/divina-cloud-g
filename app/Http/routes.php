<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*Route::get('/', 'AppController@help');
Route::post('test', 'TestController@login');
Route::post('authenticate', 'AuthController@authenticate');
Route::get('encripto', 'TestController@encripto');
Route::get('descripto', 'TestController@descripto');*/

Route::post('login', 'API\AuthController@login');
Route::get('testfile', 'API\PatientController@test');
Route::get('auth/refresh', 'API\AuthController@refresh');

Route::group(['prefix' => 'api','middleware' => ['jwt.auth']],function()
{    

    Route::get('logout', 'API\AuthController@logout');
    Route::post('patient/create', 'API\RegisterController@RegisterPatients');
    Route::post('patient/file/create','API\PatientController@FileDetailCreate');
    Route::post('patient/case/attach', 'API\RegisterController@AttachModelCase');
    Route::get('doctor/{div_user}/patients', 'API\PatientController@PatientList');
    Route::get('patient/{div_user}/{patient_id}/cases', 'API\PatientController@PatientCases');
    Route::get('patient/profile/{patient_id}','API\PatientController@PatientProfile');
    //Patient maintenance
    route::post('patient/update','API\PatientController@PatientUpdate');
    route::post('patient/delete','API\PatientController@PatientDelete');
    //Case maintenance
    route::post('case/update','API\PatientController@FileDetailUpdate');
    route::post('case/delete','API\PatientController@PatientCaseDelete');
    //Syncro Models
    Route::post('patient/model/sync','API\RegisterController@UpdateModel');
    //Retur Scan
    route::get('patient/model', 'API\PatientController@GenerateLink');
});

Route::get('protected-route/{model_id}', ['middleware' => 'signedurl','uses' => 'API\PatientController@outStorage']);